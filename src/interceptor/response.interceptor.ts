import {
  CallHandler,
  ExecutionContext,
  HttpStatus,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { request, Response } from 'express';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DefaultResponseMessages } from 'src/constant/response';

@Injectable()
export class ResponseInterceptor<T> implements NestInterceptor<T, any> {
  intercept(
    context: ExecutionContext,
    next: CallHandler<T>,
  ): Observable<any> | Promise<Observable<any>> {
    let ctx = context.switchToHttp();
    let response: any = ctx.getResponse<Response>();
    let status = response.statusCode;
    if (status >= HttpStatus.OK && status < HttpStatus.BAD_REQUEST) {
      let message = DefaultResponseMessages[HttpStatus.OK];
      return next.handle().pipe(
        map((data) => {
          response.__ss_body = {
            Status: HttpStatus.OK,
            Message: message,
            Data: data
          };
          return {
            Status: HttpStatus.OK,
            Message: message,
            Data: data,
          };
        }),
      );
    } else {
      return next.handle();
    }
  }
}