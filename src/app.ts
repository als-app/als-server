import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import { BadRequestException, ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface";
import { HttpExceptionFilter } from "src/exception/http.exception";
import { appEnv } from 'src/helpers/env.helper';
import { ResponseInterceptor } from './interceptor/response.interceptor';
import { Logger } from './helpers/logger.helper';
import { AppModule } from './app.module';

export class App {
    private static _app: NestExpressApplication = null;

    private constructor() { };

    public static async GetNestApplicationInstance(): Promise<NestExpressApplication> {
        if (App._app === null) {
            App._app = await App._createApplication();
        }
        return App._app;
    }

    private static async _createApplication(): Promise<NestExpressApplication> {
        const app = await NestFactory.create<NestExpressApplication>(AppModule, {
            logger: appEnv("DEBUG", false) ? ["log", "error", "warn", "debug"] : false
        });
        app.useGlobalPipes(
            new ValidationPipe({
                validationError: {
                    target: true,
                    value: true,
                },
                exceptionFactory: (errors) => new BadRequestException(errors),
            }),
        );
        app.useGlobalFilters(new HttpExceptionFilter());
        app.enableCors({
            credentials: true,
            origin: function (origin, callback) {
                callback(null, true)
            }
        });
        app.use(helmet());
        if (appEnv("APPLY_RATE_LIMIT", true)) {
            app.use(
                rateLimit({
                    windowMs: 15 * 60 * 1000, // 15 minutes
                    max: 100, // limit each IP to 100 requests per windowMs
                }),
            );
        }
        app.set('trust proxy', 1);
        app.useGlobalInterceptors(new ResponseInterceptor());
        app.use(Logger.GetLoggerMiddleware());

        return app;
    }
}