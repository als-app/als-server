import { appEnv } from '../helpers/env.helper';
import * as dotenv from "dotenv";

dotenv.config();

export const AWSConfig = {
  accessKeyId: appEnv('AWS_ACCESS_KEY', ''),
  secretAccessKey: appEnv('AWS_SECRET_ACCESS_KEY', ''),
  bucketURL: appEnv('AWS_BUCKET_NAME', ''),
  signatureVersion: 'v4',
  region: appEnv("AWS_REGION", '')
};
