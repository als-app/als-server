import { appEnv } from "../helpers/env.helper";
import { config } from "dotenv"
import { Logger } from "src/helpers/logger.helper";
config();

export const Config = {
  type: "mysql",
  host: appEnv("DB_MYSQL_HOST", "localhost"),
  port: appEnv("DB_MYSQL_PORT", 3306),
  username: appEnv("DB_MYSQL_USER", "root"),
  password: appEnv("DB_MYSQL_PASS", ""),
  database: appEnv("DB_MYSQL_DB_NAME", ""),
  entities: ['dist/**/*.entity.{js,ts}'],
  synchronize: false,
  migrations: ['dist/migrations/*.js'],
  logging: "all",
  logger: Logger.GetQueryLogger(),
  bigNumberStrings: false,
  legacySpatialSupport: false,
  extra: {
    connectTimeout: 600000
  }
};
