import { Injectable } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import {
  BadRequestException,
  NotFoundException,
} from "src/exception/response.exception";
import { IsFuture } from "src/helpers/date.helper";
import {
  GetPaginationOptions,
  OrderByRequestParams,
  PaginationRequestParams,
} from "src/helpers/util.helper";
import { MessageTemplates } from "src/locale/locale";
import { UserModel, UserModelType } from "../auth/user.entity";
import { UserRepository } from "../auth/user.repository";
import { MediaRepository } from "../media/media.repository";
import { CategoryModel } from "./category.entity";
import { CategoryRepository } from "./category.repository";
import { CreateCategoryRequest, FindCategoryRequest, UpdateCategoryRequest } from "./category.request";

@Injectable()
export class CategoryService {
  constructor(private _categoryRepository: CategoryRepository, private _mediaRepository: MediaRepository) {

  }

  async FindCategories(data: FindCategoryRequest) {
    let whereParams: any = {}
    let Categories = await this._categoryRepository.Find(whereParams, GetPaginationOptions(data));
    return { Categories }
  }


  async Create(data: CreateCategoryRequest, user: UserModel) {




    let Category = new CategoryModel();
    Category.Title = data.Title;
    if (data.MediaId) {
      let Media = await this._mediaRepository.FindById(data.MediaId);
      if (!Media) {
        throw new NotFoundException(MessageTemplates.MediaNotFound)
      }
      Category.MediaId = data.MediaId
    }
    Category = await this._categoryRepository.Create(Category);
    return { Message: "Success", Category }
  }

  async Update(id: number, data: UpdateCategoryRequest, user: UserModel) {
    let Category = await this._categoryRepository.FindById(id);
    if (!Category) {
      throw new NotFoundException(MessageTemplates.NotFoundError)
    }

    if (data.MediaId && Category.MediaId !== data.MediaId) {
      let Media = await this._mediaRepository.FindById(data.MediaId);
      if (!Media) {
        throw new NotFoundException(MessageTemplates.MediaNotFound)
      }
    }

    await Category.Fill(data);
    Category = await this._categoryRepository.Save(Category);
    return { Message: "updated", Category }
  }

  async Delete(id: number, user: UserModel) {
    let Category = await this._categoryRepository.FindById(id);
    if (!Category) {
      throw new NotFoundException(MessageTemplates.NotFoundError)
    }
    await this._categoryRepository.DeleteById(Category.Id);
    return { Message: "deleted" }
  }

  async Get(id: number, user: UserModel) {

    let Category = await this._categoryRepository.FindOne({ Id: id });
  
    return { Category }
}

}
