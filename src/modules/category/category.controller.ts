import { Body, Param, Query } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import { AppController } from "src/decorator/appcontroller.decorator";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { Delete } from "src/decorator/nest-ext/delete.decorator";
import { Get } from "src/decorator/nest-ext/get.decorator";
import { Patch } from "src/decorator/nest-ext/patch.decorator";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { Put } from "src/decorator/nest-ext/put.decorator";
import { UserModel, UserModelType } from "../auth/user.entity";
import { CreateCategoryRequest, FindCategoryRequest, UpdateCategoryRequest } from "./category.request";
import { CategoryResponseModel } from "./category.response";
import { CategoryService } from "./category.service";

@AppController("category")
export class CategoryController {
  constructor(private _categoryService: CategoryService) { }

  // @Authorized()
  @Get("/", CategoryResponseModel)
  async GetCategories(@Query() data: FindCategoryRequest) {
    return await this._categoryService.FindCategories(data)
  }

  @Authorized()
  @Post("/", CreateCategoryRequest)
  async Create(@Body() data: CreateCategoryRequest, @CurrentUser() user: UserModel): Promise<any> {
    return await this._categoryService.Create(data, user);
  }

  @Authorized()
  @Patch("/:id", UpdateCategoryRequest)
  async Update(@Param("id") id: number, @Body() data: UpdateCategoryRequest, @CurrentUser() user: UserModel): Promise<any> {
    return await this._categoryService.Update(id, data, user);
  }

  @Authorized()
  @Delete("/:id", MessageResponse)
  async Delete(@Param("id") id: number, @CurrentUser() user: UserModel): Promise<MessageResponse> {
    return await this._categoryService.Delete(id, user);
  }

  @Authorized()
  @Get("/:id",{})
  async Get(
    @Param("id") id: number,
    @CurrentUser() user: UserModel
  ) {
    return await this._categoryService.Get(id, user);
  }
}
