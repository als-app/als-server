import { BaseModel } from "src/model/base.entity";
import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { Exclude } from "class-transformer";
import { MySQLBaseModel } from "src/model/mysql.entity.";



@Entity("category")
export class CategoryModel extends MySQLBaseModel {
  @Column({
    name: "media_id",
    type: "bigint",
  })
  MediaId: number;

  @Column({
    name: "title",
    type: "varchar",
  })
  Title: string;

}
