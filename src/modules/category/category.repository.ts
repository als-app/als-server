import { Injectable } from "@nestjs/common";
import { OrderByRequestParams, PaginationDBParams } from "src/helpers/util.helper";
import { BaseRepository } from "src/repository/base.repository";
import { MySqlRepository } from "src/repository/mysql.repository";
import { EntityRepository, Repository, SelectQueryBuilder } from "typeorm";
import { MediaModel } from "../media/media.entity";
import { CategoryModel } from "./category.entity";

@Injectable()
@EntityRepository(CategoryModel)
export class CategoryRepository extends MySqlRepository<CategoryModel> {
  protected DefaultAlias: string = "category";

  private _getFindQueryBuilder(): SelectQueryBuilder<CategoryModel> {
    let QueryBuilder: SelectQueryBuilder<CategoryModel> = this.createQueryBuilder(this.DefaultAlias)
      .leftJoinAndMapOne("category.Media", MediaModel, "media", "category.media_id = media.id");

    return QueryBuilder;
  }

  async Find(whereParams, paginationOptions?: PaginationDBParams, orderByOptions?: OrderByRequestParams): Promise<Array<CategoryModel>> {
    let QueryBuilder = this._getFindQueryBuilder();
    QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
    QueryBuilder = this.ApplyPaginationOnQueryBuilder(QueryBuilder, paginationOptions);
    QueryBuilder = this.ApplyOrderOnQueryBuilder(QueryBuilder, orderByOptions);

    return await QueryBuilder.getMany();
  }

  async Count(whereParams): Promise<number> {
    let QueryBuilder = this._getFindQueryBuilder();
    QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);

    return await QueryBuilder.getCount();
  }
}
