import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, Length, IsOptional } from "class-validator";

export class CategoryResponseModel {
  @ApiProperty()
  Id: number;

  @ApiProperty()
  MediaId: number;

  @ApiProperty()
  Title: string;
}
