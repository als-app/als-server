import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MediaRepository } from "../media/media.repository";
import { CategoryController } from "./category.controller";
import { CategoryRepository } from "./category.repository";
import { CategoryService } from "./category.service";

@Module({
  imports: [TypeOrmModule.forFeature([CategoryRepository, MediaRepository])],
  exports: [TypeOrmModule],
  providers: [CategoryService],
  controllers: [CategoryController],
})
export class CategoryModule { }
