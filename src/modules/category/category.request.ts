import { ApiProperty } from "@nestjs/swagger";
import {
  IsArray,
  IsEnum,
  IsInt,
  IsNumber,
  IsOptional,
  Length,
  Min,
} from "class-validator";

export enum CategoryOrderByColumns {
  Id = "Id",
  CreatedAt = "CreatedAt",
}

export enum CategoryOrderByDirections {
  ASC = "ASC",
  DESC = "DESC",
}


export class FindCategoryRequest {
  @ApiProperty({ required: false, enum: CategoryOrderByColumns })
  @IsOptional()
  @IsEnum(CategoryOrderByColumns)
  Column: string;

  @ApiProperty({ required: false, enum: CategoryOrderByDirections })
  @IsOptional()
  @IsEnum(CategoryOrderByDirections)
  Direction: "ASC" | "DESC";

  @ApiProperty({ required: false })
  @IsOptional()
  Page: number;

  @ApiProperty({ required: false })
  @IsOptional()
  Limit: number;

  @ApiProperty({ required: false })
  @IsOptional()
  Before: number;

  @ApiProperty({ required: false })
  @IsOptional()
  After: number;

}


export class CreateCategoryRequest {



  @Length(1, 255)
  Title: string;


  @IsOptional()
  @IsInt()
  @Min(1)
  MediaId: number

}

export class UpdateCategoryRequest {


  @IsOptional()
  @Length(1, 255)
  Title: string;


  @IsOptional()
  @IsInt()
  @Min(1)
  MediaId: number

}
