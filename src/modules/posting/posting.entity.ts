import { MySQLBaseModel } from "src/model/mysql.entity.";
import { UserModel } from "src/modules/auth/user.entity";
import { MediaModel } from "src/modules/media/media.entity";
import { Column, Entity } from "typeorm";

@Entity("posting")
export class PostingModel extends MySQLBaseModel {

    @Column({
        name: "description",
        type: "text"
    })
    Description: string;

    @Column({
        name: "media_id",
        type: "bigint"
    })
    MediaId: number

    @Column({
        name: "category_id",
        type: "int"
    })
    CategoryId: number


    Media: MediaModel = null;
    User: UserModel = null;
}
