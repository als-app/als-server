import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MediaRepository } from "../media/media.repository";
import { PostingController } from "./posting.controller";
import { PostingRepository } from "./posting.repository";
import { PostingService } from "./posting.service";

@Module({
    imports: [TypeOrmModule.forFeature([PostingRepository, MediaRepository])],
    exports: [TypeOrmModule, PostingService],
    providers: [PostingService],
    controllers: [PostingController],
})
export class PostingModule { }
