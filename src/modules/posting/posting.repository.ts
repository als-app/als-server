import { OrderByRequestParams, PaginationDBParams } from "src/helpers/util.helper";
import { UserModel } from "src/modules/auth/user.entity";
import { MediaModel } from "src/modules/media/media.entity";
import { MySqlRepository } from "src/repository/mysql.repository";
import { EntityRepository, SelectQueryBuilder } from "typeorm";
import { PostingModel } from "./posting.entity";

@EntityRepository(PostingModel)
export class PostingRepository extends MySqlRepository<PostingModel> {
    protected DefaultAlias: string = 'posting';

    private _getFindQueryBuilder(): SelectQueryBuilder<PostingModel> {
        let QueryBuilder: SelectQueryBuilder<PostingModel> = this.createQueryBuilder(this.DefaultAlias)
            .leftJoinAndMapOne("posting.Media", MediaModel, "media", "posting.media_id = media.id");

        return QueryBuilder;
    }

    async Find(whereParams, paginationOptions?: PaginationDBParams, orderByOptions?: OrderByRequestParams): Promise<Array<PostingModel>> {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        QueryBuilder = this.ApplyPaginationOnQueryBuilder(QueryBuilder, paginationOptions);
        QueryBuilder = this.ApplyOrderOnQueryBuilder(QueryBuilder, orderByOptions);

        return await QueryBuilder.getMany();
    }

    async Count(whereParams): Promise<number> {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);

        return await QueryBuilder.getCount();
    }
}