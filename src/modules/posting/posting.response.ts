import { ApiProperty } from "@nestjs/swagger";
import { UserResponseModel } from "src/modules/auth/user.response";
import { MediaResponseModel } from "src/modules/media/media.response";

export class PromotionResponseModel {
    @ApiProperty()
    VendorId: number;

    @ApiProperty()
    Title: string;

    @ApiProperty()
    Description: string;

    @ApiProperty()
    MediaId: number;

    @ApiProperty()
    Status: number;

    @ApiProperty()
    Media: MediaResponseModel;
}

export class FindPromotionResponse {
    @ApiProperty({ isArray: true, type: PromotionResponseModel })
    Promotions: Array<PromotionResponseModel>;

    @ApiProperty()
    TotalPromotions: number;
}

export class CreatePromotionResponse {
    @ApiProperty()
    Promotion: PromotionResponseModel;

    @ApiProperty()
    Message: string
}

export class UpdatePromotionResponse {
    @ApiProperty()
    Promotion: PromotionResponseModel;

    @ApiProperty()
    Message: string
}