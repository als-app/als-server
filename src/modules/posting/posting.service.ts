import { Get, HttpStatus, Injectable } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";
import { Authorized } from "src/decorator/authorize.decorator";
import { BadRequestException, NotFoundException } from "src/exception/response.exception";
import { appEnv } from "src/helpers/env.helper";
import { GetOrderByOptions, GetPaginationOptions, OrderByRequestParams } from "src/helpers/util.helper";
import { MessageTemplates } from "src/locale/locale";
import { UserModel, UserModelType } from "src/modules/auth/user.entity";
import { MediaRepository } from "src/modules/media/media.repository";
import { BadRequestExceptionResponse } from "src/response/response.schema";
import { PostingModel } from "./posting.entity";
import { PostingRepository } from "./posting.repository";
import { CreatePostingRequest, FindPostingRequest, UpdatePostingRequest } from "./posting.request";
import { FindPromotionResponse } from "./posting.response";

@Injectable()
export class PostingService {
    constructor(private _postingRepository: PostingRepository, private _mediaRepository: MediaRepository) { }

    async Find(data: FindPostingRequest): Promise<any> {
        let WhereParams: any = {};
        if (data?.CategoryId) {
            WhereParams["CategoryId"] = data.CategoryId;
        }


        let Postings: Array<PostingModel> = await this._postingRepository.Find(WhereParams, GetPaginationOptions(data));
        let TotalPostings: number = await this._postingRepository.Count(WhereParams);

        return { Postings, TotalPostings }
    }

    async Create(data: CreatePostingRequest, user: UserModel) {




        let Posting = new PostingModel();


        Posting.Description = data.Description;
        Posting.CategoryId = data.CategoryId;
        if (data.MediaId) {
            let Media = await this._mediaRepository.FindById(data.MediaId);
            if (!Media) {
                throw new NotFoundException(MessageTemplates.MediaNotFound)
            }
            Posting.MediaId = data.MediaId
        }
        Posting = await this._postingRepository.Create(Posting);
        return { Message: "Success", Posting }
    }

    async Update(id: number, data: UpdatePostingRequest, user: UserModel) {
        let Posting = await this._postingRepository.FindById(id);
        if (!Posting) {
            throw new NotFoundException(MessageTemplates.NotFoundError)
        }

        if (data.MediaId && Posting.MediaId !== data.MediaId) {
            let Media = await this._mediaRepository.FindById(data.MediaId);
            if (!Media) {
                throw new NotFoundException(MessageTemplates.MediaNotFound)
            }
        }

        await Posting.Fill(data);
        Posting = await this._postingRepository.Save(Posting);
        return { Message: "updated", Posting }
    }

    async Delete(id: number, user: UserModel) {
        let Posting = await this._postingRepository.FindById(id);
        if (!Posting) {
            throw new NotFoundException(MessageTemplates.NotFoundError)
        }
        await this._postingRepository.DeleteById(Posting.Id);
        return { Message: "deleted" }
    }

    async Get(id: number, user: UserModel) {

        let Posting = await this._postingRepository.FindOne({ Id: id });

        return { Posting }
    }

}