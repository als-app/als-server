import { Body, HttpStatus, Param, Query } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";
import { MessageResponse } from "src/app.response";
import { AppController } from "src/decorator/appcontroller.decorator";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { Delete } from "src/decorator/nest-ext/delete.decorator";
import { Get } from "src/decorator/nest-ext/get.decorator";
import { Patch } from "src/decorator/nest-ext/patch.decorator";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { Put } from "src/decorator/nest-ext/put.decorator";
import { UserModel, UserModelType } from "src/modules/auth/user.entity";
import { CreatePostingRequest, FindPostingRequest, UpdatePostingRequest } from "./posting.request";
import { CreatePromotionResponse, FindPromotionResponse, UpdatePromotionResponse } from "./posting.response";
import { PostingService } from "./posting.service";

@AppController("posting")
export class PostingController {
    constructor(private _promotionService: PostingService) { }

    // @Authorized()
    @Get("/", {})
    async FindPromotions(@Query() data: FindPostingRequest): Promise<FindPromotionResponse> {
        return await this._promotionService.Find(data);
    }

    @Authorized()
    @Post("/", {})
    async Create(@Body() data: CreatePostingRequest, @CurrentUser() user: UserModel): Promise<any> {
        return await this._promotionService.Create(data, user);
    }

    @Authorized()
    @Patch("/:id", {})
    async Update(@Param("id") id: number, @Body() data: UpdatePostingRequest, @CurrentUser() user: UserModel): Promise<any> {
        return await this._promotionService.Update(id, data, user);
    }

    @Authorized()
    @Delete("/:id", MessageResponse)
    async Delete(@Param("id") id: number, @CurrentUser() user: UserModel): Promise<MessageResponse> {
        return await this._promotionService.Delete(id, user);
    }

    @Authorized()
    @Get("/:id",{})
    async Get(
      @Param("id") id: number,
      @CurrentUser() user: UserModel
    ) {
      return await this._promotionService.Get(id, user);
    }
}