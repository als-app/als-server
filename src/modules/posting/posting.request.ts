import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsInt, IsOptional, Length, Min } from "class-validator";
import { IsInEnum } from "src/decorator/is_in_enum.decorator";

export class FindPostingRequest {


    @ApiProperty({ required: false })
    @IsOptional()
    Title: string;

    @ApiProperty({ required: false })
    @IsOptional()
    Page: number;

    @ApiProperty({ required: false })
    @IsOptional()
    Limit: number;

    @ApiProperty({ required: false })
    @IsOptional()
    Before: number;

    @ApiProperty({ required: false })
    @IsOptional()
    After: number;

    @ApiProperty({ required: false })
    @IsOptional()
    CategoryId: number;
}

export class CreatePostingRequest {


    @Length(1)
    Description: string;

    @IsOptional()
    @IsInt()
    @Min(1)
    MediaId: number

    
    @IsOptional()
    CategoryId: number;
}

export class UpdatePostingRequest {




    @IsOptional()
    @Length(1)
    Description: string;

    @IsOptional()
    @IsInt()
    @Min(1)
    MediaId: number;

    @IsOptional()
    CategoryId: number;
}