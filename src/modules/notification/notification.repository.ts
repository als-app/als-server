import { MySqlRepository } from 'src/repository/mysql.repository';
import { EntityRepository, Repository } from 'typeorm';
import { NotificationModel } from './notification.entity';

@EntityRepository(NotificationModel)
export class NotificationRepository extends MySqlRepository<NotificationModel> {
    protected DefaultAlias: string = 'notification';
    constructor() {
        super(NotificationModel)
    }
}
