import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserDeviceRepository } from "../auth/user_device/user_device.repository";
import { NotificationController } from "./notification.controller";
import { NotificationModel } from "./notification.entity";
import { NotificationRepository } from "./notification.repository";
import { NotificationService } from "./notification.service";


@Module({
    imports: [TypeOrmModule.forFeature([
        NotificationRepository,
        UserDeviceRepository,
        NotificationModel
    ])],
    exports: [TypeOrmModule, NotificationService],
    providers: [NotificationService],
    controllers: [NotificationController]
})
export class NotificationModule { }