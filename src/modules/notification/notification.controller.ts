import { Body, HttpStatus, Query, UploadedFile, UseInterceptors } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { AppController } from "src/decorator/appcontroller.decorator";
import { UserModel } from "../auth/user.entity";
import { FileInterceptor } from "@nestjs/platform-express";
import { FileUploadOptions } from "src/helpers/media.helper";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { S3FileUploadOptions } from "src/helpers/s3.helper";
import { NotificationService } from "./notification.service";
import { Get } from "src/decorator/nest-ext/get.decorator";
import { FindNotificationRequest } from "./norification.request";

@AppController("notification")
export class NotificationController {
    constructor(private _notificationService: NotificationService) { }


    @Authorized()
    @Get("/", {})
    async FindPromotions(@Query() data: FindNotificationRequest, @CurrentUser() user: UserModel): Promise<any> {
        return await this._notificationService.Find(data, user);
    }
}