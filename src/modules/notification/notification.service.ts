import { Injectable } from "@nestjs/common";
import { DataPipeline } from "aws-sdk";
import { QueueJobs } from "src/constant/queue";
import { SendNotification } from "src/helpers/notification.helper";
import { SSQueue } from "src/helpers/queue.helper";
import { GetPaginationOptions } from "src/helpers/util.helper";
import { Locale, MessageTemplates, __T } from "src/locale/locale";
import { BaseModelEntityType } from "src/model/base.entity";
import { UserModel } from "../auth/user.entity";
import { UserDeviceModel } from "../auth/user_device/user_device.entity";
import { UserDeviceRepository } from "../auth/user_device/user_device.repository";
import { FindNotificationRequest } from "./norification.request";
import { NotificationModel, NotificationModelReadStatus, NotificationModelType } from "./notification.entity";
import { NotificationRepository } from "./notification.repository";
import { InvitationModel } from '../invitation/invitation.entity';


interface NotificationData {
    UserId: number;
    Title: string;
    Description: string;
    Type: number;
    Meta?: JSON
}

@Injectable()
export class NotificationService {
    constructor(private _notificationRepository: NotificationRepository, private _userDeviceRepository: UserDeviceRepository) { }

    async ProcessNotification(type: NotificationModelType, data) {
        switch (type) {
            case NotificationModelType.Invitation:
                return await this._GenerateInvitationNotification(data);
            case NotificationModelType.NewChatMessage:
                return await this._GenerateNewChatMessageNotification(data);
            default:
                return true
        }
    }


    private async _SaveNotifications(notificationData: NotificationData | Array<NotificationData>) {

        if (!notificationData) {
            return false
        }

        if (!Array.isArray(notificationData)) {
            notificationData = [notificationData]
        }

        let NewNotifications = notificationData.map(obj => {
            let Notification = new NotificationModel();
            Notification.ReadStatus = NotificationModelReadStatus.UnRead;
            Notification.UserId = obj.UserId;
            Notification.Title = obj.Title;
            Notification.Description = obj.Description;
            Notification.Type = obj.Type;
            if (obj.Meta) {
                Notification.Meta = obj.Meta
            }
            return Notification;
        })
        if (NewNotifications.length) {
            await this._notificationRepository.BatchCreate(NewNotifications);
        }

        return true;
    }

    private async _SendNotificationOnUserDevices(idOrIds: number | Array<number>, notificationData: NotificationData | Array<NotificationData>) {
        let UserDevices = await this._userDeviceRepository.Find({ UserId: idOrIds });
        let Promises = [];
        UserDevices.map(device => {
            let Notification: NotificationData;
            if (Array.isArray(notificationData)) {
                Notification = notificationData.find(obj => obj.UserId == device.UserId)
            } else {
                Notification = notificationData;
            }
            if (Notification && device.FCMToken) {
                Promises.push(SendNotification(device.FCMToken, Notification.Title, Notification.Description, {
                    Type: Notification.Type,
                    CreatedAt: Date.now(),
                    ...(Notification.Meta && Notification.Meta)
                }))
            }
        })
        await Promise.all(Promises);
        return true;
    }

    private async _GenerateInvitationNotification(data) {
        
        let NotificationData = [];
        let UserIds = [];
        let { Invitation, user, Title, Description }: { Invitation: InvitationModel, user: UserModel, Title: string, Description: string } = data;

        NotificationData.push({
            Title,
            Description,
            Type: NotificationModelType.Invitation,
            EntityId: Invitation.Id,
            EntityType: BaseModelEntityType.Invitation,
            UserId: Invitation.UserId,
            Meta: { InvitationId: Invitation.Id, User: user }
        })
        UserIds.push(Invitation.UserId);


        if (NotificationData.length && UserIds.length) {
            await this._SaveNotifications(NotificationData);
            await this._SendNotificationOnUserDevices(UserIds, NotificationData)
        }

        return true
    }


    private async _GenerateNewChatMessageNotification(data) {
        let Title = __T(Locale.En, MessageTemplates.NewMessageReceive);
        let { Content, ReceiverId, InvitationId, SenderId, SenderType } = data;

        let NotificationData: any = {
            Title,
            Description: Content,
            Type: NotificationModelType.NewChatMessage,
            EntityId: InvitationId,
            EntityType: BaseModelEntityType.Invitation,
            UserId: ReceiverId,
            Meta: { InvitationId }
        };


        let UserId = ReceiverId


        if (UserId && NotificationData) {
            await this._SendNotificationOnUserDevices(UserId, NotificationData)
        }

        return true;
    }



    async Find(data: FindNotificationRequest, user: UserModel) {
        let notifications = await this._notificationRepository.Find({ UserId: user.Id }, GetPaginationOptions(data));
        return { notifications }
    }


}