import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsInt, IsOptional, Length, Min } from "class-validator";


export class FindNotificationRequest {

    @ApiProperty({ required: false })
    @IsOptional()
    Page: number;

    @ApiProperty({ required: false })
    @IsOptional()
    Limit: number;

    @ApiProperty({ required: false })
    @IsOptional()
    Before: number;

    @ApiProperty({ required: false })
    @IsOptional()
    After: number;
}
