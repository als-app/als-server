import { MySQLBaseModel } from 'src/model/mysql.entity.';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { UserModel } from '../auth/user.entity'

@Entity('notification')
export class NotificationModel extends MySQLBaseModel {

    @Column({
        name: "read_status",
        type: "tinyint"
    })
    ReadStatus: number;

    @Column({
        name: "user_id",
        type: "bigint"
    })
    UserId: number;


    @Column({
        name: "title",
        type: "varchar"
    })
    Title: string;

    @Column({
        name: "description",
        type: "longtext"
    })
    Description: string;

    @Column({
        name: "meta",
        type: "json"
    })
    Meta: JSON;

    @Column({
        name: "type",
        type: "tinyint"
    })
    Type: number;

}

export enum NotificationModelReadStatus {
    Read = 1,
    UnRead = 0
}


export enum NotificationModelType {
    Invitation = 1,
    NewChatMessage = 2
}
