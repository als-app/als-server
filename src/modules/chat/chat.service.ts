import { Injectable } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import {
  BadRequestException,
  NotFoundException,
} from "src/exception/response.exception";
import { IsFuture } from "src/helpers/date.helper";
import {
  GetPaginationOptions,
  OrderByRequestParams,
  PaginationRequestParams,
} from "src/helpers/util.helper";
import { MessageTemplates } from "src/locale/locale";
import { UserModel, UserModelType } from "../auth/user.entity";
import { UserRepository } from "../auth/user.repository";
import { ChatModel } from "./chat.entity";
import { ChatRepository } from "./chat.repository";
import { CreateChatRequest, GetChatWhere } from "./chat.request";

@Injectable()
export class ChatService {
  constructor(
    private _chatRepository: ChatRepository,
  ) { }

  async GetInvitationChats(InvitationId: number, UserId?: number) {
    const where = new GetChatWhere();
    where.InvitationId = InvitationId;
    const chats = await this._chatRepository.getAll(where).getMany();

    if (UserId) {
      const unseenChatIds: number[] = chats.filter(item => item.CreatedById !== UserId && !item.Seen).map(item => item.Id);
      this._chatRepository.markChatSeen(unseenChatIds);
    }


    return chats;
  }

  async MarkChatSeen(ChatId: number, UserId: number) {
    const chat = await this._chatRepository.getUnseenChatById(ChatId, UserId).getOne();
    
    if (chat) {
      await this._chatRepository.markChatSeen([chat.Id]);
    }

    return chat;
  }

  async CreateChat(data: CreateChatRequest, user: UserModel) {

    if (!data.Message && !data.MediaId) {
      throw new NotFoundException(MessageTemplates.BadRequestError);
    }

    let chat = new ChatModel()
    chat.Message = data.Message;
    chat.CreatedById = user.Id;
    chat.InvitationId = data.InvitationId;
    chat.MediaId = data.MediaId;

    await this._chatRepository.Create(chat);
    return chat;

  }

  async getUnseenChatCountMapByInvitationIds(InvitationIds: number[], UserId: number): Promise<any> {
    const UnseenChatCountMap = {};
    if (InvitationIds?.length) {
      const UnseenChatCounts: { InvitationId: number, UnseenChatCount: number }[] = await this._chatRepository.getUnseenChatCountByInvitationIds(InvitationIds, UserId);
      for (const UnseenChatCount of UnseenChatCounts) {
        UnseenChatCountMap[UnseenChatCount.InvitationId] = UnseenChatCount.UnseenChatCount;
      }
    }
    return UnseenChatCountMap;

  }

}
