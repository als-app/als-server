import { ApiProperty } from "@nestjs/swagger";
import {
  IsArray,
  IsEnum,
  IsInt,
  IsNumber,
  IsOptional,
  Length,
  Min,
  IsDateString,
} from "class-validator";
import { Transform } from "class-transformer";


export class CreateChatRequest {

  @IsOptional()
  @Length(1, 1000)
  Message: string;

  @IsInt()
  InvitationId: number;

  @IsOptional()
  @IsInt()
  MediaId: number;

}

export class GetChatWhere {
  InvitationId: number;
}
