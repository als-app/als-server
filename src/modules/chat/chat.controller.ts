import { Body, Param, Query } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import { AppController } from "src/decorator/appcontroller.decorator";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { Delete } from "src/decorator/nest-ext/delete.decorator";
import { Get } from "src/decorator/nest-ext/get.decorator";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { Put } from "src/decorator/nest-ext/put.decorator";
import { UserModel, UserModelType } from "../auth/user.entity";
import { CreateChatRequest } from "./chat.request";
import { ChatService } from "./chat.service";
import { ChatModel } from './chat.entity';

@AppController("chat")
export class ChatController {
  constructor(private _chatService: ChatService) { }

  @Authorized()
  @Get("/:InvitationId", {})
  async GetInvitationChats(@Param('InvitationId') InvitationId: number, @CurrentUser() user: UserModel) {
    return await this._chatService.GetInvitationChats(InvitationId, user.Id);
  }

  @Authorized()
  @Post("/:ChatId/seen", {})
  async MarkChatSeen(@Param('ChatId') ChatId: string, @CurrentUser() user: UserModel) {
    return await this._chatService.MarkChatSeen(Number(ChatId), user.Id);
  }

  @Authorized()
  @Post("/", {})
  async CreateChat(@Body() data: CreateChatRequest, @CurrentUser() user: UserModel) {
    return await this._chatService.CreateChat(data, user)
  }
}
