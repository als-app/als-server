import { Injectable } from "@nestjs/common";
import { BaseRepository } from "src/repository/base.repository";
import { MySqlRepository } from "src/repository/mysql.repository";
import { EntityRepository, SelectQueryBuilder, } from "typeorm";
import { ChatModel } from "./chat.entity";
import { GetChatWhere } from './chat.request';
import { MediaModel } from '../media/media.entity';

@Injectable()
@EntityRepository(ChatModel)
export class ChatRepository extends MySqlRepository<ChatModel> {
  protected DefaultAlias: string = "chat";

  public getAll(where: GetChatWhere): SelectQueryBuilder<ChatModel> {
    const qb = this.createQueryBuilder(this.DefaultAlias)
      .innerJoin(`${this.DefaultAlias}.CreatedBy`, 'CreatedBy')
      .leftJoinAndMapOne("CreatedBy.Media", MediaModel, "CreatedByMedia", "CreatedBy.media_id = CreatedByMedia.id")
      .leftJoinAndMapOne(`${this.DefaultAlias}.Media`, MediaModel, "Media", `${this.DefaultAlias}.media_id = Media.id`)
    qb.addSelect(['CreatedBy.Id', 'CreatedBy.FullName'])

    if (where.InvitationId) {
      qb.andWhere(`${this.DefaultAlias}.InvitationId = :InvitationId`, { InvitationId: where.InvitationId });
    }

    return qb;
  }

  public getUnseenChatCountByInvitationIds(InvitationIds: number[], UserId: number): Promise<any> {
    const qb = this.createQueryBuilder(this.DefaultAlias)
      .andWhere(`${this.DefaultAlias}.CreatedById <> :UserId `, { UserId })
      .andWhere(`${this.DefaultAlias}.Seen = :Seen`, { Seen: false })
      .andWhere(`${this.DefaultAlias}.InvitationId in  (:...InvitationIds)`, { InvitationIds })
      .select([
        `${this.DefaultAlias}.InvitationId as InvitationId`,
        `count(${this.DefaultAlias}.Id) as "UnseenChatCount"`,
      ])
      .groupBy(`${this.DefaultAlias}.InvitationId`);

    return qb.getRawMany();
  }

  public getUnseenChatById(ChatId: number, UserId: number): SelectQueryBuilder<ChatModel> {
    const qb = this.createQueryBuilder(this.DefaultAlias)
      .andWhere(`${this.DefaultAlias}.Id = :ChatId`, { ChatId })
      .andWhere(`${this.DefaultAlias}.CreatedById <> :UserId `, { UserId })
      .andWhere(`${this.DefaultAlias}.Seen = :Seen`, { Seen: false });

    return qb;
  }

  public async markChatSeen(chatIds: number[]): Promise<any> {
    if (chatIds?.length) {
      const params = chatIds.map((item) => `?`);
      await this.query(` update chat set seen=1 where id in (${params.join(',')}) `, chatIds);
      return true;
    }
    return true;


  }


}
