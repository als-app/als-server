import { BaseModel } from "src/model/base.entity";
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, } from "typeorm";
import { Exclude } from "class-transformer";
import { MySQLBaseModel } from "src/model/mysql.entity.";
import { Location } from 'src/common/types';
import { UserModel } from '../auth/user.entity';
import { InvitationModel } from '../invitation/invitation.entity';



@Entity("chat")
export class ChatModel extends MySQLBaseModel {

  @Column({
    name: "message",
    type: "varchar",
  })
  Message: string;

  @Column({ name: 'invitation_id', type: 'int', nullable: false })
  public InvitationId: number;

  @ManyToOne(type => InvitationModel, invitation => invitation.Id)
  @JoinColumn({ name: 'invitation_id' })
  public Invitation: InvitationModel;

  @Column({ name: 'created_by_id', type: 'int', nullable: false })
  public CreatedById: number;

  @ManyToOne(type => UserModel, user => user.Id)
  @JoinColumn({ name: 'created_by_id' })
  public CreatedBy: UserModel;

  @Column({
    name: 'media_id',
    type: 'bigint',
    nullable: true,
  })
  MediaId: number;

  @Column({
    name: 'seen',
    type: 'boolean',
    default: false,
  })
  Seen: boolean;

}