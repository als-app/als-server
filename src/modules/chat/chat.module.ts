import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ChatRepository } from "./chat.repository";
import { ChatController } from './chat.controller';
import { ChatService } from './chat.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([ChatRepository]),
  ],
  exports: [TypeOrmModule, ChatService],
  providers: [ChatService],
  controllers: [ChatController],
})
export class ChatModule { }
