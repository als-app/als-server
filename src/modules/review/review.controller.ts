import { Body, Param, Query } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import { AppController } from "src/decorator/appcontroller.decorator";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { Delete } from "src/decorator/nest-ext/delete.decorator";
import { Get } from "src/decorator/nest-ext/get.decorator";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { Put } from "src/decorator/nest-ext/put.decorator";
import { UserModel, UserModelType } from "../auth/user.entity";
import { CreateReviewRequest } from "./review.request";
import { ReviewService } from "./review.service";

@AppController("review")
export class ReviewController {
  constructor(private _reviewService: ReviewService) { }

  @Authorized()
  @Get("/user/:UserId", {})
  async GetUserReviews(@Param('UserId') UserId: string) {
    return await this._reviewService.GetUserReviews(Number(UserId));
  }

  @Authorized()
  @Post("/", {})
  async CreateReview(@Body() data: CreateReviewRequest, @CurrentUser() user: UserModel) {
    return await this._reviewService.CreateReview(data, user)
  }

}
