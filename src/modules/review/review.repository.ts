import { Injectable } from "@nestjs/common";
import { BaseRepository } from "src/repository/base.repository";
import { MySqlRepository } from "src/repository/mysql.repository";
import { EntityRepository, SelectQueryBuilder, } from "typeorm";
import { ReviewModel } from "./review.entity";
import { GetReviewsWhere } from './review.request';
import { MediaModel } from '../media/media.entity';

@Injectable()
@EntityRepository(ReviewModel)
export class ReviewRepository extends MySqlRepository<ReviewModel> {
  protected DefaultAlias: string = "review";

  public getAll(where: GetReviewsWhere): SelectQueryBuilder<ReviewModel> {
    const qb = this.createQueryBuilder(this.DefaultAlias);

    if (where.populateUser) {
      qb.innerJoin(`${this.DefaultAlias}.User`, 'User')
        .leftJoinAndMapOne("User.Media", MediaModel, "media", "User.media_id = media.id")
      qb.addSelect(['User.Id', 'User.FullName'])
    }

    if (where.populateCreatedBy) {
      qb.innerJoin(`${this.DefaultAlias}.CreatedBy`, 'CreatedBy')
        .leftJoinAndMapOne("CreatedBy.Media", MediaModel, "CreatedByMedia", "CreatedBy.media_id = CreatedByMedia.id")
      qb.addSelect(['CreatedBy.Id', 'CreatedBy.FullName'])
    }

    if (where.CreatedById) {
      qb.andWhere(`${this.DefaultAlias}.CreatedById = :CreatedById`, { CreatedById: where.CreatedById });
    }

    if (where.UserId) {
      qb.andWhere(`${this.DefaultAlias}.UserId = :UserId`, { UserId: where.UserId });
    }

    qb.orderBy(`${this.DefaultAlias}.CreatedAt`, 'DESC')

    return qb;
  }

  public getUsersReviewCount(ids: number[]): SelectQueryBuilder<any> {
    const qb = this.createQueryBuilder(this.DefaultAlias)
      .select([
        `${this.DefaultAlias}.UserId as UserId`,
        `count(${this.DefaultAlias}.Id) as "ReviewCount"`,
      ])
      .where(`${this.DefaultAlias}.UserId in (:...ids)`, { ids })
      .groupBy(`${this.DefaultAlias}.UserId`)

    return qb;
  }

  public async getUserAvgRating(UserId): Promise<number> {
    const qb = this.createQueryBuilder(this.DefaultAlias)
      .select(`avg(${this.DefaultAlias}.rating)`, 'average')
      .andWhere(`${this.DefaultAlias}.UserId = :UserId`, { UserId: UserId });

    const data = await qb.getRawOne();
    return Math.round(data.average);
  }


}
