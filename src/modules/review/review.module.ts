import { Module, forwardRef } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ReviewRepository } from "./review.repository";
import { ReviewController } from './review.controller';
import { ReviewService } from './review.service';
import { UserModule } from '../auth/user.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ReviewRepository]),
    forwardRef(() => UserModule)
  ],
  exports: [TypeOrmModule, ReviewService],
  providers: [ReviewService],
  controllers: [ReviewController],
})
export class ReviewModule { }
