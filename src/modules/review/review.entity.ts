import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";

import { MySQLBaseModel } from "src/model/mysql.entity.";
import { UserModel } from '../auth/user.entity'
import { InvitationModel } from '../invitation/invitation.entity';

@Entity("reviews")
export class ReviewModel extends MySQLBaseModel {

  @Column({
    name: "description",
    type: "varchar",
  })
  Description: string;

  @Column({
    name: "title",
    type: "varchar",
  })
  Title: string;

  @Column({ name: 'rating', type: 'int', default: 0 })
  public Rating: number;

  @Column({ name: 'invitation_id', type: 'int', nullable: true })
  public InvitationId: number;

  @ManyToOne(type => InvitationModel, invitation => invitation.Id)
  @JoinColumn({ name: 'invitation_id' })
  public Invitation: InvitationModel;

  @Column({ name: 'created_by_id', type: 'int', nullable: true })
  public CreatedById: number;

  @ManyToOne(type => UserModel, user => user.Id)
  @JoinColumn({ name: 'created_by_id' })
  public CreatedBy: UserModel;


  @Column({ name: 'user_id', type: 'int', nullable: true })
  public UserId: number;

  @ManyToOne(type => UserModel, user => user.Id)
  @JoinColumn({ name: 'user_id' })
  public User: UserModel;



}