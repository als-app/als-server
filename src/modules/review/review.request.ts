import { ApiProperty } from "@nestjs/swagger";
import {
  IsInt,
  IsNumber,
  Length,
  IsDateString,
  IsOptional,
  Max,
  Min
} from "class-validator";
import { Transform } from "class-transformer";


export class CreateReviewRequest {


  @Length(1, 1000)
  Description: string;

  @Length(1, 1000)
  Title: string;

  @IsOptional()
  @IsInt()
  InvitationId: number;

  @IsInt()
  @Min(1)
  @Max(5)
  Rating: number;

  @IsInt()
  UserId: number;

}

export class GetReviewsWhere {
  UserId: number;
  CreatedById: number;
  populateCreatedBy: boolean = false;
  populateUser: boolean = false;
}
