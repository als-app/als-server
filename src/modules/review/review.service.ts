import { Injectable, Inject, forwardRef } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import {
  BadRequestException,
  NotFoundException,
} from "src/exception/response.exception";
import { IsFuture } from "src/helpers/date.helper";
import {
  GetPaginationOptions,
  OrderByRequestParams,
  PaginationRequestParams,
} from "src/helpers/util.helper";
import { MessageTemplates } from "src/locale/locale";
import { UserModel, UserModelType } from "../auth/user.entity";
import { UserRepository } from "../auth/user.repository";
import { ReviewModel } from "./review.entity";
import { ReviewRepository } from "./review.repository";
import { CreateReviewRequest, GetReviewsWhere } from "./review.request";
import { UserService } from '../auth/user.service';

@Injectable()
export class ReviewService {
  constructor(
    private _reviewRepository: ReviewRepository,
    @Inject(forwardRef(() => UserService))
    private _userService: UserService,
  ) { }

  async GetUserReviews(UserId: number) {
    const where = new GetReviewsWhere();
    where.UserId = UserId;
    where.populateCreatedBy = true;
    const Reviews = await this._reviewRepository.getAll(where).getMany();
    const AvgRating = await this._reviewRepository.getUserAvgRating(UserId);
    return { Reviews, AvgRating };
  }

  async GetUsersReviewCountMap(UserIds: number[]): Promise<any> {
    const reviewCountMap = {}
    if (!UserIds?.length) {
      return reviewCountMap;
    }
    const usersReviewCount: { UserId: number, ReviewCount: number }[] = await this._reviewRepository.getUsersReviewCount(UserIds).getRawMany();
    for (const userReviewCount of usersReviewCount) {
      reviewCountMap[userReviewCount.UserId] = userReviewCount.ReviewCount;
    }
    return reviewCountMap;
  }

  async CreateReview(data: CreateReviewRequest, user: UserModel) {
    const User = await this._userService.GetUserById(data.UserId);

    let review = new ReviewModel()
    review.Description = data.Description;
    review.Title = data.Title;
    review.UserId = User.Id;
    review.CreatedById = user.Id;
    review.Rating = data.Rating;
    if (data.InvitationId) {
      review.InvitationId = data.InvitationId;
    }

    await this._reviewRepository.Create(review);
    return review;

  }

}
