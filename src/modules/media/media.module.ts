import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MediaController } from "./media.controller";
import { MediaModel } from "./media.entity";
import { MediaRepository } from "./media.repository";
import { MediaService } from "./media.service";

@Module({
    imports: [TypeOrmModule.forFeature([
        MediaRepository,
        MediaModel
    ])],
    exports: [TypeOrmModule, MediaService],
    providers: [MediaService],
    controllers: [MediaController]
})
export class MediaModule { }