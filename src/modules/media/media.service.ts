import { Injectable } from "@nestjs/common";
import { UserModel } from "../auth/user.entity";
import { MediaMeta, MediaModel, MediaModelStatus, MediaModelType } from "./media.entity";
import { MediaRepository } from "./media.repository";
import { UploadFinalizeMediaRequest, UploadInitMediaRequest } from "./media.request";
import { CreateFilePath, CreateUniqueFileName, GetMediaExtension, GetMediaType, GetTemporaryCredentialsForMediaUpload, IsMediaExtensionAllowed, IsMediaSizeAllowed, MediaMaxSize } from "../../helpers/media.helper";
import { BadRequestException, FatalErrorException, ForbiddenException, NotFoundException } from "src/exception/response.exception";
import { MessageTemplates } from "src/locale/locale";
import { UploadFinalizeMediaResponse, UploadInitMediaResponse } from "./media.response";
import { appEnv } from "src/helpers/env.helper";
import { Logger } from "src/helpers/logger.helper";
import { GetObjectHeaders } from "src/helpers/s3.helper";

@Injectable()
export class MediaService {
    constructor(private _mediaRepository: MediaRepository) { }

    async Create(fileObject, activeUser: UserModel): Promise<any> {
        console.log(fileObject)
        let MediaObject = new MediaModel();
        let FileMetaObject = new MediaMeta();
        let Extension: string = GetMediaExtension(fileObject.originalname);
        FileMetaObject.Size = fileObject.size;
        FileMetaObject.Extension = Extension;
        FileMetaObject.MimeType = fileObject.mimetype;
        FileMetaObject.Name = fileObject.originalname;
        MediaObject.Meta = FileMetaObject;
        MediaObject.BasePath = `${appEnv("AWS_S3_BASE_URL", "https://event-buddy-uploads.s3.amazonaws.com")}/`

        MediaObject.Path = fileObject.key;
        MediaObject.ThumbPath = fileObject.key;
        MediaObject.UserId = activeUser.Id;
        MediaObject.Type = GetMediaType(fileObject.mimetype);
        MediaObject.Status = MediaModelStatus.Active;
        await this._mediaRepository.Save(MediaObject);
        return { Id: MediaObject.Id, MediaObject }
    }

    async GetById(id) {
        return await this._mediaRepository.FindById(id);
    }
}