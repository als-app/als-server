import { Body, HttpStatus, UploadedFile, UseInterceptors } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { AppController } from "src/decorator/appcontroller.decorator";
import { UserModel } from "../auth/user.entity";
import { UploadFinalizeMediaRequest, UploadInitMediaRequest } from "./media.request";
import { UploadFinalizeMediaResponse, UploadInitMediaResponse, UploadMediaResponse } from "./media.response";
import { MediaService } from "./media.service";
import { FileInterceptor } from "@nestjs/platform-express";
import { FileUploadOptions } from "src/helpers/media.helper";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { S3FileUploadOptions } from "src/helpers/s3.helper";

@AppController("media")
export class MediaController {
    constructor(private _mediaServie: MediaService) { }


    @Authorized()
    @Post("upload", UploadMediaResponse)
    @UseInterceptors(FileInterceptor('file', S3FileUploadOptions))
    async UploadFile(
        @UploadedFile() file,
        @CurrentUser() user: UserModel,
    ): Promise<{ Id: number }> {
        return await this._mediaServie.Create(file, user);
    }
}