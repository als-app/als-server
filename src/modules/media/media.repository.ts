import { BaseRepository } from 'src/repository/base.repository';
import { MySqlRepository } from 'src/repository/mysql.repository';
import { EntityRepository, Repository } from 'typeorm';
import { MediaModel } from './media.entity';

@EntityRepository(MediaModel)
export class MediaRepository extends MySqlRepository<MediaModel> {
    protected DefaultAlias: string = 'media';
}
