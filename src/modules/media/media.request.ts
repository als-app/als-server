import { IsInt, IsNumber, Length } from "class-validator";

export class UploadInitMediaRequest {
    @Length(1, 255)
    Name: string;

    @Length(1, 255)
    MimeType: string;

    @IsNumber()
    Size: number;
}

export class UploadFinalizeMediaRequest {
    @IsInt()
    Id: number;
}