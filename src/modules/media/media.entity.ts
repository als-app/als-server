import { appEnv } from "src/helpers/env.helper";
import { MySQLBaseModel } from "src/model/mysql.entity.";
import { AfterInsert, AfterLoad, AfterUpdate, BeforeInsert, Column, Entity } from "typeorm";

export class MediaMeta {
    Size: number;
    Extension: string;
    MimeType: string;
    Name: string;
}

@Entity("media")
export class MediaModel extends MySQLBaseModel {
    @Column({
        name: "base_path",
        type: "varchar"
    })
    BasePath: string;

    @Column({
        name: "path",
        type: "varchar",
        length: 500
    })
    Path: string;

    @Column({
        name: "thumb_path",
        type: "varchar",
        length: 500
    })
    ThumbPath: string;

    @Column({
        name: "user_id",
        type: "bigint"
    })
    UserId: number;

    @Column({
        name: "meta",
        type: "json"
    })
    Meta: MediaMeta;

    @Column({
        name: "type",
        type: "tinyint"
    })
    Type: MediaModelType;

    @Column({
        name: "status",
        type: "tinyint"
    })
    Status: number;
}

export enum MediaModelType {
    Image = 1,
    Video = 2,
    Audio = 3,
    Document = 10,
    Archive = 5
}

export enum MediaModelStatus {
    Active = 1,
    Failed = 0,
}