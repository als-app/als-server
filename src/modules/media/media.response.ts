import { ApiProperty } from "@nestjs/swagger";
import { MediaModelType } from "./media.entity";

class MediaMetaResponseModel {
    @ApiProperty()
    Size: number;

    @ApiProperty()
    MimeType: string;

    @ApiProperty()
    Name: string;

    @ApiProperty()
    Extension: string;
}
export class MediaResponseModel {

    @ApiProperty()
    Id: number;

    @ApiProperty()
    BasePath: string;

    @ApiProperty()
    Path: string;

    @ApiProperty()
    ThumbPath: string;

    @ApiProperty()
    Type: number;

    @ApiProperty()
    Meta: MediaMetaResponseModel;

    @ApiProperty()
    DeletedAt: number;

    @ApiProperty()
    CreatedAt: number;

    @ApiProperty()
    UpdatedAt: number;
}

export class UploadInitMediaResponse {
    @ApiProperty()
    MediaId: number;

    @ApiProperty()
    Path: string;

    @ApiProperty()
    Bucket: string;

    @ApiProperty()
    Region: string;

    @ApiProperty()
    AccessKeyId: string;

    @ApiProperty()
    SecretAccessKey: string;

    @ApiProperty()
    SessionToken: string;
}

export class UploadFinalizeMediaResponse {
    @ApiProperty()
    Message: string;

    @ApiProperty()
    Media: MediaResponseModel;
}

export class UploadMediaResponse {
    @ApiProperty()
    Id: number;

}