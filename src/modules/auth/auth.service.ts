import { Injectable } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { UserModel } from './user.entity';
import { UserRepository } from './user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { RedisCacheService } from 'src/redis/redis.service';
import { SocketEventSenderType } from 'src/constant/socket';

export class Auth {
  public UserId: number;
  public User: UserModel;

  constructor(userId: number, user?: UserModel) {
    this.UserId = userId;
    if (user) {
      this.User = user;
    }
  }
}

export class SocketAuth {
  public UserId: number;
  public SocketId: string;
  public UserType: SocketEventSenderType;
  public AdvisorId: number;

  constructor(socketId: string, userId: number, userType: SocketEventSenderType, advisorId?: number) {
    this.UserId = userId;
    this.SocketId = socketId;
    this.UserType = userType;
    if (advisorId) {
      this.AdvisorId = advisorId;
    }
  }
}

@Injectable()
export class AuthService {
  constructor(
    private readonly _redisCacheService: RedisCacheService,
    @InjectRepository(UserRepository)
    private _userRepository: UserRepository,
  ) { }

  private static _generateToken(): string {
    return uuid();
  }

  private static _getKey(token: string): string {
    return 'APP-TOKEN:' + token;
  }

  private static _getSocketKey(socketId: string): string {
    return 'SS-SOCK:' + socketId;
  }
  private static _getSocketEntityKey(entityId: number, entityType: SocketEventSenderType): string {
    return 'SS-SOCK-ENT:' + entityType + "-" + entityId;
  }


  public GetToken(): string {
    return AuthService._generateToken();
  }

  public async CreateSession(userId: number): Promise<string> {
    let token: string = AuthService._generateToken();
    let TokenData: Auth = new Auth(userId);
    await this._redisCacheService.Set(
      AuthService._getKey(token),
      JSON.stringify(TokenData),
    );
    return token;
  }

  public async SetSession(token: string, userId: number): Promise<boolean> {
    let TokenData: Auth = new Auth(userId);
    await this._redisCacheService.Set(
      AuthService._getKey(token),
      JSON.stringify(TokenData),
    );
    return true;
  }

  public async GetSession(token: string): Promise<Auth> {
    let RawData: string = await this._redisCacheService.Get(
      AuthService._getKey(token),
    );
    if (RawData) {
      let RawObject = JSON.parse(RawData);
      let Data: Auth = new Auth(RawObject.UserId, RawObject.AccountMemberships);
      Data.User = await this._userRepository.FindById(Data.UserId);
      return Data;
    }
    return null;
  }

  public async GetEntityDataBySocketId(socketId: string): Promise<SocketAuth> {
    let RawData = await this._redisCacheService.Get(AuthService._getSocketKey(socketId));
    try {
      let Data: SocketAuth = JSON.parse(RawData);
      return Data;
    } catch (err) {
      // Logger.Error(err, "[SOCKET]GET-ENTITY-DATA-BY-SOCKET-ID");
    }
  }

  public async SetSocketIdsByUserId(socketIds: Array<string>, userId: number, userType: SocketEventSenderType) {
    await this._redisCacheService.Set(AuthService._getSocketEntityKey(userId, userType), JSON.stringify(socketIds));
    return true;
  }

  public async SetCustomerSocketId(socketId: string, userId: number) {
    let Data: SocketAuth = new SocketAuth(socketId, userId, SocketEventSenderType.User);
    await this._redisCacheService.Set(AuthService._getSocketKey(socketId), JSON.stringify(Data));
    return true;
  }

  public async SetVendorSocketId(socketId: string, userId: number, advisorId: number) {
    let Data: SocketAuth = new SocketAuth(socketId, userId, SocketEventSenderType.Advisor, advisorId);
    await this._redisCacheService.Set(AuthService._getSocketKey(socketId), JSON.stringify(Data));
    return true;
  }

  public async GetSocketIdsByUserId(userId: number, userType: SocketEventSenderType): Promise<Array<string>> {
    let RawData = await this._redisCacheService.Get(AuthService._getSocketEntityKey(userId, userType));
    try {
      let Data: Array<string> = JSON.parse(RawData) || [];
      return Data;
    } catch (err) {
      //  Logger.Error(err, "[SOCKET]GET-SOCKET-IDS-BY-USER-ID");
      return [];
    }
  }


  public async RemoveEntityDataBySocketId(socketId: string): Promise<boolean> {
    await this._redisCacheService.Delete(AuthService._getSocketKey(socketId));
    return true;
  }

  public async RemoveSocketIdFromUserSockets(socketId: string, userId: number, userType: SocketEventSenderType): Promise<boolean> {
    let SocketIds: Array<string> = await this.GetSocketIdsByUserId(userId, userType);
    SocketIds = SocketIds.filter(id => id !== socketId);
    await this.SetSocketIdsByUserId(SocketIds, userId, userType);
    return true;
  }
}
