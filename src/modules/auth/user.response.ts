import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length, IsOptional } from 'class-validator';

export class UserResponseModel {

  @ApiProperty()
  Id: number;

  @ApiProperty()
  FullName: string;

  @ApiProperty()
  Email: string;

  @ApiProperty()
  Type: number;

  @ApiProperty()
  MediaId: number;

  @ApiProperty()
  Gender: number;
}

export class LoginResponse {
  @ApiProperty()
  DeviceUUID: string;

  @ApiProperty()
  Token: string;

  @ApiProperty()
  User: UserResponseModel;

  @ApiProperty()
  AuthCode: number;
}

export class SignUpResponse {
  @ApiProperty()
  DeviceUUID: string;

  @ApiProperty()
  Token: string;

  @ApiProperty()
  User: UserResponseModel;

  @ApiProperty()
  AuthCode: number;
}

export class ResendSignUpCodeResponse {
  @ApiProperty()
  DeviceUUID: string;
}

export class ForgetPasswordResponse {
  @ApiProperty()
  TokenUUID: string;
}

export class SignUpVerificationResponse {
  @ApiProperty()
  Message: string;

  @ApiProperty()
  Token: string;

  @ApiProperty()
  User: UserResponseModel;

  @ApiProperty()
  AuthCode: number;
}

export class ForgetPasswordVerificationResponse {
  @ApiProperty()
  Message: string;

  @ApiProperty()
  TokenUUID: string;

  @ApiProperty()
  Code: string;
}

export class ResetPasswordResponse {
  @ApiProperty()
  Message: string;
}

export class GuestLoginResponse {
  @ApiProperty()
  DeviceUUID: string;

  @ApiProperty()
  Token: string;

  @ApiProperty()
  User: UserResponseModel;
}

export class GetUserResponse {
  @ApiProperty()
  User: UserResponseModel;
}
