import { Injectable } from '@nestjs/common';
import { OrderByRequestParams, PaginationDBParams } from 'src/helpers/util.helper';
import { BaseRepository } from 'src/repository/base.repository';
import { MySqlRepository } from 'src/repository/mysql.repository';
import { Brackets, EntityRepository, Repository, SelectQueryBuilder } from 'typeorm';
import { MediaModel } from '../media/media.entity';
import { UserModel, UserModelType } from './user.entity';

@Injectable()
@EntityRepository(UserModel)
export class UserRepository extends MySqlRepository<UserModel> {
  protected DefaultAlias: string = 'user';

  protected ApplyConditionOnQueryBuilder(query: SelectQueryBuilder<UserModel>, params) {
    let Params: any = {
      Id: params.Id,
      FullName: params.FullName,
      UserName: params.UserName,
      BirthDate: params.BirthDate,
      Email: params.Email,
      PhoneNumber: params.PhoneNumber,
      Type: params.Type,
      Status: params.Status,
      MediaId: params.MediaId,
    };

    if (params.IsVerified) {
      Params['IsVerified'] = true;
    }

    if (params.AvailableForInvitation) {
      Params['AvailableForInvitation'] = true;
    }

    super.ApplyConditionOnQueryBuilder(query, Params);

    let Key = "Q_Like";
    let Val = params[Key];
    if (Val !== undefined) {
      query.andWhere(
        new Brackets(qb => {
          qb.orWhere(`user.email LIKE '%${Val}%'`);
          qb.orWhere(`user.full_name LIKE '%${Val}%'`);
          qb.orWhere("user.user_name LIKE :order_number", { order_number: Val + "%" });
          qb.orWhere(`user.phone_number LIKE '%${Val}%'`)
        })
      );
    }


    return query
  }

  private _getFindQueryBuilder(withMedia?: boolean) {
    let QueryBuilder: SelectQueryBuilder<UserModel> = this.createQueryBuilder(this.DefaultAlias)
    if (withMedia) {
      QueryBuilder.leftJoinAndMapOne("user.Media", MediaModel, "media", "user.media_id = media.id")
      QueryBuilder.leftJoinAndMapOne("user.SelfieMedia", MediaModel, "selfie", "user.media_id = selfie.id")
    }
    return QueryBuilder;
  }

  async Find(
    whereParams,
    paginationOptions?: PaginationDBParams,
    orderByOptions?: OrderByRequestParams,
    currentUserId?: number,
    populateInvitations = false,
  ) {
    let QueryBuilder = this._getFindQueryBuilder(true);
    QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
    if (currentUserId) {
      QueryBuilder.andWhere(`${this.DefaultAlias}.Id <> :currentUserId`, { currentUserId })
      if (populateInvitations) {
        QueryBuilder.innerJoinAndSelect(`${this.DefaultAlias}.Invitations`, 'Invitation')
      }
    }
    QueryBuilder = this.ApplyPaginationOnQueryBuilder(QueryBuilder, paginationOptions);
    if (orderByOptions) {
      QueryBuilder = this.ApplyOrderOnQueryBuilder(QueryBuilder, orderByOptions);
    }

    return await QueryBuilder.getMany();
  }

  async Count(whereParams, currentUserId?: number): Promise<number> {
    let QueryBuilder = this._getFindQueryBuilder();
    QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
    if (currentUserId) {
      QueryBuilder.andWhere(`${this.DefaultAlias}.Id <> :currentUserId`, { currentUserId })
    }
    return await QueryBuilder.getCount();
  }

  public getUserById(Id: number): Promise<UserModel> {
    let QueryBuilder: SelectQueryBuilder<UserModel> = this.createQueryBuilder(this.DefaultAlias)
      .leftJoinAndMapOne("user.Media", MediaModel, "media", "user.media_id = media.id")
      .where('user.Id = :Id', { Id })

    return QueryBuilder.getOne();
  }

  public getUserByEmailAndTypes(Email: string, Types: number[]): Promise<UserModel> {
    let QueryBuilder: SelectQueryBuilder<UserModel> = this.createQueryBuilder(this.DefaultAlias)
      .leftJoinAndMapOne("user.Media", MediaModel, "media", "user.media_id = media.id")
      .leftJoinAndMapOne("user.SelfieMedia", MediaModel, "selfie", "user.media_id = selfie.id")
      .andWhere(`${this.DefaultAlias}.DeletedAt = 0`)
      .andWhere(`${this.DefaultAlias}.Email = :Email`, { Email })
      .andWhere(`${this.DefaultAlias}.Type in (:...types)`, { types: Types })

    return QueryBuilder.getOne();
  }

  public async clearDatabase(): Promise<any> {
    await this.query('update `user` set deleted_at =now() where type=1');
    await this.query('update `invitation` set deleted_at =now()');
    await this.query('update `chat` set deleted_at =now()');
    await this.query('update `notification` set deleted_at =now()');
    await this.query('update `reviews` set deleted_at =now()');
    await this.query('update `media` set deleted_at =now()');
    return true;
  }

  public async updateExperts(Id): Promise<any> {
    await this.query('update `user` set type=? where type=? and id<>?', [UserModelType.NormalUser, UserModelType.DiamondUser, Id]);
    return true;
  }


}
