import { Module, forwardRef } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PassportModule } from "@nestjs/passport";
import { UserService } from "./user.service";
import { UserController } from "./user.controller";
import { UserModel } from "./user.entity";
import { UserRepository } from "./user.repository";
import { AuthService } from "./auth.service";
import { UserDeviceModel } from "./user_device/user_device.entity";
import { UserTokenModel } from "./user_token/user_token.entity";
import { UserDeviceRepository } from "./user_device/user_device.repository";
import { UserTokenRepository } from "./user_token/user_token.repository";
import { RedisModule } from "src/redis/redis.module";
import { ReviewModule } from '../review/review.module';
import { InvitationModule } from '../invitation/invitation.module';

@Module({
  imports: [
    RedisModule,
    TypeOrmModule.forFeature([
      UserModel,
      UserRepository,
      UserDeviceModel,
      UserTokenModel,
      UserDeviceRepository,
      UserTokenRepository,
    ]),
    InvitationModule,
    forwardRef(() => ReviewModule),
  ],
  exports: [AuthService, UserService],
  providers: [UserService, AuthService],
  controllers: [UserController],
})
export class UserModule { }
