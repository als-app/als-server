import { BaseModel } from 'src/model/base.entity';
import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from 'typeorm';
import { Exclude } from 'class-transformer';
import { MySQLBaseModel } from 'src/model/mysql.entity.';
import { Location } from '../../common/types';

@Entity('user')
export class UserModel extends MySQLBaseModel {
  @Column({
    name: 'full_name',
    type: 'varchar',
  })
  FullName: string;

  @Column({
    name: 'user_name',
    type: 'varchar',
  })
  UserName: string;


  @Column({
    name: 'birth_date',
    type: 'varchar',
  })
  BirthDate: string;


  @Column({
    name: 'password',
    type: 'bigint',
  })
  @Exclude()
  Password: string;

  @Column({
    name: 'details',
    type: 'varchar',
  })
  Details: string;

  @Column({
    name: 'email',
    type: 'varchar',
  })
  Email: string;

  @Column({
    name: 'phone_number',
    type: 'varchar',
  })
  PhoneNumber: string;

  @Column({
    name: 'type',
    type: 'tinyint',
  })
  Type: number;

  @Column({
    name: 'status',
    type: 'tinyint',
  })
  Status: number;

  @Column({
    name: 'media_id',
    type: 'bigint',
  })
  MediaId: number;

  @Column({
    name: 'selfie_media_id',
    type: 'bigint',
  })
  SelfieMediaId: number;

  @Column({
    name: 'about',
    type: 'varchar',
    nullable: true,
  })
  About: string;

  @Column({
    name: 'is_verified',
    type: 'boolean',
    default: false,
  })
  IsVerified: boolean;

  @Column({
    name: 'available_for_invitation',
    type: 'boolean',
    default: false,
  })
  AvailableForInvitation: boolean;

  @Column('point', {
    nullable: true,
    name: 'location',
  })
  public Location: string;

  public getLocation(): Location {
    if (
      this.Location
    ) {
      const str = 'POINT('
      if (this.Location.indexOf('POINT(') === 0) {
        const spaceIndex = this.Location.indexOf(' ');
        const Lat = this.Location.substring(str.length, spaceIndex);
        const Lng = this.Location.substring(spaceIndex + 1, this.Location.length - 1);
        if (Lat && Lng) {
          return {
            Lat: Number(Lat),
            Lng: Number(Lng),
          }
        }
      }

    }
    return null;
  }

}

export enum UserModelType {
  NormalUser = 1,
  DiamondUser = 3,
  Admin = 2
}

export enum UserModelStatus {
  Unverified = 0,
  Verified = 1,
}
