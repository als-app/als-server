import {
  Injectable,
  CanActivate,
  ExecutionContext,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AUTHORIZATION_HEADER_KEY, ROLES, OPTIONAL_AUTHORIZATION_HEADER_KEY } from 'src/constant/authentication';
import { UnAuthorizedException } from 'src/exception/response.exception';
import { MessageTemplates } from 'src/locale/locale';

import { Auth, AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private _reflector: Reflector,
    private _authService: AuthService,
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const requiredAuthorization = this._reflector.get<string[]>(
      AUTHORIZATION_HEADER_KEY,
      context.getHandler(),
    );
    const optionalAuthorization = this._reflector.get<string[]>(
      OPTIONAL_AUTHORIZATION_HEADER_KEY,
      context.getHandler(),
    );

    const roles = this._reflector.get<number[]>(ROLES, context.getHandler());
    if (requiredAuthorization || optionalAuthorization) {
      const token = request.headers[AUTHORIZATION_HEADER_KEY];
      if (!token && optionalAuthorization) {
        return true;
      }
      if (!token) {
        throw new UnAuthorizedException(MessageTemplates.UnauthorizedError);
      }

      let auth: Auth = await this._authService.GetSession(token);

      if (
        !auth ||
        (auth && !auth.User) ||
        (roles.length && !roles.includes(auth.User.Type))
      ) {
        throw new UnAuthorizedException(MessageTemplates.UnauthorizedError);
      }

      request.user = auth.User;
    }
    return true;
  }
}
