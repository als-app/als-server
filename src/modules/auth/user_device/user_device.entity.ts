import { BaseModel } from 'src/model/base.entity';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { MySQLBaseModel } from 'src/model/mysql.entity.';

@Entity('user_device')
export class UserDeviceModel extends MySQLBaseModel {
  @Column({
    name: 'user_id',
    type: 'bigint',
  })
  UserId: number;

  @Column({
    name: 'user_agent',
    type: 'varchar',
  })
  UserAgent: string;

  @Column({
    name: 'uuid',
    type: 'varchar',
  })
  UUID: string;

  @Column({
    name: 'auth_token',
    type: 'varchar',
  })
  AuthToken: string;

  @Column({
    name: 'fcm',
    type: 'varchar',
  })
  FCMToken: string;
}
