import { Injectable } from '@nestjs/common';
import { BaseRepository } from 'src/repository/base.repository';
import { MySqlRepository } from 'src/repository/mysql.repository';
import { EntityRepository, Repository } from 'typeorm';
import { UserDeviceModel } from './user_device.entity';

@Injectable()
@EntityRepository(UserDeviceModel)
export class UserDeviceRepository extends MySqlRepository<UserDeviceModel> {
  protected DefaultAlias: string = 'user_device';
}
