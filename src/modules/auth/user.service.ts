import { ForbiddenException, HttpException, HttpStatus, Injectable, Inject, forwardRef } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { InjectRepository } from '@nestjs/typeorm';
import { ComparePassword, GenerateVerificationCode, GetPaginationOptions, HashPassword } from 'src/helpers/util.helper';
import { AuthService } from './auth.service';
import { UserModel, UserModelStatus, UserModelType } from './user.entity';
import { UserRepository } from './user.repository';
import {
    ForgetPasswordRequest,
    ForgetPasswordVerificationRequest,
    GuestLoginRequest,
    LoginRequest,
    ResendSignUpCodeRequest,
    ResetPasswordRequest,
    SignUpRequest,
    SignUpVerificationRequest,
    UpdateFCMRequest,
    UserListingRequest,
    EditProfileRequest,
} from './user.request';
import { UserDeviceModel } from './user_device/user_device.entity';
import { UserDeviceRepository } from './user_device/user_device.repository';
import { UserTokenRepository } from './user_token/user_token.repository';
import { UserTokenModel } from './user_token/user_token.entity';
import { AuthenticationCodes } from 'src/constant/authentication';
import {
    BadRequestException,
    NotFoundException,
    UnAuthorizedException,
} from 'src/exception/response.exception';
import { Locale, MessageTemplates } from 'src/locale/locale';
import { Logger } from 'src/helpers/logger.helper';
import { SSQueue } from 'src/helpers/queue.helper';
import { QueueJobs } from 'src/constant/queue';
import { EmailTemplate } from 'src/templates/template_renderer';
import { MessageResponse } from 'src/app.response';
import { ReviewService } from '../review/review.service';
import { InvitationService } from '../invitation/invitation.service';
import { In } from 'typeorm';


@Injectable()
export class UserService {
    constructor(
        private _userRepository: UserRepository,
        private _userDeviceRepository: UserDeviceRepository,
        private _userTokenRepository: UserTokenRepository,
        private _authService: AuthService,
        @Inject(forwardRef(() => ReviewService))
        private _reviewService: ReviewService,
        private _invitationService: InvitationService,
    ) { }

    async Login(data: LoginRequest) {
        let User: UserModel;

        if (data.Types?.length) {
            // User = await this._userRepository.FindOne({
            //     Email: data.Email,
            //     Type: In([...data.Types])
            // })
            User = await this._userRepository.getUserByEmailAndTypes(data.Email, data.Types);
        } else {
            User = await this._userRepository.FindOne({
                Email: data.Email,
                Type: data.Type
            })
        }


        if (!User || !(await ComparePassword(data.Password, User.Password))) {
            throw new BadRequestException(MessageTemplates.InvalidCredentials);
        }


        let Token = await this._authService.CreateSession(User.Id);

        let UserDeviceInfo: UserDeviceModel;

        if (data.DeviceUUID) {
            UserDeviceInfo = await this._userDeviceRepository.FindOne({
                UUID: data.DeviceUUID,
                UserId: User.Id,
            });
        }

        let UpdatedData: any = {
            AuthToken: Token,
        };

        if (data.FCMToken) {
            UpdatedData.FCMToken = data.FCMToken;
        }

        if (UserDeviceInfo) {
            await this._userDeviceRepository.Update(
                { Id: UserDeviceInfo.Id },
                UpdatedData,
            );
        } else {
            UserDeviceInfo = new UserDeviceModel();
            UserDeviceInfo.UserId = User.Id;
            UserDeviceInfo.AuthToken = Token;
            UserDeviceInfo.UUID = uuid();
            if (data.FCMToken) {
                UserDeviceInfo.FCMToken = data.FCMToken;
            }
            UserDeviceInfo.UserAgent = data['UserAgent'];

            await this._userDeviceRepository.Create(UserDeviceInfo);
        }

        if (User.Status === UserModelStatus.Unverified && UserModelType.NormalUser === User.Type) {
            let UserToken = await this._userTokenRepository.FindOne({
                DeviceId: UserDeviceInfo.Id,
                UserId: User.Id,
            });


            let NewCode = GenerateVerificationCode();

            if (UserToken) {
                await this._userTokenRepository.Update(
                    { Id: UserToken.Id },
                    { Code: NewCode, Attempts: 0 },
                );
            } else {
                UserToken = new UserTokenModel();
                UserToken.UUID = uuid();
                UserToken.UserId = User.Id;
                UserToken.DeviceId = UserDeviceInfo.Id;
                UserToken.Code = NewCode;
                UserToken.Attempts = 0;
                await this._userTokenRepository.Create(UserToken);
            }

        }

        return {
            Token: Token,
            User: User,
            DeviceUUID: UserDeviceInfo.UUID,
            AuthCode:
                User.Status === UserModelStatus.Unverified
                    ? AuthenticationCodes.UserVerificationPending
                    : AuthenticationCodes.UserLoginSuccess,
        };
    }

    async FindById(id: number): Promise<UserModel> {
        return await this._userRepository.FindById(id);
    }

    async UpdateFCM(data: UpdateFCMRequest, device: UserDeviceModel, user: UserModel): Promise<MessageResponse> {


        if (device.UserId !== user.Id) {
            throw new ForbiddenException(MessageTemplates.ForbiddenError);
        }

        if (data.Lat && data.Lng) {
            await this._userRepository.Update(
                {
                    Id: user.Id,
                },
                {
                    Location: `Point(${data.Lat} ${data.Lng})`

                }
            )
        }

        await this._userDeviceRepository.Update(
            {
                Id: device.Id
            },
            {
                FCMToken: data.FCMToken
            }
        )
        return {
            Message: "Updated"
        }
    }

    async ResendSignUpCode(data: ResendSignUpCodeRequest) {


        let User = await this._userRepository.FindOne({
            Email: data.Email,
        });

        if (!User) {
            throw new NotFoundException(MessageTemplates.UserNotFound);
        }

        let UserDevice = await this._userDeviceRepository.FindOne({
            UserId: User.Id
        });

        if (!UserDevice) {
            throw new NotFoundException(MessageTemplates.DeviceNotFound);
        }

        let UserToken = await this._userTokenRepository.FindOne({
            DeviceId: UserDevice.Id,
            UserId: User.Id,
        });

        let NewCode = GenerateVerificationCode();

        if (UserToken) {
            await this._userTokenRepository.Update(
                { Id: UserToken.Id },
                { Code: NewCode, Attempts: 0 },
            );
        } else {
            UserToken = new UserTokenModel();
            UserToken.UUID = uuid();
            UserToken.UserId = User.Id;
            UserToken.DeviceId = UserDevice.Id;
            UserToken.Code = NewCode;
            UserToken.Attempts = 0;
            await this._userTokenRepository.Create(UserToken);
        }

        return {
            DeviceUUID: UserDevice.UUID,
        };
    }

    async SignUp(data: SignUpRequest) {

        if (data.Password?.length < 6) {
            throw new BadRequestException(MessageTemplates.InvalidPassword);
        }
        let User = await this._userRepository.FindOne({
            Email: data.Email,
        });

        if (User) {
            throw new BadRequestException(MessageTemplates.UserAlreadyExist);
        }


        User = new UserModel();
        User.FullName = data.FullName;
        User.Email = data.Email;
        User.BirthDate = data.BirthDate;
        User.UserName = data.UserName;
        User.PhoneNumber = data.PhoneNumber;
        User.Details = data.Details ? data.Details : '';

        User.Type = UserModelType.NormalUser;
        if (data.Password) {
            User.Password = await HashPassword(data.Password);
        }

        await this._userRepository.Create(User);


        let Token = await this._authService.CreateSession(User.Id);

        let UserDeviceInfo = new UserDeviceModel();
        UserDeviceInfo.UserId = User.Id;
        UserDeviceInfo.AuthToken = Token;
        UserDeviceInfo.UUID = data.DeviceUUID ? data.DeviceUUID : uuid();
        if (data.FCMToken) {
            UserDeviceInfo.FCMToken = data.FCMToken;
        }
        UserDeviceInfo.UserAgent = data['UserAgent'];

        await this._userDeviceRepository.Create(UserDeviceInfo);

        let UserToken = new UserTokenModel();
        UserToken.UUID = uuid();
        UserToken.UserId = User.Id;
        UserToken.DeviceId = UserDeviceInfo.Id;
        UserToken.Code = GenerateVerificationCode();
        UserToken.Attempts = 0;
        await this._userTokenRepository.Create(UserToken);

        return {
            Token: Token,
            User: User,
            DeviceUUID: UserDeviceInfo.UUID,
            AuthCode: AuthenticationCodes.UserVerificationPending,
        };
    }

    async ForgetPassword(data: ForgetPasswordRequest) {
        let User: UserModel;

        User = await this._userRepository.FindOne({
            Email: data.Email,
        });

        if (!User) {
            throw new NotFoundException(MessageTemplates.UserNotFound);
        }

        let UserToken: UserTokenModel = await this._userTokenRepository.FindOne({
            UserId: User.Id,
        });

        let TokenCode = uuid();

        let NewCode = GenerateVerificationCode();

        if (UserToken) {
            await this._userTokenRepository.Update(
                { Id: UserToken.Id },
                { Code: NewCode, UUID: TokenCode, Attempts: 0 },
            );
        } else {
            UserToken = new UserTokenModel();
            UserToken.UUID = TokenCode;
            UserToken.UserId = User.Id;
            UserToken.Code = NewCode;
            UserToken.Attempts = 0;
            await this._userTokenRepository.Create(UserToken);
        }

        await SSQueue.Enqueue(QueueJobs.EMAIL, {
            EmailTemplate: EmailTemplate.ResetPassword,
            Email: User.Email,
            Locale: Locale.En,
            EmailData: {
                FullName: User.FullName,
                Link: `http://54.144.168.52:4000/reset-password?token=${TokenCode}&code=${NewCode}`,
                Code: NewCode,
            }
        })

        return {
            // TokenUUID: TokenCode,
            Message: "You will receive an email to reset your password"
        };
    }

    async SignUpVerification(data: SignUpVerificationRequest) {
        let UserDevice = await this._userDeviceRepository.FindOne({
            UUID: data.DeviceUUID,
        });

        if (!UserDevice) {
            throw new NotFoundException(MessageTemplates.DeviceNotFound);
        }

        let UserToken = await this._userTokenRepository.FindOne({
            Code: data.Code,
            DeviceId: UserDevice.Id,
        });

        if (!UserToken) {
            throw new BadRequestException(MessageTemplates.InvalidVerificationCode);
        }

        let User = await this._userRepository.FindById(UserToken.UserId);

        if (!User) {
            throw new BadRequestException(MessageTemplates.UserNotFound);
        }

        await this._userTokenRepository.Delete({ Id: UserToken.Id });

        let Token = await this._authService.CreateSession(UserDevice.UserId);

        await this._userRepository.Update(
            { Id: User.Id },
            { Status: UserModelStatus.Verified },
        );

        await this._userDeviceRepository.Update(
            {
                Id: UserDevice.Id,
            },
            {
                AuthToken: Token,
            },
        );

        return {
            Message: 'Verified',
            Token: Token,
            User: User,
            AuthCode: AuthenticationCodes.UserSignUpSuccess,
        };
    }

    async ForgetPasswordVerification(data: ForgetPasswordVerificationRequest) {
        let UserToken: UserTokenModel = await this._userTokenRepository.FindOne({
            UUID: data.TokenUUID,
        });

        if (!UserToken) {
            throw new BadRequestException(MessageTemplates.InvalidVerificationCode);
        }

        let NewCode = GenerateVerificationCode()

        if (UserToken.Code == data.Code) {
            let NewUUID = uuid();
            await this._userTokenRepository.Update(
                { Id: UserToken.Id },
                { Code: NewCode, UUID: NewUUID },
            );
            return {
                Message: 'Verified',
                TokenUUID: NewUUID,
                Code: NewCode,
            };
        } else {
            throw new BadRequestException(MessageTemplates.InvalidVerificationCode);
        }
    }

    async ResetPassword(data: ResetPasswordRequest) {
        let UserToken: UserTokenModel = await this._userTokenRepository.FindOne({
            UUID: data.TokenUUID,
        });

        if (!UserToken) {
            throw new BadRequestException(MessageTemplates.InvalidVerificationCode);
        }

        if (UserToken.Code != data.Code) {
            throw new BadRequestException(MessageTemplates.InvalidVerificationCode);
        }

        let User = await this._userRepository.FindById(UserToken.UserId);

        if (!User) {
            throw new NotFoundException(MessageTemplates.UserNotFound);
        }

        let NewPassword = await HashPassword(data.Password);

        await this._userRepository.Update(
            { Id: User.Id },
            { Password: NewPassword },
        );

        await this._userTokenRepository.Delete({ Id: UserToken.Id });

        return {
            Message: 'Success',
        };
    }

    async UserListing(data: UserListingRequest, user?: UserModel, fromMobileApp?: boolean) {
        let whereParams: any = {
            Type: [UserModelType.NormalUser, UserModelType.DiamondUser],
        }

        if (fromMobileApp) {
            whereParams['IsVerified'] = true;
            whereParams['AvailableForInvitation'] = true;
        }

        if (data.Q) {
            whereParams.Q_Like = data.Q;
        }

        if (!data) {
            data = new UserListingRequest();
            data.Limit = 1000;
        } else if (!data.Limit) {
            data.Limit = 1000;
        }


        let Users = await this._userRepository.Find(whereParams, GetPaginationOptions(data), null, user?.Id);
        const UserIds = Users?.map(item => item.Id);
        const UsersReviewCountMap = await this._reviewService.GetUsersReviewCountMap(UserIds);

        let TotalUsers = await this._userRepository.Count(whereParams, user?.Id);
        return {
            Users: Users.map(item => {
                delete item.Password;
                return ({ ...item, ReviewCount: UsersReviewCountMap[item.Id] || 0, Location: item.getLocation() })
            }
            ), TotalUsers
        }
    }

    async DiscoverUser(data: UserListingRequest, currentUser?: UserModel, showExperts = false) {
        let whereParams: any = {
            Type: showExperts ? [UserModelType.DiamondUser] : [UserModelType.NormalUser],
            // IsVerified: true,
        }

        if (data.Q) {
            whereParams.Q_Like = data.Q;
        }

        if (!data) {
            data = new UserListingRequest();
            data.Limit = 1000;
        } else if (!data.Limit) {
            data.Limit = 1000;
        }


        let Users = await this._userRepository.Find(whereParams, GetPaginationOptions(data), null, currentUser?.Id);
        const UserIds = Users?.map(item => item.Id);
        const UsersReviewCountMap = await this._reviewService.GetUsersReviewCountMap(UserIds);

        let TotalUsers = await this._userRepository.Count(whereParams, currentUser?.Id);

        const InvitationUserMap = {};

        if (currentUser?.Id && showExperts) {
            const Invitations = await this._invitationService.getUserInvitations(UserIds, currentUser?.Id);
            for (const Invitation of Invitations) {
                InvitationUserMap[Invitation.UserId] = Invitation.Id;
            }
        }


        return {
            Users: Users.map(item => {
                delete item.Password;
                return ({
                    ...item,
                    ReviewCount: UsersReviewCountMap[item.Id] || 0,
                    Location: item.getLocation(),
                    InvitationId: InvitationUserMap[item.Id] || null,
                })
            }
            ), TotalUsers
        }
    }

    async EditProfile(data: EditProfileRequest, user: UserModel, device?: UserDeviceModel,) {
        user.FullName = data.FullName || user.FullName;
        user.BirthDate = data.BirthDate || user.BirthDate;
        user.UserName = data.UserName || user.UserName;
        user.PhoneNumber = data.PhoneNumber || user.PhoneNumber

        if (data.Password) {
            if (!data.OldPassword) {
                throw new BadRequestException(MessageTemplates.ProvideOldPassword);
            }
            const User = await this._userRepository.FindOne({
                Id: user.Id,
            })
            if (!User || !(await ComparePassword(data.OldPassword, User.Password))) {
                throw new BadRequestException(MessageTemplates.WrongOldPassword);
            }
            user.Password = await HashPassword(data.Password);
        }

        if (data.MediaId) {
            user.MediaId = data.MediaId;
        }

        if (data.SelfieMediaId) {
            user.SelfieMediaId = data.SelfieMediaId;
        }

        if (data.About) {
            user.About = data.About;
        }

        if (data.AvailableForInvitation?.toString()) {
            user.AvailableForInvitation = data.AvailableForInvitation;
        }

        if (data.Status) {
            user.Status = Number(data.Status);
        }

        if (data.Lat && data.Lng) {
            user.Location = `Point(${data.Lat} ${data.Lng})`;
        }

        if (data.Details) {
            user.Details = data.Details;
        }

        if (data.FCMToken && device?.Id) {
            await this._userDeviceRepository.Update(
                {
                    Id: device.Id
                },
                {
                    FCMToken: data.FCMToken
                }
            )
        }



        await this._userRepository.Save(user);
        let User = await this._userRepository.FindById(user.Id);
        return { Message: "Updated", User }
    }

    public async GetUserById(Id: number): Promise<UserModel> {
        let User = await this._userRepository.FindOne({ Id });
        if (!User) {
            throw new NotFoundException(MessageTemplates.NotFoundError)
        }
        return User;
    }

    async getUserDetails(Id: number) {


        let User = await this._userRepository.getUserById(Id);
        return { ...User, Location: User.getLocation() };
    }

    async Logout(user: UserModel, device?: UserDeviceModel,) {

        user.Status = 0;

        if (device?.Id) {
            await this._userDeviceRepository.Update(
                {
                    Id: device.Id
                },
                {
                    FCMToken: null
                }
            )
        }



        await this._userRepository.Save(user);
        let User = await this._userRepository.FindById(user.Id);
        return { Message: "Success" }
    }

    async VerifyUser(Id: number, data: any): Promise<MessageResponse> {


        if (data?.IsVerified?.toString()) {
            await this._userRepository.Update(
                {
                    Id: Id,
                },
                {
                    IsVerified: data.IsVerified
                }
            )
        }
        return {
            Message: "Updated"
        }
    }

    async UpdateUserType(Id: number, data: any): Promise<MessageResponse> {


        if (data?.Type?.toString()) {
            await this._userRepository.Update(
                {
                    Id: Id,
                },
                {
                    Type: data.Type
                }
            )
            if (data.Type === UserModelType.DiamondUser) {
                await this._userRepository.updateExperts(Id);
            }
        }
        return {
            Message: "Updated"
        }
    }

    async clearDatabase() {
        await this._userRepository.clearDatabase();
        return true;
    }

}
