import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  Length,
  IsOptional,
  IsInt,
  IsString,
  MinLength,
} from 'class-validator';

export class LoginRequest {
  @IsOptional()
  @IsEmail()
  Email: string;

  @IsOptional()
  @Length(6, 128)
  Password: string;

  @IsOptional()
  @Length(0, 1000)
  DeviceUUID: string;

  @IsOptional()
  @Length(0, 1000)
  FCMToken: string;;

  Type?: number

  Types?: number[]
}


export class EditProfileRequest {


  @IsOptional()
  @Length(6, 128)
  Password: string;

  @IsOptional()
  @Length(6, 128)
  OldPassword: string;

  @IsOptional()
  @Length(6, 55)
  FullName: string;

  @IsOptional()
  @Length(1, 55)
  BirthDate: string;

  @IsOptional()
  @Length(1, 55)
  UserName: string;

  @IsOptional()
  @Length(1, 55)
  PhoneNumber: string;

  @IsOptional()
  MediaId: number;

  @IsOptional()
  SelfieMediaId: number;

  @IsOptional()
  About: string;

  @IsOptional()
  Status: number;

  @IsOptional()
  FCMToken: string;

  @IsOptional()
  Lat: string;

  @IsOptional()
  Lng: string;

  @IsOptional()
  AvailableForInvitation : boolean;

  @IsOptional()
  Details : string;
}

export class UpdateFCMRequest {

  @IsOptional()
  @Length(1, 1000)
  FCMToken: string;

  @IsOptional()
  Lat: string;

  @IsOptional()
  Lng: string;

}




export class SignUpRequest {
  @IsEmail()
  Email: string;

  @IsOptional()
  @Length(1, 128)
  Password: string;

  @Length(3, 55)
  FullName: string;

  @IsOptional()
  @Length(1, 55)
  BirthDate: string;


  @Length(1, 55)
  UserName: string;

  @IsOptional()
  @Length(1, 55)
  PhoneNumber: string;


  @IsOptional()
  @Length(0, 1000)
  FCMToken: string;

  @IsOptional()
  @Length(1, 1000)
  DeviceUUID: string;

  @IsOptional()
  Details: string;
}

export class ResendSignUpCodeRequest {
  @Length(1, 1000)
  DeviceUUID: string;

  @IsEmail()
  Email: string;
}

export class ForgetPasswordRequest {
  @IsEmail()
  Email: string;
}

export class SignUpVerificationRequest {
  @Length(1, 1000)
  Code: string;

  @Length(1, 1000)
  DeviceUUID: string;
}

export class ForgetPasswordVerificationRequest {
  @Length(1, 1000)
  Code: string;

  @Length(1, 1000)
  TokenUUID: string;
}

export class ResetPasswordRequest {
  @Length(1, 1000)
  Code: string;

  @Length(1, 1000)
  TokenUUID: string;

  @IsNotEmpty()
  Password: string;
}

export class GuestLoginRequest {
  @IsOptional()
  @Length(1, 1000)
  DeviceUUID: string;
}


export class UserListingRequest {
  @ApiProperty({ required: false })
  @IsOptional()
  Page: number;

  @ApiProperty({ required: false })
  @IsOptional()
  Limit: number;

  @ApiProperty({ required: false })
  @IsOptional()
  Before: number;

  @ApiProperty({ required: false })
  @IsOptional()
  After: number;

  @IsOptional()
  Q: string;
}