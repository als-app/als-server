import { Injectable } from '@nestjs/common';
import { MySqlRepository } from 'src/repository/mysql.repository';
import { EntityRepository, Repository } from 'typeorm';
import { UserTokenModel } from './user_token.entity';

@Injectable()
@EntityRepository(UserTokenModel)
export class UserTokenRepository extends MySqlRepository<UserTokenModel> {
  protected DefaultAlias: string = 'user_token';
}
