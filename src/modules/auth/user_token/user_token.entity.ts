import { BaseModel } from 'src/model/base.entity';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { MySQLBaseModel } from 'src/model/mysql.entity.';

@Entity('user_token')
export class UserTokenModel extends MySQLBaseModel {
  @Column({
    name: 'user_id',
    type: 'bigint',
  })
  UserId: number;

  @Column({
    name: 'device_id',
    type: 'bigint',
  })
  DeviceId: number;

  @Column({
    name: 'code',
    type: 'varchar',
  })
  Code: string;

  @Column({
    name: 'uuid',
    type: 'varchar',
  })
  UUID: string;

  @Column({
    name: 'attempts',
    type: 'bigint',
  })
  Attempts: number;

  @Column({
    name: 'expire_time',
    type: 'bigint',
  })
  ExpireTime: number;

  @Column({
    name: 'meta',
    type: 'json',
  })
  Meta: object;
}
