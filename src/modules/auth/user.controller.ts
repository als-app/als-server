import {
  Controller,
  Request,
  Delete,
  Query,
  Req,
  UseGuards,
  UseFilters,
  Body,
  HttpStatus,
  Param,
  Headers
} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { Authorized } from 'src/decorator/authorize.decorator';
import { OptionalAuthorized } from 'src/decorator/optionalAuthorize.decorator';
import { CurrentUser } from 'src/decorator/current_user.decorator';
import { AppController } from 'src/decorator/appcontroller.decorator';
import { UserModel, UserModelType } from './user.entity';
import {
  EditProfileRequest,
  ForgetPasswordRequest,
  ForgetPasswordVerificationRequest,
  GuestLoginRequest,
  LoginRequest,
  ResendSignUpCodeRequest,
  ResetPasswordRequest,
  SignUpRequest,
  SignUpVerificationRequest,
  UpdateFCMRequest,
  UserListingRequest,
} from './user.request';
import {
  ForgetPasswordResponse,
  ForgetPasswordVerificationResponse,
  GetUserResponse,
  GuestLoginResponse,
  LoginResponse,
  ResendSignUpCodeResponse,
  ResetPasswordResponse,
  SignUpResponse,
  SignUpVerificationResponse,
  UserResponseModel,
} from './user.response';
import { UserService } from './user.service';
import { Post } from 'src/decorator/nest-ext/post.decorator';
import { Get } from 'src/decorator/nest-ext/get.decorator';
import { Put } from 'src/decorator/nest-ext/put.decorator';
import { MessageResponse } from 'src/app.response';
import { UserDeviceModel } from './user_device/user_device.entity';
import { CurrentDevice } from 'src/decorator/current_device.decorator';

@AppController("user")
export class UserController {
  constructor(private readonly _userService: UserService) { }

  @Post('/login', LoginResponse)
  async Login(@Body() data: LoginRequest) {
    data["Types"] = [UserModelType.NormalUser, UserModelType.DiamondUser]
    return await this._userService.Login(data);
  }

  @Authorized()
  @Post('/logout', {})
  async Logout(@CurrentUser() user: UserModel, @CurrentDevice() device: UserDeviceModel,) {
    return await this._userService.Logout(user, device);
  }

  @Post('/admin/login', LoginResponse)
  async AdminLogin(@Body() data: LoginRequest) {
    data["Type"] = UserModelType.Admin
    return await this._userService.Login(data);
  }


  @Post('/signup', SignUpResponse)
  async SignUp(@Body() data: SignUpRequest) {
    return await this._userService.SignUp(data);
  }



  @Post('/signup/resend/code', ResendSignUpCodeResponse)
  async ResendSignUpCode(@Body() data: ResendSignUpCodeRequest) {
    return await this._userService.ResendSignUpCode(data);
  }

  @Post('/reset-password', ResetPasswordResponse)
  async ResetPassword(@Body() data: ResetPasswordRequest) {
    return await this._userService.ResetPassword(data);
  }

  @Post('/forget-password', ForgetPasswordResponse)
  async ForgetPassword(@Body() data: ForgetPasswordRequest) {
    return await this._userService.ForgetPassword(data);
  }

  @Post('/forget-password-verification', ForgetPasswordVerificationResponse)
  async ForgetPasswordVerification(
    @Body() data: ForgetPasswordVerificationRequest,
  ) {
    return await this._userService.ForgetPasswordVerification(data);
  }

  @Post('/signup-verification', SignUpVerificationResponse)
  async SignUpVerification(@Body() data: SignUpVerificationRequest) {
    return await this._userService.SignUpVerification(data);
  }


  @Get('/admin/list', GetUserResponse)
  async UserListing(@Query() data: UserListingRequest, @CurrentUser() user: UserModel) {
    return await this._userService.UserListing(data);
  }

  @Put('/admin/verify/:Id', {})
  async verifyUser(@Param('Id') Id: string, @Body() data: any) {
    console.log('data : ', data, Id);
    return await this._userService.VerifyUser(Number(Id), data);
  }

  @Put('/admin/type/:Id', {})
  async UpdateUserType(@Param('Id') Id: string, @Body() data: any) {
    return await this._userService.UpdateUserType(Number(Id), data);
  }

  @Authorized()
  @Get('/me', GetUserResponse)
  async CurrentUser(
    @CurrentUser() user: UserModel,
    @Query() data: UpdateFCMRequest,
    @CurrentDevice() device: UserDeviceModel,) {
    await this._userService.UpdateFCM(data, device, user)
    if (user.Password) {
      delete user.Password;
    }
    return {
      User: { ...user, Location: user.getLocation() },
    };
  }


  @Authorized()
  @Put('/', SignUpResponse)
  async EditProfile(@Body() data: EditProfileRequest, @CurrentUser() user: UserModel, @CurrentDevice() device: UserDeviceModel,) {
    return await this._userService.EditProfile(data, user, device);
  }

  @Authorized()
  @Put('/fcm', MessageResponse)
  async UpdateFCM(@Body() data: UpdateFCMRequest, @CurrentDevice() device: UserDeviceModel, @CurrentUser() user: UserModel): Promise<MessageResponse> {
    return await this._userService.UpdateFCM(data, device, user)
  }

  @Authorized()
  @Get('/transaction', {})
  async GetTransactions(@CurrentUser() user: UserModel) {
    return {
      Transactions: [{
        Id: 1,
        CreatedAt: 1612206726648,
        Amount: 10,
        Title: "Tarot Reading",
      },
      {
        Id: 2,
        CreatedAt: 1612206726648,
        Amount: 10,
        Title: "Tarot Reading",
      }, {
        Id: 3,
        CreatedAt: 1612206726648,
        Amount: 10,
        Title: "Tarot Reading",
      }, {
        Id: 4,
        CreatedAt: 1612206726648,
        Amount: 10,
        Title: "Tarot Reading",
      }
        , {
        Id: 5,
        CreatedAt: 1612206726648,
        Amount: 10,
        Title: "Tarot Reading",
      }],
      TotalTransactions: 5
    };
  }

  // @Authorized()
  @OptionalAuthorized()
  @Get('/discover', GetUserResponse)
  async DiscoverUsers(@Query() data: UserListingRequest, @CurrentUser() user: UserModel) {
    return await this._userService.DiscoverUser(data, user);
  }

  // @Authorized()
  @OptionalAuthorized()
  @Get('/experts/discover', GetUserResponse)
  async DiscoverExpertUsers(@Query() data: UserListingRequest, @CurrentUser() user: UserModel) {
    return await this._userService.DiscoverUser(data, user, true);
  }

  @Authorized()
  @Get('/details/:Id', GetUserResponse)
  async getUserDetails(@Param('Id') Id: string) {
    return await this._userService.getUserDetails(Number(Id));
  }

  @Get('/clear-database', GetUserResponse)
  async clearDatabase(@Headers('auth') auth: string) {
    if (auth === '123456') {
      await this._userService.clearDatabase();
      return 'Successfully cleared database';
    }
    return 'Not Authorized';
  }

}
