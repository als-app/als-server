import { Injectable } from "@nestjs/common";
import { BaseRepository } from "src/repository/base.repository";
import { MySqlRepository } from "src/repository/mysql.repository";
import { EntityRepository, SelectQueryBuilder, } from "typeorm";
import { InvitationModel } from "./invitation.entity";
import { GetInvitationsWhere } from './invitation.request';
import { MediaModel } from '../media/media.entity';

@Injectable()
@EntityRepository(InvitationModel)
export class InvitationRepository extends MySqlRepository<InvitationModel> {
  protected DefaultAlias: string = "invitation";

  public getAll(where: GetInvitationsWhere): SelectQueryBuilder<InvitationModel> {
    const qb = this.createQueryBuilder(this.DefaultAlias);

    if (where.populateUser) {
      qb.innerJoin(`${this.DefaultAlias}.User`, 'User')
        .leftJoinAndMapOne("User.Media", MediaModel, "media", "User.media_id = media.id")
      qb.addSelect(['User.Id', 'User.FullName'])
    }

    if (where.populateCreatedBy) {
      qb.innerJoin(`${this.DefaultAlias}.CreatedBy`, 'CreatedBy')
        .leftJoinAndMapOne("CreatedBy.Media", MediaModel, "CreatedByMedia", "CreatedBy.media_id = CreatedByMedia.id")
      qb.addSelect(['CreatedBy.Id', 'CreatedBy.FullName'])
    }

    if (where.populateLastMessage) {
      qb.innerJoinAndSelect(`${this.DefaultAlias}.Chats`, 'Chats')
        .leftJoin(`${this.DefaultAlias}.Chats`, "next_chats", "Chats.CreatedAt < next_chats.CreatedAt")
        .andWhere('`next_chats`.Id is null');
    }

    if (where.CreatedById) {
      qb.andWhere(`${this.DefaultAlias}.CreatedById = :CreatedById`, { CreatedById: where.CreatedById });
    }

    if (where.UserId) {
      qb.andWhere(`${this.DefaultAlias}.UserId = :UserId`, { UserId: where.UserId });
    }

    if (where.UserIdOrCreatedById) {
      qb.andWhere(`(${this.DefaultAlias}.UserId = :UserIdOrCreatedById OR ${this.DefaultAlias}.CreatedById = :UserIdOrCreatedById) `, { UserIdOrCreatedById: where.UserIdOrCreatedById });
    }

    if (where.Id) {
      qb.andWhere(`${this.DefaultAlias}.Id = :Id`, { Id: where.Id });
    }

    if (where.Status) {
      qb.andWhere(`${this.DefaultAlias}.Status = :Status`, { Status: where.Status });
    }

    qb.orderBy(`${this.DefaultAlias}.CreatedAt`, 'DESC')

    return qb;
  }

  public getUserInvitations(UserIds: number[], CurrentUserId: number): SelectQueryBuilder<InvitationModel> {
    const qb = this.createQueryBuilder(this.DefaultAlias)
      .andWhere(`${this.DefaultAlias}.CreatedById = :CreatedById`, { CreatedById: CurrentUserId })
      .andWhere(`${this.DefaultAlias}.UserId in (:...UserIds)`, { UserIds });

    return qb;
  }


}
