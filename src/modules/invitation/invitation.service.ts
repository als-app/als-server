import { Injectable } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import {
  BadRequestException,
  NotFoundException,
} from "src/exception/response.exception";
import { IsFuture } from "src/helpers/date.helper";
import {
  GetPaginationOptions,
  OrderByRequestParams,
  PaginationRequestParams,
} from "src/helpers/util.helper";
import { MessageTemplates } from "src/locale/locale";
import { UserModel, UserModelType } from "../auth/user.entity";
import { UserRepository } from "../auth/user.repository";
import { InvitationModel, InvitationStatus } from "./invitation.entity";
import { InvitationRepository } from "./invitation.repository";
import { CreateInvitationRequest, GetInvitationsWhere } from "./invitation.request";
import { NotificationService } from '../notification/notification.service';
import { NotificationModelType } from '../notification/notification.entity';
import { ChatService } from '../chat/chat.service';

@Injectable()
export class InvitationService {
  constructor(
    private _invitationRepository: InvitationRepository,
    private _chatService: ChatService
  ) { }

  async CreateInvitation(data: CreateInvitationRequest, user: UserModel) {

    const alreadyExistInvitation = await this._invitationRepository.findOne({ UserId: data.UserId, CreatedById: user.Id });

    if (alreadyExistInvitation) {
      return alreadyExistInvitation;
    }

    let invitation = new InvitationModel()
    invitation.Title = 'Chat Invitation';
    invitation.UserId = data.UserId;
    invitation.CreatedById = user.Id;

    await this._invitationRepository.Create(invitation);

    return invitation;

  }

  async getUserInvitations(UserIds: number[], currentUserId: number) {
    const invitation = await this._invitationRepository.getUserInvitations(UserIds, currentUserId).getMany();
    return invitation;
  }

  async getInvitationDetails(Id: number) {
    const where = new GetInvitationsWhere();
    where.populateCreatedBy = true;
    where.populateUser = true;
    where.Id = Id;
    const invitation = await this._invitationRepository.getAll(where).getOne();
    return invitation;
  }

  async getInvitationMessages(user: UserModel) {
    const where = new GetInvitationsWhere();
    where.populateCreatedBy = true;
    where.populateLastMessage = true;
    where.populateUser = true;
    where.UserIdOrCreatedById = user.Id;
    const invitations = await this._invitationRepository.getAll(where).getMany();

    const InvitationIds = invitations.map(item => item.Id);

    const UnseenChatCountMap = await this._chatService.getUnseenChatCountMapByInvitationIds(InvitationIds, user.Id);
    return invitations.map(item => ({
      ...item,
      UnseenChatCount: UnseenChatCountMap[item.Id] ? UnseenChatCountMap[item.Id] : 0
    }));
  }


}
