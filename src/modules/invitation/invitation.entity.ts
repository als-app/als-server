import { BaseModel } from "src/model/base.entity";
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from "typeorm";
import { Exclude } from "class-transformer";
import { MySQLBaseModel } from "src/model/mysql.entity.";
import { Location } from 'src/common/types';
import { UserModel } from '../auth/user.entity'
import { ChatModel } from '../chat/chat.entity';


export enum InvitationStatus {
  pending = 'pending',
  accepted = 'accepted',
  rejected = 'rejected'
}


@Entity("invitation")
export class InvitationModel extends MySQLBaseModel {

  @Column({
    name: "title",
    type: "varchar",
  })
  Title: string;

  @Column({ name: 'created_by_id', type: 'int', nullable: true })
  public CreatedById: number;

  @ManyToOne(type => UserModel, user => user.Id)
  @JoinColumn({ name: 'created_by_id' })
  public CreatedBy: UserModel;


  @Column({ name: 'user_id', type: 'int', nullable: true })
  public UserId: number;

  @ManyToOne(type => UserModel, user => user.Id)
  @JoinColumn({ name: 'user_id' })
  public User: UserModel;

  @OneToMany(type => ChatModel, chat => chat.Invitation)
  public Chats: ChatModel[];

}