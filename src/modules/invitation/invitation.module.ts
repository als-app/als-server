import { Module, forwardRef } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { InvitationRepository } from "./invitation.repository";
import { InvitationController } from './invitation.controller';
import { InvitationService } from './invitation.service';
import { ChatModule } from '../chat/chat.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([InvitationRepository]),
    ChatModule,
  ],
  exports: [ InvitationService],
  providers: [InvitationService],
  controllers: [InvitationController],
})
export class InvitationModule { }
