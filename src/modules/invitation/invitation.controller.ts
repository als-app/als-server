import { Body, Param, Query } from "@nestjs/common";
import { MessageResponse } from "src/app.response";
import { AppController } from "src/decorator/appcontroller.decorator";
import { Authorized } from "src/decorator/authorize.decorator";
import { CurrentUser } from "src/decorator/current_user.decorator";
import { Delete } from "src/decorator/nest-ext/delete.decorator";
import { Get } from "src/decorator/nest-ext/get.decorator";
import { Post } from "src/decorator/nest-ext/post.decorator";
import { Put } from "src/decorator/nest-ext/put.decorator";
import { UserModel, UserModelType } from "../auth/user.entity";
import { CreateInvitationRequest } from "./invitation.request";
// import { GigResponseModel } from "./invitation.response";
import { InvitationService } from "./invitation.service";
import { InvitationStatus } from './invitation.entity';

@AppController("invitation")
export class InvitationController {
  constructor(private _invitationService: InvitationService) { }

  @Authorized()
  @Post("/", {})
  async CreateInvitation(@Body() data: CreateInvitationRequest, @CurrentUser() user: UserModel) {
    return await this._invitationService.CreateInvitation(data, user)
  }

  @Authorized()
  @Get('/details/:Id', {})
  async getUserDetails(@Param('Id') Id: string, @CurrentUser() user: UserModel) {
    return await this._invitationService.getInvitationDetails(Number(Id));
  }

  @Authorized()
  @Get('/messages', {})
  async getInvitationMessages(@CurrentUser() user: UserModel) {
    return await this._invitationService.getInvitationMessages(user);
  }

}
