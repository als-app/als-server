import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, Length, IsOptional } from "class-validator";

export class GigResponseModel {
  @ApiProperty()
  Id: number;

  @ApiProperty()
  UserId: number;

  @ApiProperty()
  FullName: string;

  @ApiProperty()
  Email: string;

  @ApiProperty()
  PhoneNumber: string;

  @ApiProperty()
  Country: string;

  @ApiProperty()
  Website: string;
}
