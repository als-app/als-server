import { ApiProperty } from "@nestjs/swagger";
import {
  IsArray,
  IsEnum,
  IsInt,
  IsNumber,
  IsOptional,
  Length,
  Min,
  IsDateString,
} from "class-validator";
import { Transform } from "class-transformer";
import { InvitationStatus } from './invitation.entity';


export class CreateInvitationRequest {

  @IsInt()
  UserId: number;

}

export class GetInvitationsWhere {
  UserId: number;
  CreatedById: number;
  Status: InvitationStatus;
  populateCreatedBy: boolean = false;
  populateUser: boolean = false;
  populateLastMessage: boolean = false;
  Id: number;
  UserIdOrCreatedById: number;
}
