import { HttpStatus } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
export class BadRequestExceptionResponse {
    @ApiProperty({ default: "Bad Request. Check Input" })
    Message: string;
    @ApiProperty({ default: HttpStatus.BAD_REQUEST })
    Status: number;
    @ApiProperty({ nullable: true, default: null })
    Data;
}
export class NotFoundExceptionResponse {
    @ApiProperty({ default: "Not Found" })
    Message: string;
    @ApiProperty({ default: HttpStatus.NOT_FOUND })
    Status: number;
    @ApiProperty({ nullable: true, default: null })
    Data;
}
export class ForbiddenExceptionResponse {
    @ApiProperty({ default: "Access Not Allowed" })
    Message: string;
    @ApiProperty({ default: HttpStatus.FORBIDDEN })
    Status: number;
    @ApiProperty({ nullable: true, default: null })
    Data;
}
export class UnauthorizedExceptionResponse {
    @ApiProperty({ default: "Not Authorized" })
    Message: string;
    @ApiProperty({ default: HttpStatus.UNAUTHORIZED })
    Status: number;
    @ApiProperty({ nullable: true, default: null })
    Data;
}
export class FatalErrorExceptionResponse {
    @ApiProperty({ default: "Something Went Wrong" })
    Message: string;
    @ApiProperty({ default: HttpStatus.INTERNAL_SERVER_ERROR })
    Status: number;
    @ApiProperty({ nullable: true, default: null })
    Data;
}