import { ApiProperty } from "@nestjs/swagger";

export class MessageResponse {
    @ApiProperty()
    Message: string;
}