import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UserModule } from "./modules/auth/user.module";
import { APP_FILTER, APP_GUARD } from "@nestjs/core";
import { HttpExceptionFilter } from "./exception/http.exception";
import { RedisModule } from "./redis/redis.module";
import { AuthGuard } from "./modules/auth/auth.guard";
import { MediaModule } from "./modules/media/media.module";
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from "path";
import { SocketModule } from "./socket/socket.module";
import { NotificationModule } from "./modules/notification/notification.module";
import { ChatModule } from "./modules/chat/chat.module";
import { InvitationModule } from './modules/invitation/invitation.module';
import { ReviewModule } from './modules/review/review.module';
import { CategoryModule } from './modules/category/category.module';
import { PostingModule } from './modules/posting/posting.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(),
    RedisModule,
    UserModule,
    MediaModule,
    SocketModule,
    NotificationModule,
    ChatModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, "..", "public/attachments/"),
      serveRoot: "/static/",
    }),
    InvitationModule,
    ReviewModule,
    CategoryModule,
    PostingModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule { }
