import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisCacheService {
  constructor(@Inject(CACHE_MANAGER) private readonly cache: Cache) { }

  async Get(key) {
    return await this.cache.get(key);
  }

  async Set(key, value, ttl = 0) {
    await this.cache.set(key, value, { ttl });
  }

  async Delete(key) {
    await this.cache.del(key);
  }
}
