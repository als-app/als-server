import { Module, CacheModule } from '@nestjs/common';
import * as redisStore from 'cache-manager-redis-store';
import { appEnv } from 'src/helpers/env.helper';
import { RedisCacheService } from './redis.service';

@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: appEnv('DB_REDIS_HOST', 'localhost'),
      port: appEnv('DB_REDIS_PORT', 6379),
    }),
  ],
  providers: [RedisCacheService],
  exports: [RedisCacheService], // This is IMPORTANT,  you need to export RedisCacheService here so that other modules can use it
})
export class RedisModule {}
