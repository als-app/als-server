import { appEnv } from "src/helpers/env.helper";

export enum AuthenticationCodes {
  UserLoginSuccess = 10,
  UserVerificationPending = 20,
  UserSignUpSuccess = 30,
  AlreadyRegistered = 71,
}

export const AUTHORIZATION_HEADER_KEY = 'authorization';
export const OPTIONAL_AUTHORIZATION_HEADER_KEY = 'optional-authorization';
export const ROLES = 'roles';
export const LOCALE_HEADER_KEY = 'locale';

export const GetVerifyUserEmailLink = (code: string) => {
  return `http://${appEnv("APP_DOMAIN", "")}verify/${code}`;
}


export const GetResetPasswordLink = (code: string) => {
  return `http://${appEnv("APP_DOMAIN", "")}reset-password/${code}`;
};
