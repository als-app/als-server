import { HttpStatus } from "@nestjs/common";

export const DefaultResponseMessages = {
    [HttpStatus.OK]: "Success",
    [HttpStatus.BAD_REQUEST]: "Bad Request",
    [HttpStatus.INTERNAL_SERVER_ERROR]: "Internal Server Error",
    [HttpStatus.NOT_FOUND]: "Resource Not Found",
    [HttpStatus.UNAUTHORIZED]: "Not Authorized"
};