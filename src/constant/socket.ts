export enum SocketEventAuthorizedEntity {
    User = 1,
    Advisor = 2,
}
export enum SocketEventSenderType {
    User = 1,
    Advisor = 2,
}

export enum SocketRoomType {
    Chat = 'ss_chat',
    Customer = "ss_customer"
}

export interface SocketEntityData {
    EId: number;
    EType: number;
    AId?: number;
}

export enum SocketEventType {
    ChatStart = "chat_start",
    ChatJoin = "chat_join",
    Connected = "connection",
    Disconnect = "disconnect",
    Message = "message",
    Typing = "typing",
    ReadMessage = "read_message"
}