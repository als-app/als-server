import { render as MustacheRender } from "mustache";
import * as fs from "fs";
import * as path from "path";
import { MessageTemplates, __T } from "src/locale/locale";
import { GetResetPasswordLink, GetVerifyUserEmailLink } from "src/constant/authentication";
import { GetPaginationOptions } from "src/helpers/util.helper";


let UserEmailVerificationTemplatePath = "/user_email_verification.html";
let ResetPasswordTemplatePath = "/user_password_reset.html";

export enum EmailTemplate {
    UserEmailVerification = "UserEmailVerification",
    ResetPassword = "ResetPassword"
}


export let TemplateRenderer: { [key in keyof typeof EmailTemplate]: (data, locale) => { subject: string; content: string } } = {
    UserEmailVerification: (data, locale) => {

        let EmailVerifyHTML = fs.readFileSync(path.resolve(__dirname + UserEmailVerificationTemplatePath), "utf-8");
        let EmailVerifyTemplate = MustacheRender(EmailVerifyHTML, {
            Address: __T(locale, MessageTemplates.Address),
            Link: GetVerifyUserEmailLink(data.Code),
            Code: data.Code,
            Year: new Date().getFullYear(),
            Footer: __T(locale, MessageTemplates.EmailFooter)
        });
        let subject = __T(locale, MessageTemplates.EmailVerificationSubject);
        return { content: EmailVerifyTemplate, subject: subject };
    },


    ResetPassword: (data, locale) => {
        let ResetPasswordHTML = fs.readFileSync(path.resolve(__dirname + ResetPasswordTemplatePath), "utf-8");
        let ResetPasswordTemplate = MustacheRender(ResetPasswordHTML, {
            Address: __T(locale, MessageTemplates.Address),
            Link: data.Link,
            Code: data.Code,
            FullName: data.FullName,
            Year: new Date().getFullYear(),
            Footer: __T(locale, MessageTemplates.EmailFooter)
        });
        let subject = __T(locale, MessageTemplates.ResetPasswordSubject);
        return { content: ResetPasswordTemplate, subject: subject };
    }
};
