import { Module } from "@nestjs/common";
import { UserModule } from "src/modules/auth/user.module";
import { RedisModule } from "src/redis/redis.module";
import { SocketEventHandler } from "./socket_event.handler";
import { SocketGateway } from "./socket.gateway";
import { ChatModule } from "src/modules/chat/chat.module";
import { MediaModule } from "src/modules/media/media.module";
import { SocketAuthGuard } from "./socket_auth.guard";
import { InvitationModule } from '../modules/invitation/invitation.module';

@Module({
    imports: [RedisModule, UserModule, MediaModule, ChatModule, InvitationModule],
    exports: [],
    providers: [SocketAuthGuard, SocketEventHandler, SocketGateway]
})
export class SocketModule { }