import { OnGatewayInit, WebSocketGateway, WebSocketServer, OnGatewayConnection, SubscribeMessage, MessageBody, ConnectedSocket, OnGatewayDisconnect } from "@nestjs/websockets";
import { SocketEventSenderType, SocketEventType } from "src/constant/socket";
import { SocketEventHandler } from "./socket_event.handler";
import ChatJoinedSocketEvent, { ChatJoinedSocketEventData } from "./events/chat/chat_joined.event";
import ChatStartedSocketEvent, { ChatStartedSocketEventData } from "./events/chat/chat_started.event";
import ConnectedSocketEvent from "./events/connected.event";
import DisconnectedSocketEvent from "./events/disconnected.event";
import MessageSocketEvent, { MessageSocketEventData } from "./events/chat/message.event";
import ReadMessageSocketEvent, { ReadMessageSocketEventData } from "./events/chat/read_message.event";
import TypingSocketEvent, { TypingSocketEventData } from "./events/chat/typing.event";
import { UseFilters, UseGuards } from "@nestjs/common";
import { SocketAuthGuard } from "./socket_auth.guard";
import { Authorized } from "src/decorator/authorize.decorator";

@UseGuards(SocketAuthGuard)
@WebSocketGateway()
export class SocketGateway implements OnGatewayInit<SocketIO.Server>, OnGatewayConnection<SocketIO.Socket>, OnGatewayDisconnect<SocketIO.Socket> {
    private static _io: SocketIO.Server;

    constructor(private _socketEventHandler: SocketEventHandler) { }

    afterInit(server: SocketIO.Server) {
        SocketGateway._io = server;
        SocketGateway._io.use(this._socketEventHandler._socketAuthentication.bind(this._socketEventHandler));
        //Logger.Info("Socket Initialized!", "SOCKET-IO");
    }

    async handleConnection(socket: SocketIO.Socket) {
        let Event: ConnectedSocketEvent = new ConnectedSocketEvent(socket);
        return await this._socketEventHandler.OnConnectedEvent(Event);
    }

    async handleDisconnect(socket: SocketIO.Socket) {
        let Event: DisconnectedSocketEvent = new DisconnectedSocketEvent(socket);
        await this._socketEventHandler.OnDisconnectEvent(Event);
    }

    @SubscribeMessage(SocketEventType.ChatJoin)
    @Authorized()
    async ChatJoinedEvent(@ConnectedSocket() socket: SocketIO.Socket, @MessageBody() data: ChatJoinedSocketEventData) {
        let Event: ChatJoinedSocketEvent = new ChatJoinedSocketEvent(socket, data);
        return await this._socketEventHandler.OnChatJoinedEvent(Event);
    }

    // @SubscribeMessage(SocketEventType.ChatStart)
    // @Authorized(SocketEventSenderType.User)
    // async ChatStartedEvent(@ConnectedSocket() socket: SocketIO.Socket, @MessageBody() data: ChatStartedSocketEventData) {
    //     let Event: ChatStartedSocketEvent = new ChatStartedSocketEvent(socket, data, SocketGateway._io);
    //     return await this._socketEventHandler.OnChatStartedEvent(Event);
    // }

    @SubscribeMessage(SocketEventType.Message)
    @Authorized()
    async MessageEvent(@ConnectedSocket() socket: SocketIO.Socket, @MessageBody() data: MessageSocketEventData) {
        let Event: MessageSocketEvent = new MessageSocketEvent(socket, data, SocketGateway._io);
        return await this._socketEventHandler.OnMessageEvent(Event);
    }

    // @SubscribeMessage(SocketEventType.Typing)
    // @Authorized()
    // async TypingEvent(@ConnectedSocket() socket: SocketIO.Socket, @MessageBody() data: TypingSocketEventData) {
    //     let Event: TypingSocketEvent = new TypingSocketEvent(socket, data);
    //     return await this._socketEventHandler.OnTypingEvent(Event);
    // }

    // @SubscribeMessage(SocketEventType.ReadMessage)
    // @Authorized()
    // async ReadMessageEvent(@ConnectedSocket() socket: SocketIO.Socket, @MessageBody() data: ReadMessageSocketEventData) {
    //     let Event: ReadMessageSocketEvent = new ReadMessageSocketEvent(socket, data);
    //     return await this._socketEventHandler.OnReadMessageEvent(Event);
    // }

}