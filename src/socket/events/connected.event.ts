import { SocketEventType } from "src/constant/socket";
import BaseSocketEvent from "./base.event";

export default class ConnectedSocketEvent extends BaseSocketEvent {
    static Name: string = SocketEventType.Connected;
    protected GetName(): string {
        return ConnectedSocketEvent.Name;
    }

    constructor(socket: SocketIO.Socket) {
        super(socket);
    }
}