import { SocketEventSenderType, SocketEventType } from "src/constant/socket";
import BaseSocketEvent from "../base.event";

export default class ReadMessageSocketEvent extends BaseSocketEvent {
    ChatEventId: number;

    constructor(socket: SocketIO.Socket, data: ReadMessageSocketEventData) {
        super(socket);

        this.ChatEventId = data.ChatEventId;
    }

    static Name: string = SocketEventType.ReadMessage;
    protected GetName(): string {
        return ReadMessageSocketEvent.Name;
    }

    GetData(): ReadMessageSocketEventData {
        return {
            ChatEventId: this.ChatEventId,
        }
    }

}

export interface ReadMessageSocketEventData {
    ChatEventId: number;
}