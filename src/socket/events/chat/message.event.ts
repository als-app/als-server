import { SocketEventSenderType, SocketEventType } from "src/constant/socket";
import { ChatModel } from "src/modules/chat/chat.entity";
import BaseSocketEvent from "../base.event";
import { MediaModel } from 'src/modules/media/media.entity';

export default class MessageSocketEvent extends BaseSocketEvent {
    InvitationId: number;
    Content?: string;
    MediaId?: number;
    Media?: MediaModel;
    Chat?: ChatModel;
    IO: SocketIO.Server;

    constructor(socket: SocketIO.Socket, data: MessageSocketEventData, io: SocketIO.Server) {
        super(socket);

        this.InvitationId = data.InvitationId;
        this.Content = data.Content;
        if (data.MediaId) {
            this.MediaId = data.MediaId;
        }
        this.IO = io;
    }

    static Name: string = SocketEventType.Message;
    protected GetName(): string {
        return MessageSocketEvent.Name;
    }

    GetData(): MessageSocketEventData {
        return {
            InvitationId: this.InvitationId,
            Content: this.Content,
            MediaId: this.MediaId,
            // ChatEvent: this.ChatEvent,
            Chat: this.Chat,
            Media: this.Media,
        }
    }

}

export interface MessageSocketEventData {
    InvitationId: number;
    Chat?: ChatModel;
    Content?: string;
    MediaId?: number;
    Media?: MediaModel;
    ChatEvent?: Object;
}