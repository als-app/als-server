import BaseSocketEvent from "../base.event";
import { SocketEventSenderType, SocketEventType } from "src/constant/socket";

export default class ChatStartedSocketEvent extends BaseSocketEvent {
    AdvisorUserId: number;
    GigId: number;
    ChatId?: number;
    Chat?: Object;
    IO: SocketIO.Server;

    static Name: string = SocketEventType.ChatStart;
    protected GetName(): string {
        return ChatStartedSocketEvent.Name;
    }

    constructor(socket: SocketIO.Socket, data: ChatStartedSocketEventData, io: SocketIO.Server) {
        super(socket);
        this.AdvisorUserId = data.AdvisorUserId;
        this.GigId = data.GigId
        this.IO = io;
        if (data.ChatId) {
            this.ChatId = data.ChatId;
        }
        if (data.Chat) {
            this.Chat = data.Chat;
        }
    }

    GetData(): ChatStartedSocketEventData {
        return {
            AdvisorUserId: this.AdvisorUserId,
            ChatId: this.ChatId,
            GigId: this.GigId,
            Chat: this.Chat
        }
    }

}

export interface ChatStartedSocketEventData {
    AdvisorUserId: number;
    ChatId?: number;
    Chat?: Object;
    GigId: number
}