import { SocketEventSenderType, SocketEventType } from "src/constant/socket";
import BaseSocketEvent from "../base.event";

export default class TypingSocketEvent extends BaseSocketEvent {
    ChatId: number;
    Typing?: string;

    constructor(socket: SocketIO.Socket, data: TypingSocketEventData) {
        super(socket);

        this.ChatId = data.ChatId;
        this.Typing = data.Typing;
    }

    static Name: string = SocketEventType.Typing;
    protected GetName(): string {
        return TypingSocketEvent.Name;
    }

    GetData(): TypingSocketEventData {
        return {
            ChatId: this.ChatId,
            Typing: this.Typing,
        }
    }

}

export interface TypingSocketEventData {
    ChatId: number;
    Typing?: string;
}