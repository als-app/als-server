import { SocketEventType } from "src/constant/socket";
import BaseSocketEvent from "../base.event";

export default class ChatJoinedSocketEvent extends BaseSocketEvent {
    InvitationId: number;

    constructor(socket: SocketIO.Socket, data: ChatJoinedSocketEventData) {
        super(socket);
        this.InvitationId = data.InvitationId;
    }

    static Name: string = SocketEventType.ChatJoin;
    protected GetName(): string {
        return ChatJoinedSocketEvent.Name;
    }

    GetData(): ChatJoinedSocketEventData {
        return {
            InvitationId: this.InvitationId
        }
    }
}

export interface ChatJoinedSocketEventData {
    InvitationId: number;
}