import { SocketEventType } from "src/constant/socket";
import BaseSocketEvent from "./base.event";

export default class DisconnectedSocketEvent extends BaseSocketEvent {
    static Name: string = SocketEventType.Disconnect;
    protected GetName(): string {
        return DisconnectedSocketEvent.Name;
    }

    constructor(socket: SocketIO.Socket) {
        super(socket);
    }
}