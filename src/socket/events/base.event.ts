import { SocketRoomType } from "src/constant/socket";
import { Locale, MessageTemplates, __T } from "src/locale/locale";

export default abstract class BaseSocketEvent {
    private _socket: SocketIO.Socket;

    constructor(socket: SocketIO.Socket) {
        this._socket = socket;
    }

    private GetChatRoom(chatId: number): string {
        return SocketRoomType.Chat + ":" + chatId;
    }

    private GetCustomerRoom(): string {
        return SocketRoomType.Customer;
    }

    protected abstract GetName(): string;

    protected GetData(): Object {
        return {};
    }

    public GetSocket(): SocketIO.Socket {
        return this._socket;
    }

    public JoinChatRoom(chatId: number) {
        this._socket.join(this.GetChatRoom(chatId));
    }

    // public JoinCustomerRoom() {
    //     this._socket.join(this.GetCustomerRoom());
    // }

    public LeaveChatRoom(chatId: number) {
        this._socket.leave(this.GetChatRoom(chatId));
    }

    public BroadcastToChatRoom(chatId: number) {
        this._socket.broadcast.to(this.GetChatRoom(chatId)).emit(this.GetName(), this.GetData());
    }

    public IOBroadcastToCustomerRoom(io: SocketIO.Server) {
        io.to(this.GetCustomerRoom()).emit(this.GetName(), this.GetData());
    }

    public EmitToIndividualSocket(socketId: string) {
        this._socket.to(socketId).emit(this.GetName(), this.GetData());
    }

    public IOEmitToIndividualSocket(io, socketId: string) {
        io.to(socketId).emit(this.GetName(), this.GetData());
    }

    public Emit() {
        this._socket.emit(this.GetName(), this.GetData());
    }

    public EmitError(statusCode: number, messageKey: MessageTemplates, messageData?: any) {
        let error: any = new Error();
        error.Status = statusCode;
        error.Message = __T(Locale.En, messageKey, messageData);
        this._socket.error(error);
    }

    public SocketJoinChatRoom(socket: SocketIO.Socket, chatId: number) {
        socket.join(this.GetChatRoom(chatId));
    }

}