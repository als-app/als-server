import { forwardRef, HttpStatus, Inject, Injectable, UseFilters } from "@nestjs/common";
import { DefaultResponseMessages } from "src/constant/response";
import { SocketEventSenderType } from "src/constant/socket";
import { BadRequestException, ForbiddenException, NotFoundException } from "src/exception/response.exception";
import { Locale, MessageTemplates, __T } from "src/locale/locale";
import { AuthService } from "src/modules/auth/auth.service";
import { UserModel, UserModelType } from "src/modules/auth/user.entity";
import { UserService } from "src/modules/auth/user.service";
// import { ChatModel } from "src/modules/chat/chat.entity";
// import { ChatService } from "src/modules/chat/chat.service";
// import { ChatEventModel, ChatEventModelType } from "src/modules/chat/chat_event/chat_event.entity";
// import { ChatEventService } from "src/modules/chat/chat_event/chat_event.service";
import { MediaModel } from "src/modules/media/media.entity";
import { MediaService } from "src/modules/media/media.service";
import ChatJoinedSocketEvent from "./events/chat/chat_joined.event";
import ChatStartedSocketEvent from "./events/chat/chat_started.event";
import ConnectedSocketEvent from "./events/connected.event";
import DisconnectedSocketEvent from "./events/disconnected.event";
import MessageSocketEvent from "./events/chat/message.event";
import ReadMessageSocketEvent from "./events/chat/read_message.event";
import TypingSocketEvent from "./events/chat/typing.event";
import { App } from "src/app";
// import { AdvisorService } from "src/modules/advisor/advisor.service";
// import { AdvisorModel } from "src/modules/advisor/advisor.entity";
import { SSQueue } from "src/helpers/queue.helper";
import { QueueJobs } from "src/constant/queue";
import { NotificationModelType } from "src/modules/notification/notification.entity";
import { ChatService } from '../modules/chat/chat.service';
import { InvitationService } from '../modules/invitation/invitation.service';
import { ChatModel } from '../modules/chat/chat.entity';

@Injectable()
export class SocketEventHandler {
    constructor(
        private _authService: AuthService,
        private _userService: UserService,
        // @Inject(forwardRef(() => AdvisorService))
        // private _advisorService: AdvisorService,
        private _chatService: ChatService,
        private _invitationService: InvitationService,
        // private _chatEventService: ChatEventService,
        private _mediaService: MediaService,

    ) { }

    async OnConnectedEvent(event: ConnectedSocketEvent) {
        let UserId: number = parseInt(event.GetSocket().handshake.query.actorId);
        let User: UserModel = await this._userService.FindById(UserId);
        if (!User) {
            throw new NotFoundException(MessageTemplates.UserNotFound);
        }

        // let Advisor: AdvisorModel = await this._advisorService.FindAdvisorByUserId(User.Id);

        // if (Advisor) {
        //     await this._authService.SetVendorSocketId(event.GetSocket().id, User.Id, Advisor.Id);
        // } else {
        //     await this._authService.SetCustomerSocketId(event.GetSocket().id, User.Id);
        //     //  event.JoinCustomerRoom();
        // }

        // let SenderType = Advisor ? SocketEventSenderType.Advisor : SocketEventSenderType.User;
        await this._authService.SetCustomerSocketId(event.GetSocket().id, User.Id);
        await this._addUserSocket(event.GetSocket().id, User.Id, SocketEventSenderType.User);
        return true;
    }

    async OnDisconnectEvent(event: DisconnectedSocketEvent) {
        let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
        await this._authService.RemoveSocketIdFromUserSockets(event.GetSocket().id, SocketData.UserId, SocketData.UserType);
        await this._authService.RemoveEntityDataBySocketId(event.GetSocket().id);
        return true;
    }

    async OnChatJoinedEvent(event: ChatJoinedSocketEvent) {
        let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
        let Invitation = await this._invitationService.getInvitationDetails(event.InvitationId);
        if (Invitation.CreatedById !== SocketData.UserId || Invitation.UserId !== SocketData.UserId) {
            throw new ForbiddenException(MessageTemplates.ForbiddenError);
        }

        event.JoinChatRoom(Invitation.Id);
        return true;
    }

    // async OnChatStartedEvent(event: ChatStartedSocketEvent) {
    //     let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
    //     // if (SocketData.UserType !== SocketEventSenderType.User) {
    //     //     throw new ForbiddenException(MessageTemplates.ForbiddenError);
    //     // }

    //     let { Chat, IsNewChat } = await this._chatService.GetOrCreateChat({ AdvisorUserId: event.AdvisorUserId, GigId: event.GigId }, SocketData.UserId);
    //     event.ChatId = Chat.Id;
    //     event.Chat = Chat;

    //     event.JoinChatRoom(Chat.Id);
    //     if (IsNewChat) {
    //         let VendorSockets = await this._authService.GetSocketIdsByUserId(event.AdvisorUserId, SocketEventSenderType.Advisor);
    //         this._joinChatRoom(VendorSockets, Chat.Id, event);

    //         event.BroadcastToChatRoom(Chat.Id);
    //     }
    //     //todo: fcm push notification
    //     return event.GetData();

    // }

    async OnMessageEvent(event: MessageSocketEvent) {
        let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
        let Invitation = await this._invitationService.getInvitationDetails(event.InvitationId);
        if (!Invitation) {
            throw new NotFoundException(MessageTemplates.NotFoundError);
        }

        let chat: ChatModel = new ChatModel();
        chat.InvitationId = Invitation.Id;
        chat.CreatedById = SocketData.UserId;
        const User = Invitation.CreatedById === chat.CreatedById ? Invitation.CreatedBy : Invitation.User;
        const OtherUser = Invitation.CreatedById === chat.CreatedById ? Invitation.User : Invitation.CreatedBy;

        if (event.MediaId) {
            let Media: MediaModel = await this._mediaService.GetById(event.MediaId);
            if (!Media) {
                throw new NotFoundException(MessageTemplates.MediaNotFound);
            }

            // if (Media.UserId && Media.UserId !== SocketData.UserId) {
            //     throw new NotFoundException(MessageTemplates.MediaNotFound);
            // }

            chat.MediaId = event.MediaId;
            event.Media = Media;
        }

        if (event.Content) {
            chat.Message = event.Content;
        }

        chat = await this._chatService.CreateChat(chat, User);
        event.Chat = chat;
        let SocketIds = await this._authService.GetSocketIdsByUserId(
            OtherUser.Id,
            SocketEventSenderType.User
        );
        this._joinChatRoom(SocketIds, Invitation.Id, event);

        event.BroadcastToChatRoom(Invitation.Id);

        await SSQueue.Enqueue(QueueJobs.NOTIFICATION, {
            Type: NotificationModelType.NewChatMessage,
            ReceiverId: OtherUser.Id,
            InvitationId: Invitation.Id,
            SenderId: User.Id,
            SenderType: SocketEventSenderType.User,
            Content: chat.Message ? chat.Message : __T(Locale.En, MessageTemplates.AttachmentShared)
        })

        return event.GetData();

    }

    // async OnTypingEvent(event: TypingSocketEvent) {
    //     event.BroadcastToChatRoom(event.ChatId);
    //     return true;
    // }



    public async _socketAuthentication(client: SocketIO.Socket, next: Function) {
        let token: string = client.handshake.query.authorization;
        let actorId: number = parseInt(client.handshake.query.actorId);
        console.log(token, actorId)
        if (token) {
            let auth = await this._authService.GetSession(token);
            console.log(auth)
            if (auth && auth.UserId === actorId) {
                return next();
            }
        }
        let error: any = new Error();
        error.Status = HttpStatus.UNAUTHORIZED;
        error.Message = DefaultResponseMessages[HttpStatus.UNAUTHORIZED]
        error.Data = null;
        client.error(error);
        // Logger.Debug({ Error: error, Token: token, ActorId: actorId }, "[SOCKET]AUTHENTICATION");
        console.log(error)
        return next(error);
    }

    private async _addUserSocket(socketId: string, userId: number, userType: SocketEventSenderType) {
        let SocketIds: Array<string> = await this._authService.GetSocketIdsByUserId(userId, userType);
        SocketIds.push(socketId);
        await this._authService.SetSocketIdsByUserId(SocketIds, userId, userType);
    }

    private _joinChatRoom(socketIds: Array<string>, InvitationId: number, event: MessageSocketEvent | ChatStartedSocketEvent) {
        for (let i = 0; i < socketIds.length; i++) {
            if (
                event.IO &&
                event.IO.sockets &&
                event.IO.sockets.connected &&
                event.IO.sockets.connected[socketIds[i]]
            ) {
                event.SocketJoinChatRoom(
                    event.IO.sockets.connected[socketIds[i]],
                    InvitationId
                );
            }
        }
    }
}