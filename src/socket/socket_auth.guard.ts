import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";
import { AUTHORIZATION_HEADER_KEY, ROLES } from "src/constant/authentication";
import { UnAuthorizedException } from "src/exception/response.exception";
import { MessageTemplates } from "src/locale/locale";
import { AuthService } from "src/modules/auth/auth.service";

@Injectable()
export class SocketAuthGuard implements CanActivate {
    constructor(private _reflector: Reflector, private _authService: AuthService) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const ctx = context.switchToWs();
        const socket = ctx.getClient<SocketIO.Socket>();
        const requiredAuthorization = this._reflector.get<string[]>(
            AUTHORIZATION_HEADER_KEY,
            context.getHandler(),
        );

        const roles = this._reflector.get<number[]>(ROLES, context.getHandler())
        return true;
    }

}