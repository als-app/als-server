import { En } from "./en.locale";
import { ReplaceObjectValuesInString } from "src/helpers/util.helper";

export enum MessageTemplates {
    NotFoundError = "NotFoundError",
    UnauthorizedError = "UnauthorizedError",
    Success = "Success",
    BadRequestError = "BadRequestError",
    InternalServerError = "InternalServerError",
    UserNotFound = "UserNotFound",
    UserAlreadyExist = "UserAlreadyExist",
    InvalidVerificationCode = "InvalidVerificationCode",
    InvalidAccessToken = "InvalidAccessToken",
    EmailDoesNotMatchWithOAuth = "EmailDoesNotMatchWithOAuth",
    DeviceNotFound = "DeviceNotFound",
    UserDeviceNotFound = "UserDeviceNotFound",
    InvalidValueFor = "InvalidValueFor",
    InvalidCredentials = "InvalidCredentials",
    ForbiddenError = "ForbiddenError",
    EmailVerificationSubject = "EmailVerificationSubject",
    ResetPasswordSubject = "ResetPasswordSubject",
    Address = "Address",
    EmailFooter = "EmailFooter",
    MediaNotFound = "MediaNotFound",
    MediaNotAllowed = "MediaNotAllowed",
    MediaSizeTooLarge = "MediaSizeTooLarge",
    MediaServiceNotAvailable = "MediaServiceNotAvailable",
    AdvisorAlreadyCreated = "AdvisorAlreadyCreated",
    PublicQuestionAlreadyAnswered = "PublicQuestionAlreadyAnswered",
    NewMessageReceive = "NewMessageReceive",
    AttachmentShared = "AttachmentShared",
    InvalidPassword = "InvalidPassword",
    ProvideOldPassword = "ProvideOldPassword",
    WrongOldPassword = "WrongOldPassword"
}

export enum Locale {
    En = "En",
    Ar = "Ar"
}

const LocaleFunc: {
    [key in keyof typeof Locale]: (key: string, data?: any) => string;
} = {
    En: (key: string, data?: any) => {
        return ReplaceObjectValuesInString(En.values[key], data);
    },
    Ar: (key: string, data?: any) => {
        return ReplaceObjectValuesInString(En.values[key], data);
    }
}

export function __T(locale: Locale, key: MessageTemplates, data?: any): string {
    let Message = key.toString();
    return LocaleFunc[locale](Message, data);
}

export interface ILocale {
    values: { [key in keyof typeof MessageTemplates]: string }
}