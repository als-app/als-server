import * as dotenv from 'dotenv';
dotenv.config();
import { App } from './app';
import { Bootstrap } from './bootstrap/bootstrap';
import { appEnv } from './helpers/env.helper';
import { Logger } from './helpers/logger.helper';
import { QueueProcessor } from './helpers/queue.processor';

(async function LoadServer() {
    //todo review this
    const app = await App.GetNestApplicationInstance();
    Logger.Info("Bootstrapping application ...", "BOOTSTRAP");
    await Bootstrap.boot(app);
    Logger.Info("Application loaded.", "BOOTSTRAP");
    await app.listen(appEnv("PORT", 3001));
    Logger.Info(`Server running on port ${appEnv("PORT", 3001)}`);
    Logger.Info("Queue Started.", "BOOTSTRAP");
    QueueProcessor.ProcessQueue()
})();
