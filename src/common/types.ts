export type Location = {
    Lat: number;
    Lng: number;
  }
  