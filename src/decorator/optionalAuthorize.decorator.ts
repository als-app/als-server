import { applyDecorators, SetMetadata } from '@nestjs/common';
import { ApiSecurity } from '@nestjs/swagger';
import { AUTHORIZATION_HEADER_KEY, ROLES, OPTIONAL_AUTHORIZATION_HEADER_KEY
 } from 'src/constant/authentication';

export const OptionalAuthorized = (roleOrRoles?: number | Array<Number>) => {
  let authorizedRoles = [];
  if (roleOrRoles)
    authorizedRoles = Array.isArray(roleOrRoles) ? roleOrRoles : [roleOrRoles];
  // return applyDecorators();
  const data = applyDecorators(
    SetMetadata(ROLES, authorizedRoles),
    SetMetadata(AUTHORIZATION_HEADER_KEY, true),
    SetMetadata(OPTIONAL_AUTHORIZATION_HEADER_KEY, true),
    ApiSecurity(AUTHORIZATION_HEADER_KEY),
  );
  return data;
};
