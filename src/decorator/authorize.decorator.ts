import { applyDecorators, SetMetadata } from '@nestjs/common';
import { ApiSecurity } from '@nestjs/swagger';
import { AUTHORIZATION_HEADER_KEY, ROLES } from 'src/constant/authentication';

export const Authorized = (roleOrRoles?: number | Array<Number>) => {
  let authorizedRoles = [];
  if (roleOrRoles)
    authorizedRoles = Array.isArray(roleOrRoles) ? roleOrRoles : [roleOrRoles];
  return applyDecorators(
    SetMetadata(ROLES, authorizedRoles),
    SetMetadata(AUTHORIZATION_HEADER_KEY, true),
    ApiSecurity(AUTHORIZATION_HEADER_KEY),
  );
};
