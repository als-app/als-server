import { createParamDecorator } from '@nestjs/common';
import { App } from 'src/app';
import { AUTHORIZATION_HEADER_KEY } from 'src/constant/authentication';
import { UserDeviceRepository } from 'src/modules/auth/user_device/user_device.repository'

export const CurrentDevice = createParamDecorator(async (data, context) => {

    const request = context.switchToHttp().getRequest();
    const token = request.headers[AUTHORIZATION_HEADER_KEY];

    let userDeviceRepo = (await App.GetNestApplicationInstance()).get(UserDeviceRepository);
    const userDevice = await userDeviceRepo.FindOne({
        AuthToken: token
    })
    return userDevice
});
