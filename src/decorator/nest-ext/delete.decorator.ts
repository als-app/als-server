import { applyDecorators, HttpStatus, Delete as NestDelete } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";
import { BadRequestExceptionResponse, FatalErrorExceptionResponse, ForbiddenExceptionResponse, NotFoundExceptionResponse, UnauthorizedExceptionResponse } from "src/response/response.schema";

export function Delete(route: string, successResponseModel: Function | any) {
    return applyDecorators(
        ApiResponse({
            type: successResponseModel,
            status: HttpStatus.OK
        }),
        ApiResponse({
            type: BadRequestExceptionResponse,
            status: HttpStatus.BAD_REQUEST
        }),
        ApiResponse({
            type: ForbiddenExceptionResponse,
            status: HttpStatus.FORBIDDEN
        }),
        ApiResponse({
            type: UnauthorizedExceptionResponse,
            status: HttpStatus.UNAUTHORIZED
        }),
        ApiResponse({
            type: NotFoundExceptionResponse,
            status: HttpStatus.NOT_FOUND
        }),
        ApiResponse({
            type: FatalErrorExceptionResponse,
            status: HttpStatus.INTERNAL_SERVER_ERROR
        }),
        NestDelete(route)
    )
}