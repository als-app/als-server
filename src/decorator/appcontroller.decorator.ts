import { applyDecorators, Controller, UseFilters } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { HttpExceptionFilter } from "src/exception/http.exception";

export function AppController(prefix?: string, moduleName?: string) {
    return applyDecorators(
        ApiTags(moduleName ? moduleName : prefix ? prefix : "default"),
        Controller(moduleName ? moduleName + "/" + prefix : prefix),
        UseFilters(HttpExceptionFilter)
    )
}