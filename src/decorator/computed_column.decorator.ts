import { Column, ColumnType } from "typeorm";

export function ComputedColumn(options: { name: string, type: ColumnType }) {
    return Column({
        name: options.name,
        type: options.type,
        select: false,
        insert: false,
        update: false,
        readonly: true
    });
}