import { IsIn } from "class-validator"

export function IsInEnum(enumObject: Object) {
    let enumValues = Object.keys(enumObject).filter(a => parseInt(a) >= 0).map(a => parseInt(a));
    return IsIn(enumValues);
}