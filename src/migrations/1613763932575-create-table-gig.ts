import { MigrationInterface, QueryRunner } from "typeorm";

export class createTableGig1613763932575 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE gig
            (
                id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
                category_id     BIGINT,
                advisor_id     BIGINT,
                title   varchar(1000),
                description   text,
                price       decimal(15,2),
                information1     text,
                information2     text,
                created_at  BIGINT   NOT NULL,
                updated_at  BIGINT   NOT NULL,
                deleted_at  BIGINT DEFAULT 0 NOT NULL
            );
            `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        DROP TABLE gig;
        `);
    }

}
