import { MigrationInterface, QueryRunner } from "typeorm";

export class advisor1613332173731 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE advisor
        (
            id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
            user_id     BIGINT,
            full_name   varchar(255),
            phone_number   varchar(255),
            email       varchar(255),
            country     varchar(255),
            website     varchar(255),
            meta        JSON DEFAULT NULL,
            type  tinyint(2) default 1,
            created_at  BIGINT   NOT NULL,
            updated_at  BIGINT   NOT NULL,
            deleted_at  BIGINT DEFAULT 0 NOT NULL
        );
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    DROP TABLE advisor;
    `);
  }
}
