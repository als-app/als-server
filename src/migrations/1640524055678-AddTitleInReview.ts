import {MigrationInterface, QueryRunner} from "typeorm";

export class AddTitleInReview1640524055678 implements MigrationInterface {
    name = 'AddTitleInReview1640524055678'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD \`title\` varchar(255) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP COLUMN \`title\``);
    }

}
