import { MigrationInterface, QueryRunner } from "typeorm";

export class alterAdvisorTable1616440661253 implements MigrationInterface {


    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE advisor ADD COLUMN status tinyint(2) default 0;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TABLE advisor drop column status;
        `);
    }

}
