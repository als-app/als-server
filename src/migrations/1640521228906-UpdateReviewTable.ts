import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateReviewTable1640521228906 implements MigrationInterface {
    name = 'UpdateReviewTable1640521228906'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_8b65f758fd563971cf97d9d4bad\``);
        await queryRunner.query(`ALTER TABLE \`reviews\` CHANGE \`invitation_id\` \`invitation_id\` bigint NULL`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_8b65f758fd563971cf97d9d4bad\` FOREIGN KEY (\`invitation_id\`) REFERENCES \`invitation\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_8b65f758fd563971cf97d9d4bad\``);
        await queryRunner.query(`ALTER TABLE \`reviews\` CHANGE \`invitation_id\` \`invitation_id\` bigint NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_8b65f758fd563971cf97d9d4bad\` FOREIGN KEY (\`invitation_id\`) REFERENCES \`invitation\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
