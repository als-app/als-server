import { MigrationInterface, QueryRunner } from "typeorm";

export class alterMediaTable1608118820647 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE media ADD base_path VARCHAR(255) NOT NULL, ADD status TINYINT(4) NOT NULL, ADD user_id BIGINT NOT NULL;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE media DROP base_path, DROP status, DROP user_id;
        `);
    }

}
