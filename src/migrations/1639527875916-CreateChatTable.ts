import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateChatTable1639527875916 implements MigrationInterface {
    name = 'CreateChatTable1639527875916'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`chat\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`created_at\` bigint NOT NULL, \`updated_at\` bigint NOT NULL, \`deleted_at\` bigint NOT NULL, \`message\` varchar(255) NOT NULL, \`invitation_id\` bigint NOT NULL, \`created_by_id\` bigint NOT NULL, \`media_id\` bigint NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`chat\` ADD CONSTRAINT \`FK_5b884afbfd6e4620288c78f902c\` FOREIGN KEY (\`invitation_id\`) REFERENCES \`invitation\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`chat\` ADD CONSTRAINT \`FK_bb0b9f6f9232bfaa3e6c9e64620\` FOREIGN KEY (\`created_by_id\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`chat\` DROP FOREIGN KEY \`FK_bb0b9f6f9232bfaa3e6c9e64620\``);
        await queryRunner.query(`ALTER TABLE \`chat\` DROP FOREIGN KEY \`FK_5b884afbfd6e4620288c78f902c\``);
        await queryRunner.query(`DROP TABLE \`chat\``);
    }

}
