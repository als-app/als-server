import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCategoryIdInPosting1643146944592 implements MigrationInterface {
    name = 'AddCategoryIdInPosting1643146944592'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`posting\` ADD \`category_id\` int NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`posting\` DROP COLUMN \`category_id\``);
    }

}
