import {MigrationInterface, QueryRunner} from "typeorm";

export class SeenAddedInChat1642613743825 implements MigrationInterface {
    name = 'SeenAddedInChat1642613743825'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`chat\` ADD \`seen\` tinyint NOT NULL DEFAULT 0`);
        await queryRunner.query(`update chat set seen=1`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`chat\` DROP COLUMN \`seen\``);
    }

}
