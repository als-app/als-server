import {MigrationInterface, QueryRunner} from "typeorm";

export class AddAboutInUser1638928049915 implements MigrationInterface {
    name = 'AddAboutInUser1638928049915'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` ADD `about` varchar(255) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `about`");
    }

}
