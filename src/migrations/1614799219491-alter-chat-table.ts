import { MigrationInterface, QueryRunner } from "typeorm";

export class alterChatTable1614799219491 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE chat ADD COLUMN status tinyint(2) default 1;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TABLE chat drop column status;
        `);
    }

}
