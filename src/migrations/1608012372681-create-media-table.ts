import { MigrationInterface, QueryRunner } from "typeorm";

export class createMediaTable1608012372681 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(` CREATE TABLE media
        (
            id              BIGINT PRIMARY KEY              NOT NULL AUTO_INCREMENT,
            path            VARCHAR(500)                    NOT NULL ,
            thumb_path      VARCHAR(500)                    ,
            meta            JSON                            ,
            type            TINYINT                            NOT NULL,
            created_at      BIGINT                          NOT NULL,
            updated_at      BIGINT                          NOT NULL,
            deleted_at      BIGINT DEFAULT 0                NOT NULL
        );`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE media;`);
    }
}

