import { MigrationInterface, QueryRunner } from "typeorm";

export class createNotificationTable1610612770653 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        CREATE TABLE notification
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            title               VARCHAR(255)            NOT NULL,
            description         TEXT,
            user_id            BIGINT,
            type              TINYINT(4) DEFAULT 0    NOT NULL,
            read_status              TINYINT(4) DEFAULT 0    NOT NULL,
            meta JSON null,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE notification;`)
    }

}
