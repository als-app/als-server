import { MigrationInterface, QueryRunner } from "typeorm";

export class alterMediaTable1608550886902 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE media MODIFY COLUMN user_id BIGINT;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TABLE media MODIFY COLUMN user_id BIGINT NOT NULL;
        `);
    }

}
