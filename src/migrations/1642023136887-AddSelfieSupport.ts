import {MigrationInterface, QueryRunner} from "typeorm";

export class AddSelfieSupport1642023136887 implements MigrationInterface {
    name = 'AddSelfieSupport1642023136887'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`selfie_media_id\` bigint NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`selfie_media_id\``);
    }

}
