import { MigrationInterface, QueryRunner } from "typeorm";

export class createTablePromotion1614710350824 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        CREATE TABLE promotion
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            title               VARCHAR(255)            NOT NULL,
            description         TEXT,
            media_id            BIGINT,
            status              TINYINT(4) DEFAULT 0    NOT NULL,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE promotion;`)
    }

}
