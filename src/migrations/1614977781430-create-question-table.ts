import { MigrationInterface, QueryRunner } from "typeorm";

export class createQuestionTable1614977781430 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        CREATE TABLE question
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            country               VARCHAR(255)            NOT NULL,
            birthdate               VARCHAR(255)            NOT NULL,
            description         TEXT,
            useR_id            BIGINT,
            gender              TINYINT(4) DEFAULT 0    NOT NULL,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE question;`)
    }

}
