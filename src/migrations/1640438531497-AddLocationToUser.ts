import {MigrationInterface, QueryRunner} from "typeorm";

export class AddLocationToUser1640438531497 implements MigrationInterface {
    name = 'AddLocationToUser1640438531497'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`location\` point NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`location\``);
    }

}
