import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateInvitationTable1639350942016 implements MigrationInterface {
    name = 'CreateInvitationTable1639350942016'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `invitation` (`id` bigint NOT NULL AUTO_INCREMENT, `created_at` bigint NOT NULL, `updated_at` bigint NOT NULL, `deleted_at` bigint NOT NULL, `title` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, `time` timestamp NULL, `location` point NULL, `address` varchar(255) NOT NULL, `created_by_id` bigint NULL, `user_id` bigint NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `invitation` ADD CONSTRAINT `FK_5b173c048c8d6297f7dbf2d853b` FOREIGN KEY (`created_by_id`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `invitation` ADD CONSTRAINT `FK_dbf801c80100c878509602456ff` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `invitation` DROP FOREIGN KEY `FK_dbf801c80100c878509602456ff`");
        await queryRunner.query("ALTER TABLE `invitation` DROP FOREIGN KEY `FK_5b173c048c8d6297f7dbf2d853b`");
        await queryRunner.query("DROP TABLE `invitation`");
    }

}
