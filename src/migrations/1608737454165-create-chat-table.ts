import { MigrationInterface, QueryRunner } from "typeorm";

export class createChatTable1608737454165 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(` CREATE TABLE chat
        (
            id                          BIGINT PRIMARY KEY          NOT NULL AUTO_INCREMENT,
            advisor_user_id              BIGINT                      NOT NULL,
            gig_id              BIGINT                      NOT NULL,
            user_id                     BIGINT                      NOT NULL,
            user_last_read_event_id     BIGINT                      ,
            advisor_last_read_event_id   BIGINT                      ,
            last_event_id               BIGINT                      ,
            created_at                  BIGINT                      NOT NULL,
            updated_at                  BIGINT                      NOT NULL,
            deleted_at                  BIGINT DEFAULT 0            NOT NULL
        );`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE chat;`)
    }

}
