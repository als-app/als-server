import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUserDeviceTable1607336706219 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE user_device
        (
            id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
            user_id  BIGINT(20)                              NOT NULL,
            user_agent  varchar(1000),
            uuid   varchar(255) DEFAULT NULL,
            auth_token       varchar(255) DEFAULT NULL,
            fcm       varchar(1000) DEFAULT NULL,
            created_at  BIGINT   NOT NULL,
            updated_at  BIGINT   NOT NULL,
            deleted_at  BIGINT DEFAULT 0 NOT NULL
        );
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    DROP TABLE user_device;
    `);
  }
}
