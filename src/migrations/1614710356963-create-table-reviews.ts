import { MigrationInterface, QueryRunner } from "typeorm";

export class createTableReviews1614710356963 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE reviews
        (
            id              BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            advisor_id         BIGINT  NOT NULL,
            user_id       BIGINT  NOT NULL,
            rating          TINYINT DEFAULT 5 NOT NULL,
            comment         TEXT,
            created_at      BIGINT   NOT NULL,
            updated_at      BIGINT   NOT NULL,
            deleted_at      BIGINT DEFAULT 0 NOT NULL
        )
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        DROP TABLE reviews;
        `)
    }

}
