import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUserTokenTable1607336712763 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE user_token
        (
            id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
            user_id  BIGINT(20)                              NOT NULL,
            device_id  BIGINT(20)                              DEFAULT NULL,
            uuid   varchar(255) DEFAULT NULL,
            code       varchar(255) DEFAULT NULL,
            meta JSON DEFAULT NULL,
            attempts BIGINT  DEFAULT 0,
            expire_time BIGINT DEFAULT NULL,
            created_at  BIGINT   NOT NULL,
            updated_at  BIGINT   NOT NULL,
            deleted_at  BIGINT DEFAULT 0 NOT NULL
        );
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    DROP TABLE user_token;
    `);
  }
}
