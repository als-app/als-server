import { MigrationInterface, QueryRunner } from "typeorm";

export class createChatEventTable1608737454165 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(` CREATE TABLE chat_event
        (
            id                          BIGINT PRIMARY KEY          NOT NULL AUTO_INCREMENT,
            chat_id                   BIGINT                      NOT NULL,
            sender_id                   BIGINT                      NOT NULL,
            sender_type                 TINYINT(4)                  NOT NULL,
            type                        TINYINT(4)                  NOT NULL,
            content                     TEXT                        ,
            media_id                    BIGINT                      ,
            created_at                  BIGINT                      NOT NULL,
            updated_at                  BIGINT                      NOT NULL,
            deleted_at                  BIGINT DEFAULT 0            NOT NULL
        );`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE chat_event;`)
    }

}
