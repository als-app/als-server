import { MigrationInterface, QueryRunner } from "typeorm";

export class createPostingTable1615067944502 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        CREATE TABLE posting
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            description         TEXT,
            media_id            BIGINT,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE posting;`)
    }

}
