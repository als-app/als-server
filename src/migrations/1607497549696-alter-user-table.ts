import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterUserTable1607497549696 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE user CHANGE status status tinyint(2) DEFAULT 0 after type;
            `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TABLE user CHANGE status status tinyint;
        `);
  }
}
