import { MigrationInterface, QueryRunner } from "typeorm";

export class alterQuestionTable1615126879174 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE question ADD COLUMN chat_id bigint;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TABLE question drop column chat_id;
        `);
    }
}
