import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterUserTableEditName1607336712763 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        ALTER TABLE user CHANGE first_name full_name VARCHAR(255) NOT NULL, DROP COLUMN last_name;
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
    ALTER TABLE user CHANGE full_name first_name VARCHAR(255) NOT NULL, ADD COLUMN last_name VARCHAR(255);
    `);
  }
}
