import { MigrationInterface, QueryRunner } from "typeorm";

export class createTableCategory1613764148096 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE category
            (
                id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
                media_id     BIGINT,
                title   varchar(1000),
                created_at  BIGINT   NOT NULL,
                updated_at  BIGINT   NOT NULL,
                deleted_at  BIGINT DEFAULT 0 NOT NULL
            );
            `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        DROP TABLE category;
        `);
    }

}
