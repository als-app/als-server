import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveExtraInvitationFields1642712731815 implements MigrationInterface {
    name = 'RemoveExtraInvitationFields1642712731815'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`description\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`time\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`location\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`address\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`status\``);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`status\` enum ('pending', 'accepted', 'rejected') NOT NULL DEFAULT 'pending'`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`address\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`location\` point NULL`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`time\` timestamp NULL`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`description\` varchar(255) NOT NULL`);
    }

}
