import { MigrationInterface, QueryRunner } from "typeorm";

export class AddStatusInInvitations1639357751704 implements MigrationInterface {
    name = 'AddStatusInInvitations1639357751704'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`status\` enum ('pending', 'accepted', 'rejected') NOT NULL DEFAULT 'pending'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`status\``);
    }

}
