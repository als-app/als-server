import {MigrationInterface, QueryRunner} from "typeorm";

export class AddIsVerifiedInUser1640901718988 implements MigrationInterface {
    name = 'AddIsVerifiedInUser1640901718988'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`is_verified\` tinyint NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`is_verified\``);
    }

}
