import { MigrationInterface, QueryRunner } from "typeorm";

export class AddRatingInReviews1640522542582 implements MigrationInterface {
    name = 'AddRatingInReviews1640522542582'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD \`rating\` int NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP COLUMN \`rating\``);
    }

}
