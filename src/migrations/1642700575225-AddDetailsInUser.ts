import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDetailsInUser1642700575225 implements MigrationInterface {
    name = 'AddDetailsInUser1642700575225'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`details\` varchar(255) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`details\``);
    }

}
