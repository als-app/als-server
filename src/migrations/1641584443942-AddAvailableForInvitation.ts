import {MigrationInterface, QueryRunner} from "typeorm";

export class AddAvailableForInvitation1641584443942 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`available_for_invitation\` tinyint NOT NULL DEFAULT 1`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`available_for_invitation\``);
    }

}
