import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
    BadRequestException,
    HttpStatus,
    InternalServerErrorException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { LOCALE_HEADER_KEY } from 'src/constant/authentication';
import { Logger } from 'src/helpers/logger.helper';
import { Locale, MessageTemplates, __T } from 'src/locale/locale';
import { FatalErrorException } from './response.exception';

function _prepareBadRequestValidationErrors(errors) {
    let Errors: any = {};
    for (const err of errors) {
        const constraint = Object.values(err.constraints) && Object.values(err.constraints).length && Object.values(err.constraints)[0];
        Errors[err.property] = constraint ? constraint : `${err.property} is invalid`;
    }
    return Errors;
}
@Catch(HttpException, Error)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException | Error, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response: any = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const locale = request.header[LOCALE_HEADER_KEY] || Locale.En;
        if (!(exception instanceof HttpException)) {
            Logger.Fatal(exception.stack ? exception.stack : exception, "ERROR")
            let ResponseToSend = {
                Status: HttpStatus.INTERNAL_SERVER_ERROR,
                Message: __T(locale, MessageTemplates.InternalServerError),
                Data: null
            };
            response.__ss_body = ResponseToSend;
            response.status(HttpStatus.OK).json(ResponseToSend);
            return;
        }
        const status = exception.getStatus();
        const exceptionResponse: any = exception.getResponse();
        if (
            exception instanceof BadRequestException &&
            exceptionResponse.message &&
            Array.isArray(exceptionResponse.message)
        ) {
            let ResponseToSend = {
                Status: status,
                Message: __T(locale, MessageTemplates.InvalidValueFor, {
                    values: exceptionResponse.message.map((x) => x.property).join(', '),
                }),
                Data: null,
                Errors: _prepareBadRequestValidationErrors(exceptionResponse.message)
            };
            response.__ss_body = ResponseToSend;
            response.status(HttpStatus.OK).json(ResponseToSend);
        } else {
            let ResponseToSend = {
                Status: status,
                Message: __T(locale, exceptionResponse.key || MessageTemplates.NotFoundError, exceptionResponse.data),
                Data: null,
            }
            response.__ss_body = ResponseToSend;
            response.status(HttpStatus.OK).json(ResponseToSend);
        }
    }
}
