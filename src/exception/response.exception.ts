import { HttpException, HttpStatus } from "@nestjs/common";
import { MessageTemplates } from "src/locale/locale";


export class NotFoundException extends HttpException {
  constructor(key: MessageTemplates, data?: any) {
    super({ key, data }, HttpStatus.NOT_FOUND);
  }
}
export class UnAuthorizedException extends HttpException {
  constructor(key: MessageTemplates, data?: any) {
    super({ key, data }, HttpStatus.UNAUTHORIZED);
  }
}
export class BadRequestException extends HttpException {
  constructor(key: MessageTemplates, data?: any) {
    super({ key, data }, HttpStatus.BAD_REQUEST);
  }
}
export class ForbiddenException extends HttpException {
  constructor(key: MessageTemplates, data?: any) {
    super({ key, data }, HttpStatus.FORBIDDEN);
  }
}
export class FatalErrorException extends HttpException {
  constructor(key: MessageTemplates, data?: any) {
    super({ key, data }, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
