import { Get } from "@nestjs/common";
import { AppService } from "./app.service";
import { AppController as BaseController } from "./decorator/appcontroller.decorator";
import { appEnv } from "./helpers/env.helper";

@BaseController()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  HealthCheck(): { Message: string } {
    return { Message: `${appEnv("APP_NAME", "NestJS")} Api` };
  }
}
