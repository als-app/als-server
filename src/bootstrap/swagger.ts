import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { getFromContainer, MetadataStorage } from "class-validator";
import { validationMetadatasToSchemas } from "class-validator-jsonschema";
import { AUTHORIZATION_HEADER_KEY } from "src/constant/authentication";
import { appEnv } from "src/helpers/env.helper";
import { Loadable } from "./bootstrap";

export class Swagger implements Loadable {
    public async Load(app: NestExpressApplication) {
        const options = new DocumentBuilder()
            .setTitle(`Event Buddy API`)
            .setDescription('API descriptions')
            .setVersion('1.0')
            .addSecurity(AUTHORIZATION_HEADER_KEY, {
                type: 'apiKey',
                description: 'Api Authorization',
                name: AUTHORIZATION_HEADER_KEY,
                in: 'header',
            })
            .build();
        const document = SwaggerModule.createDocument(app, options);
        const metadata = (getFromContainer(MetadataStorage) as any)
            .validationMetadatas;
        document.components.schemas = Object.assign(
            {},
            document.components.schemas || {},
            validationMetadatasToSchemas(metadata),
        );
        SwaggerModule.setup('api', app, document, { customCss: '@import url("http://3.87.183.56:3000/api/swagger-ui.css");' });
    };
}