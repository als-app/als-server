import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface"
import { AppModule } from "src/app.module";
import { App } from "../app";
import { Swagger } from "./swagger";

export interface Constructable<T> {
    new(): T;
}

export interface Loadable {
    Load: (app?: NestExpressApplication) => {};
}

export class Bootstrap {
    private _loaders: Array<Constructable<Loadable>> = [Swagger]

    private async _load(app: NestExpressApplication): Promise<void> {
        for (const loader of this._loaders) {
            let LoaderInstance: Loadable = new loader();
            await LoaderInstance.Load(app);
        }
    }

    public static async boot(app: NestExpressApplication): Promise<void> {
        let bootstrap = new Bootstrap();
        await bootstrap._load(app);
    }
}