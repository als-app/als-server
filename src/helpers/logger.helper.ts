import * as Chalk from "chalk"
import * as moment from "moment-timezone";
import { Logger as IQueryLogger } from "typeorm"
import * as Morgan from "morgan";
import { LogLevel } from "src/constant/logging";
import { AllowDateFormat } from "./date.helper";
import { appEnv } from "./env.helper";
import { AUTHORIZATION_HEADER_KEY } from "src/constant/authentication";

export class Logger {
    private static logTemplate = {
        [LogLevel.TRACE]: Chalk.greenBright,
        [LogLevel.DEBUG]: Chalk.whiteBright,
        [LogLevel.INFO]: Chalk.blueBright,
        [LogLevel.WARN]: Chalk.magenta,
        [LogLevel.ERROR]: Chalk.redBright,
        [LogLevel.FATAL]: Chalk.bgRed
    };

    private static queryLogger: IQueryLogger = null;
    private static loggerMiddleware: Function = null;

    private static log(logLevel: LogLevel, data: any, tag?: string) {
        if (appEnv("LOG_LEVEL", LogLevel.DEBUG) > logLevel) {
            return;
        }

        if (typeof data === "object") {
            let str = JSON.stringify(data, null, 4);
            if (str != "{}") {
                data = str;
            }
        }

        let date = moment();
        if (tag !== undefined) {
            console.log(`[${Chalk.yellowBright(appEnv("APP_NAME", "Nest"))}][${Chalk.yellowBright(date.format(AllowDateFormat.DateTime))}]  ` + Chalk.bold.underline.white(tag), this.logTemplate[logLevel](data));
        } else {
            console.log(`[${Chalk.yellowBright(appEnv("APP_NAME", "Nest"))}][${Chalk.yellowBright(date.format(AllowDateFormat.DateTime))}]  ` + this.logTemplate[logLevel](data));
        }
    }

    public static Trace(data: any, tag?: string) {
        this.log(LogLevel.TRACE, data, tag);
    }
    public static Debug(data: any, tag?: string) {
        this.log(LogLevel.DEBUG, data, tag);
    }
    public static Info(data: any, tag?: string) {
        this.log(LogLevel.INFO, data, tag);
    }
    public static Warn(data: any, tag?: string) {
        this.log(LogLevel.WARN, data, tag);
    }
    public static Error(data: any, tag?: string) {
        this.log(LogLevel.ERROR, data, tag);
    }
    public static Fatal(data: any, tag?: string) {
        this.log(LogLevel.FATAL, data, tag);
    }

    public static GetQueryLogger(): IQueryLogger {
        if (Logger.queryLogger === null) {
            let LogSqlQuery = (query, param) => {
                this.Trace(query, "Query");
                if (param) {
                    this.Trace(param, "Parameters");
                }
            };
            Logger.queryLogger = {
                log: (level, msg, queryRunner) => {
                    switch (level) {
                        case "info":
                            this.Info(msg, "TypeORM");
                            break;
                        case "log":
                            this.Debug(msg, "TypeORM");
                            break;
                        case "warn":
                            this.Warn(msg, "TypeORM");
                    }
                },
                logMigration: (msg, queryRunner) => {
                    this.Info(msg, "TypeORM");
                },
                logQuery: (query, param) => {
                    LogSqlQuery(query, param);
                },
                logQueryError: (err, query, param) => {
                    this.Error(err, "Query");
                    LogSqlQuery(query, param);
                },
                logQuerySlow: (time, query, param) => {
                    this.Warn(time, "Slow Query");
                    LogSqlQuery(query, param);
                },
                logSchemaBuild: (msg) => {
                    this.Info(msg, "TypeORM");
                }
            };

            return Logger.queryLogger;
        }
    }

    public static GetLoggerMiddleware() {
        if (Logger.loggerMiddleware === null) {
            let LoggerFormatStr =
                ":date[iso] :method :status :response-time ms :res[content-length] HTTP/:http-version :remote-addr :url :referrer :user-agent";

            if (appEnv("DEBUG", false)) {
                Morgan.token("authorization", (req: any, res: any): string => {
                    return req.headers[AUTHORIZATION_HEADER_KEY] as string;
                });

                Morgan.token("body", (req: any, res: any) => {
                    return req.body ? JSON.stringify(req.body, null, 4) : "";
                });


                Morgan.token("query", (req: any, res: any): string => {
                    return req.query
                        ? JSON.stringify(req.query, null, 4)
                        : "";
                });

                Morgan.token("params", (req: any, res: any): string => {
                    return req.params
                        ? JSON.stringify(req.params, null, 4)
                        : "";
                });

                Morgan.token("responsebody", (req: any, res: any): string => {
                    let str = "";

                    if ((res as any).__ss_body) {
                        try {
                            // to avoid runtime exception for not json response
                            str =
                                JSON.stringify((res as any).__ss_body, null, 4)
                                ;
                        } catch (e) {
                            str = (res as any).__ss_body;
                        }
                    }
                    return str;
                });

                LoggerFormatStr =
                    ":date[iso] :method :status :response-time ms :res[content-length] HTTP/:http-version :remote-addr :url :referrer :user-agent\nPath Params :params\nQuery Params :query\nRequest Body :body\nResponse Body :responsebody";
            }

            Logger.loggerMiddleware = Morgan(LoggerFormatStr, {
                stream: {
                    write: str => {
                        this.Info(str);
                    }
                }
            })
        }

        return Logger.loggerMiddleware;
    }
}