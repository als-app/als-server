import { App } from "src/app";
import { QueueJobs } from "src/constant/queue";
import { NotificationService } from "src/modules/notification/notification.service";
import { Logger } from "./logger.helper";
import { SSMailer } from "./mail.helper";
import { SSQueue } from "./queue.helper";



export class QueueProcessor {
    public static async ProcessQueue() {
        const keys = Object.keys(QueueJobs);
        let handler;
        for (let i = 0; i < keys.length; i++) {
            switch (keys[i]) {
                case QueueJobs.EMAIL:
                    handler = QueueProcessor.ProcessEmail;
                    break;
                case QueueJobs.NOTIFICATION:
                    handler = QueueProcessor.ProcessNotification;
                    break;
                default:
                    break;
            }
            if (handler) {
                SSQueue.Process(keys[i], handler);
            }
        }
    }

    public static async ProcessEmail(data, done) {
        if (data) {
            try {
                await SSMailer.SendByTemplate(data.EmailTemplate, data.Email, data.Locale, data.EmailData)
                done();
            } catch (err) {
                Logger.Error(err);
                done(new Error("Email not sent"));
            }
        } else {
            done(new Error("Email not sent"));
        }
    }

    public static async ProcessNotification(data, done) {
        if (data) {
            try {
                let notificationService = (await App.GetNestApplicationInstance()).get(NotificationService);
                await notificationService.ProcessNotification(data.Type, data);
                done();
            } catch (err) {
                Logger.Error(err);
                done(new Error("Notification not sent"));
            }
        } else {
            done(new Error("Notification not sent"));
        }
    }
}
