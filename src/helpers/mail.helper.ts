import { createTransport } from 'nodemailer';
import { appEnv } from './env.helper';
import * as Mail from 'nodemailer/lib/mailer';
import * as aws from 'aws-sdk';
import { AWSConfig } from '../config/file.config';
import { Logger } from './logger.helper';
import { EmailTemplate, TemplateRenderer } from 'src/templates/template_renderer';
import { Locale } from 'moment-timezone';


aws.config.update(AWSConfig);

export class SSMailer {
  private _transporter: Mail = null;
  private static _instance = null;

  private constructor() {
    let credentials;
    if (appEnv('USE_SMTP_CREDENTIALS', false)) {
      credentials = {
        host: appEnv('MAIL_HOST', 'smtp.mailgun.org'),
        port: appEnv('MAIL_PORT', 25),
        auth: {
          user: appEnv('MAIL_USER', null),
          pass: appEnv('MAIL_PASS', null),
        },
        service: 'smtp',
        secure: appEnv('MAIL_SECURE', true),
        pool: true,
        from: appEnv('MAIL_FROM_EMAIL', 'no-reply@abc.com'),
      };
    } else {
      credentials = {
        SES: new aws.SES({
          apiVersion: '2010-12-01',
        }),
      };
    }
    this._transporter = createTransport(credentials);
  }

  public static GetInstance(): SSMailer {
    if (this._instance === null) {
      this._instance = new this();
    }
    return this._instance;
  }

  public static async Send(
    to: string,
    subject: string,
    content,
    attachments?,
  ): Promise<boolean> {
    try {
      let message = {
        to: to,
        subject: subject,
        html: content,
      };

      if (!appEnv('USER_SMTP_CREDENTIALS', false)) {
        message['from'] = appEnv('MAIL_FROM_EMAIL', 'EBAPP84@GMAIL.COM');

      }

      if (attachments && Array.isArray(attachments) && attachments.length > 0) {
        message['attachments'] = attachments;
      }

      let result = await this.GetInstance()._transporter.sendMail(message);

      Logger.Info('Mail Sent successfully', 'MAILER');
      return true;
    } catch (e) {
      Logger.Error(e, 'MAILER');
    }
    return false;
  }

  public static async SendByTemplate(template: EmailTemplate, to, locale: Locale, data: Object) {
    let { content, subject } = TemplateRenderer[template](data, locale);
    this.Send(to, subject, content);
  }
}
