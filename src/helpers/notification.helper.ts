import * as FCM from "fcm-node";
import { appEnv } from "./env.helper";

let fcm = null;

function getFcmInstance() {
  if (fcm == null) {
    fcm = new FCM(appEnv("FCM_SERVER_KEY", "sscccdcdsccdsc6516"));
  }

  return fcm;
}

export function SendNotification(fcmToken: string, title: string, body: string, data: any = {}) {
  return new Promise((resolve, reject) => {
    const message = {
      to: fcmToken,
      notification: {
        title,
        body,
        sound: "default"
      },
      data,
      priority: "high"
    };
    getFcmInstance().send(message, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
