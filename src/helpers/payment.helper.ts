import { Stripe as stripe } from "stripe";
import { appEnv } from "./env.helper";

const Stripe = new stripe(appEnv("STRIPE_SECRET_KEY", ""), { apiVersion: "2020-08-27" });

export enum WebhookEvents {
    ChargeSucceeded = "charge.succeeded",
    ChargeFailed = "charge.failed",
    SetupIntentSucceeded = "setup_intent.succeeded",
    PaymentIntentSucceeded = "payment_intent.succeeded",
    PaymentIntentFailed = "payment_intent.payment_failed"
}

export enum ChargeFailErrorCodes {
    AuthenticationRequired = "authentication_required",
    CardDeclined = "card_declined"
}

export async function CreateCustomer(paymentToken: string, email: string, metaData, description?: string) {
    return await Stripe.customers.create({
        source: paymentToken,
        metadata: metaData,
        email: email,
        ...(description && { description: description })
    });
}

export async function UpdateCustomer(customerId: string, paymentToken: string, metaData) {
    return await Stripe.customers.update(customerId, {
        source: paymentToken,
        metadata: metaData
    });
}

export async function RetreiveCustomer(customerId: string) {
    return await Stripe.customers.retrieve(customerId);
}

export async function CreatePaymentIntent(amount: number, currency: string, customerId: string, paymentMethod: string, metaData) {
    return await Stripe.paymentIntents.create({
        amount: amount,
        currency: currency,
        customer: customerId,
        payment_method_types: ["card"],
        payment_method: paymentMethod,
        off_session: true,
        confirm: true,
        metadata: metaData
    });
}

export async function Charge(customerId: string, amount: number, currency: string, metaData) {
    return await Stripe.charges.create({
        amount: amount,
        currency: currency,
        customer: customerId,
        metadata: metaData,
        expand: ["balance_transaction"]
    });
}

export async function CreateSession(successUrl: string, cancelUrl: string) {
    return await Stripe.checkout.sessions.create({
        payment_method_types: ["card"],
        mode: "setup",
        success_url: successUrl,
        cancel_url: cancelUrl
    });
}

export async function AttachPaymentMethod(paymentMethod: string, customerId: string) {
    return await Stripe.paymentMethods.attach(paymentMethod, {
        customer: customerId
    });
}

export async function RetreivePaymentMethod(paymentMethod: string) {
    return await Stripe.paymentMethods.retrieve(paymentMethod);
}

export async function RetreiveBalanceTransaction(transactionId: string) {
    return await Stripe.balanceTransactions.retrieve(transactionId);
}

export function ConstructWebhookEvent(data: any, signature: string) {
    return Stripe.webhooks.constructEvent(data, signature, appEnv("STRIPE_WEBHOOK_SIGNATURE", ""));
}
