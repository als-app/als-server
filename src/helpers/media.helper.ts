import * as multer from "multer"; //Todo: Find better way to implement
import { MediaModelType } from "src/modules/media/media.entity";
import { v4 as uuid } from 'uuid';
import * as aws from "aws-sdk"
import { appEnv } from "./env.helper";
import { AWSConfig } from "src/config/file.config";

export const MediaMaxSize = {
    [MediaModelType.Image]: 6 * 1024 * 1024,     //  6 mb
    [MediaModelType.Video]: 25 * 1024 * 1024    //  25 mb
}

const AllowedMediaExtensions = [
    "avi",
    "mp4",
    "mov",
    "flv",
    "gif",
    "mp3",
    "wav",
    "jpg",
    "jpeg",
    "png"
];

export const IsMediaSizeAllowed = (size: number, type: MediaModelType) => {
    return size <= MediaMaxSize[type];
}

export const IsMediaExtensionAllowed = (extension: string) => {
    return AllowedMediaExtensions.includes(extension);
}

export const GetMediaType = (mime: string): MediaModelType => {
    let type: MediaModelType = MediaModelType.Image;
    if (mime.includes("video")) {
        type = MediaModelType.Video;
    }
    if (mime.includes("audio")) {
        type = MediaModelType.Audio;
    }
    return type;
}

export const GetMediaExtension = (fileName: string) => {
    return fileName.slice(((fileName.lastIndexOf(".") - 1) >>> 0) + 2).toLowerCase();
}

export const CreateUniqueFileName = (extension: string) => {
    return uuid() + "." + extension;
}

export const CreateFilePath = (fileName: string, prependPath?: string) => {
    let date = new Date();
    let name = (prependPath ? prependPath + "/" : "")
        + date.getFullYear() + "/"
        + (date.getMonth() + 1) + "/"
        + date.getDate() + "/"
        + date.getTime() + date.getMilliseconds() + "-"
        + fileName.toLowerCase().replace(/ /g, "-").replace(/[^\w-.]+/g, "");

    return name;
}

export const GetTemporaryCredentialsForMediaUpload = async (path: string, mediaId: number) => {
    aws.config.update(AWSConfig);
    let params: aws.STS.AssumeRoleRequest = {
        RoleArn: appEnv("AWS_STS_ROLE_ARN", ""),
        RoleSessionName: "mediaupload-" + mediaId,
        DurationSeconds: 60 * 60,
        Policy: JSON.stringify({
            Version: "2012-10-17",
            Statement: [
                {
                    Sid: "VisualEditor0",
                    Effect: "Allow",
                    Action: [
                        "s3:PutObject",
                        "s3:AbortMultipartUpload",
                        "s3:PutObjectAcl",
                        "s3:GetObject",
                        "s3:ListMultipartUploadParts"
                    ],
                    Resource: "arn:aws:s3:::" + appEnv("AWS_BUCKET_NAME", "") + "/" + path
                }
            ]
        })
    };
    let { Credentials } = await new aws.STS().assumeRole(params).promise();

    return {
        AccessKeyId: Credentials.AccessKeyId,
        SecretAccessKey: Credentials.SecretAccessKey,
        SessionToken: Credentials.SessionToken,
        Path: path
    };
}

export const FileUploadOptions = {
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, "./public/attachments/");
        },
        filename: (req, file, cb) => {
            file.uniqueName =
                Math.random()
                    .toString()
                    .split(".")[1] +
                Date.now() +
                file.originalname.substr(file.originalname.length - 30 >= 0 ? file.originalname.length - 30 : 0);
            cb(null, file.uniqueName, file);
        }
    })
};
