import * as twilio from "twilio";
import { isValidNumber } from "libphonenumber-js";
import { appEnv } from "./env.helper";

export class TwilioHelper {

    private static _client = null;
    private static _apiUrl = null;

    private static GetTwilioClient() {
        if (TwilioHelper._client === null) {
            TwilioHelper._client = twilio(appEnv("TWILIO_ACCOUNT_SID", ""), appEnv("TWILIO_AUTH_TOKEN", ""));
        }

        return TwilioHelper._client;
    }

    private static GetApiUrl() {
        if (TwilioHelper._apiUrl === null) {
            TwilioHelper._apiUrl = appEnv("API_URL", "");
        }

        return TwilioHelper._apiUrl;
    }


    static async Message(To, From, Body) {
        let sms = await TwilioHelper.GetTwilioClient().messages.create({
            body: Body,
            from: From,
            to: To,
            statusCallback: `${TwilioHelper.GetApiUrl()}/callback/message/status`
        });
        return sms;
    }

}
