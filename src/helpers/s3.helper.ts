import * as aws from 'aws-sdk';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';
import { appEnv } from './env.helper';
import { AWSConfig } from '../config/file.config';

aws.config.update(AWSConfig);
const S3 = new aws.S3();

export const S3FileUploadOptions = multer({
    storage: multerS3({
        s3: S3,
        bucket: appEnv('AWS_BUCKET_NAME', ''),
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            let ChatId = req.query.ChatId;
            let Path: string = '';
            let fileName =
                Math.random().toString().split('.')[1] +
                Date.now() +
                file.originalname.substr(
                    file.originalname.length - 30 >= 0
                        ? file.originalname.length - 30
                        : 0,
                );
            fileName = fileName.replace(/\s/g, '').replace(/[^\w-.]+/g, '');
            if (ChatId) {
                Path = `data/${ChatId}/${fileName}`;
            } else {
                Path = `data/${fileName}`;
            }
            cb(null, Path);
        },
    }),
    limits: {
        fileSize: 2 * 1024 * 1024 * 1024,
    },
});

export const DeleteFile = function (fileName: string) {
    return new Promise(function (resolve, reject) {
        var params = {
            Bucket: appEnv('AWS_BUCKET_NAME', ''),
            Key: decodeURIComponent(fileName),
        };
        S3.deleteObject(params, function (err, data) {
            if (data) {
                resolve(data);
            } else {
                reject(err);
            }
        });
    });
};

export const GetFile = function (fileName: string) {
    return new Promise(function (resolve, reject) {
        var params = {
            Bucket: appEnv('AWS_BUCKET_NAME', ''),
            Key: decodeURIComponent(fileName),
        };
        S3.getObject(params).send(function (err, data) {
            if (data && data.Body) {
                resolve(data.Body);
            } else {
                reject(err);
            }
        });
    });
};

export const GetSignedUrl = function (
    fileName: string,
    expirationTime: number,
) {
    return new Promise(function (resolve, reject) {
        var params = {
            Bucket: appEnv('AWS_BUCKET_NAME', ''),
            Key: decodeURIComponent(fileName),
            Expires: expirationTime,
        };

        S3.getSignedUrl('getObject', params, function (err, url) {
            if (url) {
                resolve(url);
            } else {
                reject(err);
            }
        });
    });
};

export const UploadFile = function (fileName: string, content: any) {
    return new Promise(function (resolve, reject) {
        fileName =
            Math.random().toString().split('.')[1] +
            Date.now() +
            fileName.substr(fileName.length - 30 >= 0 ? fileName.length - 30 : 0);
        fileName = fileName.replace(/\s/g, '').replace(/[^\w-.]+/g, '');
        var fileKey: string = `data/${fileName}`;
        var params = {
            Bucket: appEnv('AWS_BUCKET_NAME', ''),
            Key: fileKey,
            ACL: 'private',
            Body: content,
        };

        S3.putObject(params, function (err, data) {
            if (data) {
                resolve(fileKey);
            } else {
                reject(err);
            }
        });
    });
};

export const GetObjectHeaders = async function (key: string) {
    let result = await S3.headObject({
        Bucket: appEnv("AWS_BUCKET_NAME", ""),
        Key: key
    }).promise();

    return {
        ContentType: result.ContentType,
        ContentLength: result.ContentLength
    }
}