import * as Kue from "kue";
import { appEnv } from "./env.helper";
import { Logger } from "./logger.helper";

export class SSQueue {
    private static _instance = null;
    private _queue = (() => {
        let q = Kue.createQueue({
            prefix: "ss",
            redis: {
                host: appEnv("DB_REDIS_HOST", "localhost"),
                port: appEnv("DB_REDIS_PORT", 6379),
                db: 3 // if provided select a non-default redis db
            }
        });
        q.watchStuckJobs(1000);
        return q;
    })();
    private constructor() { }

    public static GetInstance(): SSQueue {
        if (this._instance === null) {
            this._instance = new this();
            SSQueue.HandlingQueueErrors();
        }
        return this._instance;
    }

    public static async Enqueue(task: string, data: any, attemptsCount = 1, delay?: number) {
        let job = this.GetInstance()
            ._queue.createJob(task, data)
            .attempts(attemptsCount)
            .ttl(parseInt(appEnv("QUEUE_JOBS_TTL", "360000")))
            .removeOnComplete(true);

        if (delay) {
            job = job.delay(delay);
        }

        job.save(err => {
            if (err) {
                Logger.Error(err, "QUEUE");
                return Promise.reject(err);
            }
            return Promise.resolve();
        });
    }

    public static Process(task: string, callback) {
        try {
            this.GetInstance()._queue.process(task, async function (job, done) {
                callback(job.data, done);
            });
        } catch (err) {
            Logger.Error(err, "QUEUE");
        }
    }

    public static HandlingQueueErrors() {
        this.GetInstance()._queue.on("error", function (error) {
            // Logger.Error(error, "QUEUE");
        });
    }
}
