"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostingRepository = void 0;
const util_helper_1 = require("../../helpers/util.helper");
const user_entity_1 = require("../auth/user.entity");
const media_entity_1 = require("../media/media.entity");
const mysql_repository_1 = require("../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const posting_entity_1 = require("./posting.entity");
let PostingRepository = class PostingRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = 'posting';
    }
    _getFindQueryBuilder() {
        let QueryBuilder = this.createQueryBuilder(this.DefaultAlias)
            .leftJoinAndMapOne("posting.Media", media_entity_1.MediaModel, "media", "posting.media_id = media.id");
        return QueryBuilder;
    }
    async Find(whereParams, paginationOptions, orderByOptions) {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        QueryBuilder = this.ApplyPaginationOnQueryBuilder(QueryBuilder, paginationOptions);
        QueryBuilder = this.ApplyOrderOnQueryBuilder(QueryBuilder, orderByOptions);
        return await QueryBuilder.getMany();
    }
    async Count(whereParams) {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        return await QueryBuilder.getCount();
    }
};
PostingRepository = __decorate([
    typeorm_1.EntityRepository(posting_entity_1.PostingModel)
], PostingRepository);
exports.PostingRepository = PostingRepository;
//# sourceMappingURL=posting.repository.js.map