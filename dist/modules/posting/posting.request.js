"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePostingRequest = exports.CreatePostingRequest = exports.FindPostingRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const is_in_enum_decorator_1 = require("../../decorator/is_in_enum.decorator");
class FindPostingRequest {
}
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], FindPostingRequest.prototype, "Title", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindPostingRequest.prototype, "Page", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindPostingRequest.prototype, "Limit", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindPostingRequest.prototype, "Before", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindPostingRequest.prototype, "After", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindPostingRequest.prototype, "CategoryId", void 0);
exports.FindPostingRequest = FindPostingRequest;
class CreatePostingRequest {
}
__decorate([
    class_validator_1.Length(1),
    __metadata("design:type", String)
], CreatePostingRequest.prototype, "Description", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsInt(),
    class_validator_1.Min(1),
    __metadata("design:type", Number)
], CreatePostingRequest.prototype, "MediaId", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], CreatePostingRequest.prototype, "CategoryId", void 0);
exports.CreatePostingRequest = CreatePostingRequest;
class UpdatePostingRequest {
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1),
    __metadata("design:type", String)
], UpdatePostingRequest.prototype, "Description", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsInt(),
    class_validator_1.Min(1),
    __metadata("design:type", Number)
], UpdatePostingRequest.prototype, "MediaId", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdatePostingRequest.prototype, "CategoryId", void 0);
exports.UpdatePostingRequest = UpdatePostingRequest;
//# sourceMappingURL=posting.request.js.map