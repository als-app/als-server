import { MediaResponseModel } from "src/modules/media/media.response";
export declare class PromotionResponseModel {
    VendorId: number;
    Title: string;
    Description: string;
    MediaId: number;
    Status: number;
    Media: MediaResponseModel;
}
export declare class FindPromotionResponse {
    Promotions: Array<PromotionResponseModel>;
    TotalPromotions: number;
}
export declare class CreatePromotionResponse {
    Promotion: PromotionResponseModel;
    Message: string;
}
export declare class UpdatePromotionResponse {
    Promotion: PromotionResponseModel;
    Message: string;
}
