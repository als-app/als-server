import { OrderByRequestParams, PaginationDBParams } from "src/helpers/util.helper";
import { MySqlRepository } from "src/repository/mysql.repository";
import { PostingModel } from "./posting.entity";
export declare class PostingRepository extends MySqlRepository<PostingModel> {
    protected DefaultAlias: string;
    private _getFindQueryBuilder;
    Find(whereParams: any, paginationOptions?: PaginationDBParams, orderByOptions?: OrderByRequestParams): Promise<Array<PostingModel>>;
    Count(whereParams: any): Promise<number>;
}
