"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostingService = void 0;
const common_1 = require("@nestjs/common");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const response_exception_1 = require("../../exception/response.exception");
const env_helper_1 = require("../../helpers/env.helper");
const util_helper_1 = require("../../helpers/util.helper");
const locale_1 = require("../../locale/locale");
const user_entity_1 = require("../auth/user.entity");
const media_repository_1 = require("../media/media.repository");
const response_schema_1 = require("../../response/response.schema");
const posting_entity_1 = require("./posting.entity");
const posting_repository_1 = require("./posting.repository");
let PostingService = class PostingService {
    constructor(_postingRepository, _mediaRepository) {
        this._postingRepository = _postingRepository;
        this._mediaRepository = _mediaRepository;
    }
    async Find(data) {
        let WhereParams = {};
        if (data === null || data === void 0 ? void 0 : data.CategoryId) {
            WhereParams["CategoryId"] = data.CategoryId;
        }
        let Postings = await this._postingRepository.Find(WhereParams, util_helper_1.GetPaginationOptions(data));
        let TotalPostings = await this._postingRepository.Count(WhereParams);
        return { Postings, TotalPostings };
    }
    async Create(data, user) {
        let Posting = new posting_entity_1.PostingModel();
        Posting.Description = data.Description;
        Posting.CategoryId = data.CategoryId;
        if (data.MediaId) {
            let Media = await this._mediaRepository.FindById(data.MediaId);
            if (!Media) {
                throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.MediaNotFound);
            }
            Posting.MediaId = data.MediaId;
        }
        Posting = await this._postingRepository.Create(Posting);
        return { Message: "Success", Posting };
    }
    async Update(id, data, user) {
        let Posting = await this._postingRepository.FindById(id);
        if (!Posting) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.NotFoundError);
        }
        if (data.MediaId && Posting.MediaId !== data.MediaId) {
            let Media = await this._mediaRepository.FindById(data.MediaId);
            if (!Media) {
                throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.MediaNotFound);
            }
        }
        await Posting.Fill(data);
        Posting = await this._postingRepository.Save(Posting);
        return { Message: "updated", Posting };
    }
    async Delete(id, user) {
        let Posting = await this._postingRepository.FindById(id);
        if (!Posting) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.NotFoundError);
        }
        await this._postingRepository.DeleteById(Posting.Id);
        return { Message: "deleted" };
    }
    async Get(id, user) {
        let Posting = await this._postingRepository.FindOne({ Id: id });
        return { Posting };
    }
};
PostingService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [posting_repository_1.PostingRepository, media_repository_1.MediaRepository])
], PostingService);
exports.PostingService = PostingService;
//# sourceMappingURL=posting.service.js.map