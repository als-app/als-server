import { UserModel } from "src/modules/auth/user.entity";
import { MediaRepository } from "src/modules/media/media.repository";
import { PostingModel } from "./posting.entity";
import { PostingRepository } from "./posting.repository";
import { CreatePostingRequest, FindPostingRequest, UpdatePostingRequest } from "./posting.request";
export declare class PostingService {
    private _postingRepository;
    private _mediaRepository;
    constructor(_postingRepository: PostingRepository, _mediaRepository: MediaRepository);
    Find(data: FindPostingRequest): Promise<any>;
    Create(data: CreatePostingRequest, user: UserModel): Promise<{
        Message: string;
        Posting: PostingModel;
    }>;
    Update(id: number, data: UpdatePostingRequest, user: UserModel): Promise<{
        Message: string;
        Posting: PostingModel;
    }>;
    Delete(id: number, user: UserModel): Promise<{
        Message: string;
    }>;
    Get(id: number, user: UserModel): Promise<{
        Posting: PostingModel;
    }>;
}
