import { MySQLBaseModel } from "src/model/mysql.entity.";
import { UserModel } from "src/modules/auth/user.entity";
import { MediaModel } from "src/modules/media/media.entity";
export declare class PostingModel extends MySQLBaseModel {
    Description: string;
    MediaId: number;
    CategoryId: number;
    Media: MediaModel;
    User: UserModel;
}
