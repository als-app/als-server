export declare class FindPostingRequest {
    Title: string;
    Page: number;
    Limit: number;
    Before: number;
    After: number;
    CategoryId: number;
}
export declare class CreatePostingRequest {
    Description: string;
    MediaId: number;
    CategoryId: number;
}
export declare class UpdatePostingRequest {
    Description: string;
    MediaId: number;
    CategoryId: number;
}
