"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePromotionResponse = exports.CreatePromotionResponse = exports.FindPromotionResponse = exports.PromotionResponseModel = void 0;
const swagger_1 = require("@nestjs/swagger");
const user_response_1 = require("../auth/user.response");
const media_response_1 = require("../media/media.response");
class PromotionResponseModel {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], PromotionResponseModel.prototype, "VendorId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], PromotionResponseModel.prototype, "Title", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], PromotionResponseModel.prototype, "Description", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], PromotionResponseModel.prototype, "MediaId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], PromotionResponseModel.prototype, "Status", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", media_response_1.MediaResponseModel)
], PromotionResponseModel.prototype, "Media", void 0);
exports.PromotionResponseModel = PromotionResponseModel;
class FindPromotionResponse {
}
__decorate([
    swagger_1.ApiProperty({ isArray: true, type: PromotionResponseModel }),
    __metadata("design:type", Array)
], FindPromotionResponse.prototype, "Promotions", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], FindPromotionResponse.prototype, "TotalPromotions", void 0);
exports.FindPromotionResponse = FindPromotionResponse;
class CreatePromotionResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", PromotionResponseModel)
], CreatePromotionResponse.prototype, "Promotion", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], CreatePromotionResponse.prototype, "Message", void 0);
exports.CreatePromotionResponse = CreatePromotionResponse;
class UpdatePromotionResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", PromotionResponseModel)
], UpdatePromotionResponse.prototype, "Promotion", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UpdatePromotionResponse.prototype, "Message", void 0);
exports.UpdatePromotionResponse = UpdatePromotionResponse;
//# sourceMappingURL=posting.response.js.map