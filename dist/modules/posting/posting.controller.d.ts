import { MessageResponse } from "src/app.response";
import { UserModel } from "src/modules/auth/user.entity";
import { CreatePostingRequest, FindPostingRequest, UpdatePostingRequest } from "./posting.request";
import { FindPromotionResponse } from "./posting.response";
import { PostingService } from "./posting.service";
export declare class PostingController {
    private _promotionService;
    constructor(_promotionService: PostingService);
    FindPromotions(data: FindPostingRequest): Promise<FindPromotionResponse>;
    Create(data: CreatePostingRequest, user: UserModel): Promise<any>;
    Update(id: number, data: UpdatePostingRequest, user: UserModel): Promise<any>;
    Delete(id: number, user: UserModel): Promise<MessageResponse>;
    Get(id: number, user: UserModel): Promise<{
        Posting: import("./posting.entity").PostingModel;
    }>;
}
