import { MySQLBaseModel } from "src/model/mysql.entity.";
import { UserModel } from '../auth/user.entity';
import { InvitationModel } from '../invitation/invitation.entity';
export declare class ReviewModel extends MySQLBaseModel {
    Description: string;
    Title: string;
    Rating: number;
    InvitationId: number;
    Invitation: InvitationModel;
    CreatedById: number;
    CreatedBy: UserModel;
    UserId: number;
    User: UserModel;
}
