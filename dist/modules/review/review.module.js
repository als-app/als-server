"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const review_repository_1 = require("./review.repository");
const review_controller_1 = require("./review.controller");
const review_service_1 = require("./review.service");
const user_module_1 = require("../auth/user.module");
let ReviewModule = class ReviewModule {
};
ReviewModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([review_repository_1.ReviewRepository]),
            common_1.forwardRef(() => user_module_1.UserModule)
        ],
        exports: [typeorm_1.TypeOrmModule, review_service_1.ReviewService],
        providers: [review_service_1.ReviewService],
        controllers: [review_controller_1.ReviewController],
    })
], ReviewModule);
exports.ReviewModule = ReviewModule;
//# sourceMappingURL=review.module.js.map