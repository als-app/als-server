"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewService = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const response_exception_1 = require("../../exception/response.exception");
const date_helper_1 = require("../../helpers/date.helper");
const util_helper_1 = require("../../helpers/util.helper");
const locale_1 = require("../../locale/locale");
const review_entity_1 = require("./review.entity");
const review_repository_1 = require("./review.repository");
const review_request_1 = require("./review.request");
const user_service_1 = require("../auth/user.service");
let ReviewService = class ReviewService {
    constructor(_reviewRepository, _userService) {
        this._reviewRepository = _reviewRepository;
        this._userService = _userService;
    }
    async GetUserReviews(UserId) {
        const where = new review_request_1.GetReviewsWhere();
        where.UserId = UserId;
        where.populateCreatedBy = true;
        const Reviews = await this._reviewRepository.getAll(where).getMany();
        const AvgRating = await this._reviewRepository.getUserAvgRating(UserId);
        return { Reviews, AvgRating };
    }
    async GetUsersReviewCountMap(UserIds) {
        const reviewCountMap = {};
        if (!(UserIds === null || UserIds === void 0 ? void 0 : UserIds.length)) {
            return reviewCountMap;
        }
        const usersReviewCount = await this._reviewRepository.getUsersReviewCount(UserIds).getRawMany();
        for (const userReviewCount of usersReviewCount) {
            reviewCountMap[userReviewCount.UserId] = userReviewCount.ReviewCount;
        }
        return reviewCountMap;
    }
    async CreateReview(data, user) {
        const User = await this._userService.GetUserById(data.UserId);
        let review = new review_entity_1.ReviewModel();
        review.Description = data.Description;
        review.Title = data.Title;
        review.UserId = User.Id;
        review.CreatedById = user.Id;
        review.Rating = data.Rating;
        if (data.InvitationId) {
            review.InvitationId = data.InvitationId;
        }
        await this._reviewRepository.Create(review);
        return review;
    }
};
ReviewService = __decorate([
    common_1.Injectable(),
    __param(1, common_1.Inject(common_1.forwardRef(() => user_service_1.UserService))),
    __metadata("design:paramtypes", [review_repository_1.ReviewRepository,
        user_service_1.UserService])
], ReviewService);
exports.ReviewService = ReviewService;
//# sourceMappingURL=review.service.js.map