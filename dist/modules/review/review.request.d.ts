export declare class CreateReviewRequest {
    Description: string;
    Title: string;
    InvitationId: number;
    Rating: number;
    UserId: number;
}
export declare class GetReviewsWhere {
    UserId: number;
    CreatedById: number;
    populateCreatedBy: boolean;
    populateUser: boolean;
}
