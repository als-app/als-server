"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewRepository = void 0;
const common_1 = require("@nestjs/common");
const base_repository_1 = require("../../repository/base.repository");
const mysql_repository_1 = require("../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const review_entity_1 = require("./review.entity");
const media_entity_1 = require("../media/media.entity");
let ReviewRepository = class ReviewRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = "review";
    }
    getAll(where) {
        const qb = this.createQueryBuilder(this.DefaultAlias);
        if (where.populateUser) {
            qb.innerJoin(`${this.DefaultAlias}.User`, 'User')
                .leftJoinAndMapOne("User.Media", media_entity_1.MediaModel, "media", "User.media_id = media.id");
            qb.addSelect(['User.Id', 'User.FullName']);
        }
        if (where.populateCreatedBy) {
            qb.innerJoin(`${this.DefaultAlias}.CreatedBy`, 'CreatedBy')
                .leftJoinAndMapOne("CreatedBy.Media", media_entity_1.MediaModel, "CreatedByMedia", "CreatedBy.media_id = CreatedByMedia.id");
            qb.addSelect(['CreatedBy.Id', 'CreatedBy.FullName']);
        }
        if (where.CreatedById) {
            qb.andWhere(`${this.DefaultAlias}.CreatedById = :CreatedById`, { CreatedById: where.CreatedById });
        }
        if (where.UserId) {
            qb.andWhere(`${this.DefaultAlias}.UserId = :UserId`, { UserId: where.UserId });
        }
        qb.orderBy(`${this.DefaultAlias}.CreatedAt`, 'DESC');
        return qb;
    }
    getUsersReviewCount(ids) {
        const qb = this.createQueryBuilder(this.DefaultAlias)
            .select([
            `${this.DefaultAlias}.UserId as UserId`,
            `count(${this.DefaultAlias}.Id) as "ReviewCount"`,
        ])
            .where(`${this.DefaultAlias}.UserId in (:...ids)`, { ids })
            .groupBy(`${this.DefaultAlias}.UserId`);
        return qb;
    }
    async getUserAvgRating(UserId) {
        const qb = this.createQueryBuilder(this.DefaultAlias)
            .select(`avg(${this.DefaultAlias}.rating)`, 'average')
            .andWhere(`${this.DefaultAlias}.UserId = :UserId`, { UserId: UserId });
        const data = await qb.getRawOne();
        return Math.round(data.average);
    }
};
ReviewRepository = __decorate([
    common_1.Injectable(),
    typeorm_1.EntityRepository(review_entity_1.ReviewModel)
], ReviewRepository);
exports.ReviewRepository = ReviewRepository;
//# sourceMappingURL=review.repository.js.map