import { UserModel } from "../auth/user.entity";
import { CreateReviewRequest } from "./review.request";
import { ReviewService } from "./review.service";
export declare class ReviewController {
    private _reviewService;
    constructor(_reviewService: ReviewService);
    GetUserReviews(UserId: string): Promise<{
        Reviews: import("./review.entity").ReviewModel[];
        AvgRating: number;
    }>;
    CreateReview(data: CreateReviewRequest, user: UserModel): Promise<import("./review.entity").ReviewModel>;
}
