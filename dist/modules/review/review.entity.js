"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewModel = void 0;
const typeorm_1 = require("typeorm");
const mysql_entity_1 = require("../../model/mysql.entity.");
const user_entity_1 = require("../auth/user.entity");
const invitation_entity_1 = require("../invitation/invitation.entity");
let ReviewModel = class ReviewModel extends mysql_entity_1.MySQLBaseModel {
};
__decorate([
    typeorm_1.Column({
        name: "description",
        type: "varchar",
    }),
    __metadata("design:type", String)
], ReviewModel.prototype, "Description", void 0);
__decorate([
    typeorm_1.Column({
        name: "title",
        type: "varchar",
    }),
    __metadata("design:type", String)
], ReviewModel.prototype, "Title", void 0);
__decorate([
    typeorm_1.Column({ name: 'rating', type: 'int', default: 0 }),
    __metadata("design:type", Number)
], ReviewModel.prototype, "Rating", void 0);
__decorate([
    typeorm_1.Column({ name: 'invitation_id', type: 'int', nullable: true }),
    __metadata("design:type", Number)
], ReviewModel.prototype, "InvitationId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => invitation_entity_1.InvitationModel, invitation => invitation.Id),
    typeorm_1.JoinColumn({ name: 'invitation_id' }),
    __metadata("design:type", invitation_entity_1.InvitationModel)
], ReviewModel.prototype, "Invitation", void 0);
__decorate([
    typeorm_1.Column({ name: 'created_by_id', type: 'int', nullable: true }),
    __metadata("design:type", Number)
], ReviewModel.prototype, "CreatedById", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserModel, user => user.Id),
    typeorm_1.JoinColumn({ name: 'created_by_id' }),
    __metadata("design:type", user_entity_1.UserModel)
], ReviewModel.prototype, "CreatedBy", void 0);
__decorate([
    typeorm_1.Column({ name: 'user_id', type: 'int', nullable: true }),
    __metadata("design:type", Number)
], ReviewModel.prototype, "UserId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserModel, user => user.Id),
    typeorm_1.JoinColumn({ name: 'user_id' }),
    __metadata("design:type", user_entity_1.UserModel)
], ReviewModel.prototype, "User", void 0);
ReviewModel = __decorate([
    typeorm_1.Entity("reviews")
], ReviewModel);
exports.ReviewModel = ReviewModel;
//# sourceMappingURL=review.entity.js.map