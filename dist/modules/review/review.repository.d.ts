import { MySqlRepository } from "src/repository/mysql.repository";
import { SelectQueryBuilder } from "typeorm";
import { ReviewModel } from "./review.entity";
import { GetReviewsWhere } from './review.request';
export declare class ReviewRepository extends MySqlRepository<ReviewModel> {
    protected DefaultAlias: string;
    getAll(where: GetReviewsWhere): SelectQueryBuilder<ReviewModel>;
    getUsersReviewCount(ids: number[]): SelectQueryBuilder<any>;
    getUserAvgRating(UserId: any): Promise<number>;
}
