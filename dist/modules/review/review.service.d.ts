import { UserModel } from "../auth/user.entity";
import { ReviewModel } from "./review.entity";
import { ReviewRepository } from "./review.repository";
import { CreateReviewRequest } from "./review.request";
import { UserService } from '../auth/user.service';
export declare class ReviewService {
    private _reviewRepository;
    private _userService;
    constructor(_reviewRepository: ReviewRepository, _userService: UserService);
    GetUserReviews(UserId: number): Promise<{
        Reviews: ReviewModel[];
        AvgRating: number;
    }>;
    GetUsersReviewCountMap(UserIds: number[]): Promise<any>;
    CreateReview(data: CreateReviewRequest, user: UserModel): Promise<ReviewModel>;
}
