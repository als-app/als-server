"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewController = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const delete_decorator_1 = require("../../decorator/nest-ext/delete.decorator");
const get_decorator_1 = require("../../decorator/nest-ext/get.decorator");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const put_decorator_1 = require("../../decorator/nest-ext/put.decorator");
const user_entity_1 = require("../auth/user.entity");
const review_request_1 = require("./review.request");
const review_service_1 = require("./review.service");
let ReviewController = class ReviewController {
    constructor(_reviewService) {
        this._reviewService = _reviewService;
    }
    async GetUserReviews(UserId) {
        return await this._reviewService.GetUserReviews(Number(UserId));
    }
    async CreateReview(data, user) {
        return await this._reviewService.CreateReview(data, user);
    }
};
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get("/user/:UserId", {}),
    __param(0, common_1.Param('UserId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ReviewController.prototype, "GetUserReviews", null);
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post("/", {}),
    __param(0, common_1.Body()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [review_request_1.CreateReviewRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], ReviewController.prototype, "CreateReview", null);
ReviewController = __decorate([
    appcontroller_decorator_1.AppController("review"),
    __metadata("design:paramtypes", [review_service_1.ReviewService])
], ReviewController);
exports.ReviewController = ReviewController;
//# sourceMappingURL=review.controller.js.map