export declare class UploadInitMediaRequest {
    Name: string;
    MimeType: string;
    Size: number;
}
export declare class UploadFinalizeMediaRequest {
    Id: number;
}
