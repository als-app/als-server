"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadMediaResponse = exports.UploadFinalizeMediaResponse = exports.UploadInitMediaResponse = exports.MediaResponseModel = void 0;
const swagger_1 = require("@nestjs/swagger");
class MediaMetaResponseModel {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], MediaMetaResponseModel.prototype, "Size", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], MediaMetaResponseModel.prototype, "MimeType", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], MediaMetaResponseModel.prototype, "Name", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], MediaMetaResponseModel.prototype, "Extension", void 0);
class MediaResponseModel {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], MediaResponseModel.prototype, "Id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], MediaResponseModel.prototype, "BasePath", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], MediaResponseModel.prototype, "Path", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], MediaResponseModel.prototype, "ThumbPath", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], MediaResponseModel.prototype, "Type", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", MediaMetaResponseModel)
], MediaResponseModel.prototype, "Meta", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], MediaResponseModel.prototype, "DeletedAt", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], MediaResponseModel.prototype, "CreatedAt", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], MediaResponseModel.prototype, "UpdatedAt", void 0);
exports.MediaResponseModel = MediaResponseModel;
class UploadInitMediaResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UploadInitMediaResponse.prototype, "MediaId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadInitMediaResponse.prototype, "Path", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadInitMediaResponse.prototype, "Bucket", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadInitMediaResponse.prototype, "Region", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadInitMediaResponse.prototype, "AccessKeyId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadInitMediaResponse.prototype, "SecretAccessKey", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadInitMediaResponse.prototype, "SessionToken", void 0);
exports.UploadInitMediaResponse = UploadInitMediaResponse;
class UploadFinalizeMediaResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UploadFinalizeMediaResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", MediaResponseModel)
], UploadFinalizeMediaResponse.prototype, "Media", void 0);
exports.UploadFinalizeMediaResponse = UploadFinalizeMediaResponse;
class UploadMediaResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UploadMediaResponse.prototype, "Id", void 0);
exports.UploadMediaResponse = UploadMediaResponse;
//# sourceMappingURL=media.response.js.map