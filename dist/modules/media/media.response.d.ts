declare class MediaMetaResponseModel {
    Size: number;
    MimeType: string;
    Name: string;
    Extension: string;
}
export declare class MediaResponseModel {
    Id: number;
    BasePath: string;
    Path: string;
    ThumbPath: string;
    Type: number;
    Meta: MediaMetaResponseModel;
    DeletedAt: number;
    CreatedAt: number;
    UpdatedAt: number;
}
export declare class UploadInitMediaResponse {
    MediaId: number;
    Path: string;
    Bucket: string;
    Region: string;
    AccessKeyId: string;
    SecretAccessKey: string;
    SessionToken: string;
}
export declare class UploadFinalizeMediaResponse {
    Message: string;
    Media: MediaResponseModel;
}
export declare class UploadMediaResponse {
    Id: number;
}
export {};
