"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaModelStatus = exports.MediaModelType = exports.MediaModel = exports.MediaMeta = void 0;
const env_helper_1 = require("../../helpers/env.helper");
const mysql_entity_1 = require("../../model/mysql.entity.");
const typeorm_1 = require("typeorm");
class MediaMeta {
}
exports.MediaMeta = MediaMeta;
let MediaModel = class MediaModel extends mysql_entity_1.MySQLBaseModel {
};
__decorate([
    typeorm_1.Column({
        name: "base_path",
        type: "varchar"
    }),
    __metadata("design:type", String)
], MediaModel.prototype, "BasePath", void 0);
__decorate([
    typeorm_1.Column({
        name: "path",
        type: "varchar",
        length: 500
    }),
    __metadata("design:type", String)
], MediaModel.prototype, "Path", void 0);
__decorate([
    typeorm_1.Column({
        name: "thumb_path",
        type: "varchar",
        length: 500
    }),
    __metadata("design:type", String)
], MediaModel.prototype, "ThumbPath", void 0);
__decorate([
    typeorm_1.Column({
        name: "user_id",
        type: "bigint"
    }),
    __metadata("design:type", Number)
], MediaModel.prototype, "UserId", void 0);
__decorate([
    typeorm_1.Column({
        name: "meta",
        type: "json"
    }),
    __metadata("design:type", MediaMeta)
], MediaModel.prototype, "Meta", void 0);
__decorate([
    typeorm_1.Column({
        name: "type",
        type: "tinyint"
    }),
    __metadata("design:type", Number)
], MediaModel.prototype, "Type", void 0);
__decorate([
    typeorm_1.Column({
        name: "status",
        type: "tinyint"
    }),
    __metadata("design:type", Number)
], MediaModel.prototype, "Status", void 0);
MediaModel = __decorate([
    typeorm_1.Entity("media")
], MediaModel);
exports.MediaModel = MediaModel;
var MediaModelType;
(function (MediaModelType) {
    MediaModelType[MediaModelType["Image"] = 1] = "Image";
    MediaModelType[MediaModelType["Video"] = 2] = "Video";
    MediaModelType[MediaModelType["Audio"] = 3] = "Audio";
    MediaModelType[MediaModelType["Document"] = 10] = "Document";
    MediaModelType[MediaModelType["Archive"] = 5] = "Archive";
})(MediaModelType = exports.MediaModelType || (exports.MediaModelType = {}));
var MediaModelStatus;
(function (MediaModelStatus) {
    MediaModelStatus[MediaModelStatus["Active"] = 1] = "Active";
    MediaModelStatus[MediaModelStatus["Failed"] = 0] = "Failed";
})(MediaModelStatus = exports.MediaModelStatus || (exports.MediaModelStatus = {}));
//# sourceMappingURL=media.entity.js.map