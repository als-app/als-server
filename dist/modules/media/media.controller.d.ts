import { UserModel } from "../auth/user.entity";
import { MediaService } from "./media.service";
export declare class MediaController {
    private _mediaServie;
    constructor(_mediaServie: MediaService);
    UploadFile(file: any, user: UserModel): Promise<{
        Id: number;
    }>;
}
