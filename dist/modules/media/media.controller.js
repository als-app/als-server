"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaController = void 0;
const common_1 = require("@nestjs/common");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const user_entity_1 = require("../auth/user.entity");
const media_response_1 = require("./media.response");
const media_service_1 = require("./media.service");
const platform_express_1 = require("@nestjs/platform-express");
const media_helper_1 = require("../../helpers/media.helper");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const s3_helper_1 = require("../../helpers/s3.helper");
let MediaController = class MediaController {
    constructor(_mediaServie) {
        this._mediaServie = _mediaServie;
    }
    async UploadFile(file, user) {
        return await this._mediaServie.Create(file, user);
    }
};
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post("upload", media_response_1.UploadMediaResponse),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', s3_helper_1.S3FileUploadOptions)),
    __param(0, common_1.UploadedFile()),
    __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], MediaController.prototype, "UploadFile", null);
MediaController = __decorate([
    appcontroller_decorator_1.AppController("media"),
    __metadata("design:paramtypes", [media_service_1.MediaService])
], MediaController);
exports.MediaController = MediaController;
//# sourceMappingURL=media.controller.js.map