import { MySQLBaseModel } from "src/model/mysql.entity.";
export declare class MediaMeta {
    Size: number;
    Extension: string;
    MimeType: string;
    Name: string;
}
export declare class MediaModel extends MySQLBaseModel {
    BasePath: string;
    Path: string;
    ThumbPath: string;
    UserId: number;
    Meta: MediaMeta;
    Type: MediaModelType;
    Status: number;
}
export declare enum MediaModelType {
    Image = 1,
    Video = 2,
    Audio = 3,
    Document = 10,
    Archive = 5
}
export declare enum MediaModelStatus {
    Active = 1,
    Failed = 0
}
