"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaService = void 0;
const common_1 = require("@nestjs/common");
const media_entity_1 = require("./media.entity");
const media_repository_1 = require("./media.repository");
const media_helper_1 = require("../../helpers/media.helper");
const response_exception_1 = require("../../exception/response.exception");
const locale_1 = require("../../locale/locale");
const env_helper_1 = require("../../helpers/env.helper");
const logger_helper_1 = require("../../helpers/logger.helper");
const s3_helper_1 = require("../../helpers/s3.helper");
let MediaService = class MediaService {
    constructor(_mediaRepository) {
        this._mediaRepository = _mediaRepository;
    }
    async Create(fileObject, activeUser) {
        console.log(fileObject);
        let MediaObject = new media_entity_1.MediaModel();
        let FileMetaObject = new media_entity_1.MediaMeta();
        let Extension = media_helper_1.GetMediaExtension(fileObject.originalname);
        FileMetaObject.Size = fileObject.size;
        FileMetaObject.Extension = Extension;
        FileMetaObject.MimeType = fileObject.mimetype;
        FileMetaObject.Name = fileObject.originalname;
        MediaObject.Meta = FileMetaObject;
        MediaObject.BasePath = `${env_helper_1.appEnv("AWS_S3_BASE_URL", "https://event-buddy-uploads.s3.amazonaws.com")}/`;
        MediaObject.Path = fileObject.key;
        MediaObject.ThumbPath = fileObject.key;
        MediaObject.UserId = activeUser.Id;
        MediaObject.Type = media_helper_1.GetMediaType(fileObject.mimetype);
        MediaObject.Status = media_entity_1.MediaModelStatus.Active;
        await this._mediaRepository.Save(MediaObject);
        return { Id: MediaObject.Id, MediaObject };
    }
    async GetById(id) {
        return await this._mediaRepository.FindById(id);
    }
};
MediaService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [media_repository_1.MediaRepository])
], MediaService);
exports.MediaService = MediaService;
//# sourceMappingURL=media.service.js.map