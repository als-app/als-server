import { MySqlRepository } from 'src/repository/mysql.repository';
import { MediaModel } from './media.entity';
export declare class MediaRepository extends MySqlRepository<MediaModel> {
    protected DefaultAlias: string;
}
