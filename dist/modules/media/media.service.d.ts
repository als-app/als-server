import { UserModel } from "../auth/user.entity";
import { MediaModel } from "./media.entity";
import { MediaRepository } from "./media.repository";
export declare class MediaService {
    private _mediaRepository;
    constructor(_mediaRepository: MediaRepository);
    Create(fileObject: any, activeUser: UserModel): Promise<any>;
    GetById(id: any): Promise<MediaModel>;
}
