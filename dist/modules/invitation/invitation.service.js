"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvitationService = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const response_exception_1 = require("../../exception/response.exception");
const date_helper_1 = require("../../helpers/date.helper");
const util_helper_1 = require("../../helpers/util.helper");
const locale_1 = require("../../locale/locale");
const invitation_entity_1 = require("./invitation.entity");
const invitation_repository_1 = require("./invitation.repository");
const invitation_request_1 = require("./invitation.request");
const chat_service_1 = require("../chat/chat.service");
let InvitationService = class InvitationService {
    constructor(_invitationRepository, _chatService) {
        this._invitationRepository = _invitationRepository;
        this._chatService = _chatService;
    }
    async CreateInvitation(data, user) {
        const alreadyExistInvitation = await this._invitationRepository.findOne({ UserId: data.UserId, CreatedById: user.Id });
        if (alreadyExistInvitation) {
            return alreadyExistInvitation;
        }
        let invitation = new invitation_entity_1.InvitationModel();
        invitation.Title = 'Chat Invitation';
        invitation.UserId = data.UserId;
        invitation.CreatedById = user.Id;
        await this._invitationRepository.Create(invitation);
        return invitation;
    }
    async getUserInvitations(UserIds, currentUserId) {
        const invitation = await this._invitationRepository.getUserInvitations(UserIds, currentUserId).getMany();
        return invitation;
    }
    async getInvitationDetails(Id) {
        const where = new invitation_request_1.GetInvitationsWhere();
        where.populateCreatedBy = true;
        where.populateUser = true;
        where.Id = Id;
        const invitation = await this._invitationRepository.getAll(where).getOne();
        return invitation;
    }
    async getInvitationMessages(user) {
        const where = new invitation_request_1.GetInvitationsWhere();
        where.populateCreatedBy = true;
        where.populateLastMessage = true;
        where.populateUser = true;
        where.UserIdOrCreatedById = user.Id;
        const invitations = await this._invitationRepository.getAll(where).getMany();
        const InvitationIds = invitations.map(item => item.Id);
        const UnseenChatCountMap = await this._chatService.getUnseenChatCountMapByInvitationIds(InvitationIds, user.Id);
        return invitations.map(item => (Object.assign(Object.assign({}, item), { UnseenChatCount: UnseenChatCountMap[item.Id] ? UnseenChatCountMap[item.Id] : 0 })));
    }
};
InvitationService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [invitation_repository_1.InvitationRepository,
        chat_service_1.ChatService])
], InvitationService);
exports.InvitationService = InvitationService;
//# sourceMappingURL=invitation.service.js.map