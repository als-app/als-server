import { UserModel } from "../auth/user.entity";
import { InvitationModel } from "./invitation.entity";
import { InvitationRepository } from "./invitation.repository";
import { CreateInvitationRequest } from "./invitation.request";
import { ChatService } from '../chat/chat.service';
export declare class InvitationService {
    private _invitationRepository;
    private _chatService;
    constructor(_invitationRepository: InvitationRepository, _chatService: ChatService);
    CreateInvitation(data: CreateInvitationRequest, user: UserModel): Promise<InvitationModel>;
    getUserInvitations(UserIds: number[], currentUserId: number): Promise<InvitationModel[]>;
    getInvitationDetails(Id: number): Promise<InvitationModel>;
    getInvitationMessages(user: UserModel): Promise<{
        UnseenChatCount: any;
        Title: string;
        CreatedById: number;
        CreatedBy: UserModel;
        UserId: number;
        User: UserModel;
        Chats: import("../chat/chat.entity").ChatModel[];
        Id: number;
        CreatedAt: any;
        UpdatedAt: any;
        DeletedAt: any;
    }[]>;
}
