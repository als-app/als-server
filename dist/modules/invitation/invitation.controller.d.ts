import { UserModel } from "../auth/user.entity";
import { CreateInvitationRequest } from "./invitation.request";
import { InvitationService } from "./invitation.service";
export declare class InvitationController {
    private _invitationService;
    constructor(_invitationService: InvitationService);
    CreateInvitation(data: CreateInvitationRequest, user: UserModel): Promise<import("./invitation.entity").InvitationModel>;
    getUserDetails(Id: string, user: UserModel): Promise<import("./invitation.entity").InvitationModel>;
    getInvitationMessages(user: UserModel): Promise<{
        UnseenChatCount: any;
        Title: string;
        CreatedById: number;
        CreatedBy: UserModel;
        UserId: number;
        User: UserModel;
        Chats: import("../chat/chat.entity").ChatModel[];
        Id: number;
        CreatedAt: any;
        UpdatedAt: any;
        DeletedAt: any;
    }[]>;
}
