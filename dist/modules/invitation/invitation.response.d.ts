export declare class GigResponseModel {
    Id: number;
    UserId: number;
    FullName: string;
    Email: string;
    PhoneNumber: string;
    Country: string;
    Website: string;
}
