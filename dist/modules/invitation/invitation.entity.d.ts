import { MySQLBaseModel } from "src/model/mysql.entity.";
import { UserModel } from '../auth/user.entity';
import { ChatModel } from '../chat/chat.entity';
export declare enum InvitationStatus {
    pending = "pending",
    accepted = "accepted",
    rejected = "rejected"
}
export declare class InvitationModel extends MySQLBaseModel {
    Title: string;
    CreatedById: number;
    CreatedBy: UserModel;
    UserId: number;
    User: UserModel;
    Chats: ChatModel[];
}
