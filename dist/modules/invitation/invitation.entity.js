"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvitationModel = exports.InvitationStatus = void 0;
const base_entity_1 = require("../../model/base.entity");
const typeorm_1 = require("typeorm");
const mysql_entity_1 = require("../../model/mysql.entity.");
const types_1 = require("../../common/types");
const user_entity_1 = require("../auth/user.entity");
const chat_entity_1 = require("../chat/chat.entity");
var InvitationStatus;
(function (InvitationStatus) {
    InvitationStatus["pending"] = "pending";
    InvitationStatus["accepted"] = "accepted";
    InvitationStatus["rejected"] = "rejected";
})(InvitationStatus = exports.InvitationStatus || (exports.InvitationStatus = {}));
let InvitationModel = class InvitationModel extends mysql_entity_1.MySQLBaseModel {
};
__decorate([
    typeorm_1.Column({
        name: "title",
        type: "varchar",
    }),
    __metadata("design:type", String)
], InvitationModel.prototype, "Title", void 0);
__decorate([
    typeorm_1.Column({ name: 'created_by_id', type: 'int', nullable: true }),
    __metadata("design:type", Number)
], InvitationModel.prototype, "CreatedById", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserModel, user => user.Id),
    typeorm_1.JoinColumn({ name: 'created_by_id' }),
    __metadata("design:type", user_entity_1.UserModel)
], InvitationModel.prototype, "CreatedBy", void 0);
__decorate([
    typeorm_1.Column({ name: 'user_id', type: 'int', nullable: true }),
    __metadata("design:type", Number)
], InvitationModel.prototype, "UserId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserModel, user => user.Id),
    typeorm_1.JoinColumn({ name: 'user_id' }),
    __metadata("design:type", user_entity_1.UserModel)
], InvitationModel.prototype, "User", void 0);
__decorate([
    typeorm_1.OneToMany(type => chat_entity_1.ChatModel, chat => chat.Invitation),
    __metadata("design:type", Array)
], InvitationModel.prototype, "Chats", void 0);
InvitationModel = __decorate([
    typeorm_1.Entity("invitation")
], InvitationModel);
exports.InvitationModel = InvitationModel;
//# sourceMappingURL=invitation.entity.js.map