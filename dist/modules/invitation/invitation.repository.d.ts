import { MySqlRepository } from "src/repository/mysql.repository";
import { SelectQueryBuilder } from "typeorm";
import { InvitationModel } from "./invitation.entity";
import { GetInvitationsWhere } from './invitation.request';
export declare class InvitationRepository extends MySqlRepository<InvitationModel> {
    protected DefaultAlias: string;
    getAll(where: GetInvitationsWhere): SelectQueryBuilder<InvitationModel>;
    getUserInvitations(UserIds: number[], CurrentUserId: number): SelectQueryBuilder<InvitationModel>;
}
