"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvitationController = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const delete_decorator_1 = require("../../decorator/nest-ext/delete.decorator");
const get_decorator_1 = require("../../decorator/nest-ext/get.decorator");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const put_decorator_1 = require("../../decorator/nest-ext/put.decorator");
const user_entity_1 = require("../auth/user.entity");
const invitation_request_1 = require("./invitation.request");
const invitation_service_1 = require("./invitation.service");
let InvitationController = class InvitationController {
    constructor(_invitationService) {
        this._invitationService = _invitationService;
    }
    async CreateInvitation(data, user) {
        return await this._invitationService.CreateInvitation(data, user);
    }
    async getUserDetails(Id, user) {
        return await this._invitationService.getInvitationDetails(Number(Id));
    }
    async getInvitationMessages(user) {
        return await this._invitationService.getInvitationMessages(user);
    }
};
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post("/", {}),
    __param(0, common_1.Body()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [invitation_request_1.CreateInvitationRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], InvitationController.prototype, "CreateInvitation", null);
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get('/details/:Id', {}),
    __param(0, common_1.Param('Id')), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], InvitationController.prototype, "getUserDetails", null);
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get('/messages', {}),
    __param(0, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], InvitationController.prototype, "getInvitationMessages", null);
InvitationController = __decorate([
    appcontroller_decorator_1.AppController("invitation"),
    __metadata("design:paramtypes", [invitation_service_1.InvitationService])
], InvitationController);
exports.InvitationController = InvitationController;
//# sourceMappingURL=invitation.controller.js.map