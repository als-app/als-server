"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvitationRepository = void 0;
const common_1 = require("@nestjs/common");
const base_repository_1 = require("../../repository/base.repository");
const mysql_repository_1 = require("../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const invitation_entity_1 = require("./invitation.entity");
const media_entity_1 = require("../media/media.entity");
let InvitationRepository = class InvitationRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = "invitation";
    }
    getAll(where) {
        const qb = this.createQueryBuilder(this.DefaultAlias);
        if (where.populateUser) {
            qb.innerJoin(`${this.DefaultAlias}.User`, 'User')
                .leftJoinAndMapOne("User.Media", media_entity_1.MediaModel, "media", "User.media_id = media.id");
            qb.addSelect(['User.Id', 'User.FullName']);
        }
        if (where.populateCreatedBy) {
            qb.innerJoin(`${this.DefaultAlias}.CreatedBy`, 'CreatedBy')
                .leftJoinAndMapOne("CreatedBy.Media", media_entity_1.MediaModel, "CreatedByMedia", "CreatedBy.media_id = CreatedByMedia.id");
            qb.addSelect(['CreatedBy.Id', 'CreatedBy.FullName']);
        }
        if (where.populateLastMessage) {
            qb.innerJoinAndSelect(`${this.DefaultAlias}.Chats`, 'Chats')
                .leftJoin(`${this.DefaultAlias}.Chats`, "next_chats", "Chats.CreatedAt < next_chats.CreatedAt")
                .andWhere('`next_chats`.Id is null');
        }
        if (where.CreatedById) {
            qb.andWhere(`${this.DefaultAlias}.CreatedById = :CreatedById`, { CreatedById: where.CreatedById });
        }
        if (where.UserId) {
            qb.andWhere(`${this.DefaultAlias}.UserId = :UserId`, { UserId: where.UserId });
        }
        if (where.UserIdOrCreatedById) {
            qb.andWhere(`(${this.DefaultAlias}.UserId = :UserIdOrCreatedById OR ${this.DefaultAlias}.CreatedById = :UserIdOrCreatedById) `, { UserIdOrCreatedById: where.UserIdOrCreatedById });
        }
        if (where.Id) {
            qb.andWhere(`${this.DefaultAlias}.Id = :Id`, { Id: where.Id });
        }
        if (where.Status) {
            qb.andWhere(`${this.DefaultAlias}.Status = :Status`, { Status: where.Status });
        }
        qb.orderBy(`${this.DefaultAlias}.CreatedAt`, 'DESC');
        return qb;
    }
    getUserInvitations(UserIds, CurrentUserId) {
        const qb = this.createQueryBuilder(this.DefaultAlias)
            .andWhere(`${this.DefaultAlias}.CreatedById = :CreatedById`, { CreatedById: CurrentUserId })
            .andWhere(`${this.DefaultAlias}.UserId in (:...UserIds)`, { UserIds });
        return qb;
    }
};
InvitationRepository = __decorate([
    common_1.Injectable(),
    typeorm_1.EntityRepository(invitation_entity_1.InvitationModel)
], InvitationRepository);
exports.InvitationRepository = InvitationRepository;
//# sourceMappingURL=invitation.repository.js.map