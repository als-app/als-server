import { InvitationStatus } from './invitation.entity';
export declare class CreateInvitationRequest {
    UserId: number;
}
export declare class GetInvitationsWhere {
    UserId: number;
    CreatedById: number;
    Status: InvitationStatus;
    populateCreatedBy: boolean;
    populateUser: boolean;
    populateLastMessage: boolean;
    Id: number;
    UserIdOrCreatedById: number;
}
