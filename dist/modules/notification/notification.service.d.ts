import { UserModel } from "../auth/user.entity";
import { UserDeviceRepository } from "../auth/user_device/user_device.repository";
import { FindNotificationRequest } from "./norification.request";
import { NotificationModel, NotificationModelType } from "./notification.entity";
import { NotificationRepository } from "./notification.repository";
export declare class NotificationService {
    private _notificationRepository;
    private _userDeviceRepository;
    constructor(_notificationRepository: NotificationRepository, _userDeviceRepository: UserDeviceRepository);
    ProcessNotification(type: NotificationModelType, data: any): Promise<boolean>;
    private _SaveNotifications;
    private _SendNotificationOnUserDevices;
    private _GenerateInvitationNotification;
    private _GenerateNewChatMessageNotification;
    Find(data: FindNotificationRequest, user: UserModel): Promise<{
        notifications: NotificationModel[];
    }>;
}
