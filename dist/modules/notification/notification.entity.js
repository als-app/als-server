"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationModelType = exports.NotificationModelReadStatus = exports.NotificationModel = void 0;
const mysql_entity_1 = require("../../model/mysql.entity.");
const typeorm_1 = require("typeorm");
let NotificationModel = class NotificationModel extends mysql_entity_1.MySQLBaseModel {
};
__decorate([
    typeorm_1.Column({
        name: "read_status",
        type: "tinyint"
    }),
    __metadata("design:type", Number)
], NotificationModel.prototype, "ReadStatus", void 0);
__decorate([
    typeorm_1.Column({
        name: "user_id",
        type: "bigint"
    }),
    __metadata("design:type", Number)
], NotificationModel.prototype, "UserId", void 0);
__decorate([
    typeorm_1.Column({
        name: "title",
        type: "varchar"
    }),
    __metadata("design:type", String)
], NotificationModel.prototype, "Title", void 0);
__decorate([
    typeorm_1.Column({
        name: "description",
        type: "longtext"
    }),
    __metadata("design:type", String)
], NotificationModel.prototype, "Description", void 0);
__decorate([
    typeorm_1.Column({
        name: "meta",
        type: "json"
    }),
    __metadata("design:type", Object)
], NotificationModel.prototype, "Meta", void 0);
__decorate([
    typeorm_1.Column({
        name: "type",
        type: "tinyint"
    }),
    __metadata("design:type", Number)
], NotificationModel.prototype, "Type", void 0);
NotificationModel = __decorate([
    typeorm_1.Entity('notification')
], NotificationModel);
exports.NotificationModel = NotificationModel;
var NotificationModelReadStatus;
(function (NotificationModelReadStatus) {
    NotificationModelReadStatus[NotificationModelReadStatus["Read"] = 1] = "Read";
    NotificationModelReadStatus[NotificationModelReadStatus["UnRead"] = 0] = "UnRead";
})(NotificationModelReadStatus = exports.NotificationModelReadStatus || (exports.NotificationModelReadStatus = {}));
var NotificationModelType;
(function (NotificationModelType) {
    NotificationModelType[NotificationModelType["Invitation"] = 1] = "Invitation";
    NotificationModelType[NotificationModelType["NewChatMessage"] = 2] = "NewChatMessage";
})(NotificationModelType = exports.NotificationModelType || (exports.NotificationModelType = {}));
//# sourceMappingURL=notification.entity.js.map