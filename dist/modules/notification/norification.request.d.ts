export declare class FindNotificationRequest {
    Page: number;
    Limit: number;
    Before: number;
    After: number;
}
