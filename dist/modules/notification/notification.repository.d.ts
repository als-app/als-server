import { MySqlRepository } from 'src/repository/mysql.repository';
import { NotificationModel } from './notification.entity';
export declare class NotificationRepository extends MySqlRepository<NotificationModel> {
    protected DefaultAlias: string;
    constructor();
}
