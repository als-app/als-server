import { UserModel } from "../auth/user.entity";
import { NotificationService } from "./notification.service";
import { FindNotificationRequest } from "./norification.request";
export declare class NotificationController {
    private _notificationService;
    constructor(_notificationService: NotificationService);
    FindPromotions(data: FindNotificationRequest, user: UserModel): Promise<any>;
}
