import { MySQLBaseModel } from 'src/model/mysql.entity.';
export declare class NotificationModel extends MySQLBaseModel {
    ReadStatus: number;
    UserId: number;
    Title: string;
    Description: string;
    Meta: JSON;
    Type: number;
}
export declare enum NotificationModelReadStatus {
    Read = 1,
    UnRead = 0
}
export declare enum NotificationModelType {
    Invitation = 1,
    NewChatMessage = 2
}
