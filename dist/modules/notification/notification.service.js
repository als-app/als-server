"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationService = void 0;
const common_1 = require("@nestjs/common");
const queue_1 = require("../../constant/queue");
const notification_helper_1 = require("../../helpers/notification.helper");
const queue_helper_1 = require("../../helpers/queue.helper");
const util_helper_1 = require("../../helpers/util.helper");
const locale_1 = require("../../locale/locale");
const base_entity_1 = require("../../model/base.entity");
const user_device_repository_1 = require("../auth/user_device/user_device.repository");
const notification_entity_1 = require("./notification.entity");
const notification_repository_1 = require("./notification.repository");
let NotificationService = class NotificationService {
    constructor(_notificationRepository, _userDeviceRepository) {
        this._notificationRepository = _notificationRepository;
        this._userDeviceRepository = _userDeviceRepository;
    }
    async ProcessNotification(type, data) {
        switch (type) {
            case notification_entity_1.NotificationModelType.Invitation:
                return await this._GenerateInvitationNotification(data);
            case notification_entity_1.NotificationModelType.NewChatMessage:
                return await this._GenerateNewChatMessageNotification(data);
            default:
                return true;
        }
    }
    async _SaveNotifications(notificationData) {
        if (!notificationData) {
            return false;
        }
        if (!Array.isArray(notificationData)) {
            notificationData = [notificationData];
        }
        let NewNotifications = notificationData.map(obj => {
            let Notification = new notification_entity_1.NotificationModel();
            Notification.ReadStatus = notification_entity_1.NotificationModelReadStatus.UnRead;
            Notification.UserId = obj.UserId;
            Notification.Title = obj.Title;
            Notification.Description = obj.Description;
            Notification.Type = obj.Type;
            if (obj.Meta) {
                Notification.Meta = obj.Meta;
            }
            return Notification;
        });
        if (NewNotifications.length) {
            await this._notificationRepository.BatchCreate(NewNotifications);
        }
        return true;
    }
    async _SendNotificationOnUserDevices(idOrIds, notificationData) {
        let UserDevices = await this._userDeviceRepository.Find({ UserId: idOrIds });
        let Promises = [];
        UserDevices.map(device => {
            let Notification;
            if (Array.isArray(notificationData)) {
                Notification = notificationData.find(obj => obj.UserId == device.UserId);
            }
            else {
                Notification = notificationData;
            }
            if (Notification && device.FCMToken) {
                Promises.push(notification_helper_1.SendNotification(device.FCMToken, Notification.Title, Notification.Description, Object.assign({ Type: Notification.Type, CreatedAt: Date.now() }, (Notification.Meta && Notification.Meta))));
            }
        });
        await Promise.all(Promises);
        return true;
    }
    async _GenerateInvitationNotification(data) {
        let NotificationData = [];
        let UserIds = [];
        let { Invitation, user, Title, Description } = data;
        NotificationData.push({
            Title,
            Description,
            Type: notification_entity_1.NotificationModelType.Invitation,
            EntityId: Invitation.Id,
            EntityType: base_entity_1.BaseModelEntityType.Invitation,
            UserId: Invitation.UserId,
            Meta: { InvitationId: Invitation.Id, User: user }
        });
        UserIds.push(Invitation.UserId);
        if (NotificationData.length && UserIds.length) {
            await this._SaveNotifications(NotificationData);
            await this._SendNotificationOnUserDevices(UserIds, NotificationData);
        }
        return true;
    }
    async _GenerateNewChatMessageNotification(data) {
        let Title = locale_1.__T(locale_1.Locale.En, locale_1.MessageTemplates.NewMessageReceive);
        let { Content, ReceiverId, InvitationId, SenderId, SenderType } = data;
        let NotificationData = {
            Title,
            Description: Content,
            Type: notification_entity_1.NotificationModelType.NewChatMessage,
            EntityId: InvitationId,
            EntityType: base_entity_1.BaseModelEntityType.Invitation,
            UserId: ReceiverId,
            Meta: { InvitationId }
        };
        let UserId = ReceiverId;
        if (UserId && NotificationData) {
            await this._SendNotificationOnUserDevices(UserId, NotificationData);
        }
        return true;
    }
    async Find(data, user) {
        let notifications = await this._notificationRepository.Find({ UserId: user.Id }, util_helper_1.GetPaginationOptions(data));
        return { notifications };
    }
};
NotificationService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [notification_repository_1.NotificationRepository, user_device_repository_1.UserDeviceRepository])
], NotificationService);
exports.NotificationService = NotificationService;
//# sourceMappingURL=notification.service.js.map