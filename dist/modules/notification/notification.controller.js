"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationController = void 0;
const common_1 = require("@nestjs/common");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const user_entity_1 = require("../auth/user.entity");
const media_helper_1 = require("../../helpers/media.helper");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const s3_helper_1 = require("../../helpers/s3.helper");
const notification_service_1 = require("./notification.service");
const get_decorator_1 = require("../../decorator/nest-ext/get.decorator");
const norification_request_1 = require("./norification.request");
let NotificationController = class NotificationController {
    constructor(_notificationService) {
        this._notificationService = _notificationService;
    }
    async FindPromotions(data, user) {
        return await this._notificationService.Find(data, user);
    }
};
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get("/", {}),
    __param(0, common_1.Query()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [norification_request_1.FindNotificationRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], NotificationController.prototype, "FindPromotions", null);
NotificationController = __decorate([
    appcontroller_decorator_1.AppController("notification"),
    __metadata("design:paramtypes", [notification_service_1.NotificationService])
], NotificationController);
exports.NotificationController = NotificationController;
//# sourceMappingURL=notification.controller.js.map