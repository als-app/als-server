"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCategoryRequest = exports.CreateCategoryRequest = exports.FindCategoryRequest = exports.CategoryOrderByDirections = exports.CategoryOrderByColumns = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
var CategoryOrderByColumns;
(function (CategoryOrderByColumns) {
    CategoryOrderByColumns["Id"] = "Id";
    CategoryOrderByColumns["CreatedAt"] = "CreatedAt";
})(CategoryOrderByColumns = exports.CategoryOrderByColumns || (exports.CategoryOrderByColumns = {}));
var CategoryOrderByDirections;
(function (CategoryOrderByDirections) {
    CategoryOrderByDirections["ASC"] = "ASC";
    CategoryOrderByDirections["DESC"] = "DESC";
})(CategoryOrderByDirections = exports.CategoryOrderByDirections || (exports.CategoryOrderByDirections = {}));
class FindCategoryRequest {
}
__decorate([
    swagger_1.ApiProperty({ required: false, enum: CategoryOrderByColumns }),
    class_validator_1.IsOptional(),
    class_validator_1.IsEnum(CategoryOrderByColumns),
    __metadata("design:type", String)
], FindCategoryRequest.prototype, "Column", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false, enum: CategoryOrderByDirections }),
    class_validator_1.IsOptional(),
    class_validator_1.IsEnum(CategoryOrderByDirections),
    __metadata("design:type", String)
], FindCategoryRequest.prototype, "Direction", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindCategoryRequest.prototype, "Page", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindCategoryRequest.prototype, "Limit", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindCategoryRequest.prototype, "Before", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], FindCategoryRequest.prototype, "After", void 0);
exports.FindCategoryRequest = FindCategoryRequest;
class CreateCategoryRequest {
}
__decorate([
    class_validator_1.Length(1, 255),
    __metadata("design:type", String)
], CreateCategoryRequest.prototype, "Title", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsInt(),
    class_validator_1.Min(1),
    __metadata("design:type", Number)
], CreateCategoryRequest.prototype, "MediaId", void 0);
exports.CreateCategoryRequest = CreateCategoryRequest;
class UpdateCategoryRequest {
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 255),
    __metadata("design:type", String)
], UpdateCategoryRequest.prototype, "Title", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsInt(),
    class_validator_1.Min(1),
    __metadata("design:type", Number)
], UpdateCategoryRequest.prototype, "MediaId", void 0);
exports.UpdateCategoryRequest = UpdateCategoryRequest;
//# sourceMappingURL=category.request.js.map