"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryController = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const delete_decorator_1 = require("../../decorator/nest-ext/delete.decorator");
const get_decorator_1 = require("../../decorator/nest-ext/get.decorator");
const patch_decorator_1 = require("../../decorator/nest-ext/patch.decorator");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const put_decorator_1 = require("../../decorator/nest-ext/put.decorator");
const user_entity_1 = require("../auth/user.entity");
const category_request_1 = require("./category.request");
const category_response_1 = require("./category.response");
const category_service_1 = require("./category.service");
let CategoryController = class CategoryController {
    constructor(_categoryService) {
        this._categoryService = _categoryService;
    }
    async GetCategories(data) {
        return await this._categoryService.FindCategories(data);
    }
    async Create(data, user) {
        return await this._categoryService.Create(data, user);
    }
    async Update(id, data, user) {
        return await this._categoryService.Update(id, data, user);
    }
    async Delete(id, user) {
        return await this._categoryService.Delete(id, user);
    }
    async Get(id, user) {
        return await this._categoryService.Get(id, user);
    }
};
__decorate([
    get_decorator_1.Get("/", category_response_1.CategoryResponseModel),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [category_request_1.FindCategoryRequest]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "GetCategories", null);
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post("/", category_request_1.CreateCategoryRequest),
    __param(0, common_1.Body()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [category_request_1.CreateCategoryRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "Create", null);
__decorate([
    authorize_decorator_1.Authorized(),
    patch_decorator_1.Patch("/:id", category_request_1.UpdateCategoryRequest),
    __param(0, common_1.Param("id")), __param(1, common_1.Body()), __param(2, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, category_request_1.UpdateCategoryRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "Update", null);
__decorate([
    authorize_decorator_1.Authorized(),
    delete_decorator_1.Delete("/:id", app_response_1.MessageResponse),
    __param(0, common_1.Param("id")), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "Delete", null);
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get("/:id", {}),
    __param(0, common_1.Param("id")),
    __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "Get", null);
CategoryController = __decorate([
    appcontroller_decorator_1.AppController("category"),
    __metadata("design:paramtypes", [category_service_1.CategoryService])
], CategoryController);
exports.CategoryController = CategoryController;
//# sourceMappingURL=category.controller.js.map