"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryRepository = void 0;
const common_1 = require("@nestjs/common");
const util_helper_1 = require("../../helpers/util.helper");
const base_repository_1 = require("../../repository/base.repository");
const mysql_repository_1 = require("../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const media_entity_1 = require("../media/media.entity");
const category_entity_1 = require("./category.entity");
let CategoryRepository = class CategoryRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = "category";
    }
    _getFindQueryBuilder() {
        let QueryBuilder = this.createQueryBuilder(this.DefaultAlias)
            .leftJoinAndMapOne("category.Media", media_entity_1.MediaModel, "media", "category.media_id = media.id");
        return QueryBuilder;
    }
    async Find(whereParams, paginationOptions, orderByOptions) {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        QueryBuilder = this.ApplyPaginationOnQueryBuilder(QueryBuilder, paginationOptions);
        QueryBuilder = this.ApplyOrderOnQueryBuilder(QueryBuilder, orderByOptions);
        return await QueryBuilder.getMany();
    }
    async Count(whereParams) {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        return await QueryBuilder.getCount();
    }
};
CategoryRepository = __decorate([
    common_1.Injectable(),
    typeorm_1.EntityRepository(category_entity_1.CategoryModel)
], CategoryRepository);
exports.CategoryRepository = CategoryRepository;
//# sourceMappingURL=category.repository.js.map