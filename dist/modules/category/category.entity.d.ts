import { MySQLBaseModel } from "src/model/mysql.entity.";
export declare class CategoryModel extends MySQLBaseModel {
    MediaId: number;
    Title: string;
}
