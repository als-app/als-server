"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryService = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const response_exception_1 = require("../../exception/response.exception");
const date_helper_1 = require("../../helpers/date.helper");
const util_helper_1 = require("../../helpers/util.helper");
const locale_1 = require("../../locale/locale");
const media_repository_1 = require("../media/media.repository");
const category_entity_1 = require("./category.entity");
const category_repository_1 = require("./category.repository");
let CategoryService = class CategoryService {
    constructor(_categoryRepository, _mediaRepository) {
        this._categoryRepository = _categoryRepository;
        this._mediaRepository = _mediaRepository;
    }
    async FindCategories(data) {
        let whereParams = {};
        let Categories = await this._categoryRepository.Find(whereParams, util_helper_1.GetPaginationOptions(data));
        return { Categories };
    }
    async Create(data, user) {
        let Category = new category_entity_1.CategoryModel();
        Category.Title = data.Title;
        if (data.MediaId) {
            let Media = await this._mediaRepository.FindById(data.MediaId);
            if (!Media) {
                throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.MediaNotFound);
            }
            Category.MediaId = data.MediaId;
        }
        Category = await this._categoryRepository.Create(Category);
        return { Message: "Success", Category };
    }
    async Update(id, data, user) {
        let Category = await this._categoryRepository.FindById(id);
        if (!Category) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.NotFoundError);
        }
        if (data.MediaId && Category.MediaId !== data.MediaId) {
            let Media = await this._mediaRepository.FindById(data.MediaId);
            if (!Media) {
                throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.MediaNotFound);
            }
        }
        await Category.Fill(data);
        Category = await this._categoryRepository.Save(Category);
        return { Message: "updated", Category };
    }
    async Delete(id, user) {
        let Category = await this._categoryRepository.FindById(id);
        if (!Category) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.NotFoundError);
        }
        await this._categoryRepository.DeleteById(Category.Id);
        return { Message: "deleted" };
    }
    async Get(id, user) {
        let Category = await this._categoryRepository.FindOne({ Id: id });
        return { Category };
    }
};
CategoryService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [category_repository_1.CategoryRepository, media_repository_1.MediaRepository])
], CategoryService);
exports.CategoryService = CategoryService;
//# sourceMappingURL=category.service.js.map