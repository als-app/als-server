export declare enum CategoryOrderByColumns {
    Id = "Id",
    CreatedAt = "CreatedAt"
}
export declare enum CategoryOrderByDirections {
    ASC = "ASC",
    DESC = "DESC"
}
export declare class FindCategoryRequest {
    Column: string;
    Direction: "ASC" | "DESC";
    Page: number;
    Limit: number;
    Before: number;
    After: number;
}
export declare class CreateCategoryRequest {
    Title: string;
    MediaId: number;
}
export declare class UpdateCategoryRequest {
    Title: string;
    MediaId: number;
}
