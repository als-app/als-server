import { OrderByRequestParams, PaginationDBParams } from "src/helpers/util.helper";
import { MySqlRepository } from "src/repository/mysql.repository";
import { CategoryModel } from "./category.entity";
export declare class CategoryRepository extends MySqlRepository<CategoryModel> {
    protected DefaultAlias: string;
    private _getFindQueryBuilder;
    Find(whereParams: any, paginationOptions?: PaginationDBParams, orderByOptions?: OrderByRequestParams): Promise<Array<CategoryModel>>;
    Count(whereParams: any): Promise<number>;
}
