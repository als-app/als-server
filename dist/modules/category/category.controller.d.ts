import { MessageResponse } from "src/app.response";
import { UserModel } from "../auth/user.entity";
import { CreateCategoryRequest, FindCategoryRequest, UpdateCategoryRequest } from "./category.request";
import { CategoryService } from "./category.service";
export declare class CategoryController {
    private _categoryService;
    constructor(_categoryService: CategoryService);
    GetCategories(data: FindCategoryRequest): Promise<{
        Categories: import("./category.entity").CategoryModel[];
    }>;
    Create(data: CreateCategoryRequest, user: UserModel): Promise<any>;
    Update(id: number, data: UpdateCategoryRequest, user: UserModel): Promise<any>;
    Delete(id: number, user: UserModel): Promise<MessageResponse>;
    Get(id: number, user: UserModel): Promise<{
        Category: import("./category.entity").CategoryModel;
    }>;
}
