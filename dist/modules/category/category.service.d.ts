import { UserModel } from "../auth/user.entity";
import { MediaRepository } from "../media/media.repository";
import { CategoryModel } from "./category.entity";
import { CategoryRepository } from "./category.repository";
import { CreateCategoryRequest, FindCategoryRequest, UpdateCategoryRequest } from "./category.request";
export declare class CategoryService {
    private _categoryRepository;
    private _mediaRepository;
    constructor(_categoryRepository: CategoryRepository, _mediaRepository: MediaRepository);
    FindCategories(data: FindCategoryRequest): Promise<{
        Categories: CategoryModel[];
    }>;
    Create(data: CreateCategoryRequest, user: UserModel): Promise<{
        Message: string;
        Category: CategoryModel;
    }>;
    Update(id: number, data: UpdateCategoryRequest, user: UserModel): Promise<{
        Message: string;
        Category: CategoryModel;
    }>;
    Delete(id: number, user: UserModel): Promise<{
        Message: string;
    }>;
    Get(id: number, user: UserModel): Promise<{
        Category: CategoryModel;
    }>;
}
