"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const optionalAuthorize_decorator_1 = require("../../decorator/optionalAuthorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const user_entity_1 = require("./user.entity");
const user_request_1 = require("./user.request");
const user_response_1 = require("./user.response");
const user_service_1 = require("./user.service");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const get_decorator_1 = require("../../decorator/nest-ext/get.decorator");
const put_decorator_1 = require("../../decorator/nest-ext/put.decorator");
const app_response_1 = require("../../app.response");
const user_device_entity_1 = require("./user_device/user_device.entity");
const current_device_decorator_1 = require("../../decorator/current_device.decorator");
let UserController = class UserController {
    constructor(_userService) {
        this._userService = _userService;
    }
    async Login(data) {
        data["Types"] = [user_entity_1.UserModelType.NormalUser, user_entity_1.UserModelType.DiamondUser];
        return await this._userService.Login(data);
    }
    async Logout(user, device) {
        return await this._userService.Logout(user, device);
    }
    async AdminLogin(data) {
        data["Type"] = user_entity_1.UserModelType.Admin;
        return await this._userService.Login(data);
    }
    async SignUp(data) {
        return await this._userService.SignUp(data);
    }
    async ResendSignUpCode(data) {
        return await this._userService.ResendSignUpCode(data);
    }
    async ResetPassword(data) {
        return await this._userService.ResetPassword(data);
    }
    async ForgetPassword(data) {
        return await this._userService.ForgetPassword(data);
    }
    async ForgetPasswordVerification(data) {
        return await this._userService.ForgetPasswordVerification(data);
    }
    async SignUpVerification(data) {
        return await this._userService.SignUpVerification(data);
    }
    async UserListing(data, user) {
        return await this._userService.UserListing(data);
    }
    async verifyUser(Id, data) {
        console.log('data : ', data, Id);
        return await this._userService.VerifyUser(Number(Id), data);
    }
    async UpdateUserType(Id, data) {
        return await this._userService.UpdateUserType(Number(Id), data);
    }
    async CurrentUser(user, data, device) {
        await this._userService.UpdateFCM(data, device, user);
        if (user.Password) {
            delete user.Password;
        }
        return {
            User: Object.assign(Object.assign({}, user), { Location: user.getLocation() }),
        };
    }
    async EditProfile(data, user, device) {
        return await this._userService.EditProfile(data, user, device);
    }
    async UpdateFCM(data, device, user) {
        return await this._userService.UpdateFCM(data, device, user);
    }
    async GetTransactions(user) {
        return {
            Transactions: [{
                    Id: 1,
                    CreatedAt: 1612206726648,
                    Amount: 10,
                    Title: "Tarot Reading",
                },
                {
                    Id: 2,
                    CreatedAt: 1612206726648,
                    Amount: 10,
                    Title: "Tarot Reading",
                }, {
                    Id: 3,
                    CreatedAt: 1612206726648,
                    Amount: 10,
                    Title: "Tarot Reading",
                }, {
                    Id: 4,
                    CreatedAt: 1612206726648,
                    Amount: 10,
                    Title: "Tarot Reading",
                },
                {
                    Id: 5,
                    CreatedAt: 1612206726648,
                    Amount: 10,
                    Title: "Tarot Reading",
                }],
            TotalTransactions: 5
        };
    }
    async DiscoverUsers(data, user) {
        return await this._userService.DiscoverUser(data, user);
    }
    async DiscoverExpertUsers(data, user) {
        return await this._userService.DiscoverUser(data, user, true);
    }
    async getUserDetails(Id) {
        return await this._userService.getUserDetails(Number(Id));
    }
    async clearDatabase(auth) {
        if (auth === '123456') {
            await this._userService.clearDatabase();
            return 'Successfully cleared database';
        }
        return 'Not Authorized';
    }
};
__decorate([
    post_decorator_1.Post('/login', user_response_1.LoginResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.LoginRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "Login", null);
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post('/logout', {}),
    __param(0, current_user_decorator_1.CurrentUser()), __param(1, current_device_decorator_1.CurrentDevice()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.UserModel, user_device_entity_1.UserDeviceModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "Logout", null);
__decorate([
    post_decorator_1.Post('/admin/login', user_response_1.LoginResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.LoginRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "AdminLogin", null);
__decorate([
    post_decorator_1.Post('/signup', user_response_1.SignUpResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.SignUpRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "SignUp", null);
__decorate([
    post_decorator_1.Post('/signup/resend/code', user_response_1.ResendSignUpCodeResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.ResendSignUpCodeRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "ResendSignUpCode", null);
__decorate([
    post_decorator_1.Post('/reset-password', user_response_1.ResetPasswordResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.ResetPasswordRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "ResetPassword", null);
__decorate([
    post_decorator_1.Post('/forget-password', user_response_1.ForgetPasswordResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.ForgetPasswordRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "ForgetPassword", null);
__decorate([
    post_decorator_1.Post('/forget-password-verification', user_response_1.ForgetPasswordVerificationResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.ForgetPasswordVerificationRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "ForgetPasswordVerification", null);
__decorate([
    post_decorator_1.Post('/signup-verification', user_response_1.SignUpVerificationResponse),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.SignUpVerificationRequest]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "SignUpVerification", null);
__decorate([
    get_decorator_1.Get('/admin/list', user_response_1.GetUserResponse),
    __param(0, common_1.Query()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.UserListingRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "UserListing", null);
__decorate([
    put_decorator_1.Put('/admin/verify/:Id', {}),
    __param(0, common_1.Param('Id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "verifyUser", null);
__decorate([
    put_decorator_1.Put('/admin/type/:Id', {}),
    __param(0, common_1.Param('Id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "UpdateUserType", null);
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get('/me', user_response_1.GetUserResponse),
    __param(0, current_user_decorator_1.CurrentUser()),
    __param(1, common_1.Query()),
    __param(2, current_device_decorator_1.CurrentDevice()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.UserModel,
        user_request_1.UpdateFCMRequest,
        user_device_entity_1.UserDeviceModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "CurrentUser", null);
__decorate([
    authorize_decorator_1.Authorized(),
    put_decorator_1.Put('/', user_response_1.SignUpResponse),
    __param(0, common_1.Body()), __param(1, current_user_decorator_1.CurrentUser()), __param(2, current_device_decorator_1.CurrentDevice()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.EditProfileRequest, user_entity_1.UserModel, user_device_entity_1.UserDeviceModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "EditProfile", null);
__decorate([
    authorize_decorator_1.Authorized(),
    put_decorator_1.Put('/fcm', app_response_1.MessageResponse),
    __param(0, common_1.Body()), __param(1, current_device_decorator_1.CurrentDevice()), __param(2, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.UpdateFCMRequest, user_device_entity_1.UserDeviceModel, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "UpdateFCM", null);
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get('/transaction', {}),
    __param(0, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "GetTransactions", null);
__decorate([
    optionalAuthorize_decorator_1.OptionalAuthorized(),
    get_decorator_1.Get('/discover', user_response_1.GetUserResponse),
    __param(0, common_1.Query()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.UserListingRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "DiscoverUsers", null);
__decorate([
    optionalAuthorize_decorator_1.OptionalAuthorized(),
    get_decorator_1.Get('/experts/discover', user_response_1.GetUserResponse),
    __param(0, common_1.Query()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_request_1.UserListingRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "DiscoverExpertUsers", null);
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get('/details/:Id', user_response_1.GetUserResponse),
    __param(0, common_1.Param('Id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUserDetails", null);
__decorate([
    get_decorator_1.Get('/clear-database', user_response_1.GetUserResponse),
    __param(0, common_1.Headers('auth')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "clearDatabase", null);
UserController = __decorate([
    appcontroller_decorator_1.AppController("user"),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map