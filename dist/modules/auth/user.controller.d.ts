import { UserModel } from './user.entity';
import { EditProfileRequest, ForgetPasswordRequest, ForgetPasswordVerificationRequest, LoginRequest, ResendSignUpCodeRequest, ResetPasswordRequest, SignUpRequest, SignUpVerificationRequest, UpdateFCMRequest, UserListingRequest } from './user.request';
import { UserService } from './user.service';
import { MessageResponse } from 'src/app.response';
import { UserDeviceModel } from './user_device/user_device.entity';
export declare class UserController {
    private readonly _userService;
    constructor(_userService: UserService);
    Login(data: LoginRequest): Promise<{
        Token: string;
        User: UserModel;
        DeviceUUID: string;
        AuthCode: import("../../constant/authentication").AuthenticationCodes;
    }>;
    Logout(user: UserModel, device: UserDeviceModel): Promise<{
        Message: string;
    }>;
    AdminLogin(data: LoginRequest): Promise<{
        Token: string;
        User: UserModel;
        DeviceUUID: string;
        AuthCode: import("../../constant/authentication").AuthenticationCodes;
    }>;
    SignUp(data: SignUpRequest): Promise<{
        Token: string;
        User: UserModel;
        DeviceUUID: string;
        AuthCode: import("../../constant/authentication").AuthenticationCodes;
    }>;
    ResendSignUpCode(data: ResendSignUpCodeRequest): Promise<{
        DeviceUUID: string;
    }>;
    ResetPassword(data: ResetPasswordRequest): Promise<{
        Message: string;
    }>;
    ForgetPassword(data: ForgetPasswordRequest): Promise<{
        Message: string;
    }>;
    ForgetPasswordVerification(data: ForgetPasswordVerificationRequest): Promise<{
        Message: string;
        TokenUUID: any;
        Code: string;
    }>;
    SignUpVerification(data: SignUpVerificationRequest): Promise<{
        Message: string;
        Token: string;
        User: UserModel;
        AuthCode: import("../../constant/authentication").AuthenticationCodes;
    }>;
    UserListing(data: UserListingRequest, user: UserModel): Promise<{
        Users: {
            ReviewCount: any;
            Location: import("../../common/types").Location;
            FullName: string;
            UserName: string;
            BirthDate: string;
            Password: string;
            Details: string;
            Email: string;
            PhoneNumber: string;
            Type: number;
            Status: number;
            MediaId: number;
            SelfieMediaId: number;
            About: string;
            IsVerified: boolean;
            AvailableForInvitation: boolean;
            Id: number;
            CreatedAt: any;
            UpdatedAt: any;
            DeletedAt: any;
        }[];
        TotalUsers: number;
    }>;
    verifyUser(Id: string, data: any): Promise<MessageResponse>;
    UpdateUserType(Id: string, data: any): Promise<MessageResponse>;
    CurrentUser(user: UserModel, data: UpdateFCMRequest, device: UserDeviceModel): Promise<{
        User: {
            Location: import("../../common/types").Location;
            FullName: string;
            UserName: string;
            BirthDate: string;
            Password: string;
            Details: string;
            Email: string;
            PhoneNumber: string;
            Type: number;
            Status: number;
            MediaId: number;
            SelfieMediaId: number;
            About: string;
            IsVerified: boolean;
            AvailableForInvitation: boolean;
            Id: number;
            CreatedAt: any;
            UpdatedAt: any;
            DeletedAt: any;
        };
    }>;
    EditProfile(data: EditProfileRequest, user: UserModel, device: UserDeviceModel): Promise<{
        Message: string;
        User: UserModel;
    }>;
    UpdateFCM(data: UpdateFCMRequest, device: UserDeviceModel, user: UserModel): Promise<MessageResponse>;
    GetTransactions(user: UserModel): Promise<{
        Transactions: {
            Id: number;
            CreatedAt: number;
            Amount: number;
            Title: string;
        }[];
        TotalTransactions: number;
    }>;
    DiscoverUsers(data: UserListingRequest, user: UserModel): Promise<{
        Users: {
            ReviewCount: any;
            Location: import("../../common/types").Location;
            InvitationId: any;
            FullName: string;
            UserName: string;
            BirthDate: string;
            Password: string;
            Details: string;
            Email: string;
            PhoneNumber: string;
            Type: number;
            Status: number;
            MediaId: number;
            SelfieMediaId: number;
            About: string;
            IsVerified: boolean;
            AvailableForInvitation: boolean;
            Id: number;
            CreatedAt: any;
            UpdatedAt: any;
            DeletedAt: any;
        }[];
        TotalUsers: number;
    }>;
    DiscoverExpertUsers(data: UserListingRequest, user: UserModel): Promise<{
        Users: {
            ReviewCount: any;
            Location: import("../../common/types").Location;
            InvitationId: any;
            FullName: string;
            UserName: string;
            BirthDate: string;
            Password: string;
            Details: string;
            Email: string;
            PhoneNumber: string;
            Type: number;
            Status: number;
            MediaId: number;
            SelfieMediaId: number;
            About: string;
            IsVerified: boolean;
            AvailableForInvitation: boolean;
            Id: number;
            CreatedAt: any;
            UpdatedAt: any;
            DeletedAt: any;
        }[];
        TotalUsers: number;
    }>;
    getUserDetails(Id: string): Promise<{
        Location: import("../../common/types").Location;
        FullName: string;
        UserName: string;
        BirthDate: string;
        Password: string;
        Details: string;
        Email: string;
        PhoneNumber: string;
        Type: number;
        Status: number;
        MediaId: number;
        SelfieMediaId: number;
        About: string;
        IsVerified: boolean;
        AvailableForInvitation: boolean;
        Id: number;
        CreatedAt: any;
        UpdatedAt: any;
        DeletedAt: any;
    }>;
    clearDatabase(auth: string): Promise<"Not Authorized" | "Successfully cleared database">;
}
