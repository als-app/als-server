"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AuthService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = exports.SocketAuth = exports.Auth = void 0;
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const user_repository_1 = require("./user.repository");
const typeorm_1 = require("@nestjs/typeorm");
const redis_service_1 = require("../../redis/redis.service");
const socket_1 = require("../../constant/socket");
class Auth {
    constructor(userId, user) {
        this.UserId = userId;
        if (user) {
            this.User = user;
        }
    }
}
exports.Auth = Auth;
class SocketAuth {
    constructor(socketId, userId, userType, advisorId) {
        this.UserId = userId;
        this.SocketId = socketId;
        this.UserType = userType;
        if (advisorId) {
            this.AdvisorId = advisorId;
        }
    }
}
exports.SocketAuth = SocketAuth;
let AuthService = AuthService_1 = class AuthService {
    constructor(_redisCacheService, _userRepository) {
        this._redisCacheService = _redisCacheService;
        this._userRepository = _userRepository;
    }
    static _generateToken() {
        return uuid_1.v4();
    }
    static _getKey(token) {
        return 'APP-TOKEN:' + token;
    }
    static _getSocketKey(socketId) {
        return 'SS-SOCK:' + socketId;
    }
    static _getSocketEntityKey(entityId, entityType) {
        return 'SS-SOCK-ENT:' + entityType + "-" + entityId;
    }
    GetToken() {
        return AuthService_1._generateToken();
    }
    async CreateSession(userId) {
        let token = AuthService_1._generateToken();
        let TokenData = new Auth(userId);
        await this._redisCacheService.Set(AuthService_1._getKey(token), JSON.stringify(TokenData));
        return token;
    }
    async SetSession(token, userId) {
        let TokenData = new Auth(userId);
        await this._redisCacheService.Set(AuthService_1._getKey(token), JSON.stringify(TokenData));
        return true;
    }
    async GetSession(token) {
        let RawData = await this._redisCacheService.Get(AuthService_1._getKey(token));
        if (RawData) {
            let RawObject = JSON.parse(RawData);
            let Data = new Auth(RawObject.UserId, RawObject.AccountMemberships);
            Data.User = await this._userRepository.FindById(Data.UserId);
            return Data;
        }
        return null;
    }
    async GetEntityDataBySocketId(socketId) {
        let RawData = await this._redisCacheService.Get(AuthService_1._getSocketKey(socketId));
        try {
            let Data = JSON.parse(RawData);
            return Data;
        }
        catch (err) {
        }
    }
    async SetSocketIdsByUserId(socketIds, userId, userType) {
        await this._redisCacheService.Set(AuthService_1._getSocketEntityKey(userId, userType), JSON.stringify(socketIds));
        return true;
    }
    async SetCustomerSocketId(socketId, userId) {
        let Data = new SocketAuth(socketId, userId, socket_1.SocketEventSenderType.User);
        await this._redisCacheService.Set(AuthService_1._getSocketKey(socketId), JSON.stringify(Data));
        return true;
    }
    async SetVendorSocketId(socketId, userId, advisorId) {
        let Data = new SocketAuth(socketId, userId, socket_1.SocketEventSenderType.Advisor, advisorId);
        await this._redisCacheService.Set(AuthService_1._getSocketKey(socketId), JSON.stringify(Data));
        return true;
    }
    async GetSocketIdsByUserId(userId, userType) {
        let RawData = await this._redisCacheService.Get(AuthService_1._getSocketEntityKey(userId, userType));
        try {
            let Data = JSON.parse(RawData) || [];
            return Data;
        }
        catch (err) {
            return [];
        }
    }
    async RemoveEntityDataBySocketId(socketId) {
        await this._redisCacheService.Delete(AuthService_1._getSocketKey(socketId));
        return true;
    }
    async RemoveSocketIdFromUserSockets(socketId, userId, userType) {
        let SocketIds = await this.GetSocketIdsByUserId(userId, userType);
        SocketIds = SocketIds.filter(id => id !== socketId);
        await this.SetSocketIdsByUserId(SocketIds, userId, userType);
        return true;
    }
};
AuthService = AuthService_1 = __decorate([
    common_1.Injectable(),
    __param(1, typeorm_1.InjectRepository(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [redis_service_1.RedisCacheService,
        user_repository_1.UserRepository])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map