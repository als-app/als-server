import { OrderByRequestParams, PaginationDBParams } from 'src/helpers/util.helper';
import { MySqlRepository } from 'src/repository/mysql.repository';
import { SelectQueryBuilder } from 'typeorm';
import { UserModel } from './user.entity';
export declare class UserRepository extends MySqlRepository<UserModel> {
    protected DefaultAlias: string;
    protected ApplyConditionOnQueryBuilder(query: SelectQueryBuilder<UserModel>, params: any): SelectQueryBuilder<UserModel>;
    private _getFindQueryBuilder;
    Find(whereParams: any, paginationOptions?: PaginationDBParams, orderByOptions?: OrderByRequestParams, currentUserId?: number, populateInvitations?: boolean): Promise<UserModel[]>;
    Count(whereParams: any, currentUserId?: number): Promise<number>;
    getUserById(Id: number): Promise<UserModel>;
    getUserByEmailAndTypes(Email: string, Types: number[]): Promise<UserModel>;
    clearDatabase(): Promise<any>;
    updateExperts(Id: any): Promise<any>;
}
