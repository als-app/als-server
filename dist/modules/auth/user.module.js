"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_service_1 = require("./user.service");
const user_controller_1 = require("./user.controller");
const user_entity_1 = require("./user.entity");
const user_repository_1 = require("./user.repository");
const auth_service_1 = require("./auth.service");
const user_device_entity_1 = require("./user_device/user_device.entity");
const user_token_entity_1 = require("./user_token/user_token.entity");
const user_device_repository_1 = require("./user_device/user_device.repository");
const user_token_repository_1 = require("./user_token/user_token.repository");
const redis_module_1 = require("../../redis/redis.module");
const review_module_1 = require("../review/review.module");
const invitation_module_1 = require("../invitation/invitation.module");
let UserModule = class UserModule {
};
UserModule = __decorate([
    common_1.Module({
        imports: [
            redis_module_1.RedisModule,
            typeorm_1.TypeOrmModule.forFeature([
                user_entity_1.UserModel,
                user_repository_1.UserRepository,
                user_device_entity_1.UserDeviceModel,
                user_token_entity_1.UserTokenModel,
                user_device_repository_1.UserDeviceRepository,
                user_token_repository_1.UserTokenRepository,
            ]),
            invitation_module_1.InvitationModule,
            common_1.forwardRef(() => review_module_1.ReviewModule),
        ],
        exports: [auth_service_1.AuthService, user_service_1.UserService],
        providers: [user_service_1.UserService, auth_service_1.AuthService],
        controllers: [user_controller_1.UserController],
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map