export declare class UserResponseModel {
    Id: number;
    FullName: string;
    Email: string;
    Type: number;
    MediaId: number;
    Gender: number;
}
export declare class LoginResponse {
    DeviceUUID: string;
    Token: string;
    User: UserResponseModel;
    AuthCode: number;
}
export declare class SignUpResponse {
    DeviceUUID: string;
    Token: string;
    User: UserResponseModel;
    AuthCode: number;
}
export declare class ResendSignUpCodeResponse {
    DeviceUUID: string;
}
export declare class ForgetPasswordResponse {
    TokenUUID: string;
}
export declare class SignUpVerificationResponse {
    Message: string;
    Token: string;
    User: UserResponseModel;
    AuthCode: number;
}
export declare class ForgetPasswordVerificationResponse {
    Message: string;
    TokenUUID: string;
    Code: string;
}
export declare class ResetPasswordResponse {
    Message: string;
}
export declare class GuestLoginResponse {
    DeviceUUID: string;
    Token: string;
    User: UserResponseModel;
}
export declare class GetUserResponse {
    User: UserResponseModel;
}
