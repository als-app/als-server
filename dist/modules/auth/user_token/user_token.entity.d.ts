import { MySQLBaseModel } from 'src/model/mysql.entity.';
export declare class UserTokenModel extends MySQLBaseModel {
    UserId: number;
    DeviceId: number;
    Code: string;
    UUID: string;
    Attempts: number;
    ExpireTime: number;
    Meta: object;
}
