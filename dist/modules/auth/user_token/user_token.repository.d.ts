import { MySqlRepository } from 'src/repository/mysql.repository';
import { UserTokenModel } from './user_token.entity';
export declare class UserTokenRepository extends MySqlRepository<UserTokenModel> {
    protected DefaultAlias: string;
}
