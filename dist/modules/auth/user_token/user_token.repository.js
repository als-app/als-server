"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTokenRepository = void 0;
const common_1 = require("@nestjs/common");
const mysql_repository_1 = require("../../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const user_token_entity_1 = require("./user_token.entity");
let UserTokenRepository = class UserTokenRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = 'user_token';
    }
};
UserTokenRepository = __decorate([
    common_1.Injectable(),
    typeorm_1.EntityRepository(user_token_entity_1.UserTokenModel)
], UserTokenRepository);
exports.UserTokenRepository = UserTokenRepository;
//# sourceMappingURL=user_token.repository.js.map