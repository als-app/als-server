"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTokenModel = void 0;
const base_entity_1 = require("../../../model/base.entity");
const typeorm_1 = require("typeorm");
const mysql_entity_1 = require("../../../model/mysql.entity.");
let UserTokenModel = class UserTokenModel extends mysql_entity_1.MySQLBaseModel {
};
__decorate([
    typeorm_1.Column({
        name: 'user_id',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], UserTokenModel.prototype, "UserId", void 0);
__decorate([
    typeorm_1.Column({
        name: 'device_id',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], UserTokenModel.prototype, "DeviceId", void 0);
__decorate([
    typeorm_1.Column({
        name: 'code',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserTokenModel.prototype, "Code", void 0);
__decorate([
    typeorm_1.Column({
        name: 'uuid',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserTokenModel.prototype, "UUID", void 0);
__decorate([
    typeorm_1.Column({
        name: 'attempts',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], UserTokenModel.prototype, "Attempts", void 0);
__decorate([
    typeorm_1.Column({
        name: 'expire_time',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], UserTokenModel.prototype, "ExpireTime", void 0);
__decorate([
    typeorm_1.Column({
        name: 'meta',
        type: 'json',
    }),
    __metadata("design:type", Object)
], UserTokenModel.prototype, "Meta", void 0);
UserTokenModel = __decorate([
    typeorm_1.Entity('user_token')
], UserTokenModel);
exports.UserTokenModel = UserTokenModel;
//# sourceMappingURL=user_token.entity.js.map