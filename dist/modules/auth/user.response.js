"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetUserResponse = exports.GuestLoginResponse = exports.ResetPasswordResponse = exports.ForgetPasswordVerificationResponse = exports.SignUpVerificationResponse = exports.ForgetPasswordResponse = exports.ResendSignUpCodeResponse = exports.SignUpResponse = exports.LoginResponse = exports.UserResponseModel = void 0;
const swagger_1 = require("@nestjs/swagger");
class UserResponseModel {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UserResponseModel.prototype, "Id", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UserResponseModel.prototype, "FullName", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UserResponseModel.prototype, "Email", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UserResponseModel.prototype, "Type", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UserResponseModel.prototype, "MediaId", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UserResponseModel.prototype, "Gender", void 0);
exports.UserResponseModel = UserResponseModel;
class LoginResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], LoginResponse.prototype, "DeviceUUID", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], LoginResponse.prototype, "Token", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", UserResponseModel)
], LoginResponse.prototype, "User", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], LoginResponse.prototype, "AuthCode", void 0);
exports.LoginResponse = LoginResponse;
class SignUpResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], SignUpResponse.prototype, "DeviceUUID", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], SignUpResponse.prototype, "Token", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", UserResponseModel)
], SignUpResponse.prototype, "User", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], SignUpResponse.prototype, "AuthCode", void 0);
exports.SignUpResponse = SignUpResponse;
class ResendSignUpCodeResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], ResendSignUpCodeResponse.prototype, "DeviceUUID", void 0);
exports.ResendSignUpCodeResponse = ResendSignUpCodeResponse;
class ForgetPasswordResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], ForgetPasswordResponse.prototype, "TokenUUID", void 0);
exports.ForgetPasswordResponse = ForgetPasswordResponse;
class SignUpVerificationResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], SignUpVerificationResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], SignUpVerificationResponse.prototype, "Token", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", UserResponseModel)
], SignUpVerificationResponse.prototype, "User", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], SignUpVerificationResponse.prototype, "AuthCode", void 0);
exports.SignUpVerificationResponse = SignUpVerificationResponse;
class ForgetPasswordVerificationResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], ForgetPasswordVerificationResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], ForgetPasswordVerificationResponse.prototype, "TokenUUID", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], ForgetPasswordVerificationResponse.prototype, "Code", void 0);
exports.ForgetPasswordVerificationResponse = ForgetPasswordVerificationResponse;
class ResetPasswordResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], ResetPasswordResponse.prototype, "Message", void 0);
exports.ResetPasswordResponse = ResetPasswordResponse;
class GuestLoginResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], GuestLoginResponse.prototype, "DeviceUUID", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], GuestLoginResponse.prototype, "Token", void 0);
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", UserResponseModel)
], GuestLoginResponse.prototype, "User", void 0);
exports.GuestLoginResponse = GuestLoginResponse;
class GetUserResponse {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", UserResponseModel)
], GetUserResponse.prototype, "User", void 0);
exports.GetUserResponse = GetUserResponse;
//# sourceMappingURL=user.response.js.map