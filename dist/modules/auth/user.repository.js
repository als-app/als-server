"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const common_1 = require("@nestjs/common");
const util_helper_1 = require("../../helpers/util.helper");
const base_repository_1 = require("../../repository/base.repository");
const mysql_repository_1 = require("../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const media_entity_1 = require("../media/media.entity");
const user_entity_1 = require("./user.entity");
let UserRepository = class UserRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = 'user';
    }
    ApplyConditionOnQueryBuilder(query, params) {
        let Params = {
            Id: params.Id,
            FullName: params.FullName,
            UserName: params.UserName,
            BirthDate: params.BirthDate,
            Email: params.Email,
            PhoneNumber: params.PhoneNumber,
            Type: params.Type,
            Status: params.Status,
            MediaId: params.MediaId,
        };
        if (params.IsVerified) {
            Params['IsVerified'] = true;
        }
        if (params.AvailableForInvitation) {
            Params['AvailableForInvitation'] = true;
        }
        super.ApplyConditionOnQueryBuilder(query, Params);
        let Key = "Q_Like";
        let Val = params[Key];
        if (Val !== undefined) {
            query.andWhere(new typeorm_1.Brackets(qb => {
                qb.orWhere(`user.email LIKE '%${Val}%'`);
                qb.orWhere(`user.full_name LIKE '%${Val}%'`);
                qb.orWhere("user.user_name LIKE :order_number", { order_number: Val + "%" });
                qb.orWhere(`user.phone_number LIKE '%${Val}%'`);
            }));
        }
        return query;
    }
    _getFindQueryBuilder(withMedia) {
        let QueryBuilder = this.createQueryBuilder(this.DefaultAlias);
        if (withMedia) {
            QueryBuilder.leftJoinAndMapOne("user.Media", media_entity_1.MediaModel, "media", "user.media_id = media.id");
            QueryBuilder.leftJoinAndMapOne("user.SelfieMedia", media_entity_1.MediaModel, "selfie", "user.media_id = selfie.id");
        }
        return QueryBuilder;
    }
    async Find(whereParams, paginationOptions, orderByOptions, currentUserId, populateInvitations = false) {
        let QueryBuilder = this._getFindQueryBuilder(true);
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        if (currentUserId) {
            QueryBuilder.andWhere(`${this.DefaultAlias}.Id <> :currentUserId`, { currentUserId });
            if (populateInvitations) {
                QueryBuilder.innerJoinAndSelect(`${this.DefaultAlias}.Invitations`, 'Invitation');
            }
        }
        QueryBuilder = this.ApplyPaginationOnQueryBuilder(QueryBuilder, paginationOptions);
        if (orderByOptions) {
            QueryBuilder = this.ApplyOrderOnQueryBuilder(QueryBuilder, orderByOptions);
        }
        return await QueryBuilder.getMany();
    }
    async Count(whereParams, currentUserId) {
        let QueryBuilder = this._getFindQueryBuilder();
        QueryBuilder = this.ApplyConditionOnQueryBuilder(QueryBuilder, whereParams);
        if (currentUserId) {
            QueryBuilder.andWhere(`${this.DefaultAlias}.Id <> :currentUserId`, { currentUserId });
        }
        return await QueryBuilder.getCount();
    }
    getUserById(Id) {
        let QueryBuilder = this.createQueryBuilder(this.DefaultAlias)
            .leftJoinAndMapOne("user.Media", media_entity_1.MediaModel, "media", "user.media_id = media.id")
            .where('user.Id = :Id', { Id });
        return QueryBuilder.getOne();
    }
    getUserByEmailAndTypes(Email, Types) {
        let QueryBuilder = this.createQueryBuilder(this.DefaultAlias)
            .leftJoinAndMapOne("user.Media", media_entity_1.MediaModel, "media", "user.media_id = media.id")
            .leftJoinAndMapOne("user.SelfieMedia", media_entity_1.MediaModel, "selfie", "user.media_id = selfie.id")
            .andWhere(`${this.DefaultAlias}.DeletedAt = 0`)
            .andWhere(`${this.DefaultAlias}.Email = :Email`, { Email })
            .andWhere(`${this.DefaultAlias}.Type in (:...types)`, { types: Types });
        return QueryBuilder.getOne();
    }
    async clearDatabase() {
        await this.query('update `user` set deleted_at =now() where type=1');
        await this.query('update `invitation` set deleted_at =now()');
        await this.query('update `chat` set deleted_at =now()');
        await this.query('update `notification` set deleted_at =now()');
        await this.query('update `reviews` set deleted_at =now()');
        await this.query('update `media` set deleted_at =now()');
        return true;
    }
    async updateExperts(Id) {
        await this.query('update `user` set type=? where type=? and id<>?', [user_entity_1.UserModelType.NormalUser, user_entity_1.UserModelType.DiamondUser, Id]);
        return true;
    }
};
UserRepository = __decorate([
    common_1.Injectable(),
    typeorm_1.EntityRepository(user_entity_1.UserModel)
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map