"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListingRequest = exports.GuestLoginRequest = exports.ResetPasswordRequest = exports.ForgetPasswordVerificationRequest = exports.SignUpVerificationRequest = exports.ForgetPasswordRequest = exports.ResendSignUpCodeRequest = exports.SignUpRequest = exports.UpdateFCMRequest = exports.EditProfileRequest = exports.LoginRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class LoginRequest {
    ;
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], LoginRequest.prototype, "Email", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(6, 128),
    __metadata("design:type", String)
], LoginRequest.prototype, "Password", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(0, 1000),
    __metadata("design:type", String)
], LoginRequest.prototype, "DeviceUUID", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(0, 1000),
    __metadata("design:type", String)
], LoginRequest.prototype, "FCMToken", void 0);
exports.LoginRequest = LoginRequest;
class EditProfileRequest {
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(6, 128),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "Password", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(6, 128),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "OldPassword", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(6, 55),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "FullName", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 55),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "BirthDate", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 55),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "UserName", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 55),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "PhoneNumber", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], EditProfileRequest.prototype, "MediaId", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], EditProfileRequest.prototype, "SelfieMediaId", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "About", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], EditProfileRequest.prototype, "Status", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "FCMToken", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "Lat", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "Lng", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], EditProfileRequest.prototype, "AvailableForInvitation", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditProfileRequest.prototype, "Details", void 0);
exports.EditProfileRequest = EditProfileRequest;
class UpdateFCMRequest {
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], UpdateFCMRequest.prototype, "FCMToken", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateFCMRequest.prototype, "Lat", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateFCMRequest.prototype, "Lng", void 0);
exports.UpdateFCMRequest = UpdateFCMRequest;
class SignUpRequest {
}
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], SignUpRequest.prototype, "Email", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 128),
    __metadata("design:type", String)
], SignUpRequest.prototype, "Password", void 0);
__decorate([
    class_validator_1.Length(3, 55),
    __metadata("design:type", String)
], SignUpRequest.prototype, "FullName", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 55),
    __metadata("design:type", String)
], SignUpRequest.prototype, "BirthDate", void 0);
__decorate([
    class_validator_1.Length(1, 55),
    __metadata("design:type", String)
], SignUpRequest.prototype, "UserName", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 55),
    __metadata("design:type", String)
], SignUpRequest.prototype, "PhoneNumber", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(0, 1000),
    __metadata("design:type", String)
], SignUpRequest.prototype, "FCMToken", void 0);
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], SignUpRequest.prototype, "DeviceUUID", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], SignUpRequest.prototype, "Details", void 0);
exports.SignUpRequest = SignUpRequest;
class ResendSignUpCodeRequest {
}
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], ResendSignUpCodeRequest.prototype, "DeviceUUID", void 0);
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], ResendSignUpCodeRequest.prototype, "Email", void 0);
exports.ResendSignUpCodeRequest = ResendSignUpCodeRequest;
class ForgetPasswordRequest {
}
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], ForgetPasswordRequest.prototype, "Email", void 0);
exports.ForgetPasswordRequest = ForgetPasswordRequest;
class SignUpVerificationRequest {
}
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], SignUpVerificationRequest.prototype, "Code", void 0);
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], SignUpVerificationRequest.prototype, "DeviceUUID", void 0);
exports.SignUpVerificationRequest = SignUpVerificationRequest;
class ForgetPasswordVerificationRequest {
}
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], ForgetPasswordVerificationRequest.prototype, "Code", void 0);
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], ForgetPasswordVerificationRequest.prototype, "TokenUUID", void 0);
exports.ForgetPasswordVerificationRequest = ForgetPasswordVerificationRequest;
class ResetPasswordRequest {
}
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], ResetPasswordRequest.prototype, "Code", void 0);
__decorate([
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], ResetPasswordRequest.prototype, "TokenUUID", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ResetPasswordRequest.prototype, "Password", void 0);
exports.ResetPasswordRequest = ResetPasswordRequest;
class GuestLoginRequest {
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.Length(1, 1000),
    __metadata("design:type", String)
], GuestLoginRequest.prototype, "DeviceUUID", void 0);
exports.GuestLoginRequest = GuestLoginRequest;
class UserListingRequest {
}
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UserListingRequest.prototype, "Page", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UserListingRequest.prototype, "Limit", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UserListingRequest.prototype, "Before", void 0);
__decorate([
    swagger_1.ApiProperty({ required: false }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UserListingRequest.prototype, "After", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserListingRequest.prototype, "Q", void 0);
exports.UserListingRequest = UserListingRequest;
//# sourceMappingURL=user.request.js.map