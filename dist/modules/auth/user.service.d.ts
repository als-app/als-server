import { AuthService } from './auth.service';
import { UserModel } from './user.entity';
import { UserRepository } from './user.repository';
import { ForgetPasswordRequest, ForgetPasswordVerificationRequest, LoginRequest, ResendSignUpCodeRequest, ResetPasswordRequest, SignUpRequest, SignUpVerificationRequest, UpdateFCMRequest, UserListingRequest, EditProfileRequest } from './user.request';
import { UserDeviceModel } from './user_device/user_device.entity';
import { UserDeviceRepository } from './user_device/user_device.repository';
import { UserTokenRepository } from './user_token/user_token.repository';
import { AuthenticationCodes } from 'src/constant/authentication';
import { MessageResponse } from 'src/app.response';
import { ReviewService } from '../review/review.service';
import { InvitationService } from '../invitation/invitation.service';
export declare class UserService {
    private _userRepository;
    private _userDeviceRepository;
    private _userTokenRepository;
    private _authService;
    private _reviewService;
    private _invitationService;
    constructor(_userRepository: UserRepository, _userDeviceRepository: UserDeviceRepository, _userTokenRepository: UserTokenRepository, _authService: AuthService, _reviewService: ReviewService, _invitationService: InvitationService);
    Login(data: LoginRequest): Promise<{
        Token: string;
        User: UserModel;
        DeviceUUID: string;
        AuthCode: AuthenticationCodes;
    }>;
    FindById(id: number): Promise<UserModel>;
    UpdateFCM(data: UpdateFCMRequest, device: UserDeviceModel, user: UserModel): Promise<MessageResponse>;
    ResendSignUpCode(data: ResendSignUpCodeRequest): Promise<{
        DeviceUUID: string;
    }>;
    SignUp(data: SignUpRequest): Promise<{
        Token: string;
        User: UserModel;
        DeviceUUID: string;
        AuthCode: AuthenticationCodes;
    }>;
    ForgetPassword(data: ForgetPasswordRequest): Promise<{
        Message: string;
    }>;
    SignUpVerification(data: SignUpVerificationRequest): Promise<{
        Message: string;
        Token: string;
        User: UserModel;
        AuthCode: AuthenticationCodes;
    }>;
    ForgetPasswordVerification(data: ForgetPasswordVerificationRequest): Promise<{
        Message: string;
        TokenUUID: any;
        Code: string;
    }>;
    ResetPassword(data: ResetPasswordRequest): Promise<{
        Message: string;
    }>;
    UserListing(data: UserListingRequest, user?: UserModel, fromMobileApp?: boolean): Promise<{
        Users: {
            ReviewCount: any;
            Location: import("../../common/types").Location;
            FullName: string;
            UserName: string;
            BirthDate: string;
            Password: string;
            Details: string;
            Email: string;
            PhoneNumber: string;
            Type: number;
            Status: number;
            MediaId: number;
            SelfieMediaId: number;
            About: string;
            IsVerified: boolean;
            AvailableForInvitation: boolean;
            Id: number;
            CreatedAt: any;
            UpdatedAt: any;
            DeletedAt: any;
        }[];
        TotalUsers: number;
    }>;
    DiscoverUser(data: UserListingRequest, currentUser?: UserModel, showExperts?: boolean): Promise<{
        Users: {
            ReviewCount: any;
            Location: import("../../common/types").Location;
            InvitationId: any;
            FullName: string;
            UserName: string;
            BirthDate: string;
            Password: string;
            Details: string;
            Email: string;
            PhoneNumber: string;
            Type: number;
            Status: number;
            MediaId: number;
            SelfieMediaId: number;
            About: string;
            IsVerified: boolean;
            AvailableForInvitation: boolean;
            Id: number;
            CreatedAt: any;
            UpdatedAt: any;
            DeletedAt: any;
        }[];
        TotalUsers: number;
    }>;
    EditProfile(data: EditProfileRequest, user: UserModel, device?: UserDeviceModel): Promise<{
        Message: string;
        User: UserModel;
    }>;
    GetUserById(Id: number): Promise<UserModel>;
    getUserDetails(Id: number): Promise<{
        Location: import("../../common/types").Location;
        FullName: string;
        UserName: string;
        BirthDate: string;
        Password: string;
        Details: string;
        Email: string;
        PhoneNumber: string;
        Type: number;
        Status: number;
        MediaId: number;
        SelfieMediaId: number;
        About: string;
        IsVerified: boolean;
        AvailableForInvitation: boolean;
        Id: number;
        CreatedAt: any;
        UpdatedAt: any;
        DeletedAt: any;
    }>;
    Logout(user: UserModel, device?: UserDeviceModel): Promise<{
        Message: string;
    }>;
    VerifyUser(Id: number, data: any): Promise<MessageResponse>;
    UpdateUserType(Id: number, data: any): Promise<MessageResponse>;
    clearDatabase(): Promise<boolean>;
}
