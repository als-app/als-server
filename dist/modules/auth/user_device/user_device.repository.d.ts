import { MySqlRepository } from 'src/repository/mysql.repository';
import { UserDeviceModel } from './user_device.entity';
export declare class UserDeviceRepository extends MySqlRepository<UserDeviceModel> {
    protected DefaultAlias: string;
}
