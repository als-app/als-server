import { MySQLBaseModel } from 'src/model/mysql.entity.';
export declare class UserDeviceModel extends MySQLBaseModel {
    UserId: number;
    UserAgent: string;
    UUID: string;
    AuthToken: string;
    FCMToken: string;
}
