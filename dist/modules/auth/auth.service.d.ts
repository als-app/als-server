import { UserModel } from './user.entity';
import { UserRepository } from './user.repository';
import { RedisCacheService } from 'src/redis/redis.service';
import { SocketEventSenderType } from 'src/constant/socket';
export declare class Auth {
    UserId: number;
    User: UserModel;
    constructor(userId: number, user?: UserModel);
}
export declare class SocketAuth {
    UserId: number;
    SocketId: string;
    UserType: SocketEventSenderType;
    AdvisorId: number;
    constructor(socketId: string, userId: number, userType: SocketEventSenderType, advisorId?: number);
}
export declare class AuthService {
    private readonly _redisCacheService;
    private _userRepository;
    constructor(_redisCacheService: RedisCacheService, _userRepository: UserRepository);
    private static _generateToken;
    private static _getKey;
    private static _getSocketKey;
    private static _getSocketEntityKey;
    GetToken(): string;
    CreateSession(userId: number): Promise<string>;
    SetSession(token: string, userId: number): Promise<boolean>;
    GetSession(token: string): Promise<Auth>;
    GetEntityDataBySocketId(socketId: string): Promise<SocketAuth>;
    SetSocketIdsByUserId(socketIds: Array<string>, userId: number, userType: SocketEventSenderType): Promise<boolean>;
    SetCustomerSocketId(socketId: string, userId: number): Promise<boolean>;
    SetVendorSocketId(socketId: string, userId: number, advisorId: number): Promise<boolean>;
    GetSocketIdsByUserId(userId: number, userType: SocketEventSenderType): Promise<Array<string>>;
    RemoveEntityDataBySocketId(socketId: string): Promise<boolean>;
    RemoveSocketIdFromUserSockets(socketId: string, userId: number, userType: SocketEventSenderType): Promise<boolean>;
}
