import { MySQLBaseModel } from 'src/model/mysql.entity.';
import { Location } from '../../common/types';
export declare class UserModel extends MySQLBaseModel {
    FullName: string;
    UserName: string;
    BirthDate: string;
    Password: string;
    Details: string;
    Email: string;
    PhoneNumber: string;
    Type: number;
    Status: number;
    MediaId: number;
    SelfieMediaId: number;
    About: string;
    IsVerified: boolean;
    AvailableForInvitation: boolean;
    Location: string;
    getLocation(): Location;
}
export declare enum UserModelType {
    NormalUser = 1,
    DiamondUser = 3,
    Admin = 2
}
export declare enum UserModelStatus {
    Unverified = 0,
    Verified = 1
}
