"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const util_helper_1 = require("../../helpers/util.helper");
const auth_service_1 = require("./auth.service");
const user_entity_1 = require("./user.entity");
const user_repository_1 = require("./user.repository");
const user_request_1 = require("./user.request");
const user_device_entity_1 = require("./user_device/user_device.entity");
const user_device_repository_1 = require("./user_device/user_device.repository");
const user_token_repository_1 = require("./user_token/user_token.repository");
const user_token_entity_1 = require("./user_token/user_token.entity");
const authentication_1 = require("../../constant/authentication");
const response_exception_1 = require("../../exception/response.exception");
const locale_1 = require("../../locale/locale");
const logger_helper_1 = require("../../helpers/logger.helper");
const queue_helper_1 = require("../../helpers/queue.helper");
const queue_1 = require("../../constant/queue");
const template_renderer_1 = require("../../templates/template_renderer");
const app_response_1 = require("../../app.response");
const review_service_1 = require("../review/review.service");
const invitation_service_1 = require("../invitation/invitation.service");
let UserService = class UserService {
    constructor(_userRepository, _userDeviceRepository, _userTokenRepository, _authService, _reviewService, _invitationService) {
        this._userRepository = _userRepository;
        this._userDeviceRepository = _userDeviceRepository;
        this._userTokenRepository = _userTokenRepository;
        this._authService = _authService;
        this._reviewService = _reviewService;
        this._invitationService = _invitationService;
    }
    async Login(data) {
        var _a;
        let User;
        if ((_a = data.Types) === null || _a === void 0 ? void 0 : _a.length) {
            User = await this._userRepository.getUserByEmailAndTypes(data.Email, data.Types);
        }
        else {
            User = await this._userRepository.FindOne({
                Email: data.Email,
                Type: data.Type
            });
        }
        if (!User || !(await util_helper_1.ComparePassword(data.Password, User.Password))) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidCredentials);
        }
        let Token = await this._authService.CreateSession(User.Id);
        let UserDeviceInfo;
        if (data.DeviceUUID) {
            UserDeviceInfo = await this._userDeviceRepository.FindOne({
                UUID: data.DeviceUUID,
                UserId: User.Id,
            });
        }
        let UpdatedData = {
            AuthToken: Token,
        };
        if (data.FCMToken) {
            UpdatedData.FCMToken = data.FCMToken;
        }
        if (UserDeviceInfo) {
            await this._userDeviceRepository.Update({ Id: UserDeviceInfo.Id }, UpdatedData);
        }
        else {
            UserDeviceInfo = new user_device_entity_1.UserDeviceModel();
            UserDeviceInfo.UserId = User.Id;
            UserDeviceInfo.AuthToken = Token;
            UserDeviceInfo.UUID = uuid_1.v4();
            if (data.FCMToken) {
                UserDeviceInfo.FCMToken = data.FCMToken;
            }
            UserDeviceInfo.UserAgent = data['UserAgent'];
            await this._userDeviceRepository.Create(UserDeviceInfo);
        }
        if (User.Status === user_entity_1.UserModelStatus.Unverified && user_entity_1.UserModelType.NormalUser === User.Type) {
            let UserToken = await this._userTokenRepository.FindOne({
                DeviceId: UserDeviceInfo.Id,
                UserId: User.Id,
            });
            let NewCode = util_helper_1.GenerateVerificationCode();
            if (UserToken) {
                await this._userTokenRepository.Update({ Id: UserToken.Id }, { Code: NewCode, Attempts: 0 });
            }
            else {
                UserToken = new user_token_entity_1.UserTokenModel();
                UserToken.UUID = uuid_1.v4();
                UserToken.UserId = User.Id;
                UserToken.DeviceId = UserDeviceInfo.Id;
                UserToken.Code = NewCode;
                UserToken.Attempts = 0;
                await this._userTokenRepository.Create(UserToken);
            }
        }
        return {
            Token: Token,
            User: User,
            DeviceUUID: UserDeviceInfo.UUID,
            AuthCode: User.Status === user_entity_1.UserModelStatus.Unverified
                ? authentication_1.AuthenticationCodes.UserVerificationPending
                : authentication_1.AuthenticationCodes.UserLoginSuccess,
        };
    }
    async FindById(id) {
        return await this._userRepository.FindById(id);
    }
    async UpdateFCM(data, device, user) {
        if (device.UserId !== user.Id) {
            throw new common_1.ForbiddenException(locale_1.MessageTemplates.ForbiddenError);
        }
        if (data.Lat && data.Lng) {
            await this._userRepository.Update({
                Id: user.Id,
            }, {
                Location: `Point(${data.Lat} ${data.Lng})`
            });
        }
        await this._userDeviceRepository.Update({
            Id: device.Id
        }, {
            FCMToken: data.FCMToken
        });
        return {
            Message: "Updated"
        };
    }
    async ResendSignUpCode(data) {
        let User = await this._userRepository.FindOne({
            Email: data.Email,
        });
        if (!User) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.UserNotFound);
        }
        let UserDevice = await this._userDeviceRepository.FindOne({
            UserId: User.Id
        });
        if (!UserDevice) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.DeviceNotFound);
        }
        let UserToken = await this._userTokenRepository.FindOne({
            DeviceId: UserDevice.Id,
            UserId: User.Id,
        });
        let NewCode = util_helper_1.GenerateVerificationCode();
        if (UserToken) {
            await this._userTokenRepository.Update({ Id: UserToken.Id }, { Code: NewCode, Attempts: 0 });
        }
        else {
            UserToken = new user_token_entity_1.UserTokenModel();
            UserToken.UUID = uuid_1.v4();
            UserToken.UserId = User.Id;
            UserToken.DeviceId = UserDevice.Id;
            UserToken.Code = NewCode;
            UserToken.Attempts = 0;
            await this._userTokenRepository.Create(UserToken);
        }
        return {
            DeviceUUID: UserDevice.UUID,
        };
    }
    async SignUp(data) {
        var _a;
        if (((_a = data.Password) === null || _a === void 0 ? void 0 : _a.length) < 6) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidPassword);
        }
        let User = await this._userRepository.FindOne({
            Email: data.Email,
        });
        if (User) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.UserAlreadyExist);
        }
        User = new user_entity_1.UserModel();
        User.FullName = data.FullName;
        User.Email = data.Email;
        User.BirthDate = data.BirthDate;
        User.UserName = data.UserName;
        User.PhoneNumber = data.PhoneNumber;
        User.Details = data.Details ? data.Details : '';
        User.Type = user_entity_1.UserModelType.NormalUser;
        if (data.Password) {
            User.Password = await util_helper_1.HashPassword(data.Password);
        }
        await this._userRepository.Create(User);
        let Token = await this._authService.CreateSession(User.Id);
        let UserDeviceInfo = new user_device_entity_1.UserDeviceModel();
        UserDeviceInfo.UserId = User.Id;
        UserDeviceInfo.AuthToken = Token;
        UserDeviceInfo.UUID = data.DeviceUUID ? data.DeviceUUID : uuid_1.v4();
        if (data.FCMToken) {
            UserDeviceInfo.FCMToken = data.FCMToken;
        }
        UserDeviceInfo.UserAgent = data['UserAgent'];
        await this._userDeviceRepository.Create(UserDeviceInfo);
        let UserToken = new user_token_entity_1.UserTokenModel();
        UserToken.UUID = uuid_1.v4();
        UserToken.UserId = User.Id;
        UserToken.DeviceId = UserDeviceInfo.Id;
        UserToken.Code = util_helper_1.GenerateVerificationCode();
        UserToken.Attempts = 0;
        await this._userTokenRepository.Create(UserToken);
        return {
            Token: Token,
            User: User,
            DeviceUUID: UserDeviceInfo.UUID,
            AuthCode: authentication_1.AuthenticationCodes.UserVerificationPending,
        };
    }
    async ForgetPassword(data) {
        let User;
        User = await this._userRepository.FindOne({
            Email: data.Email,
        });
        if (!User) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.UserNotFound);
        }
        let UserToken = await this._userTokenRepository.FindOne({
            UserId: User.Id,
        });
        let TokenCode = uuid_1.v4();
        let NewCode = util_helper_1.GenerateVerificationCode();
        if (UserToken) {
            await this._userTokenRepository.Update({ Id: UserToken.Id }, { Code: NewCode, UUID: TokenCode, Attempts: 0 });
        }
        else {
            UserToken = new user_token_entity_1.UserTokenModel();
            UserToken.UUID = TokenCode;
            UserToken.UserId = User.Id;
            UserToken.Code = NewCode;
            UserToken.Attempts = 0;
            await this._userTokenRepository.Create(UserToken);
        }
        await queue_helper_1.SSQueue.Enqueue(queue_1.QueueJobs.EMAIL, {
            EmailTemplate: template_renderer_1.EmailTemplate.ResetPassword,
            Email: User.Email,
            Locale: locale_1.Locale.En,
            EmailData: {
                FullName: User.FullName,
                Link: `http://54.144.168.52:4000/reset-password?token=${TokenCode}&code=${NewCode}`,
                Code: NewCode,
            }
        });
        return {
            Message: "You will receive an email to reset your password"
        };
    }
    async SignUpVerification(data) {
        let UserDevice = await this._userDeviceRepository.FindOne({
            UUID: data.DeviceUUID,
        });
        if (!UserDevice) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.DeviceNotFound);
        }
        let UserToken = await this._userTokenRepository.FindOne({
            Code: data.Code,
            DeviceId: UserDevice.Id,
        });
        if (!UserToken) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidVerificationCode);
        }
        let User = await this._userRepository.FindById(UserToken.UserId);
        if (!User) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.UserNotFound);
        }
        await this._userTokenRepository.Delete({ Id: UserToken.Id });
        let Token = await this._authService.CreateSession(UserDevice.UserId);
        await this._userRepository.Update({ Id: User.Id }, { Status: user_entity_1.UserModelStatus.Verified });
        await this._userDeviceRepository.Update({
            Id: UserDevice.Id,
        }, {
            AuthToken: Token,
        });
        return {
            Message: 'Verified',
            Token: Token,
            User: User,
            AuthCode: authentication_1.AuthenticationCodes.UserSignUpSuccess,
        };
    }
    async ForgetPasswordVerification(data) {
        let UserToken = await this._userTokenRepository.FindOne({
            UUID: data.TokenUUID,
        });
        if (!UserToken) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidVerificationCode);
        }
        let NewCode = util_helper_1.GenerateVerificationCode();
        if (UserToken.Code == data.Code) {
            let NewUUID = uuid_1.v4();
            await this._userTokenRepository.Update({ Id: UserToken.Id }, { Code: NewCode, UUID: NewUUID });
            return {
                Message: 'Verified',
                TokenUUID: NewUUID,
                Code: NewCode,
            };
        }
        else {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidVerificationCode);
        }
    }
    async ResetPassword(data) {
        let UserToken = await this._userTokenRepository.FindOne({
            UUID: data.TokenUUID,
        });
        if (!UserToken) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidVerificationCode);
        }
        if (UserToken.Code != data.Code) {
            throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.InvalidVerificationCode);
        }
        let User = await this._userRepository.FindById(UserToken.UserId);
        if (!User) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.UserNotFound);
        }
        let NewPassword = await util_helper_1.HashPassword(data.Password);
        await this._userRepository.Update({ Id: User.Id }, { Password: NewPassword });
        await this._userTokenRepository.Delete({ Id: UserToken.Id });
        return {
            Message: 'Success',
        };
    }
    async UserListing(data, user, fromMobileApp) {
        let whereParams = {
            Type: [user_entity_1.UserModelType.NormalUser, user_entity_1.UserModelType.DiamondUser],
        };
        if (fromMobileApp) {
            whereParams['IsVerified'] = true;
            whereParams['AvailableForInvitation'] = true;
        }
        if (data.Q) {
            whereParams.Q_Like = data.Q;
        }
        if (!data) {
            data = new user_request_1.UserListingRequest();
            data.Limit = 1000;
        }
        else if (!data.Limit) {
            data.Limit = 1000;
        }
        let Users = await this._userRepository.Find(whereParams, util_helper_1.GetPaginationOptions(data), null, user === null || user === void 0 ? void 0 : user.Id);
        const UserIds = Users === null || Users === void 0 ? void 0 : Users.map(item => item.Id);
        const UsersReviewCountMap = await this._reviewService.GetUsersReviewCountMap(UserIds);
        let TotalUsers = await this._userRepository.Count(whereParams, user === null || user === void 0 ? void 0 : user.Id);
        return {
            Users: Users.map(item => {
                delete item.Password;
                return (Object.assign(Object.assign({}, item), { ReviewCount: UsersReviewCountMap[item.Id] || 0, Location: item.getLocation() }));
            }),
            TotalUsers
        };
    }
    async DiscoverUser(data, currentUser, showExperts = false) {
        let whereParams = {
            Type: showExperts ? [user_entity_1.UserModelType.DiamondUser] : [user_entity_1.UserModelType.NormalUser],
        };
        if (data.Q) {
            whereParams.Q_Like = data.Q;
        }
        if (!data) {
            data = new user_request_1.UserListingRequest();
            data.Limit = 1000;
        }
        else if (!data.Limit) {
            data.Limit = 1000;
        }
        let Users = await this._userRepository.Find(whereParams, util_helper_1.GetPaginationOptions(data), null, currentUser === null || currentUser === void 0 ? void 0 : currentUser.Id);
        const UserIds = Users === null || Users === void 0 ? void 0 : Users.map(item => item.Id);
        const UsersReviewCountMap = await this._reviewService.GetUsersReviewCountMap(UserIds);
        let TotalUsers = await this._userRepository.Count(whereParams, currentUser === null || currentUser === void 0 ? void 0 : currentUser.Id);
        const InvitationUserMap = {};
        if ((currentUser === null || currentUser === void 0 ? void 0 : currentUser.Id) && showExperts) {
            const Invitations = await this._invitationService.getUserInvitations(UserIds, currentUser === null || currentUser === void 0 ? void 0 : currentUser.Id);
            for (const Invitation of Invitations) {
                InvitationUserMap[Invitation.UserId] = Invitation.Id;
            }
        }
        return {
            Users: Users.map(item => {
                delete item.Password;
                return (Object.assign(Object.assign({}, item), { ReviewCount: UsersReviewCountMap[item.Id] || 0, Location: item.getLocation(), InvitationId: InvitationUserMap[item.Id] || null }));
            }),
            TotalUsers
        };
    }
    async EditProfile(data, user, device) {
        var _a;
        user.FullName = data.FullName || user.FullName;
        user.BirthDate = data.BirthDate || user.BirthDate;
        user.UserName = data.UserName || user.UserName;
        user.PhoneNumber = data.PhoneNumber || user.PhoneNumber;
        if (data.Password) {
            if (!data.OldPassword) {
                throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.ProvideOldPassword);
            }
            const User = await this._userRepository.FindOne({
                Id: user.Id,
            });
            if (!User || !(await util_helper_1.ComparePassword(data.OldPassword, User.Password))) {
                throw new response_exception_1.BadRequestException(locale_1.MessageTemplates.WrongOldPassword);
            }
            user.Password = await util_helper_1.HashPassword(data.Password);
        }
        if (data.MediaId) {
            user.MediaId = data.MediaId;
        }
        if (data.SelfieMediaId) {
            user.SelfieMediaId = data.SelfieMediaId;
        }
        if (data.About) {
            user.About = data.About;
        }
        if ((_a = data.AvailableForInvitation) === null || _a === void 0 ? void 0 : _a.toString()) {
            user.AvailableForInvitation = data.AvailableForInvitation;
        }
        if (data.Status) {
            user.Status = Number(data.Status);
        }
        if (data.Lat && data.Lng) {
            user.Location = `Point(${data.Lat} ${data.Lng})`;
        }
        if (data.Details) {
            user.Details = data.Details;
        }
        if (data.FCMToken && (device === null || device === void 0 ? void 0 : device.Id)) {
            await this._userDeviceRepository.Update({
                Id: device.Id
            }, {
                FCMToken: data.FCMToken
            });
        }
        await this._userRepository.Save(user);
        let User = await this._userRepository.FindById(user.Id);
        return { Message: "Updated", User };
    }
    async GetUserById(Id) {
        let User = await this._userRepository.FindOne({ Id });
        if (!User) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.NotFoundError);
        }
        return User;
    }
    async getUserDetails(Id) {
        let User = await this._userRepository.getUserById(Id);
        return Object.assign(Object.assign({}, User), { Location: User.getLocation() });
    }
    async Logout(user, device) {
        user.Status = 0;
        if (device === null || device === void 0 ? void 0 : device.Id) {
            await this._userDeviceRepository.Update({
                Id: device.Id
            }, {
                FCMToken: null
            });
        }
        await this._userRepository.Save(user);
        let User = await this._userRepository.FindById(user.Id);
        return { Message: "Success" };
    }
    async VerifyUser(Id, data) {
        var _a;
        if ((_a = data === null || data === void 0 ? void 0 : data.IsVerified) === null || _a === void 0 ? void 0 : _a.toString()) {
            await this._userRepository.Update({
                Id: Id,
            }, {
                IsVerified: data.IsVerified
            });
        }
        return {
            Message: "Updated"
        };
    }
    async UpdateUserType(Id, data) {
        var _a;
        if ((_a = data === null || data === void 0 ? void 0 : data.Type) === null || _a === void 0 ? void 0 : _a.toString()) {
            await this._userRepository.Update({
                Id: Id,
            }, {
                Type: data.Type
            });
            if (data.Type === user_entity_1.UserModelType.DiamondUser) {
                await this._userRepository.updateExperts(Id);
            }
        }
        return {
            Message: "Updated"
        };
    }
    async clearDatabase() {
        await this._userRepository.clearDatabase();
        return true;
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(4, common_1.Inject(common_1.forwardRef(() => review_service_1.ReviewService))),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        user_device_repository_1.UserDeviceRepository,
        user_token_repository_1.UserTokenRepository,
        auth_service_1.AuthService,
        review_service_1.ReviewService,
        invitation_service_1.InvitationService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map