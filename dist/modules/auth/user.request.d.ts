export declare class LoginRequest {
    Email: string;
    Password: string;
    DeviceUUID: string;
    FCMToken: string;
    Type?: number;
    Types?: number[];
}
export declare class EditProfileRequest {
    Password: string;
    OldPassword: string;
    FullName: string;
    BirthDate: string;
    UserName: string;
    PhoneNumber: string;
    MediaId: number;
    SelfieMediaId: number;
    About: string;
    Status: number;
    FCMToken: string;
    Lat: string;
    Lng: string;
    AvailableForInvitation: boolean;
    Details: string;
}
export declare class UpdateFCMRequest {
    FCMToken: string;
    Lat: string;
    Lng: string;
}
export declare class SignUpRequest {
    Email: string;
    Password: string;
    FullName: string;
    BirthDate: string;
    UserName: string;
    PhoneNumber: string;
    FCMToken: string;
    DeviceUUID: string;
    Details: string;
}
export declare class ResendSignUpCodeRequest {
    DeviceUUID: string;
    Email: string;
}
export declare class ForgetPasswordRequest {
    Email: string;
}
export declare class SignUpVerificationRequest {
    Code: string;
    DeviceUUID: string;
}
export declare class ForgetPasswordVerificationRequest {
    Code: string;
    TokenUUID: string;
}
export declare class ResetPasswordRequest {
    Code: string;
    TokenUUID: string;
    Password: string;
}
export declare class GuestLoginRequest {
    DeviceUUID: string;
}
export declare class UserListingRequest {
    Page: number;
    Limit: number;
    Before: number;
    After: number;
    Q: string;
}
