"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModelStatus = exports.UserModelType = exports.UserModel = void 0;
const base_entity_1 = require("../../model/base.entity");
const typeorm_1 = require("typeorm");
const class_transformer_1 = require("class-transformer");
const mysql_entity_1 = require("../../model/mysql.entity.");
let UserModel = class UserModel extends mysql_entity_1.MySQLBaseModel {
    getLocation() {
        if (this.Location) {
            const str = 'POINT(';
            if (this.Location.indexOf('POINT(') === 0) {
                const spaceIndex = this.Location.indexOf(' ');
                const Lat = this.Location.substring(str.length, spaceIndex);
                const Lng = this.Location.substring(spaceIndex + 1, this.Location.length - 1);
                if (Lat && Lng) {
                    return {
                        Lat: Number(Lat),
                        Lng: Number(Lng),
                    };
                }
            }
        }
        return null;
    }
};
__decorate([
    typeorm_1.Column({
        name: 'full_name',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "FullName", void 0);
__decorate([
    typeorm_1.Column({
        name: 'user_name',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "UserName", void 0);
__decorate([
    typeorm_1.Column({
        name: 'birth_date',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "BirthDate", void 0);
__decorate([
    typeorm_1.Column({
        name: 'password',
        type: 'bigint',
    }),
    class_transformer_1.Exclude(),
    __metadata("design:type", String)
], UserModel.prototype, "Password", void 0);
__decorate([
    typeorm_1.Column({
        name: 'details',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "Details", void 0);
__decorate([
    typeorm_1.Column({
        name: 'email',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "Email", void 0);
__decorate([
    typeorm_1.Column({
        name: 'phone_number',
        type: 'varchar',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "PhoneNumber", void 0);
__decorate([
    typeorm_1.Column({
        name: 'type',
        type: 'tinyint',
    }),
    __metadata("design:type", Number)
], UserModel.prototype, "Type", void 0);
__decorate([
    typeorm_1.Column({
        name: 'status',
        type: 'tinyint',
    }),
    __metadata("design:type", Number)
], UserModel.prototype, "Status", void 0);
__decorate([
    typeorm_1.Column({
        name: 'media_id',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], UserModel.prototype, "MediaId", void 0);
__decorate([
    typeorm_1.Column({
        name: 'selfie_media_id',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], UserModel.prototype, "SelfieMediaId", void 0);
__decorate([
    typeorm_1.Column({
        name: 'about',
        type: 'varchar',
        nullable: true,
    }),
    __metadata("design:type", String)
], UserModel.prototype, "About", void 0);
__decorate([
    typeorm_1.Column({
        name: 'is_verified',
        type: 'boolean',
        default: false,
    }),
    __metadata("design:type", Boolean)
], UserModel.prototype, "IsVerified", void 0);
__decorate([
    typeorm_1.Column({
        name: 'available_for_invitation',
        type: 'boolean',
        default: false,
    }),
    __metadata("design:type", Boolean)
], UserModel.prototype, "AvailableForInvitation", void 0);
__decorate([
    typeorm_1.Column('point', {
        nullable: true,
        name: 'location',
    }),
    __metadata("design:type", String)
], UserModel.prototype, "Location", void 0);
UserModel = __decorate([
    typeorm_1.Entity('user')
], UserModel);
exports.UserModel = UserModel;
var UserModelType;
(function (UserModelType) {
    UserModelType[UserModelType["NormalUser"] = 1] = "NormalUser";
    UserModelType[UserModelType["DiamondUser"] = 3] = "DiamondUser";
    UserModelType[UserModelType["Admin"] = 2] = "Admin";
})(UserModelType = exports.UserModelType || (exports.UserModelType = {}));
var UserModelStatus;
(function (UserModelStatus) {
    UserModelStatus[UserModelStatus["Unverified"] = 0] = "Unverified";
    UserModelStatus[UserModelStatus["Verified"] = 1] = "Verified";
})(UserModelStatus = exports.UserModelStatus || (exports.UserModelStatus = {}));
//# sourceMappingURL=user.entity.js.map