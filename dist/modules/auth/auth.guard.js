"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const authentication_1 = require("../../constant/authentication");
const response_exception_1 = require("../../exception/response.exception");
const locale_1 = require("../../locale/locale");
const auth_service_1 = require("./auth.service");
let AuthGuard = class AuthGuard {
    constructor(_reflector, _authService) {
        this._reflector = _reflector;
        this._authService = _authService;
    }
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const requiredAuthorization = this._reflector.get(authentication_1.AUTHORIZATION_HEADER_KEY, context.getHandler());
        const optionalAuthorization = this._reflector.get(authentication_1.OPTIONAL_AUTHORIZATION_HEADER_KEY, context.getHandler());
        const roles = this._reflector.get(authentication_1.ROLES, context.getHandler());
        if (requiredAuthorization || optionalAuthorization) {
            const token = request.headers[authentication_1.AUTHORIZATION_HEADER_KEY];
            if (!token && optionalAuthorization) {
                return true;
            }
            if (!token) {
                throw new response_exception_1.UnAuthorizedException(locale_1.MessageTemplates.UnauthorizedError);
            }
            let auth = await this._authService.GetSession(token);
            if (!auth ||
                (auth && !auth.User) ||
                (roles.length && !roles.includes(auth.User.Type))) {
                throw new response_exception_1.UnAuthorizedException(locale_1.MessageTemplates.UnauthorizedError);
            }
            request.user = auth.User;
        }
        return true;
    }
};
AuthGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector,
        auth_service_1.AuthService])
], AuthGuard);
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=auth.guard.js.map