import { MySqlRepository } from "src/repository/mysql.repository";
import { SelectQueryBuilder } from "typeorm";
import { ChatModel } from "./chat.entity";
import { GetChatWhere } from './chat.request';
export declare class ChatRepository extends MySqlRepository<ChatModel> {
    protected DefaultAlias: string;
    getAll(where: GetChatWhere): SelectQueryBuilder<ChatModel>;
    getUnseenChatCountByInvitationIds(InvitationIds: number[], UserId: number): Promise<any>;
    getUnseenChatById(ChatId: number, UserId: number): SelectQueryBuilder<ChatModel>;
    markChatSeen(chatIds: number[]): Promise<any>;
}
