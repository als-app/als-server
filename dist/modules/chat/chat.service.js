"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatService = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const response_exception_1 = require("../../exception/response.exception");
const date_helper_1 = require("../../helpers/date.helper");
const util_helper_1 = require("../../helpers/util.helper");
const locale_1 = require("../../locale/locale");
const chat_entity_1 = require("./chat.entity");
const chat_repository_1 = require("./chat.repository");
const chat_request_1 = require("./chat.request");
let ChatService = class ChatService {
    constructor(_chatRepository) {
        this._chatRepository = _chatRepository;
    }
    async GetInvitationChats(InvitationId, UserId) {
        const where = new chat_request_1.GetChatWhere();
        where.InvitationId = InvitationId;
        const chats = await this._chatRepository.getAll(where).getMany();
        if (UserId) {
            const unseenChatIds = chats.filter(item => item.CreatedById !== UserId && !item.Seen).map(item => item.Id);
            this._chatRepository.markChatSeen(unseenChatIds);
        }
        return chats;
    }
    async MarkChatSeen(ChatId, UserId) {
        const chat = await this._chatRepository.getUnseenChatById(ChatId, UserId).getOne();
        if (chat) {
            await this._chatRepository.markChatSeen([chat.Id]);
        }
        return chat;
    }
    async CreateChat(data, user) {
        if (!data.Message && !data.MediaId) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.BadRequestError);
        }
        let chat = new chat_entity_1.ChatModel();
        chat.Message = data.Message;
        chat.CreatedById = user.Id;
        chat.InvitationId = data.InvitationId;
        chat.MediaId = data.MediaId;
        await this._chatRepository.Create(chat);
        return chat;
    }
    async getUnseenChatCountMapByInvitationIds(InvitationIds, UserId) {
        const UnseenChatCountMap = {};
        if (InvitationIds === null || InvitationIds === void 0 ? void 0 : InvitationIds.length) {
            const UnseenChatCounts = await this._chatRepository.getUnseenChatCountByInvitationIds(InvitationIds, UserId);
            for (const UnseenChatCount of UnseenChatCounts) {
                UnseenChatCountMap[UnseenChatCount.InvitationId] = UnseenChatCount.UnseenChatCount;
            }
        }
        return UnseenChatCountMap;
    }
};
ChatService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [chat_repository_1.ChatRepository])
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map