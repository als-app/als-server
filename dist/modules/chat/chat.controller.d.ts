import { UserModel } from "../auth/user.entity";
import { CreateChatRequest } from "./chat.request";
import { ChatService } from "./chat.service";
import { ChatModel } from './chat.entity';
export declare class ChatController {
    private _chatService;
    constructor(_chatService: ChatService);
    GetInvitationChats(InvitationId: number, user: UserModel): Promise<ChatModel[]>;
    MarkChatSeen(ChatId: string, user: UserModel): Promise<ChatModel>;
    CreateChat(data: CreateChatRequest, user: UserModel): Promise<ChatModel>;
}
