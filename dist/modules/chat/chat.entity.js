"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatModel = void 0;
const base_entity_1 = require("../../model/base.entity");
const typeorm_1 = require("typeorm");
const mysql_entity_1 = require("../../model/mysql.entity.");
const types_1 = require("../../common/types");
const user_entity_1 = require("../auth/user.entity");
const invitation_entity_1 = require("../invitation/invitation.entity");
let ChatModel = class ChatModel extends mysql_entity_1.MySQLBaseModel {
};
__decorate([
    typeorm_1.Column({
        name: "message",
        type: "varchar",
    }),
    __metadata("design:type", String)
], ChatModel.prototype, "Message", void 0);
__decorate([
    typeorm_1.Column({ name: 'invitation_id', type: 'int', nullable: false }),
    __metadata("design:type", Number)
], ChatModel.prototype, "InvitationId", void 0);
__decorate([
    typeorm_1.ManyToOne(type => invitation_entity_1.InvitationModel, invitation => invitation.Id),
    typeorm_1.JoinColumn({ name: 'invitation_id' }),
    __metadata("design:type", invitation_entity_1.InvitationModel)
], ChatModel.prototype, "Invitation", void 0);
__decorate([
    typeorm_1.Column({ name: 'created_by_id', type: 'int', nullable: false }),
    __metadata("design:type", Number)
], ChatModel.prototype, "CreatedById", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.UserModel, user => user.Id),
    typeorm_1.JoinColumn({ name: 'created_by_id' }),
    __metadata("design:type", user_entity_1.UserModel)
], ChatModel.prototype, "CreatedBy", void 0);
__decorate([
    typeorm_1.Column({
        name: 'media_id',
        type: 'bigint',
        nullable: true,
    }),
    __metadata("design:type", Number)
], ChatModel.prototype, "MediaId", void 0);
__decorate([
    typeorm_1.Column({
        name: 'seen',
        type: 'boolean',
        default: false,
    }),
    __metadata("design:type", Boolean)
], ChatModel.prototype, "Seen", void 0);
ChatModel = __decorate([
    typeorm_1.Entity("chat")
], ChatModel);
exports.ChatModel = ChatModel;
//# sourceMappingURL=chat.entity.js.map