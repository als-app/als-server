import { MySQLBaseModel } from "src/model/mysql.entity.";
import { UserModel } from '../auth/user.entity';
import { InvitationModel } from '../invitation/invitation.entity';
export declare class ChatModel extends MySQLBaseModel {
    Message: string;
    InvitationId: number;
    Invitation: InvitationModel;
    CreatedById: number;
    CreatedBy: UserModel;
    MediaId: number;
    Seen: boolean;
}
