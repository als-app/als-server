"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatController = void 0;
const common_1 = require("@nestjs/common");
const app_response_1 = require("../../app.response");
const appcontroller_decorator_1 = require("../../decorator/appcontroller.decorator");
const authorize_decorator_1 = require("../../decorator/authorize.decorator");
const current_user_decorator_1 = require("../../decorator/current_user.decorator");
const delete_decorator_1 = require("../../decorator/nest-ext/delete.decorator");
const get_decorator_1 = require("../../decorator/nest-ext/get.decorator");
const post_decorator_1 = require("../../decorator/nest-ext/post.decorator");
const put_decorator_1 = require("../../decorator/nest-ext/put.decorator");
const user_entity_1 = require("../auth/user.entity");
const chat_request_1 = require("./chat.request");
const chat_service_1 = require("./chat.service");
let ChatController = class ChatController {
    constructor(_chatService) {
        this._chatService = _chatService;
    }
    async GetInvitationChats(InvitationId, user) {
        return await this._chatService.GetInvitationChats(InvitationId, user.Id);
    }
    async MarkChatSeen(ChatId, user) {
        return await this._chatService.MarkChatSeen(Number(ChatId), user.Id);
    }
    async CreateChat(data, user) {
        return await this._chatService.CreateChat(data, user);
    }
};
__decorate([
    authorize_decorator_1.Authorized(),
    get_decorator_1.Get("/:InvitationId", {}),
    __param(0, common_1.Param('InvitationId')), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "GetInvitationChats", null);
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post("/:ChatId/seen", {}),
    __param(0, common_1.Param('ChatId')), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "MarkChatSeen", null);
__decorate([
    authorize_decorator_1.Authorized(),
    post_decorator_1.Post("/", {}),
    __param(0, common_1.Body()), __param(1, current_user_decorator_1.CurrentUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [chat_request_1.CreateChatRequest, user_entity_1.UserModel]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "CreateChat", null);
ChatController = __decorate([
    appcontroller_decorator_1.AppController("chat"),
    __metadata("design:paramtypes", [chat_service_1.ChatService])
], ChatController);
exports.ChatController = ChatController;
//# sourceMappingURL=chat.controller.js.map