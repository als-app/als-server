"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatRepository = void 0;
const common_1 = require("@nestjs/common");
const base_repository_1 = require("../../repository/base.repository");
const mysql_repository_1 = require("../../repository/mysql.repository");
const typeorm_1 = require("typeorm");
const chat_entity_1 = require("./chat.entity");
const media_entity_1 = require("../media/media.entity");
let ChatRepository = class ChatRepository extends mysql_repository_1.MySqlRepository {
    constructor() {
        super(...arguments);
        this.DefaultAlias = "chat";
    }
    getAll(where) {
        const qb = this.createQueryBuilder(this.DefaultAlias)
            .innerJoin(`${this.DefaultAlias}.CreatedBy`, 'CreatedBy')
            .leftJoinAndMapOne("CreatedBy.Media", media_entity_1.MediaModel, "CreatedByMedia", "CreatedBy.media_id = CreatedByMedia.id")
            .leftJoinAndMapOne(`${this.DefaultAlias}.Media`, media_entity_1.MediaModel, "Media", `${this.DefaultAlias}.media_id = Media.id`);
        qb.addSelect(['CreatedBy.Id', 'CreatedBy.FullName']);
        if (where.InvitationId) {
            qb.andWhere(`${this.DefaultAlias}.InvitationId = :InvitationId`, { InvitationId: where.InvitationId });
        }
        return qb;
    }
    getUnseenChatCountByInvitationIds(InvitationIds, UserId) {
        const qb = this.createQueryBuilder(this.DefaultAlias)
            .andWhere(`${this.DefaultAlias}.CreatedById <> :UserId `, { UserId })
            .andWhere(`${this.DefaultAlias}.Seen = :Seen`, { Seen: false })
            .andWhere(`${this.DefaultAlias}.InvitationId in  (:...InvitationIds)`, { InvitationIds })
            .select([
            `${this.DefaultAlias}.InvitationId as InvitationId`,
            `count(${this.DefaultAlias}.Id) as "UnseenChatCount"`,
        ])
            .groupBy(`${this.DefaultAlias}.InvitationId`);
        return qb.getRawMany();
    }
    getUnseenChatById(ChatId, UserId) {
        const qb = this.createQueryBuilder(this.DefaultAlias)
            .andWhere(`${this.DefaultAlias}.Id = :ChatId`, { ChatId })
            .andWhere(`${this.DefaultAlias}.CreatedById <> :UserId `, { UserId })
            .andWhere(`${this.DefaultAlias}.Seen = :Seen`, { Seen: false });
        return qb;
    }
    async markChatSeen(chatIds) {
        if (chatIds === null || chatIds === void 0 ? void 0 : chatIds.length) {
            const params = chatIds.map((item) => `?`);
            await this.query(` update chat set seen=1 where id in (${params.join(',')}) `, chatIds);
            return true;
        }
        return true;
    }
};
ChatRepository = __decorate([
    common_1.Injectable(),
    typeorm_1.EntityRepository(chat_entity_1.ChatModel)
], ChatRepository);
exports.ChatRepository = ChatRepository;
//# sourceMappingURL=chat.repository.js.map