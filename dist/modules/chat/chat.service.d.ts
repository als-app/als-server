import { UserModel } from "../auth/user.entity";
import { ChatModel } from "./chat.entity";
import { ChatRepository } from "./chat.repository";
import { CreateChatRequest } from "./chat.request";
export declare class ChatService {
    private _chatRepository;
    constructor(_chatRepository: ChatRepository);
    GetInvitationChats(InvitationId: number, UserId?: number): Promise<ChatModel[]>;
    MarkChatSeen(ChatId: number, UserId: number): Promise<ChatModel>;
    CreateChat(data: CreateChatRequest, user: UserModel): Promise<ChatModel>;
    getUnseenChatCountMapByInvitationIds(InvitationIds: number[], UserId: number): Promise<any>;
}
