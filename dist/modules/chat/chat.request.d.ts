export declare class CreateChatRequest {
    Message: string;
    InvitationId: number;
    MediaId: number;
}
export declare class GetChatWhere {
    InvitationId: number;
}
