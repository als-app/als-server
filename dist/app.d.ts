import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface";
export declare class App {
    private static _app;
    private constructor();
    static GetNestApplicationInstance(): Promise<NestExpressApplication>;
    private static _createApplication;
}
