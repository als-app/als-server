export declare type Location = {
    Lat: number;
    Lng: number;
};
