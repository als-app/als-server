"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const user_module_1 = require("./modules/auth/user.module");
const core_1 = require("@nestjs/core");
const http_exception_1 = require("./exception/http.exception");
const redis_module_1 = require("./redis/redis.module");
const auth_guard_1 = require("./modules/auth/auth.guard");
const media_module_1 = require("./modules/media/media.module");
const serve_static_1 = require("@nestjs/serve-static");
const path_1 = require("path");
const socket_module_1 = require("./socket/socket.module");
const notification_module_1 = require("./modules/notification/notification.module");
const chat_module_1 = require("./modules/chat/chat.module");
const invitation_module_1 = require("./modules/invitation/invitation.module");
const review_module_1 = require("./modules/review/review.module");
const category_module_1 = require("./modules/category/category.module");
const posting_module_1 = require("./modules/posting/posting.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot(),
            redis_module_1.RedisModule,
            user_module_1.UserModule,
            media_module_1.MediaModule,
            socket_module_1.SocketModule,
            notification_module_1.NotificationModule,
            chat_module_1.ChatModule,
            serve_static_1.ServeStaticModule.forRoot({
                rootPath: path_1.join(__dirname, "..", "public/attachments/"),
                serveRoot: "/static/",
            }),
            invitation_module_1.InvitationModule,
            review_module_1.ReviewModule,
            category_module_1.CategoryModule,
            posting_module_1.PostingModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [
            app_service_1.AppService,
            {
                provide: core_1.APP_FILTER,
                useClass: http_exception_1.HttpExceptionFilter,
            },
            {
                provide: core_1.APP_GUARD,
                useClass: auth_guard_1.AuthGuard,
            },
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map