import { ObjectType, Repository, UpdateResult } from 'typeorm';
import { BaseModel } from '../model/base.entity';
import { PaginationDBParams } from '../helpers/util.helper';
export declare abstract class BaseRepository<T extends BaseModel> extends Repository<T> {
    protected Model: ObjectType<T>;
    protected ConnectionName: string;
    protected DefaultOrderByColumn: string;
    protected DefaultOrderByDirection: string;
    protected PrimaryColumnKey: string;
    protected constructor(model: any, connectionName: any);
    protected SetRepo(): void;
    Create(instance: T): Promise<T>;
    Reload(instance: T): Promise<T>;
    FindOne(whereParams: any): Promise<T>;
    Find(whereParams: any, options?: PaginationDBParams): Promise<T[]>;
    Count(whereParams: any): Promise<number>;
    FindAll(options?: PaginationDBParams): Promise<T[]>;
    protected PrepareParams(whereParams: any, options?: PaginationDBParams): {};
    Save(instance: T): Promise<T>;
    Delete(param: any): Promise<UpdateResult>;
    DeleteById(id: number): Promise<UpdateResult>;
    BatchCreate(data: Array<Partial<T>>): Promise<import("typeorm").InsertResult>;
    FindById(id: number): Promise<T>;
    FindAndMap(whereParams: any, options: PaginationDBParams, callback: (instance: T) => any): Promise<any[]>;
    FindGen(whereParams: any, options: PaginationDBParams): AsyncIterableIterator<T>;
    Update(selection: any, update: any): Promise<void>;
    protected ApplyPagination(whereParams: any, options?: PaginationDBParams): any;
    protected SetTimestamps(obj: T): T;
    protected GetPrimaryColumnValue(val: any): any;
    protected GetPrimaryColumnKey(): string;
    protected ApplyOrder(whereParams: any, orderOptions?: {
        Column: string;
        Direction: 'ASC' | 'DESC';
    }): any;
    protected ApplyProjection(whereParams: any): any;
    protected LessThanOperator(val: any): any;
    protected MoreThanOperator(val: any): any;
    protected InOperator(val: any): any;
}
