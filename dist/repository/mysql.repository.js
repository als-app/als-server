"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MySqlRepository = void 0;
const base_repository_1 = require("./base.repository");
class MySqlRepository extends base_repository_1.BaseRepository {
    constructor(model) {
        super(model, 'mysql');
        this.SetRepo();
    }
    async Create(instance) {
        return this.Reload(await this.Save(instance));
    }
    ApplyConditionOnQueryBuilder(query, params) {
        if (params.DeletedAt === undefined) {
            params.DeletedAt = 0;
        }
        for (let key in params) {
            let Val = params[key];
            let Str = '';
            if (Array.isArray(Val)) {
                Str = this.ToDBCase(key) + ' IN (:...' + key + ')';
            }
            else if (Val === null) {
                Str = this.ToDBCase(key) + ' IS NULL';
            }
            else if (Val === undefined) {
                continue;
            }
            else {
                Str = this.ToDBCase(key) + ' = :' + key;
            }
            let param = {};
            param[key] = Val;
            query = query.andWhere(this.DefaultAlias + '.' + Str, param);
        }
        return query;
    }
    ToDBCase(str) {
        return typeof str === 'string'
            ? str
                .replace(/(?:^|\.?)([A-Z])/g, function (x, y) {
                return '_' + y.toLowerCase();
            })
                .replace(/^_/, '')
            : str;
    }
    ApplyOrderOnQueryBuilder(query, orderOptions) {
        let order = super.ApplyOrder({}, orderOptions).order;
        let preparedOrder = {};
        for (var key in order) {
            preparedOrder[this.DefaultAlias + '.' + key] = order[key];
        }
        return query.orderBy(preparedOrder);
    }
    ApplyPaginationOnQueryBuilder(query, options) {
        let paginationParams = super.ApplyPagination({}, options);
        if (paginationParams.take !== undefined) {
            query = query.limit(paginationParams.take);
        }
        if (paginationParams.skip !== undefined) {
            query = query.offset(paginationParams.skip);
        }
        if (options && typeof options.Offset === 'string') {
            let operator = null;
            if (options.Offset[0] == '-') {
                operator = ' < ';
            }
            else if (options.Offset[0] == '+') {
                operator = ' > ';
            }
            if (operator) {
                query = query.andWhere(this.DefaultAlias +
                    '.' +
                    this.GetPrimaryColumnKey() +
                    operator +
                    this.GetPrimaryColumnValue(options.Offset.substr(1)));
            }
        }
        return query;
    }
}
exports.MySqlRepository = MySqlRepository;
//# sourceMappingURL=mysql.repository.js.map