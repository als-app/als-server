"use strict";
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepository = void 0;
const typeorm_1 = require("typeorm");
const logger_helper_1 = require("../helpers/logger.helper");
class BaseRepository extends typeorm_1.Repository {
    constructor(model, connectionName) {
        super();
        this.DefaultOrderByColumn = 'CreatedAt';
        this.DefaultOrderByDirection = 'ASC';
        this.PrimaryColumnKey = 'Id';
        this.Model = model;
        this.ConnectionName = connectionName;
    }
    SetRepo() { }
    async Create(instance) {
        return await this.Save(instance);
    }
    async Reload(instance) {
        return this.findOne(this.getId(instance));
    }
    async FindOne(whereParams) {
        let records = await this.Find(whereParams, {
            Limit: 1,
            Offset: 0,
        });
        return records[0] ? records[0] : null;
    }
    async Find(whereParams, options) {
        let params = {
            where: this.PrepareParams(whereParams),
        };
        params = this.ApplyProjection(params);
        params = this.ApplyPagination(params, options);
        params = this.ApplyOrder(params, { 'Column': 'CreatedAt', 'Direction': 'DESC' });
        return this.find(params);
    }
    async Count(whereParams) {
        let params = {
            where: this.PrepareParams(whereParams),
        };
        return this.count(params);
    }
    async FindAll(options) {
        return this.Find({}, options);
    }
    PrepareParams(whereParams, options) {
        let whereClauses = {};
        for (let key in whereParams) {
            let val = whereParams[key];
            if (key[0] === '$') {
            }
            else if (Array.isArray(val)) {
                val = this.InOperator(val);
            }
            else if (val === null) {
                val = typeorm_1.IsNull();
            }
            whereClauses[key] = val;
        }
        whereClauses['DeletedAt'] = 0;
        if (options &&
            typeof options.Offset === 'string' &&
            !whereClauses[this.GetPrimaryColumnKey()]) {
            let func = null;
            if (options.Offset[0] == '-') {
                func = this.LessThanOperator;
            }
            else if (options.Offset[0] == '+') {
                func = this.MoreThanOperator;
            }
            if (func) {
                whereClauses[this.GetPrimaryColumnKey()] = func(this.GetPrimaryColumnValue(options.Offset.substr(1)));
            }
        }
        return whereClauses;
    }
    async Save(instance) {
        return (await this.save(this.SetTimestamps(instance)));
    }
    async Delete(param) {
        param = this.PrepareParams(param);
        return await this.update(param, {
            DeletedAt: Date.now(),
        });
    }
    async DeleteById(id) {
        return await this.Delete({
            Id: id,
        });
    }
    async BatchCreate(data) {
        data = data.map((obj) => {
            return this.SetTimestamps(obj);
        });
        return await this.createQueryBuilder()
            .insert()
            .into(this.Model)
            .values(data)
            .execute();
    }
    async FindById(id) {
        let Param = {};
        Param[this.GetPrimaryColumnKey()] = this.GetPrimaryColumnValue(id);
        return await this.FindOne(Param);
    }
    async FindAndMap(whereParams, options, callback) {
        var e_1, _a;
        const generator = this.FindGen(whereParams, options);
        const results = [];
        try {
            for (var generator_1 = __asyncValues(generator), generator_1_1; generator_1_1 = await generator_1.next(), !generator_1_1.done;) {
                const object = generator_1_1.value;
                let result = callback(object);
                if (result instanceof Promise) {
                    result = await result;
                }
                results.push(result);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (generator_1_1 && !generator_1_1.done && (_a = generator_1.return)) await _a.call(generator_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return results;
    }
    FindGen(whereParams, options) {
        return __asyncGenerator(this, arguments, function* FindGen_1() {
            if (options.Limit === -1) {
                logger_helper_1.Logger.Warn('this function is not intended to use with limit -1');
            }
            if (options.Offset !== undefined && options.Offset !== 0) {
                logger_helper_1.Logger.Warn('some results might not fetch when offset is not zero or undefined');
            }
            let Limit = options.Limit || 10;
            let Offset = options.Offset || 0;
            let Objects = [];
            do {
                Objects = yield __await(this.Find(whereParams, {
                    Limit,
                    Offset,
                }));
                Offset += Objects.length;
                for (const obj of Objects) {
                    yield yield __await(obj);
                }
            } while (Objects.length >= Limit);
        });
    }
    async Update(selection, update) {
        let params = this.PrepareParams(selection);
        update.UpdatedAt = Date.now();
        await this.update(params, update);
    }
    ApplyPagination(whereParams, options) {
        let Limit = 10;
        let Offset = 0;
        if (options) {
            if (typeof options.Offset == 'number') {
                Offset = options.Offset;
            }
            if (options.Limit) {
                Limit = options.Limit;
            }
        }
        if (Limit != -1 && options !== undefined) {
            whereParams.take = Limit;
            whereParams.skip = Offset;
        }
        return whereParams;
    }
    SetTimestamps(obj) {
        if (obj.CreatedAt === undefined) {
            obj.CreatedAt = Date.now();
        }
        obj.UpdatedAt = obj.UpdatedAt === undefined ? obj.CreatedAt : Date.now();
        if (obj.DeletedAt === undefined) {
            obj.DeletedAt = 0;
        }
        return obj;
    }
    GetPrimaryColumnValue(val) {
        return val;
    }
    GetPrimaryColumnKey() {
        return this.PrimaryColumnKey;
    }
    ApplyOrder(whereParams, orderOptions) {
        let Column = this.DefaultOrderByColumn;
        let Direction = this.DefaultOrderByDirection;
        if (orderOptions !== undefined) {
            if (orderOptions.Column) {
                Column = orderOptions.Column;
            }
            if (orderOptions.Direction) {
                Direction = orderOptions.Direction;
            }
        }
        if (whereParams.order === undefined) {
            whereParams.order = {};
        }
        whereParams.order[Column] = Direction;
        return whereParams;
    }
    ApplyProjection(whereParams) {
        return whereParams;
    }
    LessThanOperator(val) {
        return typeorm_1.LessThan(val);
    }
    MoreThanOperator(val) {
        return typeorm_1.MoreThan(val);
    }
    InOperator(val) {
        return typeorm_1.In(val);
    }
}
exports.BaseRepository = BaseRepository;
//# sourceMappingURL=base.repository.js.map