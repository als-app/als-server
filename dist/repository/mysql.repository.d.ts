import { BaseRepository } from './base.repository';
import { BaseModel } from '../model/base.entity';
import { PaginationDBParams } from '../helpers/util.helper';
import { SelectQueryBuilder } from 'typeorm';
export declare abstract class MySqlRepository<T extends BaseModel> extends BaseRepository<T> {
    protected abstract DefaultAlias: string;
    protected constructor(model: any);
    Create(instance: T): Promise<T>;
    protected ApplyConditionOnQueryBuilder(query: SelectQueryBuilder<T>, params: any): SelectQueryBuilder<T>;
    protected ToDBCase(str: string): string;
    protected ApplyOrderOnQueryBuilder(query: SelectQueryBuilder<T>, orderOptions?: {
        Column: string;
        Direction: 'ASC' | 'DESC';
    }): SelectQueryBuilder<T>;
    protected ApplyPaginationOnQueryBuilder(query: SelectQueryBuilder<T>, options?: PaginationDBParams): SelectQueryBuilder<T>;
}
