/// <reference types="socket.io" />
import { OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect } from "@nestjs/websockets";
import { SocketEventHandler } from "./socket_event.handler";
import { ChatJoinedSocketEventData } from "./events/chat/chat_joined.event";
import { MessageSocketEventData } from "./events/chat/message.event";
export declare class SocketGateway implements OnGatewayInit<SocketIO.Server>, OnGatewayConnection<SocketIO.Socket>, OnGatewayDisconnect<SocketIO.Socket> {
    private _socketEventHandler;
    private static _io;
    constructor(_socketEventHandler: SocketEventHandler);
    afterInit(server: SocketIO.Server): void;
    handleConnection(socket: SocketIO.Socket): Promise<boolean>;
    handleDisconnect(socket: SocketIO.Socket): Promise<void>;
    ChatJoinedEvent(socket: SocketIO.Socket, data: ChatJoinedSocketEventData): Promise<boolean>;
    MessageEvent(socket: SocketIO.Socket, data: MessageSocketEventData): Promise<MessageSocketEventData>;
}
