import { CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { AuthService } from "src/modules/auth/auth.service";
export declare class SocketAuthGuard implements CanActivate {
    private _reflector;
    private _authService;
    constructor(_reflector: Reflector, _authService: AuthService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
