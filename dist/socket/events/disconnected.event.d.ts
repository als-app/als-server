/// <reference types="socket.io" />
import BaseSocketEvent from "./base.event";
export default class DisconnectedSocketEvent extends BaseSocketEvent {
    static Name: string;
    protected GetName(): string;
    constructor(socket: SocketIO.Socket);
}
