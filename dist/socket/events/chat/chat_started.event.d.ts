/// <reference types="socket.io" />
import BaseSocketEvent from "../base.event";
export default class ChatStartedSocketEvent extends BaseSocketEvent {
    AdvisorUserId: number;
    GigId: number;
    ChatId?: number;
    Chat?: Object;
    IO: SocketIO.Server;
    static Name: string;
    protected GetName(): string;
    constructor(socket: SocketIO.Socket, data: ChatStartedSocketEventData, io: SocketIO.Server);
    GetData(): ChatStartedSocketEventData;
}
export interface ChatStartedSocketEventData {
    AdvisorUserId: number;
    ChatId?: number;
    Chat?: Object;
    GigId: number;
}
