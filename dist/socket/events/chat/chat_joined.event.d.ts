/// <reference types="socket.io" />
import BaseSocketEvent from "../base.event";
export default class ChatJoinedSocketEvent extends BaseSocketEvent {
    InvitationId: number;
    constructor(socket: SocketIO.Socket, data: ChatJoinedSocketEventData);
    static Name: string;
    protected GetName(): string;
    GetData(): ChatJoinedSocketEventData;
}
export interface ChatJoinedSocketEventData {
    InvitationId: number;
}
