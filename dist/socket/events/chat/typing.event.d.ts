/// <reference types="socket.io" />
import BaseSocketEvent from "../base.event";
export default class TypingSocketEvent extends BaseSocketEvent {
    ChatId: number;
    Typing?: string;
    constructor(socket: SocketIO.Socket, data: TypingSocketEventData);
    static Name: string;
    protected GetName(): string;
    GetData(): TypingSocketEventData;
}
export interface TypingSocketEventData {
    ChatId: number;
    Typing?: string;
}
