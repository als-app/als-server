/// <reference types="socket.io" />
import { ChatModel } from "src/modules/chat/chat.entity";
import BaseSocketEvent from "../base.event";
import { MediaModel } from 'src/modules/media/media.entity';
export default class MessageSocketEvent extends BaseSocketEvent {
    InvitationId: number;
    Content?: string;
    MediaId?: number;
    Media?: MediaModel;
    Chat?: ChatModel;
    IO: SocketIO.Server;
    constructor(socket: SocketIO.Socket, data: MessageSocketEventData, io: SocketIO.Server);
    static Name: string;
    protected GetName(): string;
    GetData(): MessageSocketEventData;
}
export interface MessageSocketEventData {
    InvitationId: number;
    Chat?: ChatModel;
    Content?: string;
    MediaId?: number;
    Media?: MediaModel;
    ChatEvent?: Object;
}
