"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../../constant/socket");
const base_event_1 = require("../base.event");
class ReadMessageSocketEvent extends base_event_1.default {
    constructor(socket, data) {
        super(socket);
        this.ChatEventId = data.ChatEventId;
    }
    GetName() {
        return ReadMessageSocketEvent.Name;
    }
    GetData() {
        return {
            ChatEventId: this.ChatEventId,
        };
    }
}
exports.default = ReadMessageSocketEvent;
ReadMessageSocketEvent.Name = socket_1.SocketEventType.ReadMessage;
//# sourceMappingURL=read_message.event.js.map