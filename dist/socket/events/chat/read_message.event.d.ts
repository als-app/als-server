/// <reference types="socket.io" />
import BaseSocketEvent from "../base.event";
export default class ReadMessageSocketEvent extends BaseSocketEvent {
    ChatEventId: number;
    constructor(socket: SocketIO.Socket, data: ReadMessageSocketEventData);
    static Name: string;
    protected GetName(): string;
    GetData(): ReadMessageSocketEventData;
}
export interface ReadMessageSocketEventData {
    ChatEventId: number;
}
