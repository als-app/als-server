"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../../constant/socket");
const chat_entity_1 = require("../../../modules/chat/chat.entity");
const base_event_1 = require("../base.event");
const media_entity_1 = require("../../../modules/media/media.entity");
class MessageSocketEvent extends base_event_1.default {
    constructor(socket, data, io) {
        super(socket);
        this.InvitationId = data.InvitationId;
        this.Content = data.Content;
        if (data.MediaId) {
            this.MediaId = data.MediaId;
        }
        this.IO = io;
    }
    GetName() {
        return MessageSocketEvent.Name;
    }
    GetData() {
        return {
            InvitationId: this.InvitationId,
            Content: this.Content,
            MediaId: this.MediaId,
            Chat: this.Chat,
            Media: this.Media,
        };
    }
}
exports.default = MessageSocketEvent;
MessageSocketEvent.Name = socket_1.SocketEventType.Message;
//# sourceMappingURL=message.event.js.map