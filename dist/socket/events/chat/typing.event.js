"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../../constant/socket");
const base_event_1 = require("../base.event");
class TypingSocketEvent extends base_event_1.default {
    constructor(socket, data) {
        super(socket);
        this.ChatId = data.ChatId;
        this.Typing = data.Typing;
    }
    GetName() {
        return TypingSocketEvent.Name;
    }
    GetData() {
        return {
            ChatId: this.ChatId,
            Typing: this.Typing,
        };
    }
}
exports.default = TypingSocketEvent;
TypingSocketEvent.Name = socket_1.SocketEventType.Typing;
//# sourceMappingURL=typing.event.js.map