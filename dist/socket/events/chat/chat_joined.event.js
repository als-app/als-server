"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../../constant/socket");
const base_event_1 = require("../base.event");
class ChatJoinedSocketEvent extends base_event_1.default {
    constructor(socket, data) {
        super(socket);
        this.InvitationId = data.InvitationId;
    }
    GetName() {
        return ChatJoinedSocketEvent.Name;
    }
    GetData() {
        return {
            InvitationId: this.InvitationId
        };
    }
}
exports.default = ChatJoinedSocketEvent;
ChatJoinedSocketEvent.Name = socket_1.SocketEventType.ChatJoin;
//# sourceMappingURL=chat_joined.event.js.map