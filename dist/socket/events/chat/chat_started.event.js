"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_event_1 = require("../base.event");
const socket_1 = require("../../../constant/socket");
class ChatStartedSocketEvent extends base_event_1.default {
    constructor(socket, data, io) {
        super(socket);
        this.AdvisorUserId = data.AdvisorUserId;
        this.GigId = data.GigId;
        this.IO = io;
        if (data.ChatId) {
            this.ChatId = data.ChatId;
        }
        if (data.Chat) {
            this.Chat = data.Chat;
        }
    }
    GetName() {
        return ChatStartedSocketEvent.Name;
    }
    GetData() {
        return {
            AdvisorUserId: this.AdvisorUserId,
            ChatId: this.ChatId,
            GigId: this.GigId,
            Chat: this.Chat
        };
    }
}
exports.default = ChatStartedSocketEvent;
ChatStartedSocketEvent.Name = socket_1.SocketEventType.ChatStart;
//# sourceMappingURL=chat_started.event.js.map