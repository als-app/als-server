"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../constant/socket");
const locale_1 = require("../../locale/locale");
class BaseSocketEvent {
    constructor(socket) {
        this._socket = socket;
    }
    GetChatRoom(chatId) {
        return socket_1.SocketRoomType.Chat + ":" + chatId;
    }
    GetCustomerRoom() {
        return socket_1.SocketRoomType.Customer;
    }
    GetData() {
        return {};
    }
    GetSocket() {
        return this._socket;
    }
    JoinChatRoom(chatId) {
        this._socket.join(this.GetChatRoom(chatId));
    }
    LeaveChatRoom(chatId) {
        this._socket.leave(this.GetChatRoom(chatId));
    }
    BroadcastToChatRoom(chatId) {
        this._socket.broadcast.to(this.GetChatRoom(chatId)).emit(this.GetName(), this.GetData());
    }
    IOBroadcastToCustomerRoom(io) {
        io.to(this.GetCustomerRoom()).emit(this.GetName(), this.GetData());
    }
    EmitToIndividualSocket(socketId) {
        this._socket.to(socketId).emit(this.GetName(), this.GetData());
    }
    IOEmitToIndividualSocket(io, socketId) {
        io.to(socketId).emit(this.GetName(), this.GetData());
    }
    Emit() {
        this._socket.emit(this.GetName(), this.GetData());
    }
    EmitError(statusCode, messageKey, messageData) {
        let error = new Error();
        error.Status = statusCode;
        error.Message = locale_1.__T(locale_1.Locale.En, messageKey, messageData);
        this._socket.error(error);
    }
    SocketJoinChatRoom(socket, chatId) {
        socket.join(this.GetChatRoom(chatId));
    }
}
exports.default = BaseSocketEvent;
//# sourceMappingURL=base.event.js.map