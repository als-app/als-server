"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../constant/socket");
const base_event_1 = require("./base.event");
class DisconnectedSocketEvent extends base_event_1.default {
    constructor(socket) {
        super(socket);
    }
    GetName() {
        return DisconnectedSocketEvent.Name;
    }
}
exports.default = DisconnectedSocketEvent;
DisconnectedSocketEvent.Name = socket_1.SocketEventType.Disconnect;
//# sourceMappingURL=disconnected.event.js.map