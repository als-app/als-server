/// <reference types="socket.io" />
import { MessageTemplates } from "src/locale/locale";
export default abstract class BaseSocketEvent {
    private _socket;
    constructor(socket: SocketIO.Socket);
    private GetChatRoom;
    private GetCustomerRoom;
    protected abstract GetName(): string;
    protected GetData(): Object;
    GetSocket(): SocketIO.Socket;
    JoinChatRoom(chatId: number): void;
    LeaveChatRoom(chatId: number): void;
    BroadcastToChatRoom(chatId: number): void;
    IOBroadcastToCustomerRoom(io: SocketIO.Server): void;
    EmitToIndividualSocket(socketId: string): void;
    IOEmitToIndividualSocket(io: any, socketId: string): void;
    Emit(): void;
    EmitError(statusCode: number, messageKey: MessageTemplates, messageData?: any): void;
    SocketJoinChatRoom(socket: SocketIO.Socket, chatId: number): void;
}
