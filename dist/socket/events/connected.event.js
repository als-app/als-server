"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = require("../../constant/socket");
const base_event_1 = require("./base.event");
class ConnectedSocketEvent extends base_event_1.default {
    constructor(socket) {
        super(socket);
    }
    GetName() {
        return ConnectedSocketEvent.Name;
    }
}
exports.default = ConnectedSocketEvent;
ConnectedSocketEvent.Name = socket_1.SocketEventType.Connected;
//# sourceMappingURL=connected.event.js.map