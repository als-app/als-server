"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketModule = void 0;
const common_1 = require("@nestjs/common");
const user_module_1 = require("../modules/auth/user.module");
const redis_module_1 = require("../redis/redis.module");
const socket_event_handler_1 = require("./socket_event.handler");
const socket_gateway_1 = require("./socket.gateway");
const chat_module_1 = require("../modules/chat/chat.module");
const media_module_1 = require("../modules/media/media.module");
const socket_auth_guard_1 = require("./socket_auth.guard");
const invitation_module_1 = require("../modules/invitation/invitation.module");
let SocketModule = class SocketModule {
};
SocketModule = __decorate([
    common_1.Module({
        imports: [redis_module_1.RedisModule, user_module_1.UserModule, media_module_1.MediaModule, chat_module_1.ChatModule, invitation_module_1.InvitationModule],
        exports: [],
        providers: [socket_auth_guard_1.SocketAuthGuard, socket_event_handler_1.SocketEventHandler, socket_gateway_1.SocketGateway]
    })
], SocketModule);
exports.SocketModule = SocketModule;
//# sourceMappingURL=socket.module.js.map