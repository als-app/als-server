"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketEventHandler = void 0;
const common_1 = require("@nestjs/common");
const response_1 = require("../constant/response");
const socket_1 = require("../constant/socket");
const response_exception_1 = require("../exception/response.exception");
const locale_1 = require("../locale/locale");
const auth_service_1 = require("../modules/auth/auth.service");
const user_entity_1 = require("../modules/auth/user.entity");
const user_service_1 = require("../modules/auth/user.service");
const media_entity_1 = require("../modules/media/media.entity");
const media_service_1 = require("../modules/media/media.service");
const app_1 = require("../app");
const queue_helper_1 = require("../helpers/queue.helper");
const queue_1 = require("../constant/queue");
const notification_entity_1 = require("../modules/notification/notification.entity");
const chat_service_1 = require("../modules/chat/chat.service");
const invitation_service_1 = require("../modules/invitation/invitation.service");
const chat_entity_1 = require("../modules/chat/chat.entity");
let SocketEventHandler = class SocketEventHandler {
    constructor(_authService, _userService, _chatService, _invitationService, _mediaService) {
        this._authService = _authService;
        this._userService = _userService;
        this._chatService = _chatService;
        this._invitationService = _invitationService;
        this._mediaService = _mediaService;
    }
    async OnConnectedEvent(event) {
        let UserId = parseInt(event.GetSocket().handshake.query.actorId);
        let User = await this._userService.FindById(UserId);
        if (!User) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.UserNotFound);
        }
        await this._authService.SetCustomerSocketId(event.GetSocket().id, User.Id);
        await this._addUserSocket(event.GetSocket().id, User.Id, socket_1.SocketEventSenderType.User);
        return true;
    }
    async OnDisconnectEvent(event) {
        let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
        await this._authService.RemoveSocketIdFromUserSockets(event.GetSocket().id, SocketData.UserId, SocketData.UserType);
        await this._authService.RemoveEntityDataBySocketId(event.GetSocket().id);
        return true;
    }
    async OnChatJoinedEvent(event) {
        let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
        let Invitation = await this._invitationService.getInvitationDetails(event.InvitationId);
        if (Invitation.CreatedById !== SocketData.UserId || Invitation.UserId !== SocketData.UserId) {
            throw new response_exception_1.ForbiddenException(locale_1.MessageTemplates.ForbiddenError);
        }
        event.JoinChatRoom(Invitation.Id);
        return true;
    }
    async OnMessageEvent(event) {
        let SocketData = await this._authService.GetEntityDataBySocketId(event.GetSocket().id);
        let Invitation = await this._invitationService.getInvitationDetails(event.InvitationId);
        if (!Invitation) {
            throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.NotFoundError);
        }
        let chat = new chat_entity_1.ChatModel();
        chat.InvitationId = Invitation.Id;
        chat.CreatedById = SocketData.UserId;
        const User = Invitation.CreatedById === chat.CreatedById ? Invitation.CreatedBy : Invitation.User;
        const OtherUser = Invitation.CreatedById === chat.CreatedById ? Invitation.User : Invitation.CreatedBy;
        if (event.MediaId) {
            let Media = await this._mediaService.GetById(event.MediaId);
            if (!Media) {
                throw new response_exception_1.NotFoundException(locale_1.MessageTemplates.MediaNotFound);
            }
            chat.MediaId = event.MediaId;
            event.Media = Media;
        }
        if (event.Content) {
            chat.Message = event.Content;
        }
        chat = await this._chatService.CreateChat(chat, User);
        event.Chat = chat;
        let SocketIds = await this._authService.GetSocketIdsByUserId(OtherUser.Id, socket_1.SocketEventSenderType.User);
        this._joinChatRoom(SocketIds, Invitation.Id, event);
        event.BroadcastToChatRoom(Invitation.Id);
        await queue_helper_1.SSQueue.Enqueue(queue_1.QueueJobs.NOTIFICATION, {
            Type: notification_entity_1.NotificationModelType.NewChatMessage,
            ReceiverId: OtherUser.Id,
            InvitationId: Invitation.Id,
            SenderId: User.Id,
            SenderType: socket_1.SocketEventSenderType.User,
            Content: chat.Message ? chat.Message : locale_1.__T(locale_1.Locale.En, locale_1.MessageTemplates.AttachmentShared)
        });
        return event.GetData();
    }
    async _socketAuthentication(client, next) {
        let token = client.handshake.query.authorization;
        let actorId = parseInt(client.handshake.query.actorId);
        console.log(token, actorId);
        if (token) {
            let auth = await this._authService.GetSession(token);
            console.log(auth);
            if (auth && auth.UserId === actorId) {
                return next();
            }
        }
        let error = new Error();
        error.Status = common_1.HttpStatus.UNAUTHORIZED;
        error.Message = response_1.DefaultResponseMessages[common_1.HttpStatus.UNAUTHORIZED];
        error.Data = null;
        client.error(error);
        console.log(error);
        return next(error);
    }
    async _addUserSocket(socketId, userId, userType) {
        let SocketIds = await this._authService.GetSocketIdsByUserId(userId, userType);
        SocketIds.push(socketId);
        await this._authService.SetSocketIdsByUserId(SocketIds, userId, userType);
    }
    _joinChatRoom(socketIds, InvitationId, event) {
        for (let i = 0; i < socketIds.length; i++) {
            if (event.IO &&
                event.IO.sockets &&
                event.IO.sockets.connected &&
                event.IO.sockets.connected[socketIds[i]]) {
                event.SocketJoinChatRoom(event.IO.sockets.connected[socketIds[i]], InvitationId);
            }
        }
    }
};
SocketEventHandler = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        user_service_1.UserService,
        chat_service_1.ChatService,
        invitation_service_1.InvitationService,
        media_service_1.MediaService])
], SocketEventHandler);
exports.SocketEventHandler = SocketEventHandler;
//# sourceMappingURL=socket_event.handler.js.map