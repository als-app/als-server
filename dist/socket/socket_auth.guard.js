"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const authentication_1 = require("../constant/authentication");
const response_exception_1 = require("../exception/response.exception");
const locale_1 = require("../locale/locale");
const auth_service_1 = require("../modules/auth/auth.service");
let SocketAuthGuard = class SocketAuthGuard {
    constructor(_reflector, _authService) {
        this._reflector = _reflector;
        this._authService = _authService;
    }
    async canActivate(context) {
        const ctx = context.switchToWs();
        const socket = ctx.getClient();
        const requiredAuthorization = this._reflector.get(authentication_1.AUTHORIZATION_HEADER_KEY, context.getHandler());
        const roles = this._reflector.get(authentication_1.ROLES, context.getHandler());
        return true;
    }
};
SocketAuthGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector, auth_service_1.AuthService])
], SocketAuthGuard);
exports.SocketAuthGuard = SocketAuthGuard;
//# sourceMappingURL=socket_auth.guard.js.map