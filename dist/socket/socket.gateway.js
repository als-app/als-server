"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var SocketGateway_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const socket_1 = require("../constant/socket");
const socket_event_handler_1 = require("./socket_event.handler");
const chat_joined_event_1 = require("./events/chat/chat_joined.event");
const connected_event_1 = require("./events/connected.event");
const disconnected_event_1 = require("./events/disconnected.event");
const message_event_1 = require("./events/chat/message.event");
const common_1 = require("@nestjs/common");
const socket_auth_guard_1 = require("./socket_auth.guard");
const authorize_decorator_1 = require("../decorator/authorize.decorator");
let SocketGateway = SocketGateway_1 = class SocketGateway {
    constructor(_socketEventHandler) {
        this._socketEventHandler = _socketEventHandler;
    }
    afterInit(server) {
        SocketGateway_1._io = server;
        SocketGateway_1._io.use(this._socketEventHandler._socketAuthentication.bind(this._socketEventHandler));
    }
    async handleConnection(socket) {
        let Event = new connected_event_1.default(socket);
        return await this._socketEventHandler.OnConnectedEvent(Event);
    }
    async handleDisconnect(socket) {
        let Event = new disconnected_event_1.default(socket);
        await this._socketEventHandler.OnDisconnectEvent(Event);
    }
    async ChatJoinedEvent(socket, data) {
        let Event = new chat_joined_event_1.default(socket, data);
        return await this._socketEventHandler.OnChatJoinedEvent(Event);
    }
    async MessageEvent(socket, data) {
        let Event = new message_event_1.default(socket, data, SocketGateway_1._io);
        return await this._socketEventHandler.OnMessageEvent(Event);
    }
};
__decorate([
    websockets_1.SubscribeMessage(socket_1.SocketEventType.ChatJoin),
    authorize_decorator_1.Authorized(),
    __param(0, websockets_1.ConnectedSocket()), __param(1, websockets_1.MessageBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SocketGateway.prototype, "ChatJoinedEvent", null);
__decorate([
    websockets_1.SubscribeMessage(socket_1.SocketEventType.Message),
    authorize_decorator_1.Authorized(),
    __param(0, websockets_1.ConnectedSocket()), __param(1, websockets_1.MessageBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SocketGateway.prototype, "MessageEvent", null);
SocketGateway = SocketGateway_1 = __decorate([
    common_1.UseGuards(socket_auth_guard_1.SocketAuthGuard),
    websockets_1.WebSocketGateway(),
    __metadata("design:paramtypes", [socket_event_handler_1.SocketEventHandler])
], SocketGateway);
exports.SocketGateway = SocketGateway;
//# sourceMappingURL=socket.gateway.js.map