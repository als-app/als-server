/// <reference types="socket.io" />
import { AuthService } from "src/modules/auth/auth.service";
import { UserService } from "src/modules/auth/user.service";
import { MediaService } from "src/modules/media/media.service";
import ChatJoinedSocketEvent from "./events/chat/chat_joined.event";
import ConnectedSocketEvent from "./events/connected.event";
import DisconnectedSocketEvent from "./events/disconnected.event";
import MessageSocketEvent from "./events/chat/message.event";
import { ChatService } from '../modules/chat/chat.service';
import { InvitationService } from '../modules/invitation/invitation.service';
export declare class SocketEventHandler {
    private _authService;
    private _userService;
    private _chatService;
    private _invitationService;
    private _mediaService;
    constructor(_authService: AuthService, _userService: UserService, _chatService: ChatService, _invitationService: InvitationService, _mediaService: MediaService);
    OnConnectedEvent(event: ConnectedSocketEvent): Promise<boolean>;
    OnDisconnectEvent(event: DisconnectedSocketEvent): Promise<boolean>;
    OnChatJoinedEvent(event: ChatJoinedSocketEvent): Promise<boolean>;
    OnMessageEvent(event: MessageSocketEvent): Promise<import("./events/chat/message.event").MessageSocketEventData>;
    _socketAuthentication(client: SocketIO.Socket, next: Function): Promise<any>;
    private _addUserSocket;
    private _joinChatRoom;
}
