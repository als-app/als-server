"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemplateRenderer = exports.EmailTemplate = void 0;
const mustache_1 = require("mustache");
const fs = require("fs");
const path = require("path");
const locale_1 = require("../locale/locale");
const authentication_1 = require("../constant/authentication");
const util_helper_1 = require("../helpers/util.helper");
let UserEmailVerificationTemplatePath = "/user_email_verification.html";
let ResetPasswordTemplatePath = "/user_password_reset.html";
var EmailTemplate;
(function (EmailTemplate) {
    EmailTemplate["UserEmailVerification"] = "UserEmailVerification";
    EmailTemplate["ResetPassword"] = "ResetPassword";
})(EmailTemplate = exports.EmailTemplate || (exports.EmailTemplate = {}));
exports.TemplateRenderer = {
    UserEmailVerification: (data, locale) => {
        let EmailVerifyHTML = fs.readFileSync(path.resolve(__dirname + UserEmailVerificationTemplatePath), "utf-8");
        let EmailVerifyTemplate = mustache_1.render(EmailVerifyHTML, {
            Address: locale_1.__T(locale, locale_1.MessageTemplates.Address),
            Link: authentication_1.GetVerifyUserEmailLink(data.Code),
            Code: data.Code,
            Year: new Date().getFullYear(),
            Footer: locale_1.__T(locale, locale_1.MessageTemplates.EmailFooter)
        });
        let subject = locale_1.__T(locale, locale_1.MessageTemplates.EmailVerificationSubject);
        return { content: EmailVerifyTemplate, subject: subject };
    },
    ResetPassword: (data, locale) => {
        let ResetPasswordHTML = fs.readFileSync(path.resolve(__dirname + ResetPasswordTemplatePath), "utf-8");
        let ResetPasswordTemplate = mustache_1.render(ResetPasswordHTML, {
            Address: locale_1.__T(locale, locale_1.MessageTemplates.Address),
            Link: data.Link,
            Code: data.Code,
            FullName: data.FullName,
            Year: new Date().getFullYear(),
            Footer: locale_1.__T(locale, locale_1.MessageTemplates.EmailFooter)
        });
        let subject = locale_1.__T(locale, locale_1.MessageTemplates.ResetPasswordSubject);
        return { content: ResetPasswordTemplate, subject: subject };
    }
};
//# sourceMappingURL=template_renderer.js.map