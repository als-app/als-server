export declare enum EmailTemplate {
    UserEmailVerification = "UserEmailVerification",
    ResetPassword = "ResetPassword"
}
export declare let TemplateRenderer: {
    [key in keyof typeof EmailTemplate]: (data: any, locale: any) => {
        subject: string;
        content: string;
    };
};
