"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FatalErrorException = exports.ForbiddenException = exports.BadRequestException = exports.UnAuthorizedException = exports.NotFoundException = void 0;
const common_1 = require("@nestjs/common");
const locale_1 = require("../locale/locale");
class NotFoundException extends common_1.HttpException {
    constructor(key, data) {
        super({ key, data }, common_1.HttpStatus.NOT_FOUND);
    }
}
exports.NotFoundException = NotFoundException;
class UnAuthorizedException extends common_1.HttpException {
    constructor(key, data) {
        super({ key, data }, common_1.HttpStatus.UNAUTHORIZED);
    }
}
exports.UnAuthorizedException = UnAuthorizedException;
class BadRequestException extends common_1.HttpException {
    constructor(key, data) {
        super({ key, data }, common_1.HttpStatus.BAD_REQUEST);
    }
}
exports.BadRequestException = BadRequestException;
class ForbiddenException extends common_1.HttpException {
    constructor(key, data) {
        super({ key, data }, common_1.HttpStatus.FORBIDDEN);
    }
}
exports.ForbiddenException = ForbiddenException;
class FatalErrorException extends common_1.HttpException {
    constructor(key, data) {
        super({ key, data }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
exports.FatalErrorException = FatalErrorException;
//# sourceMappingURL=response.exception.js.map