import { HttpException } from "@nestjs/common";
import { MessageTemplates } from "src/locale/locale";
export declare class NotFoundException extends HttpException {
    constructor(key: MessageTemplates, data?: any);
}
export declare class UnAuthorizedException extends HttpException {
    constructor(key: MessageTemplates, data?: any);
}
export declare class BadRequestException extends HttpException {
    constructor(key: MessageTemplates, data?: any);
}
export declare class ForbiddenException extends HttpException {
    constructor(key: MessageTemplates, data?: any);
}
export declare class FatalErrorException extends HttpException {
    constructor(key: MessageTemplates, data?: any);
}
