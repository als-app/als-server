"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const authentication_1 = require("../constant/authentication");
const logger_helper_1 = require("../helpers/logger.helper");
const locale_1 = require("../locale/locale");
function _prepareBadRequestValidationErrors(errors) {
    let Errors = {};
    for (const err of errors) {
        const constraint = Object.values(err.constraints) && Object.values(err.constraints).length && Object.values(err.constraints)[0];
        Errors[err.property] = constraint ? constraint : `${err.property} is invalid`;
    }
    return Errors;
}
let HttpExceptionFilter = class HttpExceptionFilter {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const locale = request.header[authentication_1.LOCALE_HEADER_KEY] || locale_1.Locale.En;
        if (!(exception instanceof common_1.HttpException)) {
            logger_helper_1.Logger.Fatal(exception.stack ? exception.stack : exception, "ERROR");
            let ResponseToSend = {
                Status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                Message: locale_1.__T(locale, locale_1.MessageTemplates.InternalServerError),
                Data: null
            };
            response.__ss_body = ResponseToSend;
            response.status(common_1.HttpStatus.OK).json(ResponseToSend);
            return;
        }
        const status = exception.getStatus();
        const exceptionResponse = exception.getResponse();
        if (exception instanceof common_1.BadRequestException &&
            exceptionResponse.message &&
            Array.isArray(exceptionResponse.message)) {
            let ResponseToSend = {
                Status: status,
                Message: locale_1.__T(locale, locale_1.MessageTemplates.InvalidValueFor, {
                    values: exceptionResponse.message.map((x) => x.property).join(', '),
                }),
                Data: null,
                Errors: _prepareBadRequestValidationErrors(exceptionResponse.message)
            };
            response.__ss_body = ResponseToSend;
            response.status(common_1.HttpStatus.OK).json(ResponseToSend);
        }
        else {
            let ResponseToSend = {
                Status: status,
                Message: locale_1.__T(locale, exceptionResponse.key || locale_1.MessageTemplates.NotFoundError, exceptionResponse.data),
                Data: null,
            };
            response.__ss_body = ResponseToSend;
            response.status(common_1.HttpStatus.OK).json(ResponseToSend);
        }
    }
};
HttpExceptionFilter = __decorate([
    common_1.Catch(common_1.HttpException, Error)
], HttpExceptionFilter);
exports.HttpExceptionFilter = HttpExceptionFilter;
//# sourceMappingURL=http.exception.js.map