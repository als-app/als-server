export declare class QueueProcessor {
    static ProcessQueue(): Promise<void>;
    static ProcessEmail(data: any, done: any): Promise<void>;
    static ProcessNotification(data: any, done: any): Promise<void>;
}
