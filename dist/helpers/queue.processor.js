"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueueProcessor = void 0;
const app_1 = require("../app");
const queue_1 = require("../constant/queue");
const notification_service_1 = require("../modules/notification/notification.service");
const logger_helper_1 = require("./logger.helper");
const mail_helper_1 = require("./mail.helper");
const queue_helper_1 = require("./queue.helper");
class QueueProcessor {
    static async ProcessQueue() {
        const keys = Object.keys(queue_1.QueueJobs);
        let handler;
        for (let i = 0; i < keys.length; i++) {
            switch (keys[i]) {
                case queue_1.QueueJobs.EMAIL:
                    handler = QueueProcessor.ProcessEmail;
                    break;
                case queue_1.QueueJobs.NOTIFICATION:
                    handler = QueueProcessor.ProcessNotification;
                    break;
                default:
                    break;
            }
            if (handler) {
                queue_helper_1.SSQueue.Process(keys[i], handler);
            }
        }
    }
    static async ProcessEmail(data, done) {
        if (data) {
            try {
                await mail_helper_1.SSMailer.SendByTemplate(data.EmailTemplate, data.Email, data.Locale, data.EmailData);
                done();
            }
            catch (err) {
                logger_helper_1.Logger.Error(err);
                done(new Error("Email not sent"));
            }
        }
        else {
            done(new Error("Email not sent"));
        }
    }
    static async ProcessNotification(data, done) {
        if (data) {
            try {
                let notificationService = (await app_1.App.GetNestApplicationInstance()).get(notification_service_1.NotificationService);
                await notificationService.ProcessNotification(data.Type, data);
                done();
            }
            catch (err) {
                logger_helper_1.Logger.Error(err);
                done(new Error("Notification not sent"));
            }
        }
        else {
            done(new Error("Notification not sent"));
        }
    }
}
exports.QueueProcessor = QueueProcessor;
//# sourceMappingURL=queue.processor.js.map