"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SSQueue = void 0;
const Kue = require("kue");
const env_helper_1 = require("./env.helper");
const logger_helper_1 = require("./logger.helper");
class SSQueue {
    constructor() {
        this._queue = (() => {
            let q = Kue.createQueue({
                prefix: "ss",
                redis: {
                    host: env_helper_1.appEnv("DB_REDIS_HOST", "localhost"),
                    port: env_helper_1.appEnv("DB_REDIS_PORT", 6379),
                    db: 3
                }
            });
            q.watchStuckJobs(1000);
            return q;
        })();
    }
    static GetInstance() {
        if (this._instance === null) {
            this._instance = new this();
            SSQueue.HandlingQueueErrors();
        }
        return this._instance;
    }
    static async Enqueue(task, data, attemptsCount = 1, delay) {
        let job = this.GetInstance()
            ._queue.createJob(task, data)
            .attempts(attemptsCount)
            .ttl(parseInt(env_helper_1.appEnv("QUEUE_JOBS_TTL", "360000")))
            .removeOnComplete(true);
        if (delay) {
            job = job.delay(delay);
        }
        job.save(err => {
            if (err) {
                logger_helper_1.Logger.Error(err, "QUEUE");
                return Promise.reject(err);
            }
            return Promise.resolve();
        });
    }
    static Process(task, callback) {
        try {
            this.GetInstance()._queue.process(task, async function (job, done) {
                callback(job.data, done);
            });
        }
        catch (err) {
            logger_helper_1.Logger.Error(err, "QUEUE");
        }
    }
    static HandlingQueueErrors() {
        this.GetInstance()._queue.on("error", function (error) {
        });
    }
}
exports.SSQueue = SSQueue;
SSQueue._instance = null;
//# sourceMappingURL=queue.helper.js.map