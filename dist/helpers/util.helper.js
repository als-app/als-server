"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateVerificationCode = exports.ReplaceObjectValuesInString = exports.ConvertVersionStringToFloatNumber = exports.IsAndroid = exports.SortObjectByKeys = exports.traverseObjectWithSearchKey = exports.GetEnumKeyByEnumValue = exports.SplitName = exports.ConvertToBase = exports.ObjectIdToHexString = exports.GetOrderByOptions = exports.GetPaginationOptions = exports.ComparePassword = exports.HashPassword = void 0;
const bcrypt_nodejs_1 = require("bcrypt-nodejs");
const anyBase = require("any-base");
const env_helper_1 = require("./env.helper");
const gpc = require("generate-pincode");
async function HashPassword(plainText) {
    return new Promise(function (resolve, reject) {
        bcrypt_nodejs_1.genSalt(10, function (error, salt) {
            if (error) {
                reject(error);
            }
            else {
                bcrypt_nodejs_1.hash(plainText, salt, null, function (error, hash) {
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(hash);
                    }
                });
            }
        });
    });
}
exports.HashPassword = HashPassword;
async function ComparePassword(plainText, hash) {
    return new Promise(function (resolve, reject) {
        bcrypt_nodejs_1.compare(plainText, hash, function (error, result) {
            if (error) {
                reject(error);
            }
            else {
                resolve(result);
            }
        });
    });
}
exports.ComparePassword = ComparePassword;
function GetPaginationOptions(params) {
    let options = {
        Limit: 20,
        Offset: 0,
    };
    let Limit = params.Limit;
    let Page = params.Page || 1;
    let After = params.After;
    let Before = params.Before;
    if (Limit) {
        options.Limit = parseInt(Limit.toString());
    }
    if (After) {
        options.Offset = '+' + After;
    }
    else if (Before) {
        options.Offset = '-' + Before;
    }
    else if (Page) {
        options.Offset = options.Limit * Math.max(Page - 1, 0);
    }
    return options;
}
exports.GetPaginationOptions = GetPaginationOptions;
function GetOrderByOptions(params) {
    let Column = params.Column;
    let Direction = params.Direction;
    let options = {
        Column: 'Id',
        Direction: 'DESC',
    };
    if (Column) {
        options.Column = Column;
    }
    if (Direction) {
        options.Direction = Direction;
    }
    return options;
}
exports.GetOrderByOptions = GetOrderByOptions;
function ObjectIdToHexString(value, obj) {
    if (!Array.isArray(ObjectIdToHexString.prototype.hexTable)) {
        ObjectIdToHexString.prototype.hexTable = [];
        for (let i = 0; i < 256; i++) {
            ObjectIdToHexString.prototype.hexTable[i] =
                (i <= 15 ? '0' : '') + i.toString(16);
        }
    }
    const id = value && typeof value == 'object' && value.id
        ? Object.keys(value.id).map((key) => value.id[key])
        : [];
    let hexString = '';
    for (const el of id) {
        hexString += ObjectIdToHexString.prototype.hexTable[el];
    }
    return hexString;
}
exports.ObjectIdToHexString = ObjectIdToHexString;
function ConvertToBase(sourceString, sourceBase, destBase) {
    let converter = anyBase(sourceBase, destBase);
    return converter(sourceString);
}
exports.ConvertToBase = ConvertToBase;
function SplitName(name) {
    let FullName = name.split(' ');
    let LastName = FullName.length > 1 ? FullName.pop() : null;
    let FirstName = FullName.join(' ');
    return { FirstName, LastName };
}
exports.SplitName = SplitName;
function GetEnumKeyByEnumValue(myEnum, enumValue) {
    let keys = Object.keys(myEnum).filter((x) => myEnum[x] == enumValue);
    return keys.length > 0 ? keys[0] : null;
}
exports.GetEnumKeyByEnumValue = GetEnumKeyByEnumValue;
async function traverseObjectWithSearchKey(objOrArray, searchKeys, modifier) {
    if (!objOrArray) {
        return;
    }
    if (Array.isArray(objOrArray)) {
        for (let val of objOrArray) {
            await this.traverseObjectWithSearchKey(val, searchKeys, modifier);
        }
    }
    else if (typeof objOrArray === 'object') {
        let objectKeys = Object.keys(objOrArray);
        for (let key of objectKeys) {
            if (searchKeys.includes(key)) {
                objOrArray[key] = await modifier(objOrArray[key]);
            }
            await this.traverseObjectWithSearchKey(objOrArray[key], searchKeys, modifier);
        }
    }
}
exports.traverseObjectWithSearchKey = traverseObjectWithSearchKey;
function SortObjectByKeys(data) {
    let orignalKeys = Object.keys(data);
    let sortedKeys = orignalKeys.sort();
    let obj = {};
    sortedKeys.forEach((key) => {
        obj[key] = data[key];
    });
    return obj;
}
exports.SortObjectByKeys = SortObjectByKeys;
function IsAndroid(userAgent) {
    let result = false;
    let androidRegex = new RegExp(/android/i);
    if (androidRegex.test(userAgent)) {
        result = true;
    }
    return result;
}
exports.IsAndroid = IsAndroid;
function ConvertVersionStringToFloatNumber(versionString) {
    return parseFloat(versionString.split('.')[0] +
        '.' +
        versionString.split('.').slice(1).join(''));
}
exports.ConvertVersionStringToFloatNumber = ConvertVersionStringToFloatNumber;
function ReplaceObjectValuesInString(text, obj = {}) {
    for (const field in obj) {
        text = text.replace(new RegExp(`{{${field}}}`, "g"), obj[field]);
    }
    return text;
}
exports.ReplaceObjectValuesInString = ReplaceObjectValuesInString;
function GenerateVerificationCode() {
    return env_helper_1.appEnv("USE_DUMMY_VERIFICATION_CODES", false) ? "0000" : gpc(4);
}
exports.GenerateVerificationCode = GenerateVerificationCode;
;
//# sourceMappingURL=util.helper.js.map