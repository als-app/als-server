"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendNotification = void 0;
const FCM = require("fcm-node");
const env_helper_1 = require("./env.helper");
let fcm = null;
function getFcmInstance() {
    if (fcm == null) {
        fcm = new FCM(env_helper_1.appEnv("FCM_SERVER_KEY", "sscccdcdsccdsc6516"));
    }
    return fcm;
}
function SendNotification(fcmToken, title, body, data = {}) {
    return new Promise((resolve, reject) => {
        const message = {
            to: fcmToken,
            notification: {
                title,
                body,
                sound: "default"
            },
            data,
            priority: "high"
        };
        getFcmInstance().send(message, (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
exports.SendNotification = SendNotification;
//# sourceMappingURL=notification.helper.js.map