export declare class SSQueue {
    private static _instance;
    private _queue;
    private constructor();
    static GetInstance(): SSQueue;
    static Enqueue(task: string, data: any, attemptsCount?: number, delay?: number): Promise<void>;
    static Process(task: string, callback: any): void;
    static HandlingQueueErrors(): void;
}
