import * as moment from 'moment-timezone';
export declare enum AllowDateFormat {
    ISODate = "YYYY-MM-DD",
    YearMonth = "YYYY-MM",
    ShortDate = "MM/DD/YYYY",
    LongDate = "MMM DD YYYY",
    StringDate = "YYYY-MM-DDTHH:mm:ss.sssZ",
    DateTime = "YYYY-MM-DD HH:mm:ss",
    TimeWithOutSeconds = "LT",
    TimeWithSeconds = "LTS",
    DD = "DD",
    MM = "MM",
    YYYY = "YYYY",
    YearWeek = "GGGG-WW"
}
export declare function Now(): Date;
export declare function IsSameOrAfter(firstDate: any, secondDate: any): boolean;
export declare function IsBefore(firstDate: any, secondDate: any): boolean;
export declare function ConvertToSpecificFormat(date: Date, format: AllowDateFormat): string;
export declare function SubtractDays(date: any, days: any): Date;
export declare function AddDays(date: any, days: any): Date;
export declare function AddMonths(date: any, month: any): Date;
export declare function SubtractMonths(date: any, month: any): Date;
export declare function SubtractHours(date: any, hour: any): Date;
export declare function DiffBetweenTwoDates(firstDate: any, secondDate: any): number;
export declare function StartDateOfMonth(date: any): Date;
export declare function EndDateOfMonth(date: any): Date;
export declare function StartOfDay(date: any): Date;
export declare function EndOfDay(date: any): Date;
export declare function GetDate(date: Date): number;
export declare function GetMonth(date: Date): number;
export declare function GetYear(date: Date): number;
export declare function GetMomentFromString(dateString: any, format: AllowDateFormat, tz?: string): moment.Moment;
export declare function GetMomentFromTimezone(date: Date, tz: any): moment.Moment;
export declare function GetTimezoneDate(date: Date, tz: any): Date;
export declare function GetLastDayOfYear(year: any): Date;
export declare function IsFuture(date: Date): boolean;
export declare function getSeries(startDate: moment.Moment, endDate: moment.Moment, type: moment.unitOfTime.DurationConstructor): string[];
export declare function getYearKeys(year: any): string[];
export declare function generateLastXMonthKeys(today: moment.Moment, Months: number): string[];
export declare function generateDayIntervalKeys(endDate: moment.Moment, interval: any): string[];
export declare function getPreviousQuarterHourRange(dateTime?: moment.Moment): {
    start: moment.Moment;
    end: moment.Moment;
};
export declare function getAvailableTimezones(): string[];
export declare function getDayChangedTimezones(dateTime: moment.Moment): string[];
export declare function getWeekChangedTimezones(dateTime: moment.Moment): string[];
export declare function getMonthChangedTimezones(dateTime: moment.Moment): string[];
