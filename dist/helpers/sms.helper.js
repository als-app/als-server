"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwilioHelper = void 0;
const twilio = require("twilio");
const env_helper_1 = require("./env.helper");
class TwilioHelper {
    static GetTwilioClient() {
        if (TwilioHelper._client === null) {
            TwilioHelper._client = twilio(env_helper_1.appEnv("TWILIO_ACCOUNT_SID", ""), env_helper_1.appEnv("TWILIO_AUTH_TOKEN", ""));
        }
        return TwilioHelper._client;
    }
    static GetApiUrl() {
        if (TwilioHelper._apiUrl === null) {
            TwilioHelper._apiUrl = env_helper_1.appEnv("API_URL", "");
        }
        return TwilioHelper._apiUrl;
    }
    static async Message(To, From, Body) {
        let sms = await TwilioHelper.GetTwilioClient().messages.create({
            body: Body,
            from: From,
            to: To,
            statusCallback: `${TwilioHelper.GetApiUrl()}/callback/message/status`
        });
        return sms;
    }
}
exports.TwilioHelper = TwilioHelper;
TwilioHelper._client = null;
TwilioHelper._apiUrl = null;
//# sourceMappingURL=sms.helper.js.map