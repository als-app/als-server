"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConstructWebhookEvent = exports.RetreiveBalanceTransaction = exports.RetreivePaymentMethod = exports.AttachPaymentMethod = exports.CreateSession = exports.Charge = exports.CreatePaymentIntent = exports.RetreiveCustomer = exports.UpdateCustomer = exports.CreateCustomer = exports.ChargeFailErrorCodes = exports.WebhookEvents = void 0;
const stripe_1 = require("stripe");
const env_helper_1 = require("./env.helper");
const Stripe = new stripe_1.Stripe(env_helper_1.appEnv("STRIPE_SECRET_KEY", ""), { apiVersion: "2020-08-27" });
var WebhookEvents;
(function (WebhookEvents) {
    WebhookEvents["ChargeSucceeded"] = "charge.succeeded";
    WebhookEvents["ChargeFailed"] = "charge.failed";
    WebhookEvents["SetupIntentSucceeded"] = "setup_intent.succeeded";
    WebhookEvents["PaymentIntentSucceeded"] = "payment_intent.succeeded";
    WebhookEvents["PaymentIntentFailed"] = "payment_intent.payment_failed";
})(WebhookEvents = exports.WebhookEvents || (exports.WebhookEvents = {}));
var ChargeFailErrorCodes;
(function (ChargeFailErrorCodes) {
    ChargeFailErrorCodes["AuthenticationRequired"] = "authentication_required";
    ChargeFailErrorCodes["CardDeclined"] = "card_declined";
})(ChargeFailErrorCodes = exports.ChargeFailErrorCodes || (exports.ChargeFailErrorCodes = {}));
async function CreateCustomer(paymentToken, email, metaData, description) {
    return await Stripe.customers.create(Object.assign({ source: paymentToken, metadata: metaData, email: email }, (description && { description: description })));
}
exports.CreateCustomer = CreateCustomer;
async function UpdateCustomer(customerId, paymentToken, metaData) {
    return await Stripe.customers.update(customerId, {
        source: paymentToken,
        metadata: metaData
    });
}
exports.UpdateCustomer = UpdateCustomer;
async function RetreiveCustomer(customerId) {
    return await Stripe.customers.retrieve(customerId);
}
exports.RetreiveCustomer = RetreiveCustomer;
async function CreatePaymentIntent(amount, currency, customerId, paymentMethod, metaData) {
    return await Stripe.paymentIntents.create({
        amount: amount,
        currency: currency,
        customer: customerId,
        payment_method_types: ["card"],
        payment_method: paymentMethod,
        off_session: true,
        confirm: true,
        metadata: metaData
    });
}
exports.CreatePaymentIntent = CreatePaymentIntent;
async function Charge(customerId, amount, currency, metaData) {
    return await Stripe.charges.create({
        amount: amount,
        currency: currency,
        customer: customerId,
        metadata: metaData,
        expand: ["balance_transaction"]
    });
}
exports.Charge = Charge;
async function CreateSession(successUrl, cancelUrl) {
    return await Stripe.checkout.sessions.create({
        payment_method_types: ["card"],
        mode: "setup",
        success_url: successUrl,
        cancel_url: cancelUrl
    });
}
exports.CreateSession = CreateSession;
async function AttachPaymentMethod(paymentMethod, customerId) {
    return await Stripe.paymentMethods.attach(paymentMethod, {
        customer: customerId
    });
}
exports.AttachPaymentMethod = AttachPaymentMethod;
async function RetreivePaymentMethod(paymentMethod) {
    return await Stripe.paymentMethods.retrieve(paymentMethod);
}
exports.RetreivePaymentMethod = RetreivePaymentMethod;
async function RetreiveBalanceTransaction(transactionId) {
    return await Stripe.balanceTransactions.retrieve(transactionId);
}
exports.RetreiveBalanceTransaction = RetreiveBalanceTransaction;
function ConstructWebhookEvent(data, signature) {
    return Stripe.webhooks.constructEvent(data, signature, env_helper_1.appEnv("STRIPE_WEBHOOK_SIGNATURE", ""));
}
exports.ConstructWebhookEvent = ConstructWebhookEvent;
//# sourceMappingURL=payment.helper.js.map