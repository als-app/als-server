"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetObjectHeaders = exports.UploadFile = exports.GetSignedUrl = exports.GetFile = exports.DeleteFile = exports.S3FileUploadOptions = void 0;
const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");
const env_helper_1 = require("./env.helper");
const file_config_1 = require("../config/file.config");
aws.config.update(file_config_1.AWSConfig);
const S3 = new aws.S3();
exports.S3FileUploadOptions = multer({
    storage: multerS3({
        s3: S3,
        bucket: env_helper_1.appEnv('AWS_BUCKET_NAME', ''),
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            let ChatId = req.query.ChatId;
            let Path = '';
            let fileName = Math.random().toString().split('.')[1] +
                Date.now() +
                file.originalname.substr(file.originalname.length - 30 >= 0
                    ? file.originalname.length - 30
                    : 0);
            fileName = fileName.replace(/\s/g, '').replace(/[^\w-.]+/g, '');
            if (ChatId) {
                Path = `data/${ChatId}/${fileName}`;
            }
            else {
                Path = `data/${fileName}`;
            }
            cb(null, Path);
        },
    }),
    limits: {
        fileSize: 2 * 1024 * 1024 * 1024,
    },
});
const DeleteFile = function (fileName) {
    return new Promise(function (resolve, reject) {
        var params = {
            Bucket: env_helper_1.appEnv('AWS_BUCKET_NAME', ''),
            Key: decodeURIComponent(fileName),
        };
        S3.deleteObject(params, function (err, data) {
            if (data) {
                resolve(data);
            }
            else {
                reject(err);
            }
        });
    });
};
exports.DeleteFile = DeleteFile;
const GetFile = function (fileName) {
    return new Promise(function (resolve, reject) {
        var params = {
            Bucket: env_helper_1.appEnv('AWS_BUCKET_NAME', ''),
            Key: decodeURIComponent(fileName),
        };
        S3.getObject(params).send(function (err, data) {
            if (data && data.Body) {
                resolve(data.Body);
            }
            else {
                reject(err);
            }
        });
    });
};
exports.GetFile = GetFile;
const GetSignedUrl = function (fileName, expirationTime) {
    return new Promise(function (resolve, reject) {
        var params = {
            Bucket: env_helper_1.appEnv('AWS_BUCKET_NAME', ''),
            Key: decodeURIComponent(fileName),
            Expires: expirationTime,
        };
        S3.getSignedUrl('getObject', params, function (err, url) {
            if (url) {
                resolve(url);
            }
            else {
                reject(err);
            }
        });
    });
};
exports.GetSignedUrl = GetSignedUrl;
const UploadFile = function (fileName, content) {
    return new Promise(function (resolve, reject) {
        fileName =
            Math.random().toString().split('.')[1] +
                Date.now() +
                fileName.substr(fileName.length - 30 >= 0 ? fileName.length - 30 : 0);
        fileName = fileName.replace(/\s/g, '').replace(/[^\w-.]+/g, '');
        var fileKey = `data/${fileName}`;
        var params = {
            Bucket: env_helper_1.appEnv('AWS_BUCKET_NAME', ''),
            Key: fileKey,
            ACL: 'private',
            Body: content,
        };
        S3.putObject(params, function (err, data) {
            if (data) {
                resolve(fileKey);
            }
            else {
                reject(err);
            }
        });
    });
};
exports.UploadFile = UploadFile;
const GetObjectHeaders = async function (key) {
    let result = await S3.headObject({
        Bucket: env_helper_1.appEnv("AWS_BUCKET_NAME", ""),
        Key: key
    }).promise();
    return {
        ContentType: result.ContentType,
        ContentLength: result.ContentLength
    };
};
exports.GetObjectHeaders = GetObjectHeaders;
//# sourceMappingURL=s3.helper.js.map