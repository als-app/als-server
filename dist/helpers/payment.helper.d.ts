import { Stripe as stripe } from "stripe";
export declare enum WebhookEvents {
    ChargeSucceeded = "charge.succeeded",
    ChargeFailed = "charge.failed",
    SetupIntentSucceeded = "setup_intent.succeeded",
    PaymentIntentSucceeded = "payment_intent.succeeded",
    PaymentIntentFailed = "payment_intent.payment_failed"
}
export declare enum ChargeFailErrorCodes {
    AuthenticationRequired = "authentication_required",
    CardDeclined = "card_declined"
}
export declare function CreateCustomer(paymentToken: string, email: string, metaData: any, description?: string): Promise<stripe.Response<stripe.Customer>>;
export declare function UpdateCustomer(customerId: string, paymentToken: string, metaData: any): Promise<stripe.Response<stripe.Customer>>;
export declare function RetreiveCustomer(customerId: string): Promise<stripe.Response<stripe.Customer | stripe.DeletedCustomer>>;
export declare function CreatePaymentIntent(amount: number, currency: string, customerId: string, paymentMethod: string, metaData: any): Promise<stripe.Response<stripe.PaymentIntent>>;
export declare function Charge(customerId: string, amount: number, currency: string, metaData: any): Promise<stripe.Response<stripe.Charge>>;
export declare function CreateSession(successUrl: string, cancelUrl: string): Promise<stripe.Response<stripe.Checkout.Session>>;
export declare function AttachPaymentMethod(paymentMethod: string, customerId: string): Promise<stripe.Response<stripe.PaymentMethod>>;
export declare function RetreivePaymentMethod(paymentMethod: string): Promise<stripe.Response<stripe.PaymentMethod>>;
export declare function RetreiveBalanceTransaction(transactionId: string): Promise<stripe.Response<stripe.BalanceTransaction>>;
export declare function ConstructWebhookEvent(data: any, signature: string): stripe.Event;
