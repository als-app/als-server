import { MediaModelType } from "src/modules/media/media.entity";
export declare const MediaMaxSize: {
    1: number;
    2: number;
};
export declare const IsMediaSizeAllowed: (size: number, type: MediaModelType) => boolean;
export declare const IsMediaExtensionAllowed: (extension: string) => boolean;
export declare const GetMediaType: (mime: string) => MediaModelType;
export declare const GetMediaExtension: (fileName: string) => string;
export declare const CreateUniqueFileName: (extension: string) => string;
export declare const CreateFilePath: (fileName: string, prependPath?: string) => string;
export declare const GetTemporaryCredentialsForMediaUpload: (path: string, mediaId: number) => Promise<{
    AccessKeyId: string;
    SecretAccessKey: string;
    SessionToken: string;
    Path: string;
}>;
export declare const FileUploadOptions: {
    storage: any;
};
