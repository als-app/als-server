"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appEnv = void 0;
function appEnv(name, def) {
    let val = undefined;
    if (process.env[name] === undefined) {
        val = def;
    }
    else {
        val = process.env[name];
    }
    if (typeof val == 'string') {
        if (val.toLowerCase() == 'true') {
            val = true;
        }
        else if (val.toLowerCase() == 'false') {
            val = false;
        }
    }
    return val;
}
exports.appEnv = appEnv;
//# sourceMappingURL=env.helper.js.map