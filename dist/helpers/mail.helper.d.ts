import { EmailTemplate } from 'src/templates/template_renderer';
import { Locale } from 'moment-timezone';
export declare class SSMailer {
    private _transporter;
    private static _instance;
    private constructor();
    static GetInstance(): SSMailer;
    static Send(to: string, subject: string, content: any, attachments?: any): Promise<boolean>;
    static SendByTemplate(template: EmailTemplate, to: any, locale: Locale, data: Object): Promise<void>;
}
