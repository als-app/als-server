import { Logger as IQueryLogger } from "typeorm";
export declare class Logger {
    private static logTemplate;
    private static queryLogger;
    private static loggerMiddleware;
    private static log;
    static Trace(data: any, tag?: string): void;
    static Debug(data: any, tag?: string): void;
    static Info(data: any, tag?: string): void;
    static Warn(data: any, tag?: string): void;
    static Error(data: any, tag?: string): void;
    static Fatal(data: any, tag?: string): void;
    static GetQueryLogger(): IQueryLogger;
    static GetLoggerMiddleware(): Function;
}
