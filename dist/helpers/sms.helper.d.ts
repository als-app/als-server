export declare class TwilioHelper {
    private static _client;
    private static _apiUrl;
    private static GetTwilioClient;
    private static GetApiUrl;
    static Message(To: any, From: any, Body: any): Promise<any>;
}
