export declare function SendNotification(fcmToken: string, title: string, body: string, data?: any): Promise<unknown>;
