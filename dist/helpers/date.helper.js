"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMonthChangedTimezones = exports.getWeekChangedTimezones = exports.getDayChangedTimezones = exports.getAvailableTimezones = exports.getPreviousQuarterHourRange = exports.generateDayIntervalKeys = exports.generateLastXMonthKeys = exports.getYearKeys = exports.getSeries = exports.IsFuture = exports.GetLastDayOfYear = exports.GetTimezoneDate = exports.GetMomentFromTimezone = exports.GetMomentFromString = exports.GetYear = exports.GetMonth = exports.GetDate = exports.EndOfDay = exports.StartOfDay = exports.EndDateOfMonth = exports.StartDateOfMonth = exports.DiffBetweenTwoDates = exports.SubtractHours = exports.SubtractMonths = exports.AddMonths = exports.AddDays = exports.SubtractDays = exports.ConvertToSpecificFormat = exports.IsBefore = exports.IsSameOrAfter = exports.Now = exports.AllowDateFormat = void 0;
const moment = require("moment-timezone");
var AllowDateFormat;
(function (AllowDateFormat) {
    AllowDateFormat["ISODate"] = "YYYY-MM-DD";
    AllowDateFormat["YearMonth"] = "YYYY-MM";
    AllowDateFormat["ShortDate"] = "MM/DD/YYYY";
    AllowDateFormat["LongDate"] = "MMM DD YYYY";
    AllowDateFormat["StringDate"] = "YYYY-MM-DDTHH:mm:ss.sssZ";
    AllowDateFormat["DateTime"] = "YYYY-MM-DD HH:mm:ss";
    AllowDateFormat["TimeWithOutSeconds"] = "LT";
    AllowDateFormat["TimeWithSeconds"] = "LTS";
    AllowDateFormat["DD"] = "DD";
    AllowDateFormat["MM"] = "MM";
    AllowDateFormat["YYYY"] = "YYYY";
    AllowDateFormat["YearWeek"] = "GGGG-WW";
})(AllowDateFormat = exports.AllowDateFormat || (exports.AllowDateFormat = {}));
function Now() {
    return moment().toDate();
}
exports.Now = Now;
function IsSameOrAfter(firstDate, secondDate) {
    return moment(firstDate).isSameOrAfter(secondDate);
}
exports.IsSameOrAfter = IsSameOrAfter;
function IsBefore(firstDate, secondDate) {
    return moment(firstDate).isBefore(secondDate);
}
exports.IsBefore = IsBefore;
function ConvertToSpecificFormat(date, format) {
    return moment(date).format(format);
}
exports.ConvertToSpecificFormat = ConvertToSpecificFormat;
function SubtractDays(date, days) {
    return moment(date).subtract(days, 'days').toDate();
}
exports.SubtractDays = SubtractDays;
function AddDays(date, days) {
    return moment(date).add(days, 'days').toDate();
}
exports.AddDays = AddDays;
function AddMonths(date, month) {
    return moment(date).add(month, 'month').toDate();
}
exports.AddMonths = AddMonths;
function SubtractMonths(date, month) {
    return moment(date).subtract(month, 'month').toDate();
}
exports.SubtractMonths = SubtractMonths;
function SubtractHours(date, hour) {
    return moment(date).subtract(24, 'hour').toDate();
}
exports.SubtractHours = SubtractHours;
function DiffBetweenTwoDates(firstDate, secondDate) {
    return moment(secondDate).diff(moment(firstDate), 'days');
}
exports.DiffBetweenTwoDates = DiffBetweenTwoDates;
function StartDateOfMonth(date) {
    return moment(date).startOf('month').toDate();
}
exports.StartDateOfMonth = StartDateOfMonth;
function EndDateOfMonth(date) {
    return moment(date).endOf('month').toDate();
}
exports.EndDateOfMonth = EndDateOfMonth;
function StartOfDay(date) {
    return moment(date).startOf('day').toDate();
}
exports.StartOfDay = StartOfDay;
function EndOfDay(date) {
    return moment(date).endOf('day').toDate();
}
exports.EndOfDay = EndOfDay;
function GetDate(date) {
    return moment(date).date();
}
exports.GetDate = GetDate;
function GetMonth(date) {
    return moment(date).month();
}
exports.GetMonth = GetMonth;
function GetYear(date) {
    return moment(date).year();
}
exports.GetYear = GetYear;
function GetMomentFromString(dateString, format, tz = 'UTC') {
    return moment.tz(dateString, format, tz);
}
exports.GetMomentFromString = GetMomentFromString;
function GetMomentFromTimezone(date, tz) {
    return moment.tz(date, tz);
}
exports.GetMomentFromTimezone = GetMomentFromTimezone;
function GetTimezoneDate(date, tz) {
    return moment.tz(date, tz).toDate();
}
exports.GetTimezoneDate = GetTimezoneDate;
function GetLastDayOfYear(year) {
    return moment(year, 'YYYY').endOf('year').toDate();
}
exports.GetLastDayOfYear = GetLastDayOfYear;
function IsFuture(date) {
    return moment(date).isAfter(moment());
}
exports.IsFuture = IsFuture;
function getSeries(startDate, endDate, type) {
    if (startDate.isSameOrAfter(endDate)) {
        throw new Error('Invalid date range supplied');
    }
    let series = [];
    let format = null;
    switch (type) {
        case 'year':
            format = AllowDateFormat.YYYY;
            break;
        case 'month':
            format = AllowDateFormat.YYYY + '-' + AllowDateFormat.MM;
            break;
        case 'week':
            format = 'gggg-ww';
            break;
        default:
            type = 'day';
            format =
                AllowDateFormat.YYYY +
                    '-' +
                    AllowDateFormat.MM +
                    '-' +
                    AllowDateFormat.DD;
    }
    while (true) {
        series.push(startDate.format(format));
        startDate = startDate.add(1, type);
        if (startDate.isAfter(endDate)) {
            break;
        }
    }
    return series;
}
exports.getSeries = getSeries;
function getYearKeys(year) {
    let startDate = moment(year, AllowDateFormat.YYYY).startOf('year');
    let endDate = startDate.clone().endOf('year');
    let keys = getSeries(startDate, endDate, 'month');
    return keys;
}
exports.getYearKeys = getYearKeys;
function generateLastXMonthKeys(today, Months) {
    let endDate = today;
    let startDate = today.clone().subtract(Months - 1, 'months');
    let keys = getSeries(startDate, endDate, 'month');
    return keys;
}
exports.generateLastXMonthKeys = generateLastXMonthKeys;
function generateDayIntervalKeys(endDate, interval) {
    let startDate = endDate.clone().subtract(interval - 1, 'days');
    let keys = getSeries(startDate, endDate, 'day');
    return keys;
}
exports.generateDayIntervalKeys = generateDayIntervalKeys;
function getPreviousQuarterHourRange(dateTime = moment()) {
    if (typeof dateTime === 'string') {
        dateTime = moment(dateTime, AllowDateFormat.DateTime);
    }
    let startTime = dateTime.clone().subtract(15, 'minutes');
    startTime.minutes(Math.floor(startTime.minutes() / 15) * 15);
    startTime.seconds(0);
    let endTime = startTime.clone();
    endTime.add(15, 'minutes').subtract(1, 'second');
    return {
        start: startTime,
        end: endTime,
    };
}
exports.getPreviousQuarterHourRange = getPreviousQuarterHourRange;
function getAvailableTimezones() {
    return moment.tz.names().filter((tz) => {
        let tzl = tz.toLowerCase();
        return tzl.indexOf('etc/') === -1 && tzl.indexOf('/') !== -1;
    });
}
exports.getAvailableTimezones = getAvailableTimezones;
function getDayChangedTimezones(dateTime) {
    let tzs = [];
    return getAvailableTimezones().filter((tz) => {
        let dt = dateTime.clone();
        dt.tz(tz).set('seconds', 0);
        return dt.format('HH:mm:ss') == '00:00:00';
    });
}
exports.getDayChangedTimezones = getDayChangedTimezones;
function getWeekChangedTimezones(dateTime) {
    let tzs = [];
    return getDayChangedTimezones(dateTime).filter((tz) => {
        let dt = dateTime.clone();
        dt.tz(tz).set('seconds', 0);
        let dt2 = dt.clone();
        dt2.subtract('second', 1);
        return (dt.format(AllowDateFormat.YearWeek) !=
            dt2.format(AllowDateFormat.YearWeek));
    });
}
exports.getWeekChangedTimezones = getWeekChangedTimezones;
function getMonthChangedTimezones(dateTime) {
    let tzs = [];
    return getDayChangedTimezones(dateTime).filter((tz) => {
        let dt = dateTime.clone();
        dt.tz(tz).set('seconds', 0);
        let dt2 = dt.clone();
        dt2.subtract('seconds', 1);
        return (dt.format(AllowDateFormat.YearMonth) !=
            dt2.format(AllowDateFormat.YearMonth));
    });
}
exports.getMonthChangedTimezones = getMonthChangedTimezones;
//# sourceMappingURL=date.helper.js.map