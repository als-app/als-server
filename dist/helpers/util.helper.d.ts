export declare function HashPassword(plainText: string): Promise<any>;
export declare function ComparePassword(plainText: any, hash: any): Promise<any>;
export interface PaginationRequestParams {
    Limit: number;
    Page?: number;
    Before?: number;
    After?: number;
}
export interface PaginationDBParams {
    Limit: number;
    Offset: number | string;
}
export interface OrderByRequestParams {
    Column: string;
    Direction: 'ASC' | 'DESC';
}
export declare function GetPaginationOptions(params: PaginationRequestParams): PaginationDBParams;
export declare function GetOrderByOptions(params: OrderByRequestParams): OrderByRequestParams;
export declare function ObjectIdToHexString(value: any, obj: any): string;
export declare function ConvertToBase(sourceString: string, sourceBase: any, destBase: any): any;
export declare function SplitName(name: any): {
    FirstName: any;
    LastName: any;
};
export declare function GetEnumKeyByEnumValue(myEnum: any, enumValue: any): string;
export declare function traverseObjectWithSearchKey(objOrArray: object | Array<object>, searchKeys: Array<String>, modifier: (val: object | Array<object> | string | number) => {}): Promise<void>;
export declare function SortObjectByKeys(data: any): {};
export declare function IsAndroid(userAgent: string): boolean;
export declare function ConvertVersionStringToFloatNumber(versionString: any): number;
export declare function ReplaceObjectValuesInString(text: string, obj?: {}): string;
export declare function GenerateVerificationCode(): string;
