"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SSMailer = void 0;
const nodemailer_1 = require("nodemailer");
const env_helper_1 = require("./env.helper");
const aws = require("aws-sdk");
const file_config_1 = require("../config/file.config");
const logger_helper_1 = require("./logger.helper");
const template_renderer_1 = require("../templates/template_renderer");
aws.config.update(file_config_1.AWSConfig);
class SSMailer {
    constructor() {
        this._transporter = null;
        let credentials;
        if (env_helper_1.appEnv('USE_SMTP_CREDENTIALS', false)) {
            credentials = {
                host: env_helper_1.appEnv('MAIL_HOST', 'smtp.mailgun.org'),
                port: env_helper_1.appEnv('MAIL_PORT', 25),
                auth: {
                    user: env_helper_1.appEnv('MAIL_USER', null),
                    pass: env_helper_1.appEnv('MAIL_PASS', null),
                },
                service: 'smtp',
                secure: env_helper_1.appEnv('MAIL_SECURE', true),
                pool: true,
                from: env_helper_1.appEnv('MAIL_FROM_EMAIL', 'no-reply@abc.com'),
            };
        }
        else {
            credentials = {
                SES: new aws.SES({
                    apiVersion: '2010-12-01',
                }),
            };
        }
        this._transporter = nodemailer_1.createTransport(credentials);
    }
    static GetInstance() {
        if (this._instance === null) {
            this._instance = new this();
        }
        return this._instance;
    }
    static async Send(to, subject, content, attachments) {
        try {
            let message = {
                to: to,
                subject: subject,
                html: content,
            };
            if (!env_helper_1.appEnv('USER_SMTP_CREDENTIALS', false)) {
                message['from'] = env_helper_1.appEnv('MAIL_FROM_EMAIL', 'EBAPP84@GMAIL.COM');
            }
            if (attachments && Array.isArray(attachments) && attachments.length > 0) {
                message['attachments'] = attachments;
            }
            let result = await this.GetInstance()._transporter.sendMail(message);
            logger_helper_1.Logger.Info('Mail Sent successfully', 'MAILER');
            return true;
        }
        catch (e) {
            logger_helper_1.Logger.Error(e, 'MAILER');
        }
        return false;
    }
    static async SendByTemplate(template, to, locale, data) {
        let { content, subject } = template_renderer_1.TemplateRenderer[template](data, locale);
        this.Send(to, subject, content);
    }
}
exports.SSMailer = SSMailer;
SSMailer._instance = null;
//# sourceMappingURL=mail.helper.js.map