export declare const S3FileUploadOptions: any;
export declare const DeleteFile: (fileName: string) => Promise<unknown>;
export declare const GetFile: (fileName: string) => Promise<unknown>;
export declare const GetSignedUrl: (fileName: string, expirationTime: number) => Promise<unknown>;
export declare const UploadFile: (fileName: string, content: any) => Promise<unknown>;
export declare const GetObjectHeaders: (key: string) => Promise<{
    ContentType: string;
    ContentLength: number;
}>;
