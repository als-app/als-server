"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileUploadOptions = exports.GetTemporaryCredentialsForMediaUpload = exports.CreateFilePath = exports.CreateUniqueFileName = exports.GetMediaExtension = exports.GetMediaType = exports.IsMediaExtensionAllowed = exports.IsMediaSizeAllowed = exports.MediaMaxSize = void 0;
const multer = require("multer");
const media_entity_1 = require("../modules/media/media.entity");
const uuid_1 = require("uuid");
const aws = require("aws-sdk");
const env_helper_1 = require("./env.helper");
const file_config_1 = require("../config/file.config");
exports.MediaMaxSize = {
    [media_entity_1.MediaModelType.Image]: 6 * 1024 * 1024,
    [media_entity_1.MediaModelType.Video]: 25 * 1024 * 1024
};
const AllowedMediaExtensions = [
    "avi",
    "mp4",
    "mov",
    "flv",
    "gif",
    "mp3",
    "wav",
    "jpg",
    "jpeg",
    "png"
];
const IsMediaSizeAllowed = (size, type) => {
    return size <= exports.MediaMaxSize[type];
};
exports.IsMediaSizeAllowed = IsMediaSizeAllowed;
const IsMediaExtensionAllowed = (extension) => {
    return AllowedMediaExtensions.includes(extension);
};
exports.IsMediaExtensionAllowed = IsMediaExtensionAllowed;
const GetMediaType = (mime) => {
    let type = media_entity_1.MediaModelType.Image;
    if (mime.includes("video")) {
        type = media_entity_1.MediaModelType.Video;
    }
    if (mime.includes("audio")) {
        type = media_entity_1.MediaModelType.Audio;
    }
    return type;
};
exports.GetMediaType = GetMediaType;
const GetMediaExtension = (fileName) => {
    return fileName.slice(((fileName.lastIndexOf(".") - 1) >>> 0) + 2).toLowerCase();
};
exports.GetMediaExtension = GetMediaExtension;
const CreateUniqueFileName = (extension) => {
    return uuid_1.v4() + "." + extension;
};
exports.CreateUniqueFileName = CreateUniqueFileName;
const CreateFilePath = (fileName, prependPath) => {
    let date = new Date();
    let name = (prependPath ? prependPath + "/" : "")
        + date.getFullYear() + "/"
        + (date.getMonth() + 1) + "/"
        + date.getDate() + "/"
        + date.getTime() + date.getMilliseconds() + "-"
        + fileName.toLowerCase().replace(/ /g, "-").replace(/[^\w-.]+/g, "");
    return name;
};
exports.CreateFilePath = CreateFilePath;
const GetTemporaryCredentialsForMediaUpload = async (path, mediaId) => {
    aws.config.update(file_config_1.AWSConfig);
    let params = {
        RoleArn: env_helper_1.appEnv("AWS_STS_ROLE_ARN", ""),
        RoleSessionName: "mediaupload-" + mediaId,
        DurationSeconds: 60 * 60,
        Policy: JSON.stringify({
            Version: "2012-10-17",
            Statement: [
                {
                    Sid: "VisualEditor0",
                    Effect: "Allow",
                    Action: [
                        "s3:PutObject",
                        "s3:AbortMultipartUpload",
                        "s3:PutObjectAcl",
                        "s3:GetObject",
                        "s3:ListMultipartUploadParts"
                    ],
                    Resource: "arn:aws:s3:::" + env_helper_1.appEnv("AWS_BUCKET_NAME", "") + "/" + path
                }
            ]
        })
    };
    let { Credentials } = await new aws.STS().assumeRole(params).promise();
    return {
        AccessKeyId: Credentials.AccessKeyId,
        SecretAccessKey: Credentials.SecretAccessKey,
        SessionToken: Credentials.SessionToken,
        Path: path
    };
};
exports.GetTemporaryCredentialsForMediaUpload = GetTemporaryCredentialsForMediaUpload;
exports.FileUploadOptions = {
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, "./public/attachments/");
        },
        filename: (req, file, cb) => {
            file.uniqueName =
                Math.random()
                    .toString()
                    .split(".")[1] +
                    Date.now() +
                    file.originalname.substr(file.originalname.length - 30 >= 0 ? file.originalname.length - 30 : 0);
            cb(null, file.uniqueName, file);
        }
    })
};
//# sourceMappingURL=media.helper.js.map