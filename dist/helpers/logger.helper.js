"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
const Chalk = require("chalk");
const moment = require("moment-timezone");
const Morgan = require("morgan");
const logging_1 = require("../constant/logging");
const date_helper_1 = require("./date.helper");
const env_helper_1 = require("./env.helper");
const authentication_1 = require("../constant/authentication");
class Logger {
    static log(logLevel, data, tag) {
        if (env_helper_1.appEnv("LOG_LEVEL", logging_1.LogLevel.DEBUG) > logLevel) {
            return;
        }
        if (typeof data === "object") {
            let str = JSON.stringify(data, null, 4);
            if (str != "{}") {
                data = str;
            }
        }
        let date = moment();
        if (tag !== undefined) {
            console.log(`[${Chalk.yellowBright(env_helper_1.appEnv("APP_NAME", "Nest"))}][${Chalk.yellowBright(date.format(date_helper_1.AllowDateFormat.DateTime))}]  ` + Chalk.bold.underline.white(tag), this.logTemplate[logLevel](data));
        }
        else {
            console.log(`[${Chalk.yellowBright(env_helper_1.appEnv("APP_NAME", "Nest"))}][${Chalk.yellowBright(date.format(date_helper_1.AllowDateFormat.DateTime))}]  ` + this.logTemplate[logLevel](data));
        }
    }
    static Trace(data, tag) {
        this.log(logging_1.LogLevel.TRACE, data, tag);
    }
    static Debug(data, tag) {
        this.log(logging_1.LogLevel.DEBUG, data, tag);
    }
    static Info(data, tag) {
        this.log(logging_1.LogLevel.INFO, data, tag);
    }
    static Warn(data, tag) {
        this.log(logging_1.LogLevel.WARN, data, tag);
    }
    static Error(data, tag) {
        this.log(logging_1.LogLevel.ERROR, data, tag);
    }
    static Fatal(data, tag) {
        this.log(logging_1.LogLevel.FATAL, data, tag);
    }
    static GetQueryLogger() {
        if (Logger.queryLogger === null) {
            let LogSqlQuery = (query, param) => {
                this.Trace(query, "Query");
                if (param) {
                    this.Trace(param, "Parameters");
                }
            };
            Logger.queryLogger = {
                log: (level, msg, queryRunner) => {
                    switch (level) {
                        case "info":
                            this.Info(msg, "TypeORM");
                            break;
                        case "log":
                            this.Debug(msg, "TypeORM");
                            break;
                        case "warn":
                            this.Warn(msg, "TypeORM");
                    }
                },
                logMigration: (msg, queryRunner) => {
                    this.Info(msg, "TypeORM");
                },
                logQuery: (query, param) => {
                    LogSqlQuery(query, param);
                },
                logQueryError: (err, query, param) => {
                    this.Error(err, "Query");
                    LogSqlQuery(query, param);
                },
                logQuerySlow: (time, query, param) => {
                    this.Warn(time, "Slow Query");
                    LogSqlQuery(query, param);
                },
                logSchemaBuild: (msg) => {
                    this.Info(msg, "TypeORM");
                }
            };
            return Logger.queryLogger;
        }
    }
    static GetLoggerMiddleware() {
        if (Logger.loggerMiddleware === null) {
            let LoggerFormatStr = ":date[iso] :method :status :response-time ms :res[content-length] HTTP/:http-version :remote-addr :url :referrer :user-agent";
            if (env_helper_1.appEnv("DEBUG", false)) {
                Morgan.token("authorization", (req, res) => {
                    return req.headers[authentication_1.AUTHORIZATION_HEADER_KEY];
                });
                Morgan.token("body", (req, res) => {
                    return req.body ? JSON.stringify(req.body, null, 4) : "";
                });
                Morgan.token("query", (req, res) => {
                    return req.query
                        ? JSON.stringify(req.query, null, 4)
                        : "";
                });
                Morgan.token("params", (req, res) => {
                    return req.params
                        ? JSON.stringify(req.params, null, 4)
                        : "";
                });
                Morgan.token("responsebody", (req, res) => {
                    let str = "";
                    if (res.__ss_body) {
                        try {
                            str =
                                JSON.stringify(res.__ss_body, null, 4);
                        }
                        catch (e) {
                            str = res.__ss_body;
                        }
                    }
                    return str;
                });
                LoggerFormatStr =
                    ":date[iso] :method :status :response-time ms :res[content-length] HTTP/:http-version :remote-addr :url :referrer :user-agent\nPath Params :params\nQuery Params :query\nRequest Body :body\nResponse Body :responsebody";
            }
            Logger.loggerMiddleware = Morgan(LoggerFormatStr, {
                stream: {
                    write: str => {
                        this.Info(str);
                    }
                }
            });
        }
        return Logger.loggerMiddleware;
    }
}
exports.Logger = Logger;
Logger.logTemplate = {
    [logging_1.LogLevel.TRACE]: Chalk.greenBright,
    [logging_1.LogLevel.DEBUG]: Chalk.whiteBright,
    [logging_1.LogLevel.INFO]: Chalk.blueBright,
    [logging_1.LogLevel.WARN]: Chalk.magenta,
    [logging_1.LogLevel.ERROR]: Chalk.redBright,
    [logging_1.LogLevel.FATAL]: Chalk.bgRed
};
Logger.queryLogger = null;
Logger.loggerMiddleware = null;
//# sourceMappingURL=logger.helper.js.map