"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FatalErrorExceptionResponse = exports.UnauthorizedExceptionResponse = exports.ForbiddenExceptionResponse = exports.NotFoundExceptionResponse = exports.BadRequestExceptionResponse = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
class BadRequestExceptionResponse {
}
__decorate([
    swagger_1.ApiProperty({ default: "Bad Request. Check Input" }),
    __metadata("design:type", String)
], BadRequestExceptionResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty({ default: common_1.HttpStatus.BAD_REQUEST }),
    __metadata("design:type", Number)
], BadRequestExceptionResponse.prototype, "Status", void 0);
__decorate([
    swagger_1.ApiProperty({ nullable: true, default: null }),
    __metadata("design:type", Object)
], BadRequestExceptionResponse.prototype, "Data", void 0);
exports.BadRequestExceptionResponse = BadRequestExceptionResponse;
class NotFoundExceptionResponse {
}
__decorate([
    swagger_1.ApiProperty({ default: "Not Found" }),
    __metadata("design:type", String)
], NotFoundExceptionResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty({ default: common_1.HttpStatus.NOT_FOUND }),
    __metadata("design:type", Number)
], NotFoundExceptionResponse.prototype, "Status", void 0);
__decorate([
    swagger_1.ApiProperty({ nullable: true, default: null }),
    __metadata("design:type", Object)
], NotFoundExceptionResponse.prototype, "Data", void 0);
exports.NotFoundExceptionResponse = NotFoundExceptionResponse;
class ForbiddenExceptionResponse {
}
__decorate([
    swagger_1.ApiProperty({ default: "Access Not Allowed" }),
    __metadata("design:type", String)
], ForbiddenExceptionResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty({ default: common_1.HttpStatus.FORBIDDEN }),
    __metadata("design:type", Number)
], ForbiddenExceptionResponse.prototype, "Status", void 0);
__decorate([
    swagger_1.ApiProperty({ nullable: true, default: null }),
    __metadata("design:type", Object)
], ForbiddenExceptionResponse.prototype, "Data", void 0);
exports.ForbiddenExceptionResponse = ForbiddenExceptionResponse;
class UnauthorizedExceptionResponse {
}
__decorate([
    swagger_1.ApiProperty({ default: "Not Authorized" }),
    __metadata("design:type", String)
], UnauthorizedExceptionResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty({ default: common_1.HttpStatus.UNAUTHORIZED }),
    __metadata("design:type", Number)
], UnauthorizedExceptionResponse.prototype, "Status", void 0);
__decorate([
    swagger_1.ApiProperty({ nullable: true, default: null }),
    __metadata("design:type", Object)
], UnauthorizedExceptionResponse.prototype, "Data", void 0);
exports.UnauthorizedExceptionResponse = UnauthorizedExceptionResponse;
class FatalErrorExceptionResponse {
}
__decorate([
    swagger_1.ApiProperty({ default: "Something Went Wrong" }),
    __metadata("design:type", String)
], FatalErrorExceptionResponse.prototype, "Message", void 0);
__decorate([
    swagger_1.ApiProperty({ default: common_1.HttpStatus.INTERNAL_SERVER_ERROR }),
    __metadata("design:type", Number)
], FatalErrorExceptionResponse.prototype, "Status", void 0);
__decorate([
    swagger_1.ApiProperty({ nullable: true, default: null }),
    __metadata("design:type", Object)
], FatalErrorExceptionResponse.prototype, "Data", void 0);
exports.FatalErrorExceptionResponse = FatalErrorExceptionResponse;
//# sourceMappingURL=response.schema.js.map