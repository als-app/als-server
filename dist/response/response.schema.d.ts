export declare class BadRequestExceptionResponse {
    Message: string;
    Status: number;
    Data: any;
}
export declare class NotFoundExceptionResponse {
    Message: string;
    Status: number;
    Data: any;
}
export declare class ForbiddenExceptionResponse {
    Message: string;
    Status: number;
    Data: any;
}
export declare class UnauthorizedExceptionResponse {
    Message: string;
    Status: number;
    Data: any;
}
export declare class FatalErrorExceptionResponse {
    Message: string;
    Status: number;
    Data: any;
}
