"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AWSConfig = void 0;
const env_helper_1 = require("../helpers/env.helper");
const dotenv = require("dotenv");
dotenv.config();
exports.AWSConfig = {
    accessKeyId: env_helper_1.appEnv('AWS_ACCESS_KEY', ''),
    secretAccessKey: env_helper_1.appEnv('AWS_SECRET_ACCESS_KEY', ''),
    bucketURL: env_helper_1.appEnv('AWS_BUCKET_NAME', ''),
    signatureVersion: 'v4',
    region: env_helper_1.appEnv("AWS_REGION", '')
};
//# sourceMappingURL=file.config.js.map