export declare const AWSConfig: {
    accessKeyId: any;
    secretAccessKey: any;
    bucketURL: any;
    signatureVersion: string;
    region: any;
};
