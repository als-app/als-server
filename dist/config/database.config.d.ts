export declare const Config: {
    type: string;
    host: any;
    port: any;
    username: any;
    password: any;
    database: any;
    entities: string[];
    synchronize: boolean;
    migrations: string[];
    logging: string;
    logger: import("typeorm").Logger;
    bigNumberStrings: boolean;
    legacySpatialSupport: boolean;
    extra: {
        connectTimeout: number;
    };
};
