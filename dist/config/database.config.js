"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Config = void 0;
const env_helper_1 = require("../helpers/env.helper");
const dotenv_1 = require("dotenv");
const logger_helper_1 = require("../helpers/logger.helper");
dotenv_1.config();
exports.Config = {
    type: "mysql",
    host: env_helper_1.appEnv("DB_MYSQL_HOST", "localhost"),
    port: env_helper_1.appEnv("DB_MYSQL_PORT", 3306),
    username: env_helper_1.appEnv("DB_MYSQL_USER", "root"),
    password: env_helper_1.appEnv("DB_MYSQL_PASS", ""),
    database: env_helper_1.appEnv("DB_MYSQL_DB_NAME", ""),
    entities: ['dist/**/*.entity.{js,ts}'],
    synchronize: false,
    migrations: ['dist/migrations/*.js'],
    logging: "all",
    logger: logger_helper_1.Logger.GetQueryLogger(),
    bigNumberStrings: false,
    legacySpatialSupport: false,
    extra: {
        connectTimeout: 600000
    }
};
//# sourceMappingURL=database.config.js.map