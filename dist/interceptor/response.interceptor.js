"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseInterceptor = void 0;
const common_1 = require("@nestjs/common");
const operators_1 = require("rxjs/operators");
const response_1 = require("../constant/response");
let ResponseInterceptor = class ResponseInterceptor {
    intercept(context, next) {
        let ctx = context.switchToHttp();
        let response = ctx.getResponse();
        let status = response.statusCode;
        if (status >= common_1.HttpStatus.OK && status < common_1.HttpStatus.BAD_REQUEST) {
            let message = response_1.DefaultResponseMessages[common_1.HttpStatus.OK];
            return next.handle().pipe(operators_1.map((data) => {
                response.__ss_body = {
                    Status: common_1.HttpStatus.OK,
                    Message: message,
                    Data: data
                };
                return {
                    Status: common_1.HttpStatus.OK,
                    Message: message,
                    Data: data,
                };
            }));
        }
        else {
            return next.handle();
        }
    }
};
ResponseInterceptor = __decorate([
    common_1.Injectable()
], ResponseInterceptor);
exports.ResponseInterceptor = ResponseInterceptor;
//# sourceMappingURL=response.interceptor.js.map