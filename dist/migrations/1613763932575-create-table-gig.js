"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTableGig1613763932575 = void 0;
class createTableGig1613763932575 {
    async up(queryRunner) {
        await queryRunner.query(`
            CREATE TABLE gig
            (
                id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
                category_id     BIGINT,
                advisor_id     BIGINT,
                title   varchar(1000),
                description   text,
                price       decimal(15,2),
                information1     text,
                information2     text,
                created_at  BIGINT   NOT NULL,
                updated_at  BIGINT   NOT NULL,
                deleted_at  BIGINT DEFAULT 0 NOT NULL
            );
            `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        DROP TABLE gig;
        `);
    }
}
exports.createTableGig1613763932575 = createTableGig1613763932575;
//# sourceMappingURL=1613763932575-create-table-gig.js.map