"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddStatusInInvitations1639357751704 = void 0;
class AddStatusInInvitations1639357751704 {
    constructor() {
        this.name = 'AddStatusInInvitations1639357751704';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`status\` enum ('pending', 'accepted', 'rejected') NOT NULL DEFAULT 'pending'`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`status\``);
    }
}
exports.AddStatusInInvitations1639357751704 = AddStatusInInvitations1639357751704;
//# sourceMappingURL=1639357751704-AddStatusInInvitations.js.map