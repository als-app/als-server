"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createChatTable1608737454165 = void 0;
class createChatTable1608737454165 {
    async up(queryRunner) {
        await queryRunner.query(` CREATE TABLE chat
        (
            id                          BIGINT PRIMARY KEY          NOT NULL AUTO_INCREMENT,
            advisor_user_id              BIGINT                      NOT NULL,
            gig_id              BIGINT                      NOT NULL,
            user_id                     BIGINT                      NOT NULL,
            user_last_read_event_id     BIGINT                      ,
            advisor_last_read_event_id   BIGINT                      ,
            last_event_id               BIGINT                      ,
            created_at                  BIGINT                      NOT NULL,
            updated_at                  BIGINT                      NOT NULL,
            deleted_at                  BIGINT DEFAULT 0            NOT NULL
        );`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE chat;`);
    }
}
exports.createChatTable1608737454165 = createChatTable1608737454165;
//# sourceMappingURL=1608737454165-create-chat-table.js.map