import { MigrationInterface, QueryRunner } from "typeorm";
export declare class SeenAddedInChat1642613743825 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
