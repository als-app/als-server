"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUserTokenTable1607336712763 = void 0;
class createUserTokenTable1607336712763 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE user_token
        (
            id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
            user_id  BIGINT(20)                              NOT NULL,
            device_id  BIGINT(20)                              DEFAULT NULL,
            uuid   varchar(255) DEFAULT NULL,
            code       varchar(255) DEFAULT NULL,
            meta JSON DEFAULT NULL,
            attempts BIGINT  DEFAULT 0,
            expire_time BIGINT DEFAULT NULL,
            created_at  BIGINT   NOT NULL,
            updated_at  BIGINT   NOT NULL,
            deleted_at  BIGINT DEFAULT 0 NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
    DROP TABLE user_token;
    `);
    }
}
exports.createUserTokenTable1607336712763 = createUserTokenTable1607336712763;
//# sourceMappingURL=1607336712763-create-user-token-table.js.map