"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUserDeviceTable1607336706219 = void 0;
class createUserDeviceTable1607336706219 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE user_device
        (
            id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
            user_id  BIGINT(20)                              NOT NULL,
            user_agent  varchar(1000),
            uuid   varchar(255) DEFAULT NULL,
            auth_token       varchar(255) DEFAULT NULL,
            fcm       varchar(1000) DEFAULT NULL,
            created_at  BIGINT   NOT NULL,
            updated_at  BIGINT   NOT NULL,
            deleted_at  BIGINT DEFAULT 0 NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
    DROP TABLE user_device;
    `);
    }
}
exports.createUserDeviceTable1607336706219 = createUserDeviceTable1607336706219;
//# sourceMappingURL=1607336706219-create-user-device-table.js.map