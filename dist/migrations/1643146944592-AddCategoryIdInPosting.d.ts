import { MigrationInterface, QueryRunner } from "typeorm";
export declare class AddCategoryIdInPosting1643146944592 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
