"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddSelfieSupport1642023136887 = void 0;
class AddSelfieSupport1642023136887 {
    constructor() {
        this.name = 'AddSelfieSupport1642023136887';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`selfie_media_id\` bigint NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`selfie_media_id\``);
    }
}
exports.AddSelfieSupport1642023136887 = AddSelfieSupport1642023136887;
//# sourceMappingURL=1642023136887-AddSelfieSupport.js.map