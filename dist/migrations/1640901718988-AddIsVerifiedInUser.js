"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddIsVerifiedInUser1640901718988 = void 0;
class AddIsVerifiedInUser1640901718988 {
    constructor() {
        this.name = 'AddIsVerifiedInUser1640901718988';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`is_verified\` tinyint NOT NULL DEFAULT 0`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`is_verified\``);
    }
}
exports.AddIsVerifiedInUser1640901718988 = AddIsVerifiedInUser1640901718988;
//# sourceMappingURL=1640901718988-AddIsVerifiedInUser.js.map