"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createQuestionTable1614977781430 = void 0;
class createQuestionTable1614977781430 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE question
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            country               VARCHAR(255)            NOT NULL,
            birthdate               VARCHAR(255)            NOT NULL,
            description         TEXT,
            useR_id            BIGINT,
            gender              TINYINT(4) DEFAULT 0    NOT NULL,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE question;`);
    }
}
exports.createQuestionTable1614977781430 = createQuestionTable1614977781430;
//# sourceMappingURL=1614977781430-create-question-table.js.map