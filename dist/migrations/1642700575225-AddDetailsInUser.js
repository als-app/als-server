"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddDetailsInUser1642700575225 = void 0;
class AddDetailsInUser1642700575225 {
    constructor() {
        this.name = 'AddDetailsInUser1642700575225';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`details\` varchar(255) NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`details\``);
    }
}
exports.AddDetailsInUser1642700575225 = AddDetailsInUser1642700575225;
//# sourceMappingURL=1642700575225-AddDetailsInUser.js.map