"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddLocationToUser1640438531497 = void 0;
class AddLocationToUser1640438531497 {
    constructor() {
        this.name = 'AddLocationToUser1640438531497';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`location\` point NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`location\``);
    }
}
exports.AddLocationToUser1640438531497 = AddLocationToUser1640438531497;
//# sourceMappingURL=1640438531497-AddLocationToUser.js.map