"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMediaTable1608012372681 = void 0;
class createMediaTable1608012372681 {
    async up(queryRunner) {
        await queryRunner.query(` CREATE TABLE media
        (
            id              BIGINT PRIMARY KEY              NOT NULL AUTO_INCREMENT,
            path            VARCHAR(500)                    NOT NULL ,
            thumb_path      VARCHAR(500)                    ,
            meta            JSON                            ,
            type            TINYINT                            NOT NULL,
            created_at      BIGINT                          NOT NULL,
            updated_at      BIGINT                          NOT NULL,
            deleted_at      BIGINT DEFAULT 0                NOT NULL
        );`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE media;`);
    }
}
exports.createMediaTable1608012372681 = createMediaTable1608012372681;
//# sourceMappingURL=1608012372681-create-media-table.js.map