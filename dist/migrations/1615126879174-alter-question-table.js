"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterQuestionTable1615126879174 = void 0;
class alterQuestionTable1615126879174 {
    async up(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE question ADD COLUMN chat_id bigint;
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        ALTER TABLE question drop column chat_id;
        `);
    }
}
exports.alterQuestionTable1615126879174 = alterQuestionTable1615126879174;
//# sourceMappingURL=1615126879174-alter-question-table.js.map