"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterMediaTable1608550886902 = void 0;
class alterMediaTable1608550886902 {
    async up(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE media MODIFY COLUMN user_id BIGINT;
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        ALTER TABLE media MODIFY COLUMN user_id BIGINT NOT NULL;
        `);
    }
}
exports.alterMediaTable1608550886902 = alterMediaTable1608550886902;
//# sourceMappingURL=1608550886902-alter-media-table.js.map