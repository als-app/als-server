"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterAdvisorTable1616440661253 = void 0;
class alterAdvisorTable1616440661253 {
    async up(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE advisor ADD COLUMN status tinyint(2) default 0;
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        ALTER TABLE advisor drop column status;
        `);
    }
}
exports.alterAdvisorTable1616440661253 = alterAdvisorTable1616440661253;
//# sourceMappingURL=1616440661253-alter-advisor-table.js.map