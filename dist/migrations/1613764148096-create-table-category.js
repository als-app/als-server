"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTableCategory1613764148096 = void 0;
class createTableCategory1613764148096 {
    async up(queryRunner) {
        await queryRunner.query(`
            CREATE TABLE category
            (
                id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
                media_id     BIGINT,
                title   varchar(1000),
                created_at  BIGINT   NOT NULL,
                updated_at  BIGINT   NOT NULL,
                deleted_at  BIGINT DEFAULT 0 NOT NULL
            );
            `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        DROP TABLE category;
        `);
    }
}
exports.createTableCategory1613764148096 = createTableCategory1613764148096;
//# sourceMappingURL=1613764148096-create-table-category.js.map