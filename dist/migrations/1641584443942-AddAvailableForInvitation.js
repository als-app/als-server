"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddAvailableForInvitation1641584443942 = void 0;
class AddAvailableForInvitation1641584443942 {
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`available_for_invitation\` tinyint NOT NULL DEFAULT 1`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`available_for_invitation\``);
    }
}
exports.AddAvailableForInvitation1641584443942 = AddAvailableForInvitation1641584443942;
//# sourceMappingURL=1641584443942-AddAvailableForInvitation.js.map