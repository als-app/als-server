"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createNotificationTable1610612770653 = void 0;
class createNotificationTable1610612770653 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE notification
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            title               VARCHAR(255)            NOT NULL,
            description         TEXT,
            user_id            BIGINT,
            type              TINYINT(4) DEFAULT 0    NOT NULL,
            read_status              TINYINT(4) DEFAULT 0    NOT NULL,
            meta JSON null,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE notification;`);
    }
}
exports.createNotificationTable1610612770653 = createNotificationTable1610612770653;
//# sourceMappingURL=1610612770653-create_notification_table.js.map