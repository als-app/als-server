"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterUserTable1607497549696 = void 0;
class alterUserTable1607497549696 {
    async up(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE user CHANGE status status tinyint(2) DEFAULT 0 after type;
            `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        ALTER TABLE user CHANGE status status tinyint;
        `);
    }
}
exports.alterUserTable1607497549696 = alterUserTable1607497549696;
//# sourceMappingURL=1607497549696-alter-user-table.js.map