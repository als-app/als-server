import { MigrationInterface, QueryRunner } from "typeorm";
export declare class AddRatingInReviews1640522542582 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
