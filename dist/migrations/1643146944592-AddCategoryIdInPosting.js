"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddCategoryIdInPosting1643146944592 = void 0;
class AddCategoryIdInPosting1643146944592 {
    constructor() {
        this.name = 'AddCategoryIdInPosting1643146944592';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`posting\` ADD \`category_id\` int NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`posting\` DROP COLUMN \`category_id\``);
    }
}
exports.AddCategoryIdInPosting1643146944592 = AddCategoryIdInPosting1643146944592;
//# sourceMappingURL=1643146944592-AddCategoryIdInPosting.js.map