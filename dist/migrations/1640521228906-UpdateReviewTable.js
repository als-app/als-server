"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateReviewTable1640521228906 = void 0;
class UpdateReviewTable1640521228906 {
    constructor() {
        this.name = 'UpdateReviewTable1640521228906';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_8b65f758fd563971cf97d9d4bad\``);
        await queryRunner.query(`ALTER TABLE \`reviews\` CHANGE \`invitation_id\` \`invitation_id\` bigint NULL`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_8b65f758fd563971cf97d9d4bad\` FOREIGN KEY (\`invitation_id\`) REFERENCES \`invitation\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_8b65f758fd563971cf97d9d4bad\``);
        await queryRunner.query(`ALTER TABLE \`reviews\` CHANGE \`invitation_id\` \`invitation_id\` bigint NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_8b65f758fd563971cf97d9d4bad\` FOREIGN KEY (\`invitation_id\`) REFERENCES \`invitation\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
}
exports.UpdateReviewTable1640521228906 = UpdateReviewTable1640521228906;
//# sourceMappingURL=1640521228906-UpdateReviewTable.js.map