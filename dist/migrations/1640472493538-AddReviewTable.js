"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddReviewTable1640472493538 = void 0;
class AddReviewTable1640472493538 {
    constructor() {
        this.name = 'AddReviewTable1640472493538';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE \`reviews\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`created_at\` bigint NOT NULL, \`updated_at\` bigint NOT NULL, \`deleted_at\` bigint NOT NULL, \`description\` varchar(255) NOT NULL, \`invitation_id\` bigint NOT NULL, \`created_by_id\` bigint NULL, \`user_id\` bigint NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_8b65f758fd563971cf97d9d4bad\` FOREIGN KEY (\`invitation_id\`) REFERENCES \`invitation\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_39a4bb3e746e2ef1a70cf96537f\` FOREIGN KEY (\`created_by_id\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD CONSTRAINT \`FK_728447781a30bc3fcfe5c2f1cdf\` FOREIGN KEY (\`user_id\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_728447781a30bc3fcfe5c2f1cdf\``);
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_39a4bb3e746e2ef1a70cf96537f\``);
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP FOREIGN KEY \`FK_8b65f758fd563971cf97d9d4bad\``);
        await queryRunner.query(`DROP TABLE \`reviews\``);
    }
}
exports.AddReviewTable1640472493538 = AddReviewTable1640472493538;
//# sourceMappingURL=1640472493538-AddReviewTable.js.map