"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUserTable1606854833864 = void 0;
class createUserTable1606854833864 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE user
        (
            id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
            first_name  varchar(255),
            last_name   varchar(255),
            user_name   varchar(255),
            birth_date   varchar(255),
            phone_number   varchar(255),
            email       varchar(255),
            password    varchar(255),
            status      TINYINT,
            type        TINYINT,
            media_id   BIGINT(20) DEFAULT NULL,
            created_at  BIGINT   NOT NULL,
            updated_at  BIGINT   NOT NULL,
            deleted_at  BIGINT DEFAULT 0 NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        DROP TABLE user;
        `);
    }
}
exports.createUserTable1606854833864 = createUserTable1606854833864;
//# sourceMappingURL=1606854833864-create-user-table.js.map