"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTableReviews1614710356963 = void 0;
class createTableReviews1614710356963 {
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE reviews
        (
            id              BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            advisor_id         BIGINT  NOT NULL,
            user_id       BIGINT  NOT NULL,
            rating          TINYINT DEFAULT 5 NOT NULL,
            comment         TEXT,
            created_at      BIGINT   NOT NULL,
            updated_at      BIGINT   NOT NULL,
            deleted_at      BIGINT DEFAULT 0 NOT NULL
        )
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        DROP TABLE reviews;
        `);
    }
}
exports.createTableReviews1614710356963 = createTableReviews1614710356963;
//# sourceMappingURL=1614710356963-create-table-reviews.js.map