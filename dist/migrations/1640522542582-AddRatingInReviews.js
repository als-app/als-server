"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddRatingInReviews1640522542582 = void 0;
class AddRatingInReviews1640522542582 {
    constructor() {
        this.name = 'AddRatingInReviews1640522542582';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD \`rating\` int NOT NULL DEFAULT '0'`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP COLUMN \`rating\``);
    }
}
exports.AddRatingInReviews1640522542582 = AddRatingInReviews1640522542582;
//# sourceMappingURL=1640522542582-AddRatingInReviews.js.map