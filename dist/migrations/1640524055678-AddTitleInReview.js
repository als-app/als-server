"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddTitleInReview1640524055678 = void 0;
class AddTitleInReview1640524055678 {
    constructor() {
        this.name = 'AddTitleInReview1640524055678';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` ADD \`title\` varchar(255) NOT NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`reviews\` DROP COLUMN \`title\``);
    }
}
exports.AddTitleInReview1640524055678 = AddTitleInReview1640524055678;
//# sourceMappingURL=1640524055678-AddTitleInReview.js.map