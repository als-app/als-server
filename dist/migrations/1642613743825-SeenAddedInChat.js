"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeenAddedInChat1642613743825 = void 0;
class SeenAddedInChat1642613743825 {
    constructor() {
        this.name = 'SeenAddedInChat1642613743825';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`chat\` ADD \`seen\` tinyint NOT NULL DEFAULT 0`);
        await queryRunner.query(`update chat set seen=1`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`chat\` DROP COLUMN \`seen\``);
    }
}
exports.SeenAddedInChat1642613743825 = SeenAddedInChat1642613743825;
//# sourceMappingURL=1642613743825-SeenAddedInChat.js.map