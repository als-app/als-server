"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createPostingTable1615067944502 = void 0;
class createPostingTable1615067944502 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE posting
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            description         TEXT,
            media_id            BIGINT,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE posting;`);
    }
}
exports.createPostingTable1615067944502 = createPostingTable1615067944502;
//# sourceMappingURL=1615067944502-create-posting-table.js.map