import { MigrationInterface, QueryRunner } from "typeorm";
export declare class RemoveExtraInvitationFields1642712731815 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
