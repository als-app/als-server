"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemoveExtraInvitationFields1642712731815 = void 0;
class RemoveExtraInvitationFields1642712731815 {
    constructor() {
        this.name = 'RemoveExtraInvitationFields1642712731815';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`description\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`time\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`location\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`address\``);
        await queryRunner.query(`ALTER TABLE \`invitation\` DROP COLUMN \`status\``);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`status\` enum ('pending', 'accepted', 'rejected') NOT NULL DEFAULT 'pending'`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`address\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`location\` point NULL`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`time\` timestamp NULL`);
        await queryRunner.query(`ALTER TABLE \`invitation\` ADD \`description\` varchar(255) NOT NULL`);
    }
}
exports.RemoveExtraInvitationFields1642712731815 = RemoveExtraInvitationFields1642712731815;
//# sourceMappingURL=1642712731815-RemoveExtraInvitationFields.js.map