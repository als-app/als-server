"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddAboutInUser1638928049915 = void 0;
class AddAboutInUser1638928049915 {
    constructor() {
        this.name = 'AddAboutInUser1638928049915';
    }
    async up(queryRunner) {
        await queryRunner.query("ALTER TABLE `user` ADD `about` varchar(255) NULL");
    }
    async down(queryRunner) {
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `about`");
    }
}
exports.AddAboutInUser1638928049915 = AddAboutInUser1638928049915;
//# sourceMappingURL=1638928049915-AddAboutInUser.js.map