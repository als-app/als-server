"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterUserTableEditName1607336712763 = void 0;
class alterUserTableEditName1607336712763 {
    async up(queryRunner) {
        await queryRunner.query(`
        ALTER TABLE user CHANGE first_name full_name VARCHAR(255) NOT NULL, DROP COLUMN last_name;
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
    ALTER TABLE user CHANGE full_name first_name VARCHAR(255) NOT NULL, ADD COLUMN last_name VARCHAR(255);
    `);
    }
}
exports.alterUserTableEditName1607336712763 = alterUserTableEditName1607336712763;
//# sourceMappingURL=1607336712763-alter-user-table-edit-name.js.map