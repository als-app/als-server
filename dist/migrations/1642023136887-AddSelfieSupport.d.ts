import { MigrationInterface, QueryRunner } from "typeorm";
export declare class AddSelfieSupport1642023136887 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
