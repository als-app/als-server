"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterMediaTable1608118820647 = void 0;
class alterMediaTable1608118820647 {
    async up(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE media ADD base_path VARCHAR(255) NOT NULL, ADD status TINYINT(4) NOT NULL, ADD user_id BIGINT NOT NULL;
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE media DROP base_path, DROP status, DROP user_id;
        `);
    }
}
exports.alterMediaTable1608118820647 = alterMediaTable1608118820647;
//# sourceMappingURL=1608118820647-alter-media-table.js.map