"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTablePromotion1614710350824 = void 0;
class createTablePromotion1614710350824 {
    async up(queryRunner) {
        await queryRunner.query(`
        CREATE TABLE promotion
        (
            id                  BIGINT PRIMARY KEY      NOT NULL AUTO_INCREMENT,
            title               VARCHAR(255)            NOT NULL,
            description         TEXT,
            media_id            BIGINT,
            status              TINYINT(4) DEFAULT 0    NOT NULL,
            created_at          BIGINT                  NOT NULL,
            updated_at          BIGINT                  NOT NULL,
            deleted_at          BIGINT DEFAULT 0        NOT NULL
        );
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE promotion;`);
    }
}
exports.createTablePromotion1614710350824 = createTablePromotion1614710350824;
//# sourceMappingURL=1614710350824-create-table-promotion.js.map