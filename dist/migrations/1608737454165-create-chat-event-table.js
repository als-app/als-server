"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createChatEventTable1608737454165 = void 0;
class createChatEventTable1608737454165 {
    async up(queryRunner) {
        await queryRunner.query(` CREATE TABLE chat_event
        (
            id                          BIGINT PRIMARY KEY          NOT NULL AUTO_INCREMENT,
            chat_id                   BIGINT                      NOT NULL,
            sender_id                   BIGINT                      NOT NULL,
            sender_type                 TINYINT(4)                  NOT NULL,
            type                        TINYINT(4)                  NOT NULL,
            content                     TEXT                        ,
            media_id                    BIGINT                      ,
            created_at                  BIGINT                      NOT NULL,
            updated_at                  BIGINT                      NOT NULL,
            deleted_at                  BIGINT DEFAULT 0            NOT NULL
        );`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE chat_event;`);
    }
}
exports.createChatEventTable1608737454165 = createChatEventTable1608737454165;
//# sourceMappingURL=1608737454165-create-chat-event-table.js.map