import { MigrationInterface, QueryRunner } from "typeorm";
export declare class AddIsVerifiedInUser1640901718988 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
