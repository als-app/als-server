"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alterChatTable1614799219491 = void 0;
class alterChatTable1614799219491 {
    async up(queryRunner) {
        await queryRunner.query(`
            ALTER TABLE chat ADD COLUMN status tinyint(2) default 1;
        `);
    }
    async down(queryRunner) {
        await queryRunner.query(`
        ALTER TABLE chat drop column status;
        `);
    }
}
exports.alterChatTable1614799219491 = alterChatTable1614799219491;
//# sourceMappingURL=1614799219491-alter-chat-table.js.map