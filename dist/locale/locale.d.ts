export declare enum MessageTemplates {
    NotFoundError = "NotFoundError",
    UnauthorizedError = "UnauthorizedError",
    Success = "Success",
    BadRequestError = "BadRequestError",
    InternalServerError = "InternalServerError",
    UserNotFound = "UserNotFound",
    UserAlreadyExist = "UserAlreadyExist",
    InvalidVerificationCode = "InvalidVerificationCode",
    InvalidAccessToken = "InvalidAccessToken",
    EmailDoesNotMatchWithOAuth = "EmailDoesNotMatchWithOAuth",
    DeviceNotFound = "DeviceNotFound",
    UserDeviceNotFound = "UserDeviceNotFound",
    InvalidValueFor = "InvalidValueFor",
    InvalidCredentials = "InvalidCredentials",
    ForbiddenError = "ForbiddenError",
    EmailVerificationSubject = "EmailVerificationSubject",
    ResetPasswordSubject = "ResetPasswordSubject",
    Address = "Address",
    EmailFooter = "EmailFooter",
    MediaNotFound = "MediaNotFound",
    MediaNotAllowed = "MediaNotAllowed",
    MediaSizeTooLarge = "MediaSizeTooLarge",
    MediaServiceNotAvailable = "MediaServiceNotAvailable",
    AdvisorAlreadyCreated = "AdvisorAlreadyCreated",
    PublicQuestionAlreadyAnswered = "PublicQuestionAlreadyAnswered",
    NewMessageReceive = "NewMessageReceive",
    AttachmentShared = "AttachmentShared",
    InvalidPassword = "InvalidPassword",
    ProvideOldPassword = "ProvideOldPassword",
    WrongOldPassword = "WrongOldPassword"
}
export declare enum Locale {
    En = "En",
    Ar = "Ar"
}
export declare function __T(locale: Locale, key: MessageTemplates, data?: any): string;
export interface ILocale {
    values: {
        [key in keyof typeof MessageTemplates]: string;
    };
}
