"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.__T = exports.Locale = exports.MessageTemplates = void 0;
const en_locale_1 = require("./en.locale");
const util_helper_1 = require("../helpers/util.helper");
var MessageTemplates;
(function (MessageTemplates) {
    MessageTemplates["NotFoundError"] = "NotFoundError";
    MessageTemplates["UnauthorizedError"] = "UnauthorizedError";
    MessageTemplates["Success"] = "Success";
    MessageTemplates["BadRequestError"] = "BadRequestError";
    MessageTemplates["InternalServerError"] = "InternalServerError";
    MessageTemplates["UserNotFound"] = "UserNotFound";
    MessageTemplates["UserAlreadyExist"] = "UserAlreadyExist";
    MessageTemplates["InvalidVerificationCode"] = "InvalidVerificationCode";
    MessageTemplates["InvalidAccessToken"] = "InvalidAccessToken";
    MessageTemplates["EmailDoesNotMatchWithOAuth"] = "EmailDoesNotMatchWithOAuth";
    MessageTemplates["DeviceNotFound"] = "DeviceNotFound";
    MessageTemplates["UserDeviceNotFound"] = "UserDeviceNotFound";
    MessageTemplates["InvalidValueFor"] = "InvalidValueFor";
    MessageTemplates["InvalidCredentials"] = "InvalidCredentials";
    MessageTemplates["ForbiddenError"] = "ForbiddenError";
    MessageTemplates["EmailVerificationSubject"] = "EmailVerificationSubject";
    MessageTemplates["ResetPasswordSubject"] = "ResetPasswordSubject";
    MessageTemplates["Address"] = "Address";
    MessageTemplates["EmailFooter"] = "EmailFooter";
    MessageTemplates["MediaNotFound"] = "MediaNotFound";
    MessageTemplates["MediaNotAllowed"] = "MediaNotAllowed";
    MessageTemplates["MediaSizeTooLarge"] = "MediaSizeTooLarge";
    MessageTemplates["MediaServiceNotAvailable"] = "MediaServiceNotAvailable";
    MessageTemplates["AdvisorAlreadyCreated"] = "AdvisorAlreadyCreated";
    MessageTemplates["PublicQuestionAlreadyAnswered"] = "PublicQuestionAlreadyAnswered";
    MessageTemplates["NewMessageReceive"] = "NewMessageReceive";
    MessageTemplates["AttachmentShared"] = "AttachmentShared";
    MessageTemplates["InvalidPassword"] = "InvalidPassword";
    MessageTemplates["ProvideOldPassword"] = "ProvideOldPassword";
    MessageTemplates["WrongOldPassword"] = "WrongOldPassword";
})(MessageTemplates = exports.MessageTemplates || (exports.MessageTemplates = {}));
var Locale;
(function (Locale) {
    Locale["En"] = "En";
    Locale["Ar"] = "Ar";
})(Locale = exports.Locale || (exports.Locale = {}));
const LocaleFunc = {
    En: (key, data) => {
        return util_helper_1.ReplaceObjectValuesInString(en_locale_1.En.values[key], data);
    },
    Ar: (key, data) => {
        return util_helper_1.ReplaceObjectValuesInString(en_locale_1.En.values[key], data);
    }
};
function __T(locale, key, data) {
    let Message = key.toString();
    return LocaleFunc[locale](Message, data);
}
exports.__T = __T;
//# sourceMappingURL=locale.js.map