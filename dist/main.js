"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
const app_1 = require("./app");
const bootstrap_1 = require("./bootstrap/bootstrap");
const env_helper_1 = require("./helpers/env.helper");
const logger_helper_1 = require("./helpers/logger.helper");
const queue_processor_1 = require("./helpers/queue.processor");
(async function LoadServer() {
    const app = await app_1.App.GetNestApplicationInstance();
    logger_helper_1.Logger.Info("Bootstrapping application ...", "BOOTSTRAP");
    await bootstrap_1.Bootstrap.boot(app);
    logger_helper_1.Logger.Info("Application loaded.", "BOOTSTRAP");
    await app.listen(env_helper_1.appEnv("PORT", 3001));
    logger_helper_1.Logger.Info(`Server running on port ${env_helper_1.appEnv("PORT", 3001)}`);
    logger_helper_1.Logger.Info("Queue Started.", "BOOTSTRAP");
    queue_processor_1.QueueProcessor.ProcessQueue();
})();
//# sourceMappingURL=main.js.map