"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Swagger = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const class_validator_jsonschema_1 = require("class-validator-jsonschema");
const authentication_1 = require("../constant/authentication");
const env_helper_1 = require("../helpers/env.helper");
class Swagger {
    async Load(app) {
        const options = new swagger_1.DocumentBuilder()
            .setTitle(`Event Buddy API`)
            .setDescription('API descriptions')
            .setVersion('1.0')
            .addSecurity(authentication_1.AUTHORIZATION_HEADER_KEY, {
            type: 'apiKey',
            description: 'Api Authorization',
            name: authentication_1.AUTHORIZATION_HEADER_KEY,
            in: 'header',
        })
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, options);
        const metadata = class_validator_1.getFromContainer(class_validator_1.MetadataStorage)
            .validationMetadatas;
        document.components.schemas = Object.assign({}, document.components.schemas || {}, class_validator_jsonschema_1.validationMetadatasToSchemas(metadata));
        swagger_1.SwaggerModule.setup('api', app, document, { customCss: '@import url("http://3.87.183.56:3000/api/swagger-ui.css");' });
    }
    ;
}
exports.Swagger = Swagger;
//# sourceMappingURL=swagger.js.map