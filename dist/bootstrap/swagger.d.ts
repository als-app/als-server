import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface";
import { Loadable } from "./bootstrap";
export declare class Swagger implements Loadable {
    Load(app: NestExpressApplication): Promise<void>;
}
