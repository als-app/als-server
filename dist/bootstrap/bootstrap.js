"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bootstrap = void 0;
const app_module_1 = require("../app.module");
const swagger_1 = require("./swagger");
class Bootstrap {
    constructor() {
        this._loaders = [swagger_1.Swagger];
    }
    async _load(app) {
        for (const loader of this._loaders) {
            let LoaderInstance = new loader();
            await LoaderInstance.Load(app);
        }
    }
    static async boot(app) {
        let bootstrap = new Bootstrap();
        await bootstrap._load(app);
    }
}
exports.Bootstrap = Bootstrap;
//# sourceMappingURL=bootstrap.js.map