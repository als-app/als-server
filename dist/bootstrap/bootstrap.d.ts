import { NestExpressApplication } from "@nestjs/platform-express/interfaces/nest-express-application.interface";
export interface Constructable<T> {
    new (): T;
}
export interface Loadable {
    Load: (app?: NestExpressApplication) => {};
}
export declare class Bootstrap {
    private _loaders;
    private _load;
    static boot(app: NestExpressApplication): Promise<void>;
}
