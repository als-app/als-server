export declare enum AuthenticationCodes {
    UserLoginSuccess = 10,
    UserVerificationPending = 20,
    UserSignUpSuccess = 30,
    AlreadyRegistered = 71
}
export declare const AUTHORIZATION_HEADER_KEY = "authorization";
export declare const OPTIONAL_AUTHORIZATION_HEADER_KEY = "optional-authorization";
export declare const ROLES = "roles";
export declare const LOCALE_HEADER_KEY = "locale";
export declare const GetVerifyUserEmailLink: (code: string) => string;
export declare const GetResetPasswordLink: (code: string) => string;
