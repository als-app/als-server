"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketEventType = exports.SocketRoomType = exports.SocketEventSenderType = exports.SocketEventAuthorizedEntity = void 0;
var SocketEventAuthorizedEntity;
(function (SocketEventAuthorizedEntity) {
    SocketEventAuthorizedEntity[SocketEventAuthorizedEntity["User"] = 1] = "User";
    SocketEventAuthorizedEntity[SocketEventAuthorizedEntity["Advisor"] = 2] = "Advisor";
})(SocketEventAuthorizedEntity = exports.SocketEventAuthorizedEntity || (exports.SocketEventAuthorizedEntity = {}));
var SocketEventSenderType;
(function (SocketEventSenderType) {
    SocketEventSenderType[SocketEventSenderType["User"] = 1] = "User";
    SocketEventSenderType[SocketEventSenderType["Advisor"] = 2] = "Advisor";
})(SocketEventSenderType = exports.SocketEventSenderType || (exports.SocketEventSenderType = {}));
var SocketRoomType;
(function (SocketRoomType) {
    SocketRoomType["Chat"] = "ss_chat";
    SocketRoomType["Customer"] = "ss_customer";
})(SocketRoomType = exports.SocketRoomType || (exports.SocketRoomType = {}));
var SocketEventType;
(function (SocketEventType) {
    SocketEventType["ChatStart"] = "chat_start";
    SocketEventType["ChatJoin"] = "chat_join";
    SocketEventType["Connected"] = "connection";
    SocketEventType["Disconnect"] = "disconnect";
    SocketEventType["Message"] = "message";
    SocketEventType["Typing"] = "typing";
    SocketEventType["ReadMessage"] = "read_message";
})(SocketEventType = exports.SocketEventType || (exports.SocketEventType = {}));
//# sourceMappingURL=socket.js.map