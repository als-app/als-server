"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetResetPasswordLink = exports.GetVerifyUserEmailLink = exports.LOCALE_HEADER_KEY = exports.ROLES = exports.OPTIONAL_AUTHORIZATION_HEADER_KEY = exports.AUTHORIZATION_HEADER_KEY = exports.AuthenticationCodes = void 0;
const env_helper_1 = require("../helpers/env.helper");
var AuthenticationCodes;
(function (AuthenticationCodes) {
    AuthenticationCodes[AuthenticationCodes["UserLoginSuccess"] = 10] = "UserLoginSuccess";
    AuthenticationCodes[AuthenticationCodes["UserVerificationPending"] = 20] = "UserVerificationPending";
    AuthenticationCodes[AuthenticationCodes["UserSignUpSuccess"] = 30] = "UserSignUpSuccess";
    AuthenticationCodes[AuthenticationCodes["AlreadyRegistered"] = 71] = "AlreadyRegistered";
})(AuthenticationCodes = exports.AuthenticationCodes || (exports.AuthenticationCodes = {}));
exports.AUTHORIZATION_HEADER_KEY = 'authorization';
exports.OPTIONAL_AUTHORIZATION_HEADER_KEY = 'optional-authorization';
exports.ROLES = 'roles';
exports.LOCALE_HEADER_KEY = 'locale';
const GetVerifyUserEmailLink = (code) => {
    return `http://${env_helper_1.appEnv("APP_DOMAIN", "")}verify/${code}`;
};
exports.GetVerifyUserEmailLink = GetVerifyUserEmailLink;
const GetResetPasswordLink = (code) => {
    return `http://${env_helper_1.appEnv("APP_DOMAIN", "")}reset-password/${code}`;
};
exports.GetResetPasswordLink = GetResetPasswordLink;
//# sourceMappingURL=authentication.js.map