"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultResponseMessages = void 0;
const common_1 = require("@nestjs/common");
exports.DefaultResponseMessages = {
    [common_1.HttpStatus.OK]: "Success",
    [common_1.HttpStatus.BAD_REQUEST]: "Bad Request",
    [common_1.HttpStatus.INTERNAL_SERVER_ERROR]: "Internal Server Error",
    [common_1.HttpStatus.NOT_FOUND]: "Resource Not Found",
    [common_1.HttpStatus.UNAUTHORIZED]: "Not Authorized"
};
//# sourceMappingURL=response.js.map