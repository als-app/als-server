export declare const DefaultResponseMessages: {
    200: string;
    400: string;
    500: string;
    404: string;
    401: string;
};
