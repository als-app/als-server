import { Cache } from 'cache-manager';
export declare class RedisCacheService {
    private readonly cache;
    constructor(cache: Cache);
    Get(key: any): Promise<any>;
    Set(key: any, value: any, ttl?: number): Promise<void>;
    Delete(key: any): Promise<void>;
}
