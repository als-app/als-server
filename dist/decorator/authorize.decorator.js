"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Authorized = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const authentication_1 = require("../constant/authentication");
const Authorized = (roleOrRoles) => {
    let authorizedRoles = [];
    if (roleOrRoles)
        authorizedRoles = Array.isArray(roleOrRoles) ? roleOrRoles : [roleOrRoles];
    return common_1.applyDecorators(common_1.SetMetadata(authentication_1.ROLES, authorizedRoles), common_1.SetMetadata(authentication_1.AUTHORIZATION_HEADER_KEY, true), swagger_1.ApiSecurity(authentication_1.AUTHORIZATION_HEADER_KEY));
};
exports.Authorized = Authorized;
//# sourceMappingURL=authorize.decorator.js.map