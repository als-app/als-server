"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentDevice = void 0;
const common_1 = require("@nestjs/common");
const app_1 = require("../app");
const authentication_1 = require("../constant/authentication");
const user_device_repository_1 = require("../modules/auth/user_device/user_device.repository");
exports.CurrentDevice = common_1.createParamDecorator(async (data, context) => {
    const request = context.switchToHttp().getRequest();
    const token = request.headers[authentication_1.AUTHORIZATION_HEADER_KEY];
    let userDeviceRepo = (await app_1.App.GetNestApplicationInstance()).get(user_device_repository_1.UserDeviceRepository);
    const userDevice = await userDeviceRepo.FindOne({
        AuthToken: token
    });
    return userDevice;
});
//# sourceMappingURL=current_device.decorator.js.map