"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const http_exception_1 = require("../exception/http.exception");
function AppController(prefix, moduleName) {
    return common_1.applyDecorators(swagger_1.ApiTags(moduleName ? moduleName : prefix ? prefix : "default"), common_1.Controller(moduleName ? moduleName + "/" + prefix : prefix), common_1.UseFilters(http_exception_1.HttpExceptionFilter));
}
exports.AppController = AppController;
//# sourceMappingURL=appcontroller.decorator.js.map