"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsInEnum = void 0;
const class_validator_1 = require("class-validator");
function IsInEnum(enumObject) {
    let enumValues = Object.keys(enumObject).filter(a => parseInt(a) >= 0).map(a => parseInt(a));
    return class_validator_1.IsIn(enumValues);
}
exports.IsInEnum = IsInEnum;
//# sourceMappingURL=is_in_enum.decorator.js.map