"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComputedColumn = void 0;
const typeorm_1 = require("typeorm");
function ComputedColumn(options) {
    return typeorm_1.Column({
        name: options.name,
        type: options.type,
        select: false,
        insert: false,
        update: false,
        readonly: true
    });
}
exports.ComputedColumn = ComputedColumn;
//# sourceMappingURL=computed_column.decorator.js.map