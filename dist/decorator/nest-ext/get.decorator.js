"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Get = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const response_schema_1 = require("../../response/response.schema");
function Get(route, successResponseModel) {
    return common_1.applyDecorators(swagger_1.ApiResponse({
        type: successResponseModel,
        status: common_1.HttpStatus.OK
    }), swagger_1.ApiResponse({
        type: response_schema_1.BadRequestExceptionResponse,
        status: common_1.HttpStatus.BAD_REQUEST
    }), swagger_1.ApiResponse({
        type: response_schema_1.ForbiddenExceptionResponse,
        status: common_1.HttpStatus.FORBIDDEN
    }), swagger_1.ApiResponse({
        type: response_schema_1.UnauthorizedExceptionResponse,
        status: common_1.HttpStatus.UNAUTHORIZED
    }), swagger_1.ApiResponse({
        type: response_schema_1.NotFoundExceptionResponse,
        status: common_1.HttpStatus.NOT_FOUND
    }), swagger_1.ApiResponse({
        type: response_schema_1.FatalErrorExceptionResponse,
        status: common_1.HttpStatus.INTERNAL_SERVER_ERROR
    }), common_1.Get(route));
}
exports.Get = Get;
//# sourceMappingURL=get.decorator.js.map