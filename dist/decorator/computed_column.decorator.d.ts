import { ColumnType } from "typeorm";
export declare function ComputedColumn(options: {
    name: string;
    type: ColumnType;
}): PropertyDecorator;
