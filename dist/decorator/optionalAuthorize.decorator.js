"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OptionalAuthorized = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const authentication_1 = require("../constant/authentication");
const OptionalAuthorized = (roleOrRoles) => {
    let authorizedRoles = [];
    if (roleOrRoles)
        authorizedRoles = Array.isArray(roleOrRoles) ? roleOrRoles : [roleOrRoles];
    const data = common_1.applyDecorators(common_1.SetMetadata(authentication_1.ROLES, authorizedRoles), common_1.SetMetadata(authentication_1.AUTHORIZATION_HEADER_KEY, true), common_1.SetMetadata(authentication_1.OPTIONAL_AUTHORIZATION_HEADER_KEY, true), swagger_1.ApiSecurity(authentication_1.AUTHORIZATION_HEADER_KEY));
    return data;
};
exports.OptionalAuthorized = OptionalAuthorized;
//# sourceMappingURL=optionalAuthorize.decorator.js.map