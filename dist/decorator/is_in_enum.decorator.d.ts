export declare function IsInEnum(enumObject: Object): (object: Object, propertyName: string) => void;
