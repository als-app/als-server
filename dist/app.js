"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const helmet = require("helmet");
const rateLimit = require("express-rate-limit");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const http_exception_1 = require("./exception/http.exception");
const env_helper_1 = require("./helpers/env.helper");
const response_interceptor_1 = require("./interceptor/response.interceptor");
const logger_helper_1 = require("./helpers/logger.helper");
const app_module_1 = require("./app.module");
class App {
    constructor() { }
    ;
    static async GetNestApplicationInstance() {
        if (App._app === null) {
            App._app = await App._createApplication();
        }
        return App._app;
    }
    static async _createApplication() {
        const app = await core_1.NestFactory.create(app_module_1.AppModule, {
            logger: env_helper_1.appEnv("DEBUG", false) ? ["log", "error", "warn", "debug"] : false
        });
        app.useGlobalPipes(new common_1.ValidationPipe({
            validationError: {
                target: true,
                value: true,
            },
            exceptionFactory: (errors) => new common_1.BadRequestException(errors),
        }));
        app.useGlobalFilters(new http_exception_1.HttpExceptionFilter());
        app.enableCors({
            credentials: true,
            origin: function (origin, callback) {
                callback(null, true);
            }
        });
        app.use(helmet());
        if (env_helper_1.appEnv("APPLY_RATE_LIMIT", true)) {
            app.use(rateLimit({
                windowMs: 15 * 60 * 1000,
                max: 100,
            }));
        }
        app.set('trust proxy', 1);
        app.useGlobalInterceptors(new response_interceptor_1.ResponseInterceptor());
        app.use(logger_helper_1.Logger.GetLoggerMiddleware());
        return app;
    }
}
exports.App = App;
App._app = null;
//# sourceMappingURL=app.js.map