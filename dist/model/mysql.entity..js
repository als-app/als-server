"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MySQLBaseModel = void 0;
const base_entity_1 = require("./base.entity");
const typeorm_1 = require("typeorm");
const class_transformer_1 = require("class-transformer");
class MySQLBaseModel extends base_entity_1.BaseModel {
    castIdToNumber() {
        this.Id = typeof this.Id === "string" ? parseInt(this.Id) : this.Id;
    }
}
__decorate([
    typeorm_1.PrimaryGeneratedColumn({
        name: 'id',
        type: 'bigint',
    }),
    __metadata("design:type", Number)
], MySQLBaseModel.prototype, "Id", void 0);
__decorate([
    typeorm_1.Column({
        name: 'created_at',
        type: 'bigint',
    }),
    __metadata("design:type", Object)
], MySQLBaseModel.prototype, "CreatedAt", void 0);
__decorate([
    typeorm_1.Column({
        name: 'updated_at',
        type: 'bigint',
    }),
    __metadata("design:type", Object)
], MySQLBaseModel.prototype, "UpdatedAt", void 0);
__decorate([
    typeorm_1.Column({
        name: 'deleted_at',
        type: 'bigint',
    }),
    class_transformer_1.Exclude(),
    __metadata("design:type", Object)
], MySQLBaseModel.prototype, "DeletedAt", void 0);
__decorate([
    typeorm_1.AfterInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], MySQLBaseModel.prototype, "castIdToNumber", null);
exports.MySQLBaseModel = MySQLBaseModel;
//# sourceMappingURL=mysql.entity..js.map