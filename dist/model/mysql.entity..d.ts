import { BaseModel } from './base.entity';
export declare abstract class MySQLBaseModel extends BaseModel {
    Id: number;
    CreatedAt: any;
    UpdatedAt: any;
    DeletedAt: any;
    castIdToNumber(): void;
}
