"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseModelEntityType = exports.BaseModel = void 0;
const class_transformer_1 = require("class-transformer");
const typeorm_1 = require("typeorm");
class BaseModel extends typeorm_1.BaseEntity {
    Fill(obj) {
        let restrictedFields = this._GetRestrictedFields();
        let keys = Object.keys(obj);
        for (let i = 0; i < restrictedFields.length; i++) {
            if (keys.includes(restrictedFields[i])) {
                delete obj[restrictedFields[i]];
            }
        }
        keys = Object.keys(obj);
        for (let i = 0; i < keys.length; i++) {
            if (keys[i] in this && (obj[keys[i]] || obj[keys[i]] === 0)) {
                this[keys[i]] = obj[keys[i]];
            }
        }
        return this;
    }
    _GetRestrictedFields() {
        return ['Id'];
    }
    toJSON() {
        return class_transformer_1.classToPlain(this);
    }
}
exports.BaseModel = BaseModel;
var BaseModelEntityType;
(function (BaseModelEntityType) {
    BaseModelEntityType[BaseModelEntityType["Invitation"] = 1] = "Invitation";
    BaseModelEntityType[BaseModelEntityType["Chat"] = 2] = "Chat";
})(BaseModelEntityType = exports.BaseModelEntityType || (exports.BaseModelEntityType = {}));
//# sourceMappingURL=base.entity.js.map