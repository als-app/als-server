import { BaseEntity } from 'typeorm';
export declare abstract class BaseModel extends BaseEntity {
    CreatedAt: any;
    UpdatedAt: any;
    DeletedAt: any;
    Fill(obj: any): any;
    protected _GetRestrictedFields(): string[];
    toJSON(): Record<string, any>;
}
export declare enum BaseModelEntityType {
    Invitation = 1,
    Chat = 2
}
