module.exports = {
  apps: [
    {
      name: 'server',
      script: './dist/main.js', //todo: add npm run migrate
      watch: false,
      // cwd:"",
      args: '',
      interpreter_args: '',
      // instances:-1,
      env: {
        NODE_ENV: 'development',
      },
    },
  ],
};
